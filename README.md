ProActive CLIF
==============

![paclif_100](artifacts/clif-artifacts/clif-icons/icons-common/src/main/resources/icons/paclif_100.png)
***CLIF is a Load Injection Framework***

Welcome to the ProActive-based CLIF repository.
Unlike repository [clif-legacy](https://gitlab.ow2.org/clif/clif-legacy), this one features a Maven-based build chain. But the core difference is the underlying communication middleware which replaces Fractal RMI with ProActive, a much more advanced and well supported library.

disclaimer
----------

This is still work in progress. Basic features - namely the CLIF server runtime - is OK but all graphical user interface related stuff is not functional.

difference with clif-legacy
---------------------------

CLIF test plans and ISAC scenarios are compatible without change, except when they use recent updates that have not been merged yet (with regard to ISAC plug-ins).
Typically, CLIF examples provided by the clif-legacy repository are supported.

CLIF set-up is hold by a separate configuration file, namely `paclif.opts`, making it possible to support Legacy CLIF and ProActive CLIF within a single CLIF test project.
Network-related configuration properties are different. Refer the [ProActive documentation](https://doc.activeeon.com/latest/admin/ProActiveAdminGuide.html#_network_properties).

contents
--------

Files are organized in a tree:
* all POMs rely on a CLIF parent POM to be installed from directory `parent`
* once the parent POM is installed, you may install the clif-api artifact from directory `clif-api`
* then, you may install a number of CLIF artifacts and dependencies from directory `artifacts`
* finally, you may generate the CLIF runtime distributions from directory `distributions/clif-packages`; get the ProActive CLIF server distribution from the `clif-server/target` subdirectory.
* documentation directory: documentation on CLIF's maven builds.
* docker directory: docker files for CLIF.

Resources
---------

* [CLIF main documentation site](http://clif.ow2.io/)
* [latest ProActive CLIF server distribution](http://clif.ow2.io/clif-proactive/download/)
* [latest ISAC plug-ins reference manual](http://clif.ow2.io/clif-legacy/idoc/) for writing scenarios
* [CLIF Performance testing Plug-in for Jenkins](https://plugins.jenkins.io/clif-performance-testing)