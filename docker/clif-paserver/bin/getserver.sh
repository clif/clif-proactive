#!/bin/sh

CREDENTIALS="/opt/clif/etc/pa_credentials"

if test ! -f $CREDENTIALS
then
	echo "ERROR: Activeeon credentials must be set in file: $CREDENTIALS"
	exit 1
fi

. $CREDENTIALS

wget --quiet --http-user="$PA_USERNAME" --http-password="$PA_PASSWORD" http://www.activeeon.com/public_content/tools/orange/FUT/paclif-2.1.2-server-r3346.zip 
unzip paclif-2.1.2-server-r3346.zip
rm paclif-2.1.2-server-r3346.zip
ln -s paclif-2.1.2-server paclif-server

