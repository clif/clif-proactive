#!/bin/sh

# Starting scheduler
cd $PASC_HOME/bin
exec jrunscript start-server.js > /opt/clif/log/start-scheduler.log 2>&1 &

# Staring jenkins
/opt/clif/bin/start-jenkins.sh

# tailing all logs
tail -f /opt/clif/log/*.log /opt/clif/log/proactive/*.log /opt/clif/log/jenkins/*.log

