#!/bin/sh

CREDENTIALS="/opt/clif/etc/pa_credentials"

if test ! -f $CREDENTIALS
then
	echo "ERROR: Activeeon credentials must be set in file: $CREDENTIALS"
	exit 1
fi

. $CREDENTIALS

wget --no-verbose --http-user="$PA_USERNAME" --http-password="$PA_PASSWORD" http://www.activeeon.com/public_content/tools/orange/FUT/ProActiveScheduling-3.4.3-snecma_bin_full.zip 
unzip ProActiveScheduling-3.4.3-snecma_bin_full.zip
rm ProActiveScheduling-3.4.3-snecma_bin_full.zip
ln -s ProActiveScheduling-3.4.3-snecma_bin_full ProActiveScheduling

