#!/bin/sh

cd $PASC_HOME/bin
exec jrunscript start-server.js > /opt/clif/log/start-scheduler.out 2>&1 &

# just waiting 5 seconds...
sleep 5

# Printing logs
tail -f /opt/clif/log/proactive/*

