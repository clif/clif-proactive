/*
 * ################################################################
 *
 * ProActive Parallel Suite(TM): The Java(TM) library for
 *    Parallel, Distributed, Multi-Core Computing for
 *    Enterprise Grids & Clouds
 *
 * Copyright (C) 1997-2011 INRIA/University of
 *                 Nice-Sophia Antipolis/ActiveEon
 * Contact: proactive@ow2.org or contact@activeeon.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation; version 3 of
 * the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * If needed, contact us to obtain a release under GPL Version 2 or 3
 * or a different license than the AGPL.
 *
 *  Initial developer(s):               The ActiveEon Team
 *                        http://www.activeeon.com/
 *  Contributor(s):
 *
 * ################################################################
 * $ACTIVEEON_INITIAL_DEV$
 */

import java.net.URI
import org.ow2.clif.server.lib.ClifServerImpl
import org.ow2.clif.util.ExecutionContext
import org.ow2.clif.deploy.ClifRegistry
import org.ow2.clif.server.api.ClifServerControl;
import org.objectweb.fractal.api.control.LifeCycleController

import org.etsi.uri.gcm.api.control.GCMLifeCycleController
import org.etsi.uri.gcm.util.GCM

import org.objectweb.fractal.api.Component
import org.objectweb.proactive.core.remoteobject.RemoteObjectHelper
import org.objectweb.proactive.core.remoteobject.RemoteObject
import org.objectweb.proactive.api.PAActiveObject
import org.objectweb.proactive.core.node.Node
import org.ow2.proactive.utils.Tools

// The name of the Clif server
String serverName = System.getProperty('pas.task.name')
//  The url of the code server
String codeServerUrl = args[0]
System.setProperty("clif.pacodeserver", codeServerUrl);
System.setProperty("clif.codeserver.impl", "proactive");
// Optional walltime
long walltime = Long.parseLong(args[1])
// Check if the server has been stopped every
long checkPeriodInMs = 5000
// Clif properties
loadProps(args[2])

Node node = PAActiveObject.getNode()
String currentUrl = node.getNodeInformation().getURL()
println('Starting Clif server ' + serverName + ' ' + currentUrl)

ClifRegistry clifReg = null
try {
	URI uri = URI.create(codeServerUrl+'/ClifRegistry')
	RemoteObject<ClifRegistry> ro = RemoteObjectHelper.lookup(uri)
	clifReg = (ClifRegistry)RemoteObjectHelper.generatedObjectStub(ro)
} catch (Throwable t) {
    throw new IllegalStateException('Unable to lookup the ClifRegistry on ' +  codeServerUrl, t)
}

Component clifServer = null
try {
	clifServer = ClifServerImpl.create(serverName, clifReg, node)
} catch (Throwable t) {	
	throw new IllegalStateException('Unable to create CLIF server on ' + currentUrl, t)
}

GCMLifeCycleController controller = GCM.getGCMLifeCycleController(clifServer)

// Wait until the clif server becomes stopped
long awaitedTime = 0;
while (true) {
	// If wall time reached stop and terminate
	if (awaitedTime >= walltime) {
		terminate(clifServer, true)
		throw new IllegalStateException('Walltime reached for ' + serverName + ' on ' + currentUrl)
	}
	// If already stopped just terminate
	if (controller.getFcState() == LifeCycleController.STOPPED) {
		println(serverName + ' was stopped on ' + currentUrl)
		terminate(clifServer, true)
		break;
	}		
	Thread.sleep(checkPeriodInMs)
	awaitedTime += checkPeriodInMs
}

def terminate(Component c, boolean stop) {
	if (stop) {
		try {
			GCM.getGCMLifeCycleController(c).stopFc()
		} catch (Exception e) {}
	}
	try {
		((ClifServerControl) c.getFcInterface(ClifServerControl.CLIF_SERVER_CONTROL)).removeAllBlades();
	} catch (Exception e) {}

	try {
		GCM.getGCMLifeCycleController(c).terminateGCMComponent();
	} catch (Exception e) {}
}

// Parse and load props
def loadProps(String rawProps) {
	if (rawProps == null) return
	println('Loading properties: ' + rawProps)
	String[] props = rawProps.split(' ')
	for (String prop : props) {
		String[] splitted = prop.split("=")
		if (splitted.length == 0) {
			continue
		}
		String firstPart = splitted[0]
		if (!firstPart.startsWith("-D")) {
			continue
		}
		String propName = splitted[0].split("-D")[1]
		String propValue = splitted[1]
		System.setProperty(propName, propValue)
	}
}