package org.ow2.clif.maven.plugin;

import org.codehaus.plexus.util.cli.StreamConsumer;

/**
 * Utility class to test system output of mojo.
 *
 * @author Julien Coste
 */
public class BufferStreamConsumer implements StreamConsumer
{
    private StringBuilder buffer = new StringBuilder();

    public void consumeLine( String line )
    {
        this.buffer.append(line);
    }

    public String getOutput()
    {
        return this.buffer.toString();
    }
}
