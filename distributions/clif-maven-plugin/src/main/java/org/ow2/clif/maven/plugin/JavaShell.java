/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.maven.plugin;

import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.cli.shell.Shell;

import java.util.ArrayList;
import java.util.List;

/**
 * plexus-util hack to run a command WITHOUT a shell
 *
 * @author Julien Coste
 * @see PLXUTILS-107
 */
public class JavaShell
    extends Shell
{

    protected List<String> getRawCommandLine( String executable, String[] arguments )
    {
        List<String> commandLine = new ArrayList<String>();
        if ( executable != null )
        {
            commandLine.add( executable );
        }
        for ( String arg : arguments )
        {
            if ( isQuotedArgumentsEnabled() )
            {
                char[] escapeChars =
                    getEscapeChars( isSingleQuotedExecutableEscaped(), isDoubleQuotedExecutableEscaped() );

                commandLine.add(
                    StringUtils.quoteAndEscape( arg, getArgumentQuoteDelimiter(), escapeChars, getQuotingTriggerChars(),
                                                '\\', false ) );
            }
            else
            {
                commandLine.add( arg );
            }
        }
        return commandLine;
    }
}
