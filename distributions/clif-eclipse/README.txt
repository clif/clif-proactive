------------------------------------------------------------------------------

WARNING : CLIF's Eclipse mavenization is still under test as shouldn't be
	considered as functional for the moment.

------------------------------------------------------------------------------

CLIF's clif-eclipse directory contains maven projects which build Eclipse RCP
	plugins and distributions.

------------------------------------------------------------------------------
Launching build

in the current directory, just type :

mvn clean install

the build process should then be completed without error in a few minute.


------------------------------------------------------------------------------
Build Structure

The build is divided into 3 parts :

- prepare/ : all non OSGi jars will be retreived from Maven repository. They 
	will be injected into Eclipse plugin directories (see tycho/) and their 
	MANIFEST.MF will be generated.
	
- tycho/ : builds Eclipse plugins and distribution.

- repack/ : re-packs Eclipse plugins into a zip file wich can dropped into any
	Eclipse's "dropins" directory (old style distribution).
	
------------------------------------------------------------------------------

 