/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/


import org.ow2.clif.scenario.multithread.MTScenario;
import org.ow2.clif.scenario.multithread.MTScenarioSession;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.supervisor.api.ClifException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import java.util.StringTokenizer;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;


/**
 * Example of MTScenario utilization, with actions consisting in HTTP Get ou Post requests.
 * The constructor String argument should contain 5 arguments: the three first arguments are
 * those required for MTScenario constructor, and the two trailing arguments respectively
 * give the think time in seconds between two HTTP requests (for each session), and the name
 * of the file holding the list of URLs to visit. This URL list file should reside at a
 * suitable location (typically in the base directory of the example class itself).
 * <P>
 * URLs file format:
 * <UL>
 * <LI>lines beginning with character '#' are ignored
 * <LI>lines beginning with character 'p' or 'P' should contain a trailing http URL which will be
 * loaded using an HTTP POST method
 * <LI>other lines should contain a URL, which will be loaded using an HTTP GET method
 * </UL>
 * @author Bruno Dillenseger
 */
public class Webtest extends MTScenario
{
	static private Map urlsCache = new HashMap();


	static protected synchronized void clearURLs()
	{
		urlsCache.clear();
	}


	static protected String[] initURLs(String filename)
	{
		String[] urls;
		if (urlsCache.containsKey(filename))
		{
			urls = (String[])urlsCache.get(filename);
		}
		else
		{
			synchronized(urlsCache)
			{
				if (urlsCache.containsKey(filename))
				{
					urls = (String[])urlsCache.get(filename);
				}
				else
				{
					try
					{
						List urlList = new ArrayList();
						BufferedReader br = new BufferedReader(
							new InputStreamReader(ClifClassLoader.getClassLoader().getResourceAsStream(filename)));
						String line = null;
						while ((line = br.readLine()) != null)
						{
							line = line.trim();
							if (! line.startsWith("#"))
							{
								urlList.add(line);
							}
						}
						br.close();
						urls = (String[])urlList.toArray(new String[urlList.size()]);
						urlsCache.put(filename, urls);
					}
					catch (Exception ex)
					{
						ex.printStackTrace(System.err);
						urls = null;
					}
				}
			}
		}
		return urls;
	}


	long arg_sleep_ms = 0;


	public void init(Serializable testId)
		throws ClifException
	{
		clearURLs();
		super.init(testId);
	}


	/**
	 * @param sessionId the session identifier
	 * @param arg should contain 2 arguments: first, an integer setting the think time in seconds
	 * between 2 HTTP requests, then a file name containing the list of URLs to visit. The location
	 * of this file must be included in the RMI server codebase (typically the directory where this
	 * class has been compiled to). URLs are loaded via a GET method, unless the URL is prefixed by
	 * the 'post' keyword (actually any string starting with character 'p' or 'P' will be OK), in
	 * which case a POST method will be issued for the trailing URL.
	 * @return a new Webtest session
	 */
	public MTScenarioSession newSession(int sessionId, String arg)
		throws ClifException
	{
		MTScenarioSession session = null;

		try
		{
			StringTokenizer parser = new StringTokenizer(arg);
			arg_sleep_ms = Integer.parseInt(parser.nextToken()) * 1000;
			session = new Session(sessionId, initURLs(parser.nextToken()));
		}
		catch (Exception ex)
		{
			throw new ClifException(
				"Webtest requires 5 arguments:\n\t<number of concurrent threads>\n\t<test duration in seconds>\n\t<ramp-up duration in seconds>\n\t<think time in s>\n\t<file containing URL list>",
				ex);
		}
		return session;
	}


	class Session extends HttpClient implements MTScenarioSession
	{
		int index;
		int id;
		HttpMethod[] methods;

		public Session(int id, String[] urls)
		{
			super();
			this.id = id;
			index = 0;
			getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
			getParams().makeLenient();
			methods = new HttpMethod[urls.length];
			for (int i=0 ; i<urls.length ; ++i)
			{
				if (urls[i].charAt(0) == 'p' || urls[i].charAt(0) == 'P')
				{
					methods[i] = new PostMethod(urls[i].substring(urls[i].indexOf("http")));
				}
				else
				{
					methods[i] = new GetMethod(urls[i]);
				}
				methods[i].setFollowRedirects(true);
			}
		}

		public ActionEvent action(ActionEvent report)
		{
			try
			{
				if (methods[index] instanceof PostMethod)
				{
					report.type = "HTTP POST";
				}
				else
				{
					report.type = "HTTP GET";
				}
				report.setDate(System.currentTimeMillis());
				try
				{
					executeMethod(methods[index]);
					report.result = methods[index].getStatusCode() + " - " + methods[index].getStatusText();
					report.successful = true;
				}
				catch (Exception ex)
				{
					report.result = ex.toString();
					report.successful = false;
				}
				report.duration = (int) (System.currentTimeMillis() - report.getDate());
				methods[index].releaseConnection();
				report.comment = methods[index].getURI().toString();
				report.sessionId = id;
				if (++index == methods.length)
				{
					index = 0;
				}
				Thread.sleep(arg_sleep_ms);
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
			return report;
		}
	}
}
