package org.ow2.isac.plugin.helloworld;
/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/


import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.storage.api.ActionEvent;
import java.util.Hashtable;
import java.util.Map;


/**
 *
 * @author Bruno Dillenseger
 */
public class SessionObject implements SampleAction, SessionObjectAction
{
	// samples identifier
	static protected final int SAY_HELLO = 0;
	static protected final int SAY_WORLD = 1;
	static protected final int SAY_NEXT_WORD = 2;
	static protected final int SAY_USER_DEFINED = 3;

	// name of the params
	static protected final String USER_DEFINED_ARG = "user_string_arg";
	static protected final String HELLO_ARG = "hello_arg";
	static protected final String WORLD_ARG = "world_arg";

	boolean wordSwitch = true;
	String hello = "Hello";
	String world = "World";

	/**
	 * Build a new SessionObject for this plugin
	 *
	 * @param params
	 *            The table containing all the so params
	 */
	public SessionObject(Hashtable params)
	{
		String value = (String)params.get(HELLO_ARG);
		if (value != null && value.length() > 0)
		{
			hello = value;
		}
		value = (String)params.get(WORLD_ARG);
		if (value != null && value.length() > 0)
		{
			world = value;
		}
	}


	/**
	 * This constructor is used to clone the specified session object
	 *
	 * @param toClone
	 *            The session object to clone
	 */
	private SessionObject(SessionObject toClone)
	{
		hello = toClone.hello;
		world = toClone.world;
		wordSwitch = toClone.wordSwitch;
	}


	////////////////////////////
	// SampleAction interface //
	////////////////////////////


	public ActionEvent doSample(int number, Map params, ActionEvent report)
		throws IsacRuntimeException
	{
		synchronized(System.out)
		{
			report.successful = true;
			report.setDate(System.currentTimeMillis());
			switch (number)
			{
				case SAY_HELLO:
					report.result = hello;
					report.type = "HELLO";
					break;
				case SAY_WORLD:
					report.result = world;
					report.type = "WORLD";
					break;
				case SAY_NEXT_WORD:
					report.result = wordSwitch ? hello : world;
					wordSwitch = ! wordSwitch;
					report.type = "NEXT_WORD";
					break;
				case SAY_USER_DEFINED:
					report.type = "USER_DEFINED";
					report.result = (String)params.get(USER_DEFINED_ARG);
					break;
				default:
					report.comment = "no such sample number: " + number;
					report.type = "ERROR";
					report.successful = false;
			}
			if (report.isSuccessful())
			{
				System.out.println(report.result);
				System.out.flush();
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
			}
		}
		return report;
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	public void close()
	{
	}


	public void reset()
	{
	}
}
