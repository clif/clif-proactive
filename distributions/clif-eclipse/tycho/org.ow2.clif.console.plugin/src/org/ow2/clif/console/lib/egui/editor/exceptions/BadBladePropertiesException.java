/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor.exceptions;

/**
 * Exception when an blade properties is not valid.
 * Id, role and blade class must be defined.
 * The exception display the
 * "Bad probe or injector properties. Id, server name, role and class must be defined." message.
 * 
 * @author Manuel AZEMA
 */
public class BadBladePropertiesException extends Exception {
	private static final long serialVersionUID = 8876339036754929547L;

	/**
     * Create a new exception with
     * "Bad probe or injector properties. Id, server name, role and class must be defined." message.
     */
    public BadBladePropertiesException() {
        super("Bad probe or injector properties.\n"
                + "Id, server name, role and class must be defined.");
    }
}
