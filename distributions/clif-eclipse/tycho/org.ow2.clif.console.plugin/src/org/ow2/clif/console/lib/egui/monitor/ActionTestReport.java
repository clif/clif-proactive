/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.monitor;

import org.eclipse.swt.SWTException;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * Thread which collect data at each interval of time
 * @author Tsirimiaina ANDRIANAVONIMIARINA JAONA
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class ActionTestReport extends Thread {
    
    private Object[] injectors;
    private boolean collect;
    private ClifAppFacade clifApp;
    private TableGraphComposite graph;
    
    private int time;
    private long beginTime = 0;
    
    private boolean isStopped;
    
    /**
     * Constructor
     * @param clifApp 
     * @param graph  
     */
    public ActionTestReport(ClifAppFacade clifApp, TableGraphComposite graph) {
        super();
        
        this.clifApp = clifApp;
        this.graph = graph;
        isStopped = false;
    }
    
    /**
     * Set the interval of time between each thread cycle
     * @param time time in millisecond
     */
    public void setDelay(int time) {
        this.time = time;
    }
    
    /**
     * Reset the global timer and reset the graph (remove all points)
     */
    public void reset() {
        beginTime = System.currentTimeMillis();
        graph.removeAllPoints();
    }
    
    /**
     * Stop the ActionTestReport thread
     */
    public synchronized void stopThread(){
        isStopped=true;
        notify();
    }
    
    /**
     * Implementation of ActionTestReport
     * @see org.eclipse.jface.action.IAction#run()
     */
    public void run() {
        beginTime = System.currentTimeMillis();
        
        while(!graph.isDisposed() && !isStopped){
            int totalTime = (int) (((System.currentTimeMillis() - beginTime)/1000)*1000);

            /* Get injectors to collect */
            try {
                graph.getDisplay().syncExec(new Runnable(){
                    public void run () {
                        injectors = graph.getInjectorsToCollect();
                        collect=graph.getCollectDataValue();
                    }
                });
            } catch(SWTException e) {
                return;
            }
            
            /* For each injector to collect, collect stats */
            if(injectors != null) {
            	for (int i = 0; i < injectors.length; i++) {
            		String id = (String) injectors[i];

            		String[] labels = clifApp.getStatLabels(id);
            		long[] stats = clifApp.getStats(id);

            		/* Add stats to graph */
            		if(stats != null){
            			for (int j = 0; j < stats.length; j++) {
            				graph.addPoint(id, labels[j], totalTime, stats[j]);
            			}
            			/* if stats have to be collected */
            			if (collect){
            				graph.storeMonitoring(id, labels, totalTime, stats);
            			}
            		}                    
            	}
            }
            
            /* Draw the new graph */
            try{
                graph.getDisplay().syncExec(new Runnable(){
                    public void run () {
                        graph.getCanvas().redraw();
                    }
                });
            }catch(SWTException e) {
                return;
            }
            
            /* Wait the specified amount of time */ 
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (this) {
            try {
                if(!isStopped) {
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
