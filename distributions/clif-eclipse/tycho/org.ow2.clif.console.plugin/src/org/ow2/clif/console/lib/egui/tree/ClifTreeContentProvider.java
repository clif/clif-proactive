/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.tree;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.ow2.clif.console.lib.ClifDeployDefinition;

/**
 * Provides content for the clif tree viewer.
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class ClifTreeContentProvider implements ITreeContentProvider{
    
    private ClifTreeParent root;
    private Map<String,ClifDeployDefinition> testPlan;
    
    /**
     * Constructor
     */
    public ClifTreeContentProvider(){}
    
    /**
     * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
     */
    public Object[] getChildren(Object parentElement) {
        if (parentElement instanceof ClifTreeParent){
            return ((ClifTreeParent)parentElement).getChildren();
        }
        return null;
    }
    
    /**
     * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
     */
    public Object getParent(Object element) {
        if (element instanceof ClifTreeObject){
            return ((ClifTreeObject)element).getParent();
        }
        return null;
    }
    
    /**
     * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
     */
    public boolean hasChildren(Object element) {
        if (element instanceof ClifTreeParent){
            return ((ClifTreeParent)element).hasChildren();
        }
        return false;
    }
    
    /**
     * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
     */
    public Object[] getElements(Object inputElement) {
        if(inputElement instanceof Map<?,?>){
            testPlan = (Map<String,ClifDeployDefinition>) inputElement;
            if(testPlan != null && root == null && !testPlan.isEmpty()){
                initialize();
                return getChildren(root);
            }
            else {
                return getChildren(new ClifTreeParent(""));
            }
        }
        return getChildren(inputElement);
    }
    
    /**
     * Initialize tree. Server are tree parent and blades are children.
     */
    private void initialize() {
        root = new ClifTreeParent("");
        
        Map<String,ClifTreeParent> servers = new HashMap<String,ClifTreeParent>();
        
        for (Map.Entry<String,ClifDeployDefinition> entry : testPlan.entrySet())
        {
            String id = entry.getKey();
            ClifDeployDefinition def = entry.getValue();
            String bClassTest = def.getClassName();
            
            if(!servers.containsKey(def.getServerName())){
                servers.put(def.getServerName(), new ClifTreeParent(def.getServerName()));
                root.addChild(servers.get(def.getServerName()));
            }
            servers.get(def.getServerName()).addChild(new ClifTreeObject(bClassTest + " " + id));
        }
    }
    
    /**
     * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        testPlan = (Map<String,ClifDeployDefinition>) newInput;
        root = null;
    }
    
    public void dispose() {}
}
