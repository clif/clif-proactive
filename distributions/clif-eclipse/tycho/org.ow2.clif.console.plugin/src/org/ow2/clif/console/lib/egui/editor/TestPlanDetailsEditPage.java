/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import java.util.Map;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.console.lib.egui.editor.exceptions.BadBladePropertiesException;
import org.ow2.clif.console.lib.egui.editor.exceptions.ExistingBladeIdException;

/**
 * Injectors and probes details page in Master/Details pattern.
 * <ul>
 * <li>Id : blade unique id.</li>
 * <li>Server : deploy server.</li>
 * <li>Class : kind of blade (probes: cpu, memory, system, ...).</li>
 * <li>Parameters : according to class.</li>
 * <li>Comment : the comment played by the scenario (an arbitrary string).</li>
 * </ul>
 * 
 * @author Manuel AZEMA
 * @author Joan Chaumont
 */
public class TestPlanDetailsEditPage implements IDetailsPage {
    
    //The parent form.
    private IManagedForm form;	
    
    //Textfields for Id, Class, Argument and Comment properties.
    private Text tId, tClass, tArgument, tComment;
    
    //Combos for Server (Available Servers) and Role (injector or probe).
    private CCombo cRole, cServer;
    
    //The master part in the Master/Details pattern.
    private TestPlanMasterEditPage master;
    
    private Map<String,ClifDeployDefinition> testPlan;
    
    private String selectedBladeId;
    private String newSelectedBladeId;  
    
    private boolean propertiesChanged;
    
    private FocusListener focusListener;
    private ModifyListener changeListener;
    private SelectionListener selectListener;
    private KeyListener keyListener;
    
    /**
     * Create a new injectors and probes details page.
     * @param master master page in master/detail pattern
     * @param testPlan Map with used test plan
     */
    public TestPlanDetailsEditPage(TestPlanMasterEditPage master, Map<String,ClifDeployDefinition> testPlan) {
        super();
        
        this.master = master;
        this.testPlan = testPlan;
        
        this.selectedBladeId = "";
        this.newSelectedBladeId = "";
        
        this.propertiesChanged = false;
        
        final TestPlanMasterEditPage master2 = master;
        changeListener = new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                propertiesChanged = true;
                master2.setDirty(true);
            }
        };
        
        selectListener = new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                if(e.widget instanceof CCombo) {
                    if(((CCombo)e.widget).getSelectionIndex()>=0) {
                        propertiesChanged = true;
                        commit(true);
                        master2.setDirty(true);
                    }
                }
            }
        };
        
        focusListener = new FocusListener() {
            public void focusGained(FocusEvent e) {}

            public void focusLost(FocusEvent e) {
                commit(true);
            }
        };
        
        keyListener = new KeyListener() {

            public void keyPressed(KeyEvent e) {}
            
            public void keyReleased(KeyEvent e) {
                if(e.character == '\r'){
                    commit(true);
                }
            } 
        };
    }
    
    /**
     * Save the managed form.
     * @param form the parent form
     */
    public void initialize(IManagedForm form) {
        this.form = form;
    }
    
    /**
     * Create injectors and probes properties labels and textfields.
     * @param parent the parent composite
     */
    public void createContents(Composite parent) {
        FormToolkit toolkit = form.getToolkit();
        parent.setLayout(new GridLayout());
        
        /* Create "Properties" section */
        Section section = toolkit.createSection(parent,
                Section.DESCRIPTION | Section.TWISTIE | Section.EXPANDED);
        section.setText("Properties");
        section.setDescription("Manage injector and probe properties");
        toolkit.createCompositeSeparator(section);
        section.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        section.addExpansionListener(new ExpansionAdapter() {
            public void expansionStateChanged(ExpansionEvent e) {
                form.getForm().reflow(true);
            }
        });
        
        /* Composite client for injectors and probes properties labels and textfields */
        Composite sectionClient = toolkit.createComposite(section);
        section.setClient(sectionClient);
        GridLayout layout = new GridLayout();
        sectionClient.setLayout(layout);
        layout.numColumns = 2;
        layout.horizontalSpacing = 4;
        layout.verticalSpacing = 10;
        
        /* System dark blue color (looks like "link blue") */
        Color blue = form.getForm().getBody().getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE);	
        
        /* 5 labels and textfields, 2 labels and combos */
        /* Id */
        Label label = toolkit.createLabel(sectionClient,"Id* :");
        label.setForeground(blue);
        label.setToolTipText("an unique String that can be used to identify this probe or injector");
        tId = toolkit.createText(sectionClient,"");
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        tId.setLayoutData(gd);
        
        /* Server */
        label = toolkit.createLabel(sectionClient,"Server* :");
        label.setForeground(blue);
        label.setToolTipText("the name of the CLIF server where this scenario must be deployed");
        Composite serversEdit = toolkit.createComposite(sectionClient);
        GridLayout serversLayout = new GridLayout(2,false);
        serversLayout.marginWidth = 0;
        serversLayout.marginHeight = 0;
        serversEdit.setLayout(serversLayout);
        cServer = new CCombo(serversEdit,SWT.BORDER);
        updateAvailableServers();
        toolkit.adapt(cServer);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        cServer.setLayoutData(gd);
        toolkit.paintBordersFor(serversEdit);	
        
        /* Refresh available servers button */
        Button refreshAvailableServers = toolkit.createButton(serversEdit, "Refresh", SWT.PUSH);
        refreshAvailableServers.setToolTipText("Refresh available servers list");
        refreshAvailableServers.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
        refreshAvailableServers.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                updateAvailableServers();
            }			
        });
        serversEdit.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        
        /* Role : ccombo probe or injector */
        label = toolkit.createLabel(sectionClient,"Role* :");
        label.setForeground(blue);
        label.setToolTipText("a probe or an injector");
        cRole = new CCombo(sectionClient,SWT.READ_ONLY | SWT.BORDER);
        toolkit.adapt(cRole);
        cRole.setItems(new String[] {"injector", "probe"});
        gd = new GridData(GridData.FILL_HORIZONTAL);
        cRole.setLayoutData(gd);
        
        /* Class */ 
        label = toolkit.createLabel(sectionClient,"Class* :");
        label.setForeground(blue);
        label.setToolTipText("the probe or injector class");
        tClass = toolkit.createText(sectionClient,"");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        tClass.setLayoutData(gd);
        
        /* Arguments */
        label = toolkit.createLabel(sectionClient,"Arguments :");
        label.setForeground(blue);
        label.setToolTipText("the blade argument to be set");
        tArgument = toolkit.createText(sectionClient,"");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        tArgument.setLayoutData(gd);
        
        /* Comment */
        label = toolkit.createLabel(sectionClient,"Comment :");
        label.setForeground(blue);
        label.setToolTipText("an arbitrary String to be associated with this blade (any useful information or comment)");
        tComment = toolkit.createText(sectionClient,"");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        tComment.setLayoutData(gd);
        
        //Paint borders
        toolkit.paintBordersFor(sectionClient);
    }
    
    /**
     * Release allocated resources.
     * @see org.eclipse.ui.forms.IFormPart#dispose()
     */
    public void dispose() {}
    
    /**
     * Detail part is dirty if properties have been changed and no commit.
     * @return true if properties changed and no commit
     */
    public boolean isDirty() {
        return propertiesChanged;
    }
    
    /**
     * Part needs not to react. No input change.
     * @return false
     * @see org.eclipse.ui.forms.IFormPart#setFormInput(Object input)
     */
    public boolean setFormInput(Object input) {
        return false;
    }
    
    /**
     * No model modification without direct user interaction.
     * @return false
     * @see org.eclipse.ui.forms.IFormPart#isStale()
     */
    public boolean isStale() {
        return false;
    }
    
    /**
     * No default property textfield focus.
     * @see org.eclipse.ui.forms.IFormPart#isStale()
     */
    public void setFocus() {
        form.getForm().setFocus();
    }
    
    /**
     * Not called because the part is not stale.
     * @see org.eclipse.ui.forms.IFormPart#refresh()
     */
    public void refresh() {}
    
    /**
     * Set the list of the available servers in the server combo box.
     */
    public void updateAvailableServers() {
        String selectedServerName = cServer.getText();

        try
        {
        	master.updateClifProperties();
            cServer.setItems(ClifConsolePlugin.getDefault().getRegistry(true).getServers());
        }
        catch (ClifException ex)
        {
        	ex.printStackTrace(System.err);
            cServer.setItems(new String[]{ExecutionContext.DEFAULT_SERVER});
        } 
    	if (cServer.indexOf(ExecutionContext.DEFAULT_SERVER) != 0 || cServer.indexOf(selectedServerName) == -1) {
    		cServer.add(selectedServerName);  
    	}
    	cServer.select(cServer.indexOf(selectedServerName));
    }
    
    public void commit(boolean onSave) {
        if(propertiesChanged && testPlan.get(selectedBladeId) != null 
                && !tId.getText().trim().equals("")) {
            try {
                String serverName = ((cServer.getSelectionIndex()>=0)?
                        cServer.getItem(cServer.getSelectionIndex()):cServer.getText());
                
                String newBladeId = master.modifyBlade(selectedBladeId,
                        tId.getText().trim(),
                        serverName,
                        tClass.getText().trim(),
                        tArgument.getText().trim(),
                        tComment.getText().trim(),
                        cRole.getSelectionIndex() == cRole.indexOf("probe"));
                
                if(selectedBladeId.equals(newSelectedBladeId)) {
                    selectedBladeId = newBladeId;
                }
                else {
                    selectedBladeId = newSelectedBladeId;
                }
                
            } catch(ExistingBladeIdException ebie) {			
                MessageDialog.openError(tClass.getShell(),
                        "Change injector or probe id",
                        ebie.getMessage());
                selectedBladeId = newSelectedBladeId;
            } catch (BadBladePropertiesException bbpe) {			
                MessageDialog.openError(tClass.getShell(),
                        "Invalid probe or injector properties",
                        bbpe.getMessage());
                selectedBladeId = newSelectedBladeId;
            }
        }
        else {
            selectedBladeId = newSelectedBladeId;
        }
        propertiesChanged = false;
        master.setChanged(true);
    }
    
    /**
     * Show selected blade properties.<br/>
     * With the id of the selected blade, we take its blade definition
     * in the test plan and fill textfield with properties.
     */
    public void selectionChanged(IFormPart part, ISelection selection) {
        /* Get selection id */
        if(selection.isEmpty()) {
            newSelectedBladeId = "";
        }
        else {
            newSelectedBladeId = 
                ((IStructuredSelection)selection).getFirstElement().toString();
        }
        
        commit(true);
        
        this.removeAllModifyListener();
        
        /* Get blade definition */
        ClifDeployDefinition def = testPlan.get(selectedBladeId);
        if(def != null) {
            /* Fill textfield */
            tId.setText(selectedBladeId);
            
            if (cServer.indexOf(def.getServerName()) >= 0)
            	cServer.select(cServer.indexOf(def.getServerName()));
            else {
            	cServer.setText(def.getServerName());
            }	
            
            /* Differently for injectors and probes */
            if(def.isProbe()) {
                String probeName = def.getContext().get("insert");
                probeName = probeName.substring(0, probeName.lastIndexOf('.'));		
                tClass.setText(probeName.substring(1 + probeName.lastIndexOf('.')));
                cRole.select(cRole.indexOf("probe"));			
            }
            else {
                tClass.setText(def.getContext().get("insert").toString());
                cRole.select(cRole.indexOf("injector"));
            }
            tArgument.setText(def.getArgument());
            tComment.setText(def.getComment());
        }
        
        this.addAllModifyListener();
    }
    
    /* Add changeListener to all text field and combo box. */
    private void addAllModifyListener() {
        tId.addModifyListener(changeListener);
        cServer.addSelectionListener(selectListener);
        cServer.addModifyListener(changeListener);
        tArgument.addModifyListener(changeListener);
        tComment.addModifyListener(changeListener);
        cRole.addSelectionListener(selectListener);
        tClass.addModifyListener(changeListener);
        
        tId.addFocusListener(focusListener);
        tId.addKeyListener(keyListener);
        tClass.addFocusListener(focusListener);
        tClass.addKeyListener(keyListener);  
        tArgument.addFocusListener(focusListener);
        tArgument.addKeyListener(keyListener);  
        tComment.addFocusListener(focusListener);
        tComment.addKeyListener(keyListener);  
    }
    
    /* Remove changeListener to all text field and combo box. */
    private void removeAllModifyListener() {
        tId.removeModifyListener(changeListener);
        cServer.removeSelectionListener(selectListener);
        cServer.removeModifyListener(changeListener);
        tArgument.removeModifyListener(changeListener);
        tComment.removeModifyListener(changeListener);
        cRole.removeSelectionListener(selectListener);
        tClass.removeModifyListener(changeListener);
        
        tId.removeFocusListener(focusListener);
        tId.removeKeyListener(keyListener);  
        tClass.removeFocusListener(focusListener);
        tClass.removeKeyListener(keyListener);  
        tArgument.removeFocusListener(focusListener);
        tArgument.removeKeyListener(keyListener);  
        tComment.removeFocusListener(focusListener);
        tComment.removeKeyListener(keyListener);  
    }
    
}
