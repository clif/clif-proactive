/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009, 2010 France Telecom R&D
 * Copyright (C) 2014 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.FileEditorInput;
import org.ow2.clif.console.lib.egui.ClifConsolePerspective;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.console.lib.egui.editor.TestPlanEditor;
import org.ow2.clif.console.lib.egui.editor.TestPlanVisualDisplay;
import org.ow2.clif.deploy.ClifAnalyzerAppFacade;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ThrowableHelper;
import java.io.File;

/**
 * Main class for Deployment Wizard. Used to deploy a test plan and start registry
 * if it is not started yet.
 *
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class NewTestDeploymentWizard extends Wizard implements INewWizard{

    private MainTestDeploymentPage mainPage;
    private ISelection selection;
    private IWorkbenchWindow window;

    /**
     * Constructor
     */
    public NewTestDeploymentWizard() {
        super();
        setNeedsProgressMonitor(true);
        setHelpAvailable(false);
        setWindowTitle("New Test Deployment");
    }

    /**
     * Adding pages to the wizard.
     */
    public void addPages() {
        mainPage = new MainTestDeploymentPage(selection);
        addPage(mainPage);
    }

    /**
     * Checks if container exist and if the right properties are loaded. If not, restart
     * Eclipse console. Then, start registry.
     */
    public boolean performFinish() {
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IResource resource = root.findMember(new Path(mainPage.getContainerName()));
        if (resource == null || !resource.exists() || !(resource instanceof IContainer)) {
            MessageDialog.openError(
                    getShell(),
                    "CLIF ISAC Plug_in",
                    "Container \"" + mainPage.getContainerName() + "\" does not exist.");
            return false;
        }
        IProject project = resource.getProject();
        File clifPropsProjectFile = new File(
        	project.getLocation().toFile(),
        	ClifConsolePlugin.CLIF_PROPS);
        if (!clifPropsProjectFile.exists()) {
            MessageDialog.openError(
                    getShell(),
                    "CLIF ISAC Plug_in",
                    "File clif.props does not exist in the specified container.");
            return false;
        }
        ClifAnalyzerAppFacade.updateProperties(clifPropsProjectFile);
        if (ClifConsolePlugin.getDefault().mustRestart())
        {
        	if (! MessageDialog.openConfirm(
        		window.getShell(),
        		"Confirm hazardous deployment",
        		"Some settings changes require the console to be restarted.\nTry to deploy anyway?"))
        	{
        		return false;
        	}
        }
        try
        {
        	ClifConsolePlugin.getDefault().getRegistry(true);
        }
        catch (ClifException ex)
        {
			ex.printStackTrace(System.err);
        	MultiStatus status = new MultiStatus(
				ClifConsolePlugin.PLUGIN_ID,
				0,
				ThrowableHelper.getMessages(ex),
				null);
        	for (String line : ThrowableHelper.getStackTraceLines(ex))
        	{
        		status.add(new Status(
        			IStatus.ERROR,
        			ClifConsolePlugin.PLUGIN_ID,
        			line,
        			null));
        	}
			ErrorDialog.openError(
				window.getShell(),
				"Registry error",
				"Unable to connect to registry",
				status);
        	return false;
        }

        /*Open CLIF Console perspective if needed */
        if (!window.getActivePage().getPerspective().getId().equals(ClifConsolePerspective.CLIF_CONSOLE_PERSPECTIVE_ID)) {
            try {
                window.getWorkbench().showPerspective(
                    ClifConsolePerspective.CLIF_CONSOLE_PERSPECTIVE_ID, window);
            } catch (WorkbenchException e) {
				e.printStackTrace(System.err);
				MultiStatus status = new MultiStatus(
					ClifConsolePlugin.PLUGIN_ID,
					0,
					ThrowableHelper.getMessages(e),
					null);
				for (String line : ThrowableHelper.getStackTraceLines(e))
				{
					status.add(new Status(
						IStatus.ERROR,
						ClifConsolePlugin.PLUGIN_ID,
						line,
						null));
				}
				ErrorDialog.openError(
					window.getShell(),
					"Can't set CLIF perspective",
					"Unable to set CLIF perspective",
					status);
            }
        }

        /* Create the test tab for the deployed test plan */
        IEditorReference[] tEdit = window.getActivePage().getEditorReferences();
        
        try
        {
	        for (IEditorReference aTEdit : tEdit) {
	            if (aTEdit.getName().equals(mainPage.getTestPlan())) {
	                if (aTEdit.getEditor(false) instanceof TestPlanEditor) {
	                    ((TestPlanEditor) aTEdit.getEditor(false)).createPageTest();
	                }
	            }
	        }
        }
        catch (ClifException ex)
        {
        	return false;
        }

        TestPlanEditor.setDeployedTest(mainPage.getTestPlan());

        IWorkspaceRoot workRoot = ResourcesPlugin.getWorkspace().getRoot();
        final IFile file = workRoot.getFile(
                workRoot.findMember(mainPage.getContainerName() + "/" + mainPage.getTestPlan()).getFullPath());
        IEditorInput editorInput = new FileEditorInput(file);
        try {
            window.getActivePage().openEditor(editorInput, "org.ow2.clif.console.lib.egui.testPlanEditor");
        } catch (PartInitException e) {
            e.printStackTrace(System.err);
        }

        /* Refresh all blades of the active TestPlanEditor */
        /* This action change the color of the blade line if the server of the blade is registered */
        for (IEditorReference aTEdit : tEdit) {
            if (aTEdit.getEditor(false) instanceof TestPlanEditor) {
                TestPlanVisualDisplay tpvd = ((TestPlanEditor) aTEdit.getEditor(false)).getEditPage().getTableBlade();
                tpvd.refresh();
            }
        }
        return true;
    }

    public void init(IWorkbench workbench, IStructuredSelection selection) {
        this.selection = selection;
        this.window = workbench.getActiveWorkbenchWindow();
    }
}
