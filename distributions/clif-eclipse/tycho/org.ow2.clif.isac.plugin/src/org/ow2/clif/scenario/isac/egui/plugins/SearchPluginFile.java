/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * This class implements method which is able to search file in a tree structure
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Thomas Escalle
 * @author O Beyler
 */
public class SearchPluginFile {
    /**
     * Search Jar Files into the directory who contain a specific fileName.
     * @param dirName the name of the directory
     * @param fileName 
     * @return a List of file
     */
    public static List<File> searchPlugins(String dirName, String fileName) {
    	List<File> result = new ArrayList<File>();
   		File[] files = new File(dirName).listFiles();   				
   		for (int i = 0 ; i < files.length; i++) {
   			if (isFileExistIntoJar(files[i],fileName)){
   				result.add(files[i]);     			
   			}
   		}
		return result;
    }
    /**
     * Check the existence of a file into a jar.
     * @param file  
     * @param fileName name of the search file 
     * @return true if fileName is present into file
     */
    public static boolean isFileExistIntoJar(File file, String fileName) {
    	try {    		    		
    		JarFile jfile = new JarFile(file.getPath());
    			
    		for (Enumeration<JarEntry> entries = jfile.entries(); entries.hasMoreElements();) {
    				JarEntry element = entries.nextElement();	    				
    				if(element.getName().endsWith(fileName)){
    					return true;
    				}
    			}    		    					
		} catch (IOException e) {
			e.printStackTrace();			
		}
    	return false;
    }
    /**
     * Extract a list of input stream  of a file into a jar whose name is ending by fileName.
     * @param file  
     * @param fileName name of the search file pattern
     * @return the List of InputStream
     */
    public static List<InputStream> searchInputStreams(File file, String fileName) {
    	List<InputStream> result = new ArrayList<InputStream>();
    	try {    		    		
    		JarFile jfile = null;
    		jfile = new JarFile(file.getPath());
    			
    		for (Enumeration<JarEntry> entries = jfile.entries(); entries.hasMoreElements();) {
    				JarEntry element = entries.nextElement();	
    				
    				// Si le nom de l'entrée finit par fileName
    				if(element.getName().endsWith(fileName)){
    					//result.add(element.getName());
    					result.add(jfile.getInputStream(element));
    				}
    			}    		    					
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
    	return result;
    }  
    /**
     * Extract the first of input stream  of a file into a jar.
     * @param file  
     * @param fileName name of the search file 
     * @return InputStream
     */
    public static InputStream searchInputStream(File file, String fileName) {    	
    	try {    		    		
    		JarFile jfile = null;
    		jfile = new JarFile(file.getPath());
    			
    		for (Enumeration<JarEntry> entries = jfile.entries(); entries.hasMoreElements();) {
    				JarEntry element = entries.nextElement();	
    				
    				// Si le nom de l'entrée finit par fileName
    				if(element.getName().endsWith(fileName)){
    					//result.add(element.getName());
    					return (jfile.getInputStream(element));
    				}
    			}    		    					
		} catch (IOException e) {
			e.printStackTrace();			
		}
    	return null;
    }  
}
