/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.gui;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.ow2.clif.scenario.isac.egui.plugins.ActionDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ObjectDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ParameterDescription;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;

/**
 * Implementation of the panel which will store all the paraemters widgets
 *
 * @author JC Meillaud
 * @author A Peyrard
 */
public class ParameterPanel extends Composite {
    private Map<String,ParameterWidget> params;
    
    /**
     * Build a new parameter panel
     * @param parent The parent composite of this panel
     */
    public ParameterPanel(Composite parent) {
        super(parent, SWT.FLAT);
        this.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
        this.setLayout(new GridLayout());
        this.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        this.params = new HashMap<String,ParameterWidget>();
    }
    
    /**
     * Add a new widget to this panel
     *
     * @param params The widgets parameters
     * @param style The style of the widget
     * @param name The name of the parameter which is representing by this widget
     * @param label 
     * @param c 
     * @param listener The modify listener which will be used for each parameter 
     * widgets
     * @return Composite
     *
     */
    public Composite addWidget(Map<String,Object> params, int style, String name,
            String label, Composite c, ModifyListener listener) {
        ParameterWidget pw = 
            new ParameterWidget(style, name, label, params, c, listener);
        this.params.put(name, pw);
        this.layout();
        return pw.getComposite();
    }
    
    /**
     * Builds a map with all parameters names and parameters values
     *
     * @return The map containing the values and names of parameters
     */
    public Map getParametersValues() {
        Map<String,String> values = new HashMap<String,String>();
        Iterator iter = this.params.values().iterator();
        while (iter.hasNext()) {
            ParameterWidget temp = (ParameterWidget) iter.next();
            if (temp.getStyle() != WidgetDescription.GROUP) {
                if (temp.getValue() != null) {
                    values.put(temp.getName(), temp.getValue());
                }
                else {
                    values.put(temp.getName(), "");
                }
            }
        }
        return values;
    }
    
    /**
     * Analyse a gui XML file, this kind of file define all the panels
     * parameters for a plugin
     * @param ids 
     * @param plugin The plugin description
     * @param fileName The fileName to be analysed
     * @param panels The table where the panels must be stored
     */
    public static void createNewPanelsFromXML(Vector ids, PluginDescription plugin,
            InputStream fileName, Map<String,ParametersWidgetsNode> panels) {
        /* if the file does not exist, create the default panels */
        if (fileName == null){
            createNewDefaultsPanels(ids, plugin, panels);
        }
        /* load the gui description file and get the errors */
        else {
        	GUIDescriptionParser.loadGUIDescriptionFile(ids, fileName, plugin, panels);
        }
    }
    
    /**
     * create all the default widgets tree for the actions of the given plugin
     * @param ids 
     * @param plugin The plugin
     * @param panels The table of widgets tree, to be completed
     */
    private static void createNewDefaultsPanels(
            Vector ids, PluginDescription plugin, Map<String,ParametersWidgetsNode> panels) {
        /* load the object description */
        ObjectDescription obj = plugin.getObject();
        ParametersWidgetsNode tree = null;
        /* get the parameters */
        if (obj != null) {
            Vector params = obj.getParams();
            tree = ParametersWidgetsNode
            .createParametersWidgetsNode(ids, params, Node.USE,
                    plugin.getName());
            int intKey = tree.hashCode();
            while (panels.containsKey(String.valueOf(intKey))) {
                intKey++;
            }
            panels.put(String.valueOf(intKey), tree);
            obj.setGUIKey(String.valueOf(intKey));
        }
        
        /* load the samples descriptions */
        Map<String,ActionDescription> samples = plugin.getSamples();
        if (samples != null) {
            /* for each sample */
            Iterator<String> samplesKeys = samples.keySet().iterator();
            while (samplesKeys.hasNext()) {
                String sampleKey = samplesKeys.next();
                ActionDescription sample = samples.get(sampleKey);
                Vector params = sample.getParams();
                tree = ParametersWidgetsNode.createParametersWidgetsNode(
                	ids, params, Node.SAMPLE, plugin.getName());
                int intKey = tree.hashCode();
                while (panels.containsKey(String.valueOf(intKey))) {
                    intKey++;
                }
                panels.put(String.valueOf(intKey), tree);
                sample.setGUIKey(String.valueOf(intKey));
            }
        }
        
        /* load the timers descriptions */
        Map<String,ActionDescription> timers = plugin.getTimers();
        if (timers != null) {
            /* for each timer */
            Iterator<String> timersKeys = timers.keySet().iterator();
            while (timersKeys.hasNext()) {
                String timerKey = timersKeys.next();
                ActionDescription timer = timers.get(timerKey);
                Vector params = timer.getParams();
                tree = ParametersWidgetsNode.createParametersWidgetsNode(
                	ids, params, Node.TIMER, plugin.getName());
                int intKey = tree.hashCode();
                while (panels.containsKey(String.valueOf(intKey))) {
                    intKey++;
                }
                panels.put(String.valueOf(intKey), tree);
                timer.setGUIKey(String.valueOf(intKey));
            }
        }
        
        /* load the tests descriptions */
        Map<String,ActionDescription> tests = plugin.getTests();
        if (tests != null) {
            /* for each test */
            Iterator<String> testsKeys = tests.keySet().iterator();
            while (testsKeys.hasNext()) {
                String testKey = testsKeys.next();
                ActionDescription test = tests.get(testKey);
                Vector<ParameterDescription> params = test.getParams();
                tree = ParametersWidgetsNode.createParametersWidgetsNode(
                	ids, params, Node.TEST, plugin.getName());
                int intKey = tree.hashCode();
                while (panels.containsKey(String.valueOf(intKey))) {
                    intKey++;
                }
                panels.put(String.valueOf(intKey), tree);
                test.setGUIKey(String.valueOf(intKey));
            }
        }
        
        /* load the controls descriptions */
        Map<String,ActionDescription> controls = plugin.getControls();
        if (controls != null) {
            /* for each control */
            Iterator<String> controlsKeys = controls.keySet().iterator();
            while (controlsKeys.hasNext()) {
                String testKey = controlsKeys.next();
                ActionDescription control = controls.get(testKey);
                Vector<ParameterDescription> params = control.getParams();
                tree = ParametersWidgetsNode.createParametersWidgetsNode(
                	ids, params, Node.CONTROL, plugin.getName());

                int intKey = tree.hashCode();
                while (panels.containsKey(String.valueOf(intKey))) {
                    intKey++;
                }
                panels.put(String.valueOf(intKey), tree);
                control.setGUIKey(String.valueOf(intKey));
            }
        }
    }
    
    /**
     * Build a new parameter panel from a parameters widgets tree
     *
     * @param node The tree
     * @param parent The parent composite
     * @param listener The modify listener which will be used 
     * for each parameter widgets
     * @return The parameter panel created
     */
    public static ParameterPanel createParameterPanel(
            ParametersWidgetsNode node, Composite parent,
            ModifyListener listener) {
        ParameterPanel panel = new ParameterPanel(parent);
        analyseParametersWidgetsNode(panel, node, panel, listener);
        return panel;
    }
    
    /**
     * Analyse a parameters widgets node, to build the panel
     * @param panel The panel to be build
     * @param node The node to be analysed
     * @param parent The parent composite to add the new widget
     * @param listener modify listener which will be used 
     * for each parameter widgets
     */
    private static void analyseParametersWidgetsNode(ParameterPanel panel,
            ParametersWidgetsNode node, Composite parent,
            ModifyListener listener) {
        WidgetDescription desc = node.getWidget();
        Composite composite = null;
        if (desc != null) {
            composite = panel.addWidget(desc.getParams(), desc.getType(), desc
                    .getText(), desc.getLabel(), parent, listener);
            if (composite == null) {
                return;
            }
        } 
        else {
            composite = parent;
        }
        /* analyse the children of the node */
        Vector children = node.getChildren();
        for (int i = 0; i < children.size(); i++) {
            ParametersWidgetsNode child = 
                (ParametersWidgetsNode) children.elementAt(i);
            analyseParametersWidgetsNode(panel, child, composite, listener);
        }
    }
    
    /**
     * method wich set the values for the combo which is named "id"
     * @param values The values
     */
    public void setComboValues(Vector values) {
        if (this.params.containsKey("id")) {
            ParameterWidget pw = (ParameterWidget) this.params.get("id");
            pw.setComboValues(values);
        }
    }
    
    /**
     * method which set the values of the differents parameters
     * @param values The parameters values
     */
    public void setParametersValues(Map<String,String> values) {
        if (values == null) {
            return;
        }
        // get the parameters name
        Iterator iter = values.entrySet().iterator();
        while (iter.hasNext())
        {
        	Map.Entry entry = (Map.Entry)iter.next();
            String name = (String)entry.getKey();
            if (this.params.containsKey(name))
            {
                this.params.get(name).setValue((String)entry.getValue());
            } 
            else
            {
                throw new Error("unknown parameter name \"" + name + "\"");
            }
        }
    }
    
    /**
     * @param source
     * @return boolean
     */
    public boolean addButtonSelected(Object source) {
        Iterator elements = params.values().iterator();
        while (elements.hasNext()) {
            ParameterWidget pw = (ParameterWidget) elements.next();
            if (pw.getStyle() == WidgetDescription.NFIELD) {
                if (pw.addEmptyFieldForNField(source))
                    return true;
            } 
            else if (pw.getStyle() == WidgetDescription.TABLE) {
                if (pw.addEmptyEntryForTable(source)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * @param source
     * @return boolean
     */
    public boolean removeButtonSelected(Object source) {
        Iterator elements = params.values().iterator();
        while (elements.hasNext()) {
            ParameterWidget pw = (ParameterWidget) elements.next();
            if (pw.getStyle() == WidgetDescription.NFIELD) {
                if (pw.removeLastFieldForNField(source)) {
                    return true;
                }
            } 
            else if (pw.getStyle() == WidgetDescription.TABLE) {
                if (pw.removeLastEntryForTable(source)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * @param source
     * @return boolean
     */
    public boolean modifySomething(Object source) {
        Iterator elements = params.values().iterator();
        while (elements.hasNext()) {
            ParameterWidget pw = (ParameterWidget) elements.next();
            if (pw.getStyle() == WidgetDescription.TABLE) {
                if (pw.modifyText(source)) {
                    return true;
                }
            }
            if (pw.getStyle() == WidgetDescription.TEXT_FIELD) {
                if (pw.getValue().equals("true")) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * dispose all the elements of the panel
     */
    public void dispose() {
        if (params != null) {
            Iterator elements = params.values().iterator();
            while (elements.hasNext()) {
                ParameterWidget pw = (ParameterWidget) elements.next();
                pw.dispose();
            }
        }
        this.dispose();
    }
}