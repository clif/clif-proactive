/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wst.xml.core.internal.document.DocumentImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.IsacScenarioPlugin;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.loadprofile.LoadProfile;
import org.ow2.clif.scenario.isac.egui.loadprofile.ProfileWizard;
import org.ow2.clif.scenario.isac.egui.model.ModelReaderXIS;
import org.ow2.clif.scenario.isac.egui.model.ModelWriterXIS;
import static org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement.AFTER_NODE;
import static org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement.BEFORE_NODE;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.util.BehaviorUtil;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Master part of the pattern Master/Details.
 * It display a table of all actions in a behavior and
 * define a set of actions for managing these actions.
 * It also add a text field for changing behavior name
 * and manage the profile assigned to this behavior.
 * @author Joan Chaumont
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class BehaviorMasterPage extends MasterDetailsBlock
        implements ISelectionChangedListener, KeyListener {

    public static final String ADD_THEN = "Add \"Then\"";
    public static final String ADD_ELSE = "Add \"Else\"";
    public static final String ADD_CHOICE = "Add \"Choice\"";
    public static final String ADD_CHILD = "Add child";

    private static final String INSERT_END = "Insert at end";
    private static final String INSERT_BEGIN = "Insert at begin";

    private ScenarioManager scenario;
    private IsacEditor editor;

    private BehaviorTreeViewer behaviorTree;
    private String behaviorId;
    private int behaviorPos;
    private LoadProfile behaviorProfile;

    private Text textBehaviorId;

    private SelectionListener actionEditProfile;
    private SelectionListener actionAddAction;
    private SelectionListener actionRemoveAction;
    private SelectionListener actionClearActions;
    private SelectionListener actionHelpActions;
    private SelectionListener actionDuplicateBehavior;
    private SelectionListener actionCreateBehavior;
    private SelectionListener actionRemoveBehavior;
    private SelectionListener actionRemoveProfile;
    private SelectionListener actionUp;
    private SelectionListener actionDown;
    private SelectionListener actionInsertBeforeAction;
    private SelectionListener actionInsertAfterAction;

    private BehaviorDetailsPage detailsPage;
    private IManagedForm managedForm;

    private SectionPart spart;
    private Label profileWarning;
    private Button profileEdit;
    private Button profileRemove;
    private Button bAdd;
    private Button bInsertBefore;
    private Button bInsertAfter;

    private Object lastSel;

    /**
     * Constructor
     * @param scenario    Instance of the scenario manager
     * @param editor      Instance of the isac editor
     * @param behaviorId  id of the behaviour page
     * @param behaviorPos Position of the behaviour page
     * @param profile
     */
    public BehaviorMasterPage(
            ScenarioManager scenario, IsacEditor editor,
            String behaviorId, int behaviorPos, LoadProfile profile)
    {
        super();
        this.scenario = scenario;
        this.editor = editor;

        this.behaviorId = behaviorId;
        this.behaviorPos = behaviorPos;
        this.behaviorProfile = profile;

        detailsPage = new BehaviorDetailsPage(this.editor, this.scenario);
    }

    protected void createMasterPart(IManagedForm managedForm, Composite parent)
    {
        this.managedForm = managedForm;
        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        form.setText("Behavior Page");

        Listener mouseClicListener = new Listener() {
            public void handleEvent(Event event)
            {
                Point p = new Point(event.x, event.y);
                if (behaviorTree.getItem(p) == null)
                {
                    behaviorTree.setSelection(null);
                }
            }
        };

        /* Create "behavior" section */
        Section sectionBehavior = toolkit.createSection(parent,
                Section.DESCRIPTION | Section.EXPANDED);
        sectionBehavior.setText("Edition page for behavior description");
        toolkit.createCompositeSeparator(sectionBehavior);
        sectionBehavior.marginWidth = 10;
        sectionBehavior.marginHeight = 5;

        /* Composite client main */
        Composite main = toolkit.createComposite(sectionBehavior, SWT.FILL);
        main.setLayout(new GridLayout());
        main.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Composite idComp = toolkit.createComposite(main);
        idComp.setLayout(new GridLayout(2, false));
        idComp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        toolkit.createLabel(idComp, "Behavior name");
        textBehaviorId = toolkit.createText(idComp, behaviorId);
        textBehaviorId.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        textBehaviorId.addKeyListener(this);

        createActions();

        /* Behaviors creation remove and duplicate buttons */
        Composite behaviorButtons = toolkit.createComposite(main);
        behaviorButtons.setLayout(new GridLayout(3, true));

        Button bCreate = toolkit.createButton(
                behaviorButtons, "New", SWT.PUSH);
        bCreate.setToolTipText("Create a new behavior");
        bCreate.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bCreate.addSelectionListener(actionCreateBehavior);

        Button bDuplicate = toolkit.createButton(
                behaviorButtons, "Duplicate", SWT.PUSH);
        bDuplicate.setToolTipText("Duplicate this behavior");
        bDuplicate.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bDuplicate.addSelectionListener(actionDuplicateBehavior);

        Button bRemoveB = toolkit.createButton(
                behaviorButtons, "Remove", SWT.PUSH);
        bRemoveB.setToolTipText("Delete this behavior");
        bRemoveB.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bRemoveB.addSelectionListener(actionRemoveBehavior);

        /* access to load profile management */
        Composite sectionPClient = toolkit.createComposite(main);
        sectionPClient.setLayout(new GridLayout(4, false));
        sectionPClient.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        toolkit.createLabel(sectionPClient, "Load profile");
        profileEdit = toolkit.createButton(sectionPClient, "Modify", SWT.PUSH);
        profileRemove = toolkit.createButton(sectionPClient, "Delete", SWT.PUSH);
        profileWarning = toolkit.createLabel(sectionPClient, "");
		Color orange = form.getForm().getBody().getDisplay().getSystemColor(SWT.COLOR_DARK_YELLOW);
        profileWarning.setForeground(orange);
        enableProfileActions();

        /* Tree behavior section */
        Section sectionTree = toolkit.createSection(main,
                Section.TWISTIE | Section.EXPANDED);
        toolkit.createCompositeSeparator(sectionTree);
        sectionTree.setText("Behavior definition");
        sectionTree.setLayoutData(new GridData(GridData.FILL_BOTH));

        Composite compositeTree = toolkit.createComposite(sectionTree, SWT.FILL);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 1;
        layout.marginHeight = 1;
        compositeTree.setLayout(layout);
        compositeTree.setLayoutData(new GridData(GridData.FILL_BOTH));
        compositeTree.addMouseListener(new MouseListener() {
            public void mouseDoubleClick(MouseEvent e)
            {
            }

            public void mouseDown(MouseEvent e)
            {
                behaviorTree.setSelection(null);
            }

            public void mouseUp(MouseEvent e)
            {
                behaviorTree.setSelection(null);
            }
        });

        sectionTree.setClient(compositeTree);
        this.spart = new SectionPart(sectionTree);
        final SectionPart spart = this.spart;
        managedForm.addPart(spart);

        behaviorTree = new BehaviorTreeViewer(compositeTree,
                SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER,
                scenario, behaviorPos);

        behaviorTree.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));
        behaviorTree.addListener(SWT.MouseDoubleClick, mouseClicListener);
        behaviorTree.addSelectionChangedListener(this);


        profileEdit.addSelectionListener(actionEditProfile);
        profileRemove.addSelectionListener(actionRemoveProfile);

        /* Edit buttons composite */
        Composite sectionButtons = toolkit.createComposite(compositeTree, SWT.WRAP);
        sectionButtons.setLayout(new GridLayout());
        sectionButtons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));

        /* Add an action */
        bAdd = toolkit.createButton(sectionButtons, "Add", SWT.PUSH);
        bAdd.setToolTipText("Add a new action");
        bAdd.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bAdd.addSelectionListener(actionAddAction);
        bAdd.setVisible(false);

        /* Insert an action before selected action */
        bInsertBefore = toolkit.createButton(sectionButtons, INSERT_BEGIN, SWT.PUSH);
        bInsertBefore.setToolTipText("Add a new action before the selected action");
        bInsertBefore.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bInsertBefore.addSelectionListener(actionInsertBeforeAction);

        /* Insert an action before selected action */
        bInsertAfter = toolkit.createButton(sectionButtons, INSERT_END, SWT.PUSH);
        bInsertAfter.setToolTipText("Add a new action after the selected action");
        bInsertAfter.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bInsertAfter.addSelectionListener(actionInsertAfterAction);

        /* Remove selected action */
        Button bRemove = toolkit.createButton(sectionButtons, "Remove", SWT.PUSH);
        bRemove.setToolTipText("Remove the selected action");
        bRemove.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bRemove.addSelectionListener(actionRemoveAction);

        /* Remove all actions */
        Button bRemoveAll = toolkit.createButton(sectionButtons, "Clear", SWT.PUSH);
        bRemoveAll.setToolTipText("Clear the behavior");
        bRemoveAll.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bRemoveAll.addSelectionListener(actionClearActions);

        /* Show help for action */
        Button bHelp = toolkit.createButton(sectionButtons, "Help", SWT.PUSH);
        bHelp.setToolTipText("Action help");
        bHelp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bHelp.addSelectionListener(actionHelpActions);

        /* Up and down buttons */
        Composite upDown = toolkit.createComposite(sectionButtons);
        upDown.setLayout(new GridLayout(2, true));
        Button bUp = toolkit.createButton(upDown, "Up", SWT.PUSH);
        bUp.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bUp.addSelectionListener(actionUp);
        Button bDown = toolkit.createButton(upDown, "Down", SWT.PUSH);
        bDown.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        bDown.addSelectionListener(actionDown);

        sectionBehavior.setClient(main);
        toolkit.paintBordersFor(main);
        toolkit.paintBordersFor(idComp);
        toolkit.paintBordersFor(compositeTree);
        toolkit.paintBordersFor(sectionTree);
    }

    /* Change edit/modify profile name
      * if a profile exists or not */
    private void enableProfileActions()
    {
        if (behaviorProfile == null)
        {
            profileEdit.setText("Create");
            profileRemove.setEnabled(false);
            profileWarning.setText("Note: profile is empty");
        } else
        {
            profileEdit.setText("Modify");
            profileRemove.setEnabled(true);
            profileWarning.setText("");
        }
    }

    private void createActions()
    {
        /* Behavior creation ... */
        createActionCreateBehavior();
        createActionRemoveBehavior();
        createActionDuplicateBehavior();

        /* Profile edition */
        createActionEditProfile();
        createActionRemoveProfile();

        /* Manage actions */
        createActionAddAction();
        createActionInsertAfter();
        createActionInsertBefore();
        createActionRemoveAction();
        createActionClearActions();
        createActionHelpActions();

        /* Up and down action */
        createActionUp();
        createActionDown();
    }

    private void createActionCreateBehavior()
    {
        actionCreateBehavior = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                final String[] ids = ModelReaderXIS.getBehaviorsId(doc);

                /* Ask for new behavior id */
                Shell s = ((Button) e.getSource()).getShell();

                InputValidator inputValidator = new InputValidator(ids);
                String initialValue = inputValidator.getStringValid(textBehaviorId.getText());

                InputDialog dia = new InputDialog(s, "New Behavior",
                        "Name of the new behavior", initialValue, inputValidator);
                if (dia.open() == InputDialog.OK)
                {
                    String id = dia.getValue();

                    /*Create a new behavior */
                    Node behaviors = doc.getElementsByTagName("behaviors").item(0);
                    Element behavior = doc.createElement("behavior");
                    behavior.setAttribute("id", id);
                    behaviors.appendChild(behavior);
                    editor.modelChanged();
                }
            }
        };
    }

    private void createActionRemoveBehavior()
    {
        actionRemoveBehavior = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();

                /* Ask for confirm */
                Shell s = ((Button) e.getSource()).getShell();
                boolean ok = MessageDialog.openConfirm(s, "Remove behavior",
                        "Do you really want to remove this behavior?");
                if (ok)
                {
                    Node behaviorsNode = doc.getElementsByTagName("behaviors").item(0);
                    NodeList behaviors = doc.getElementsByTagName("behavior");
                    behaviorsNode.removeChild(behaviors.item(behaviorPos));
                    editor.modelChanged();
                }
            }
        };
    }

    private void createActionDuplicateBehavior()
    {
        actionDuplicateBehavior = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                final String[] ids = ModelReaderXIS.getBehaviorsId(doc);

                /* Ask for new behavior id */
                Shell s = ((Button) e.getSource()).getShell();
                InputValidator inputValidator = new InputValidator(ids);
                String initialValue = inputValidator.getStringValid(textBehaviorId.getText());
                InputDialog dia = new InputDialog(s, "Behavior Duplication",
                        "Name of the new behavior", initialValue, inputValidator);
                if (dia.open() == InputDialog.CANCEL)
                {
                    return;
                }
                String id = dia.getValue();

                /*Copy the current behavior */
                Node behaviors = doc.getElementsByTagName("behaviors").item(0);
                NodeList behaviorList = doc.getElementsByTagName("behavior");

                Node behavior = behaviorList.item(behaviorPos);
                Node dupli = behavior.cloneNode(true);
                dupli.getAttributes().getNamedItem("id").setNodeValue(id);
                behaviors.appendChild(dupli);

                Node profiles = doc.getElementsByTagName("loadprofile").item(0);
                NodeList groups = doc.getElementsByTagName("group");
                int nbGroups = groups.getLength();

                for (int i = 0; i < nbGroups; i++)
                {
                    Element tmp = (Element) groups.item(i);
                    if (tmp.getAttribute("behavior").equals(behaviorId))
                    {
                        Node dupli2 = tmp.cloneNode(true);
                        dupli2.getAttributes().getNamedItem("behavior").setNodeValue(id);
                        profiles.appendChild(dupli2);
                        break;
                    }
                }

                editor.modelChanged();
            }
        };
    }

    private void createActionEditProfile()
    {
        actionEditProfile = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                Shell s = ((Button) e.getSource()).getShell();
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();

                WizardDialog dialog =
                        new WizardDialog(s, new ProfileWizard(
                                doc, behaviorId, behaviorProfile));
                dialog.setPageSize(300, 400);
                if (dialog.open() == WizardDialog.OK)
                {
                    editor.modelChanged();
                }
            }
        };
    }

    private void createActionRemoveProfile()
    {
        actionRemoveProfile = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                /* Ask for confirm */
                Shell s = ((Button) e.getSource()).getShell();
                boolean ok = MessageDialog.openConfirm(s, "Remove profile",
                        "Do you really want to remove this profile?");
                if (!ok)
                {
                    return;
                }

                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                NodeList profiles = doc.getElementsByTagName("group");
                int nbProf = profiles.getLength();

                for (int i = 0; i < nbProf; i++)
                {
                    Element profile = (Element) profiles.item(i);
                    if (profile.getAttribute("behavior").equals(behaviorId))
                    {
                        Node parent = profile.getParentNode();
                        parent.removeChild(profile);
                    }
                }
                editor.modelChanged();
            }
        };
    }

    /**
     * Add simple node if it's a then, else or choice
     * else open a actionWizard to let the user select
     * one action
     */
    private void createActionAddAction()
    {
        actionAddAction = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                IStructuredSelection sel =
                        (IStructuredSelection) behaviorTree.getSelection();
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                Element elt = (Element) sel.getFirstElement();

                Element behavior = ModelReaderXIS.getBehaviorById(doc, behaviorId);
                Element behaviors = (Element) behavior.getParentNode();
                if (!behaviors.getNodeName().equals("behaviors"))
                {
                    ModelWriterXIS.correctBehaviorsNode(doc);
                }

                /* Simple "action" */
                if (bAdd.getText().equals(ADD_THEN))
                {
                    Element elseNode = (Element) elt.getElementsByTagName("else").item(0);
                    if (elseNode != null)
                    {
                        elt.insertBefore(doc.createElement("then"), elseNode);
                    } else
                    {
                        elt.appendChild(doc.createElement("then"));
                    }
                } else if (bAdd.getText().equals(ADD_ELSE))
                {
                    elt.appendChild(doc.createElement("else"));
                } else if (bAdd.getText().equals(ADD_CHOICE))
                {
                    Element choice = doc.createElement("choice");
                    choice.setAttribute("proba", "");
                    elt.appendChild(choice);
                } else
                {
                    Shell s = ((Button) e.getSource()).getShell();
                    Map<String, String> usedPlugins = ModelReaderXIS.getPlugins(doc);
                    Map<String, PluginDescription> plugins = new HashMap<String, PluginDescription>();

                    /* Get the list of plug-ins */
                    for (Map.Entry<String, String> entryIdDescription : usedPlugins.entrySet())
                    {
                        plugins.put(entryIdDescription.getKey(),
                                scenario.getPluginManager().getDescription(entryIdDescription.getValue()));
                    }

                    /* create new add wizard with the list of plug-ins */
                    WizardDialog dialog =
                            new WizardDialog(s, new ActionWizard(doc, elt, behavior, plugins));
                    dialog.setPageSize(300, 400);
                    dialog.open();
                }
                behaviorTree.refresh();
                refresh();
            }
        };
    }

    private void createActionInsertBefore()
    {
        actionInsertBeforeAction = createWidgetSelectedForInsert(BEFORE_NODE);
    }

    private void createActionInsertAfter()
    {
        actionInsertAfterAction = createWidgetSelectedForInsert(AFTER_NODE);
    }

    /**
     * Create the widget selected for insert actions
     * @param placement Placement of the element to insert
     * @return The selection adapter
     */
    private SelectionListener createWidgetSelectedForInsert(final ActionPlacement placement)
    {
        return new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e)
            {
                IStructuredSelection sel =
                        (IStructuredSelection) behaviorTree.getSelection();
                DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                Element elt = (Element) sel.getFirstElement();

                Element behavior = ModelReaderXIS.getBehaviorById(doc, behaviorId);
                Element behaviors = (Element) behavior.getParentNode();
                if (!behaviors.getNodeName().equals("behaviors"))
                {
                    ModelWriterXIS.correctBehaviorsNode(doc);
                }

                Shell s = ((Button) e.getSource()).getShell();
                Map<String, String> usedPlugins = ModelReaderXIS.getPlugins(doc);
                Map<String, PluginDescription> plugins = new HashMap<String, PluginDescription>();

                /* Get the list of plug-ins */
                for (Map.Entry<String, String> entryIdDescription : usedPlugins.entrySet())
                {
                    plugins.put(entryIdDescription.getKey(),
                            scenario.getPluginManager().getDescription(entryIdDescription.getValue()));
                }

                /* create new add wizard with the list of plug-ins */
                WizardDialog dialog =
                        new WizardDialog(s, new ActionWizard(doc, elt, behavior, plugins, placement));
                dialog.setPageSize(300, 400);
                dialog.open();

                behaviorTree.refresh();
                refresh();
            }
        };
    }


    /* Remove a single selected action */
    private void createActionRemoveAction()
    {
        actionRemoveAction = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                IStructuredSelection sel =
                        (IStructuredSelection) behaviorTree.getSelection();

                Iterator iter = sel.iterator();
                while (iter.hasNext())
                {
                    Element elt = (Element) iter.next();
                    DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
                    Element parent = (Element) elt.getParentNode();

                    /* Change else to then if a then node is removed */
                    if (elt.getNodeName().equals("then"))
                    {
                        Element elseNode =
                                (Element) parent.getElementsByTagName("else").item(0);
                        if (elseNode != null)
                        {
                            NodeList childs = elseNode.getChildNodes();
                            int nbChilds = childs.getLength();

                            Element newThen = doc.createElement("then");

                            for (int i = 0; i < nbChilds; i++)
                            {
                                newThen.appendChild(childs.item(i));
                            }
                            parent.removeChild(elseNode);
                            parent.appendChild(newThen);
                        }
                    }
                    parent.removeChild(elt);
                }
                behaviorTree.refresh();
                refresh();
            }
        };
    }

    private void createActionClearActions()
    {
        actionClearActions = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                /* Ask for confirm */
                Shell s = ((Button) e.getSource()).getShell();
                boolean ok = MessageDialog.openConfirm(s, "Clear behavior",
                        "Do you really want to clear this behavior?");
                if (ok)
                {
                    DocumentImpl doc =
                            (DocumentImpl) behaviorTree.getInput();
                    Element behavior =
                            ModelReaderXIS.getBehaviorById(doc, behaviorId);

                    while (behavior.hasChildNodes())
                    {
                        behavior.removeChild(behavior.getFirstChild());
                    }
                    behaviorTree.refresh();
                    refresh();
                }
            }
        };
    }

    private void createActionHelpActions()
    {
        actionHelpActions = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {

                Tree tree = behaviorTree.getTree();

                if (tree != null && tree.getSelectionCount() > 0)
                {

                    TreeItem item = tree.getSelection()[0];

                    Element element = (Element) item.getData();
                    Vector<String> help = ModelReaderXIS.getActionHelp(element, scenario.getPluginManager());
                    StringBuilder sbHelp = new StringBuilder();
                    if (help == null)
                    {
                        sbHelp.append("No action help.");
                    }
                    else
                    {
                        for (String helpLine : help)
                        {
                            sbHelp.append(helpLine + "\n");
                        }
                    }

                    Shell shell = ((Button) e.getSource()).getShell();
                    Shell s = new Shell(shell, SWT.SHELL_TRIM);
                    s.setLayout(new GridLayout(1, true));
                    s.setText(item.getText());

                    Text tHelp = new Text(s, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.WRAP);
                    GridData gdata = new GridData(GridData.FILL_BOTH);
                    tHelp.setLayoutData(gdata);
                    tHelp.setText(sbHelp.toString());
                    tHelp.setEditable(false);
                    tHelp.setSize(400, 250);

                    s.setSize(400, 250);
                    s.open();
                }
            }
        };
    }

    /* Move a action up in parent tree (and file) */
    private void createActionUp()
    {
        actionUp = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                IStructuredSelection sel =
                        (IStructuredSelection) behaviorTree.getSelection();
                List<Element> listElement = sel.toList();
                Element elt = (Element) sel.getFirstElement();

                if (elt != null)
                {
                    Node n = elt.getPreviousSibling();

                    /* find previous Element*/
                    while (n != null && n.getNodeType() != Node.ELEMENT_NODE)
                    {
                        n = n.getPreviousSibling();
                    }
                    /* insert selection before the previous element */
                    Element parent = (Element) elt.getParentNode();
                    if (n != null)
                    {
                        for (Element element : listElement)
                        {
                            parent.insertBefore(element, n);
                        }
                    } else
                    {
                        for (Element element : listElement)
                        {
                            parent.insertBefore(element, n);
                        }
                    }
                }
                behaviorTree.refresh();
            }
        };
    }

    /* Move a action down in parent tree (and file) */
    private void createActionDown()
    {
        actionDown = new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                IStructuredSelection sel =
                        (IStructuredSelection) behaviorTree.getSelection();
                Element elt = (Element) sel.toList().get(sel.size() - 1);
                List<Element> listElement = sel.toList();
                if (elt != null)
                {
                    Node n = elt.getNextSibling();

                    /* find next Element*/
                    while (n != null && n.getNodeType() != Node.ELEMENT_NODE)
                    {
                        n = n.getNextSibling();
                    }
                    /* insert selection after the next element */
                    Element parent = (Element) elt.getParentNode();
                    for (Element element : listElement)
                    {
                        parent.insertBefore(element, n);
                    }
                    parent.insertBefore(n, (Node) sel.getFirstElement());
                }
                behaviorTree.refresh();
            }
        };
    }

    protected void registerPages(DetailsPart detailsPart)
    {
        detailsPart.registerPage(ElementImpl.class, detailsPage);
    }

    protected void createToolBarActions(IManagedForm managedForm)
    {
        final ScrolledForm form = managedForm.getForm();

        /* Horizontal orientation action */
        Action haction = new Action("hor", Action.AS_RADIO_BUTTON) {
            public void run()
            {
                sashForm.setOrientation(SWT.HORIZONTAL);
                form.reflow(true);
            }
        };
        haction.setChecked(true);
        haction.setToolTipText("Horizontal orientation");
        haction.setImageDescriptor(IsacScenarioPlugin.imageDescriptorFromPlugin(
                IsacScenarioPlugin.PLUGIN_ID, "icons/hor.ico"));

        /* Vertical orientation action */
        Action vaction = new Action("ver", Action.AS_RADIO_BUTTON) {
            public void run()
            {
                sashForm.setOrientation(SWT.VERTICAL);
                form.reflow(true);
            }
        };
        vaction.setChecked(false);
        vaction.setToolTipText("Vertical orientation");
        vaction.setImageDescriptor(IsacScenarioPlugin.imageDescriptorFromPlugin(
                IsacScenarioPlugin.PLUGIN_ID, "icons/vor.ico"));

        /* Add actions to toolbar */
        form.getToolBarManager().add(haction);
        form.getToolBarManager().add(vaction);
    }

    /**
     * Set document
     * @param doc
     */
    public void setDocument(IDocument doc)
    {
        behaviorTree.setDocument(doc);
    }

    /**
     * Set behavior Id
     * @param behaviorId
     */
    public void setBehaviorId(String behaviorId)
    {
        this.behaviorId = behaviorId;
    }

    /**
     * Set behavior position
     * @param behaviorPos
     */
    public void setBehaviorPos(int behaviorPos)
    {
        behaviorTree.setBehaviorPos(behaviorPos);
    }

    /**
     * Set behavior profile
     * @param behaviorProfile
     */
    public void setBehaviorProfile(LoadProfile behaviorProfile)
    {
        this.behaviorProfile = behaviorProfile;
        enableProfileActions();
    }

    /**
     * Refresh details part and tree
     */
    public void refresh()
    {
        detailsPage.refresh();
        behaviorTree.refresh();
        selectionChanged(new SelectionChangedEvent(behaviorTree, behaviorTree.getSelection()));
    }

    /* Refresh button on selection
      * change add button text if the selected node
      * is a "nchoice" or an "if"
      * If there is no selected item or if new item will be
      * added before selected item change add button text to "insert"
      */
    private void refreshAddButton(IStructuredSelection sel)
    {
        bAdd.setText(BehaviorUtil.getAddText(sel));
        if (bAdd.getText().equals(""))
        {
            bAdd.setVisible(false);
        } else
        {
            bAdd.setVisible(true);
        }
    }

    /**
     * Refresh the insert buttons depending on the selection. <br />
     * When unable to insert a sibling, it proposes to insert element to the head/tail of the list.
     * @param sel The selection
     */
    private void refreshInsertButtons(IStructuredSelection sel)
    {
        Element elt = (Element) sel.getFirstElement();
        bInsertAfter.setText(INSERT_END);
        bInsertBefore.setText(INSERT_BEGIN);
        if (elt != null)
        {
            bInsertAfter.setText("Insert after");
            bInsertBefore.setText("Insert before");
        }
    }


    public void selectionChanged(SelectionChangedEvent event)
    {
        IStructuredSelection sel = (IStructuredSelection) event.getSelection();
        refreshAddButton(sel);
        refreshInsertButtons(sel);

        if (sel.equals(lastSel))
        {
            return;
        }

        final IManagedForm mForm = managedForm;
        final SectionPart spart = this.spart;
        lastSel = sel;
        mForm.fireSelectionChanged(spart, sel);
    }


    /* Listener used by the id text field */
    public void keyPressed(KeyEvent e)
    {
    }

    public void keyReleased(KeyEvent e)
    {
        if (!behaviorId.equals(textBehaviorId.getText()))
        {
            DocumentImpl doc = (DocumentImpl) behaviorTree.getInput();
            NodeList behaviorList = doc.getElementsByTagName("behavior");
            Element behavior = (Element) behaviorList.item(behaviorPos);

            NodeList groups = doc.getElementsByTagName("group");
            int nbGroups = groups.getLength();
            if (behaviorProfile != null)
            {
                Element grp;
                for (int i = 0; i < nbGroups; i++)
                {
                    grp = (Element) groups.item(i);
                    if (grp.getAttribute("behavior").equals(behaviorId))
                    {
                        grp.setAttribute("behavior", textBehaviorId.getText());
                        break;
                    }
                }
            }
            behavior.setAttribute("id", textBehaviorId.getText());
            editor.modelChanged();
        }
    }
}
