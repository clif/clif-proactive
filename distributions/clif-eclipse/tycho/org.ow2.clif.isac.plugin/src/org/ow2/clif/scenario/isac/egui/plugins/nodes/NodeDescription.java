/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of an object storing the full description of a tree node
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class NodeDescription {
    private String type;
    private Map<String,String> params;
    private String plugin;
    private String actionName;

    /**
     * Build a node description element, the type of the node must be defined in
     * parameter
     * @param type The node type
     */
    public NodeDescription(String type) {
        this.type = type;
        this.params = new HashMap<String,String>() ;
        this.plugin = null;
        this.actionName = null;
    }

    /**
     * Build a new element which is a copy of the given one
     * @param node The node description to be copied
     */
    public NodeDescription(NodeDescription node) {
        if (node != null) {
            this.type = new String(node.getType());
            this.params = new HashMap<String,String>(node.getParams());
            if (node.getPlugin() != null) {
                this.plugin = new String(node.getPlugin());
            }
            if (node.getActionName() != null) {
                this.actionName = new String(node.getActionName());
            }
        }
    }

    /**
     * Attribute actionName getter
     * @return The name of the action representing by this node
     */
    public String getActionName() {
        return actionName;
    }

    /**
     * Attribute params getter
     * @return The parameters values in a hashtable
     */
    public Map<String,String> getParams() {
        return params;
    }

    /**
     * Attribute plugin getter
     * @return The plugin name
     */
    public String getPlugin() {
        return plugin;
    }

    /**
     * Attribute type getter
     * @return The type of the node
     */
    public String getType() {
        return type;
    }

    /**
     * Attribute actionName setter
     * @param string The name of the action representing by this node
     */
    public void setActionName(String string) {
        actionName = string;
    }

    /**
     * Attribute params setter
     * @param map The table containing all parameters values
     */
    public void setParams(Map<String,String> map) {
        params = map;
    }

    /**
     * Attribute plugin setter
     * @param string The plugin name
     */
    public void setPlugin(String string) {
        plugin = string;
    }

    /**
     * Create a new node description of the specified type, if this type is
     * known
     * @param type The type of the node which will be created
     * @return The node description
     */
    public static NodeDescription createNonePluginNode(String type) {
        NodeDescription node = NonePluginNode.createNodeDescription(type) ;
        return node;
    }

    /**
     * This method serialize the object
     * @return the object informations representing by a string
     */
    public String toString() {
        return new String("{type:"+type+",params:"+params+",plugin"+plugin+",an:"+actionName+"}") ;
    }
}