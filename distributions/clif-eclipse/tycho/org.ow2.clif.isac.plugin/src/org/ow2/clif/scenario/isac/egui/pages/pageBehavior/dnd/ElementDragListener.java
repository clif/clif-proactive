/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.ow2.clif.scenario.isac.egui.util.BehaviorUtil;
import org.w3c.dom.Element;

import java.util.List;

/**
 * Supports dragging ScenarioNodes from a structured viewer
 * @author JC Meillaud
 * @author A Peyrard
 */
public class ElementDragListener extends DragSourceAdapter {
    private StructuredViewer viewer;

    /**
     * Constructor
     * @param viewer The viewer where listen
     */
    public ElementDragListener(StructuredViewer viewer)
    {
        this.viewer = viewer;
    }

    /**
     * Method declared on DragSourceListener
     * @param event The event source
     */
    public void dragFinished(DragSourceEvent event)
    {
        if (!event.doit)
        {
            return;
        }

        /* if the node was moved, remove it from the source viewer */
        if (event.detail == DND.DROP_MOVE)
        {
            IStructuredSelection selection =
                    (IStructuredSelection) viewer.getSelection();
            List<Element> elt = selection.toList();
            for (Element element : elt)
            {
                element.getParentNode().removeChild(element);
            }
            viewer.refresh();
        }
    }

    /**
     * Method declared on DragSourceListener
     * @param event The event source
     */
    public void dragSetData(DragSourceEvent event)
    {
        IStructuredSelection selection =
                (IStructuredSelection) viewer.getSelection();
        List<Element> tree = selection.toList();

        if (ElementTransfer.getInstance().isSupportedType(event.dataType))
        {
            event.data = tree;
        }
    }

    /**
     * Method declared on DragSourceListener
     * @param event The event source
     */
    public void dragStart(DragSourceEvent event)
    {
        IStructuredSelection selection =
                (IStructuredSelection) viewer.getSelection();
        if (selection.size() >= 1)
        {
            event.doit = true;
            List<Element> list = selection.toList();
            for (Element element : list)
            {
                event.doit = event.doit && BehaviorUtil.isAllowedToMove(element);
            }
            return;
        }
        event.doit = false;
    }
}
