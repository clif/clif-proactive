/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.model;

import java.util.Iterator;

import org.ow2.clif.scenario.isac.egui.plugins.ParameterDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Test if the model is right and correct it if it's not
 * @author Joan Chaumont
 *
 */
public class ModelWriterXIS {
    
    /**
     * Add the scenario node
     * @param doc
     * @return Element the element scenario
     */
    public static Element correctScenario(Document doc) {
        Element scenario =  doc.createElement("scenario");
        doc.appendChild(scenario);
        /* correct sub elements : behaviors and profiles*/
        correctBehaviorsNode(doc);
        correctLoadProfile(doc);
        
        return scenario;
     }
    
    /**
     * Add the behaviors node
     * @param doc
     * @return Element the element behaviors
     */
    public static Element correctBehaviorsNode(Document doc) {
        /* Test the scenario node */
        Element scenario = (Element)doc.getElementsByTagName("scenario").item(0);
        if(scenario == null) {
            scenario = correctScenario(doc);
            return (Element)doc.getElementsByTagName("behaviors").item(0);
        }
        
        /* Add the behaviors node */
        Element behaviorsNode = (Element)doc.getElementsByTagName("behaviors").item(0);
        if(behaviorsNode == null) {
            behaviorsNode = doc.createElement("behaviors");
        }
        
        /* Test the loadprofile node */
        Element loadProfile = (Element)doc.getElementsByTagName("loadprofile").item(0);
        if(loadProfile == null) {
            loadProfile = correctLoadProfile(doc);
        }
        scenario.insertBefore(behaviorsNode, loadProfile);
        
        /* Add all existing behavior nodes to the behaviorS node */
        NodeList behaviors = doc.getElementsByTagName("behavior");
        appendChilds(behaviorsNode, behaviors);
        
        /* Test and correct plugins node */
        correctPlugins(doc);
        
        return behaviorsNode;
    }
    
    /**
     * Add loadprofile node
     * @param doc
     * @return Element the element loadprofile
     */
    public static Element correctLoadProfile(Document doc) {
        /* Test the scenario node */
        Element scenario = (Element)doc.getElementsByTagName("scenario").item(0);
        if(scenario == null) {
            scenario = correctScenario(doc);
            return (Element)doc.getElementsByTagName("loadprofile").item(0);
        }
        /* Add loadprofile node */
        Element loadprofile = (Element)doc.getElementsByTagName("loadprofile").item(0);
        if(loadprofile == null) {
            loadprofile = doc.createElement("loadprofile");
        }
        scenario.appendChild(loadprofile);
        
        /* Add all existing group nodes to the loadprofile node */
        NodeList groups = doc.getElementsByTagName("group");
        appendChilds(loadprofile, groups);
        
        return loadprofile;
    }
    
    /**
     * Add the plugins node
     * @param doc
     * @return Element the element plugins
     */
    public static Element correctPlugins(Document doc) {
        /* Test the behaviors node */
        Element behaviors = (Element)doc.getElementsByTagName("behaviors").item(0);
        if(behaviors == null) {
            behaviors = correctBehaviorsNode(doc);
        }
        /* Add plugins node */
        Element plugins = (Element)doc.getElementsByTagName("plugins").item(0);
        if(plugins == null) {
            plugins = doc.createElement("plugins");
        }
        behaviors.insertBefore(plugins, behaviors.getFirstChild());
        
        /* Add all existing use nodes to the plugins node */
        NodeList uses = doc.getElementsByTagName("use");
        appendChilds(plugins, uses);
        
        return plugins;
    }

    /* Append the list of childs to a parent */
    private static void appendChilds(Element newParent, NodeList childs) {
        int nbChilds = childs.getLength();
        for (int i = 0; i < nbChilds; i++) {
            Element tmp = (Element)childs.item(i);
            Element parent = (Element)tmp.getParentNode();
            if(!parent.equals(newParent)) {
                newParent.appendChild(tmp);
            }
        }
    }


    /**
     * Sets the params section of an action or test (aka condition)
     * @param doc the target XML document
     * @param parent the target action (sample, control, timer) or test element
     * @param params the hash table of key-value pairs (null means no parameter to add)
     */
    static public void addParams(
    	Document doc,
    	Element parent,
    	Iterable<ParameterDescription> params)
    {
    	if (params != null)
    	{
	    	Element paramsElt = doc.createElement(Node.PARAMS);
	    	parent.appendChild(paramsElt);
	    	Iterator<ParameterDescription> iter = params.iterator();
	    	while (iter.hasNext())
	    	{
	    		ParameterDescription paramDesc = iter.next();
	    		Element param = doc.createElement(Node.PARAM);
	    		param.setAttribute("name", paramDesc.getName());
	    		param.setAttribute("value", paramDesc.getDefaultValue());
	    		paramsElt.appendChild(param);
	    	}
    	}
    }
}
