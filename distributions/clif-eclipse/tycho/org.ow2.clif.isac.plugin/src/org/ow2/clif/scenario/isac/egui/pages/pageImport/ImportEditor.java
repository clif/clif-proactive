/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageImport;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.ManagedForm;
import org.eclipse.ui.part.EditorPart;
import org.ow2.clif.scenario.isac.egui.IsacEditor;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;

/**
 * There is only one import page editor
 * for each scenario
 * 
 * @author Joan Chaumont
 */
public class ImportEditor extends EditorPart {

    private ScenarioManager scenario;
    private IsacEditor editor;
    private ImportMasterPage importPage;

    /**
     * Constructor
     * @param scenario
     * @param editor
     */
    public ImportEditor (ScenarioManager scenario, IsacEditor editor) {
        this.scenario = scenario;
        this.editor = editor;
    }
    
    public void doSave(IProgressMonitor monitor) {}

    public void doSaveAs() {}

    public void init(IEditorSite site, IEditorInput input)
            throws PartInitException {
        setInput(input);
        setSite(site);
    }

    public boolean isDirty() {
        return false;
    }

    public boolean isSaveAsAllowed() {
        return false;
    }

    public void createPartControl(Composite parent) {
        importPage = new ImportMasterPage(scenario, editor);
        importPage.createContent(new ManagedForm(parent));
    }

    public void setFocus() {}
    
    /**
     * Refresh components in this editor
     */
    public void refresh() {
        importPage.refresh();
    }
    
    /**
     * Set the edited document 
     * @param doc
     */
    public void setDocument(IDocument doc) {
        importPage.setDocument(doc);
    }
    
    /**
     * @return ScenarioManager
     */
    public ScenarioManager getScenario() {
        return scenario;
    }
}
