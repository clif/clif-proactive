/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards.httpCapture;

import java.io.IOException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.*;

/**
 * This is a new Http Capture wizard. Its role is to use MaxQ software
 * for recording an http scenario. This wizard will creates (or updates)
 * a maxq.properties file and a scenario file (under ISAC format).
 *
 * @author Florian Francheteau
 */

public class HttpCaptureWizard extends Wizard implements INewWizard {
    
	private MainHttpCaptureWizardPage mainPage;
    private RunHttpCaptureWizardPage runPage;
    private ISelection selection;
    
    /** Constructor for HttpCaptureWizard. */
    public HttpCaptureWizard() {
        super();
        setNeedsProgressMonitor(false);
        setHelpAvailable(false);
        setWindowTitle("HTTP Capture");
    }
    
    /** Adding pages to the wizard. */
    public void addPages() {
    	mainPage = new MainHttpCaptureWizardPage(selection);
    	addPage(mainPage);
    	runPage = new RunHttpCaptureWizardPage(selection);
    	addPage(runPage);
    }
    /**
     * We will accept the selection in the workbench to see if
     * we can initialize from it.
     * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection selection) {
    	this.selection = selection;
    }

    /**
     * Closing proxy connections and cleaning configuration
     * when user click on "Finish" button
     */
    @Override
    public boolean performFinish() {
    	try {
			runPage.getProxy().getServerSocket().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	runPage.getProxy().interrupt();
    	ConfigIsac.removeConfig();
		IWorkspaceRoot workspace = ResourcesPlugin.getWorkspace().getRoot();
		try {
            workspace.refreshLocal(IProject.DEPTH_INFINITE, null);
        } catch (CoreException e) {
            e.printStackTrace();
        }
    	return true;
    }
    
    /**
     * Closing proxy connections and cleaning configuration
     * when user click on "Cancel" button
     */
    @Override
    public boolean performCancel() {
    	try {
    		if (runPage.getProxy()!=null){
    			if (runPage.getProxy().getServerSocket()!=null){
    				runPage.getProxy().getServerSocket().close();
    				runPage.getProxy().interrupt();
    			}
    		}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		if (runPage.getTimer()!=null){
			runPage.getTimer().cancel();
		}
    	ConfigIsac.removeConfig();
    	return true;
    }
}