/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.Transfer;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd.ElementTransfer;

/**
 * Copy action used in editors : import and behavior
 * @author Joan Chaumont
 */
public class CopyAction extends Action {

    private TreeViewer viewer;

    /**
     * Constructor
     * @param v this viewer in which we copy an element
     */
    public CopyAction(TreeViewer v)
    {
        super();

        viewer = v;

        setAccelerator(SWT.CTRL | 'C');
        setText("Copy");
        try
        {
            ImageRegistry reg = Icons.getImageRegistry();
            setImageDescriptor(reg.getDescriptor(FileName.COPY_ICON));
        } catch (Exception e)
        {
        }
    }

    public void run()
    {
        Clipboard clipboard = ActionsClipboard.getClipboard();
        ElementTransfer transfer = ElementTransfer.getInstance();

        /* Copy selection to clipboard */
        IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
        if (sel.isEmpty())
        {
            return;
        }
        clipboard.setContents(new Object[]{sel.toList()},
                new Transfer[]{transfer});
    }
}
