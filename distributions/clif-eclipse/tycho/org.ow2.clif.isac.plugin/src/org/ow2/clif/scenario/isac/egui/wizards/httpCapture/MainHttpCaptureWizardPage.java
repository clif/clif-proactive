/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards.httpCapture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.ow2.clif.util.ExecutionContext;

/**
 * This is the first page of Http Capture wizard. Its role is to set maxq configuration.
 * Fields will be initialised with max.properties file located on project folder.
 * If this file does not exist, it will be recovered from plugin location.
 *
 * @author Florian Francheteau
 */
public class MainHttpCaptureWizardPage extends WizardPage {

	private Text containerText;
	private Button browseButton;
	private Text fileText;
	private Text portText;
	private Button useRemoteProxy;
	private Text proxyHostText;
	private Text proxyPortText;
	private Combo timerList;
	private Combo randomDistList;
	private Label randomDistLabel;
	private Label randomDistDeltaLabel;
	private Text randomDistDeltaText;
	private Label randomDistDeviationLabel;
	private Text randomDistDeviationText;
	

	private File propertiesFile;
	private IPath containerPath;

	private static final int DEFAULT_PORT = 8090;
	private static final int TIMER_CONSTANT = 0;
	private static final int TIMER_NULL = 1;
	private static final int TIMER_RANDOM = 2;
	private static final int DIST_UNIFORM = 0;
	private static final int DIST_GAUSSIAN = 1;
	private static final int DIST_POISSON = 2;
	private static final int DIST_NEGEXPO = 3;

	private ISelection selection;

	/**
	 * Constructor for MainHttpCaptureWizardPage
	 * @param selection
	 */
	public MainHttpCaptureWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("New HTTP Capture");
		setDescription("This wizard allows the user to run an HTTP Capture and to save it into ISAC format.");
		this.selection = selection;
	}

	/**
	 * Creates all controls for MainHttpCaptureWizardPage
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 5;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;

		//Container Label
		Label label = new Label(container, SWT.NULL);
		label.setText("&Container:");

		//Container Text
		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Browse Button
		browseButton = new Button(container, SWT.PUSH);
		browseButton.setText("Browse...");
		browseButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});

		//Result file name Label
		label = new Label(container, SWT.NULL);
		label.setText("&File name:");

		//Result file name Text
		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 4;
		fileText.setLayoutData(gd);
		fileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Local Port Label
		label = new Label(container, SWT.NULL);
		label.setText("&Port:");

		//Local Port Text
		portText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=50;
		gd.horizontalSpan = 4;
		portText.setLayoutData(gd);
		portText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Remote Proxy Checkbox
		useRemoteProxy = new Button(container, SWT.CHECK);
		useRemoteProxy.setText("&Use Remote Proxy");
		useRemoteProxy.setSelection(false);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 5;
		useRemoteProxy.setLayoutData(gd);
		useRemoteProxy.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectRemoteProxy();
				dialogChanged();
			}
		}
		);

		//Remote Host Label
		label = new Label(container, SWT.NULL);
		label.setText("Proxy &Host:");

		//Remote Host Text
		proxyHostText = new Text(container, SWT.BORDER | SWT.SINGLE);
		proxyHostText.setEnabled(false);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.widthHint=200;
		proxyHostText.setLayoutData(gd);
		proxyHostText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Remote Port Label
		label = new Label(container, SWT.NULL);
		label.setText("Proxy P&ort:");
		gd = new GridData(GridData.END);
		label.setLayoutData(gd);

		//Remote Port Text
		proxyPortText = new Text(container, SWT.BORDER | SWT.SINGLE);
		proxyPortText.setEnabled(false);
		gd = new GridData(GridData.BEGINNING);
		gd.horizontalSpan = 2;
		gd.widthHint=50;
		proxyPortText.setLayoutData(gd);
		proxyPortText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Timer Label
		label = new Label(container, SWT.NULL);
		label.setText("&Timer:");

		//Timer Combo
		timerList = new Combo(container, SWT.READ_ONLY | SWT.BORDER);
		timerList.add("ConstantTimer");
		timerList.add("null");
		timerList.add("Random");
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=80;
		gd.horizontalSpan = 4;
		timerList.setLayoutData(gd);
		timerList.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				timerChanged();
				dialogChanged();
			}
		});

		//Random Distribution Label
		randomDistLabel = new Label(container, SWT.NULL);
		randomDistLabel.setText("&Random dist:");
		randomDistLabel.setVisible(false);

		//Random Distribution Combo
		randomDistList = new Combo(container, SWT.READ_ONLY | SWT.BORDER);
		randomDistList.add("uniform");
		randomDistList.add("gaussian");
		randomDistList.add("poisson");
		randomDistList.add("negexpo");
		randomDistList.setVisible(false);
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=80;
		gd.horizontalSpan = 4;
		randomDistList.setLayoutData(gd);
		randomDistList.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				timerChanged();
				dialogChanged();
			}
		});

		//Random Delta or Unit Distribution Label
		randomDistDeltaLabel = new Label(container, SWT.NULL);
		randomDistDeltaLabel.setVisible(false);
		randomDistDeltaLabel.setText("&Delta:");

		//Random Delta or Unit Distribution Text
		randomDistDeltaText = new Text(container, SWT.BORDER | SWT.SINGLE);
		randomDistDeltaText.setVisible(false);
		gd = new GridData(GridData.BEGINNING);
		gd.horizontalSpan = 1;
		gd.widthHint=50;
		randomDistDeltaText.setLayoutData(gd);
		randomDistDeltaText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		//Random Deviation Distribution Label
		randomDistDeviationLabel = new Label(container, SWT.NULL);
		randomDistDeviationLabel.setVisible(false);
		randomDistDeviationLabel.setText("D&eviation:");
		
		//Random Deviation Distribution Text
		randomDistDeviationText = new Text(container, SWT.BORDER | SWT.SINGLE);
		randomDistDeviationText.setVisible(false);
		gd = new GridData(GridData.BEGINNING);
		gd.horizontalSpan = 1;
		gd.widthHint=50;
		randomDistDeviationText.setLayoutData(gd);
		randomDistDeviationText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		initialize();
		setControl(container);
	}

	/** 
	 * Tests if the current workbench selection is a suitable container to use. 
	 */
	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				containerText.setText(container.getFullPath().toString());
				containerPath = container.getLocation();
				IPath propPath = containerPath.append("maxq.properties");
				propertiesFile = new File(propPath.toOSString());

				//if properties file does not exist in container
				if (!propertiesFile.exists()){
					createPropertiesFile();
				}
				ExecutionContext.setMaxQPropFile(propPath.toOSString());
				//initializing fields by reading properties file
				readPropertiesFile();
			}
		}
		fileText.setText("new_capture.xis");        
	}

	/**
	 * Uses the standard container selection dialog to choose 
	 * the new value for the container field. 
	 */
	private void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
		"Select new file container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerPath=((Path) result[0]);
				containerText.setText(containerPath.toString());
				containerPath=ResourcesPlugin.getWorkspace().getRoot().getLocation().append(containerPath);
				IPath propPath = containerPath.append("maxq.properties");
				propertiesFile = new File(propPath.toOSString());
				//if properties file does not exist in container
				if (!propertiesFile.exists()){
					createPropertiesFile();
				}
				ExecutionContext.setMaxQPropFile(propPath.toOSString());
				//initializing fields by reading properties file
				readPropertiesFile();
			}
		}
	}

	/**
	 * Ensures that both text fields are set. 
	 */
	private void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
		.findMember(new Path(getContainerName()));
		String fileName = getFileName();
		String portNumber = getPortNumber();
		String proxyPortNumber = getProxyPortNumber();
		String proxyHost = getProxyHost();
		String randomDistDelta = getRandomDistDelta();
		String randomDistDeviation = getRandomDistDeviation();
		int port;
		//Ensures that container name is correct
		if (getContainerName().length() == 0) {
			updateStatus("File container must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("File container must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		//Ensures that file name is correct
		if (fileName.length() == 0) {
			updateStatus("File name must be specified");
			return;
		}
		if (fileName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("File name must be valid");
			return;
		}
		int dotLoc = fileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = fileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("xis") == false) {
				updateStatus("File extension must be \"xis\"");
				return;
			}
		}
		//Ensures that local port number is correct 
		if (!verifyNumber(portNumber, "Port")){
			return;
		}
		if (portNumber.length() == 0) {
			updateStatus("Port number must be specified");
			return;
		}
		try {
			port = Integer.parseInt(portNumber);
		}
		catch(NumberFormatException e){
			updateStatus("Port number must be an integer");
			return;
		}
		if (port<0 || port>65536){
			updateStatus("Port number must be valid");
			return;
		}
		if (useRemoteProxy.getSelection()){
			//Ensures that remote port and host are correct
			if (proxyHost.length() == 0){
				updateStatus("Proxy Host must be specified");
				return;
			}
			if (!verifyNumber(proxyPortNumber, "Proxy Host")){
				return;
			}
			if (port<0 || port>65536){
				updateStatus("Proxy Port number must be valid");
				return;
			}
		}
		if (timerList.getText().equals("Random")){
			//Ensures that delta or unit number is correct
			if (!verifyNumber(randomDistDelta, "Delta or Unit")){
				return;
			}
			//Ensures that deviation number is correct
			if (randomDistList.getText().equals("gaussian")){
				if (!verifyNumber(randomDistDeviation, "Deviation")){
					return;
				}
			}
		}
		updateStatus(null);
	}

	/** 
	 * Verifies if string is formated like an integer
	 * @param number	string value to verify
	 * @param label		label of this value
	 * @return boolean	number is a positive integer 
	 */ 
	private boolean verifyNumber(String number, String label){
		if (number.length() == 0) {
			updateStatus(label+" number must be specified");
			return false;
		}
		try {
			Integer.parseInt(number);
		}
		catch(NumberFormatException e){
			updateStatus(label+" number must be an integer");
			return false;
		}
		return true;
	}

	/** 
	 * Enable or disable proxy fields when proxy checkbox is 
	 * selected or not
	 */ 
	private void selectRemoteProxy(){
		if (useRemoteProxy.getSelection()){
			proxyHostText.setEnabled(true);
			proxyPortText.setEnabled(true);
		}else{
			proxyHostText.setEnabled(false);
			proxyPortText.setEnabled(false);	
		}
	}

	/**
	 * Updates "Random" fields when a random distribution is selected
	 */
	private void timerChanged(){
		if (timerList.getText().equals("Random")){
			randomDistList.setVisible(true);
			randomDistLabel.setVisible(true);
			randomDistDeltaLabel.setVisible(true);
			randomDistDeltaText.setVisible(true);
			if ((randomDistList.getText().equals("uniform"))
					||(randomDistList.getText().equals("gaussian"))
					||(randomDistList.getText().equals("negexpo")))
			{
				randomDistDeltaLabel.setText("&Delta:");
				if (randomDistList.getText().equals("gaussian")){
					randomDistDeviationLabel.setVisible(true);
					randomDistDeviationText.setVisible(true);
				}else{
					randomDistDeviationLabel.setVisible(false);
					randomDistDeviationText.setVisible(false);
				}
			}else{
				randomDistDeltaLabel.setText("U&nit:");
				randomDistDeviationLabel.setVisible(false);
				randomDistDeviationText.setVisible(false);
			}
			if (randomDistList.getSelectionIndex()==-1){
				randomDistList.select(DIST_UNIFORM);
			}
		}else{
			randomDistList.setVisible(false);
			randomDistLabel.setVisible(false);
			randomDistDeltaLabel.setVisible(false);
			randomDistDeltaText.setVisible(false);
			randomDistDeviationLabel.setVisible(false);
			randomDistDeviationText.setVisible(false);
		}
	}

	/** 
	 * Updates wizard status message
	 */ 
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	/** 
	 * Disable all fields
	 */ 
	public void disableFields(){
		containerText.setEnabled(false);
		browseButton.setEnabled(false);
		fileText.setEnabled(false);
		portText.setEnabled(false);
		useRemoteProxy.setEnabled(false);
		proxyHostText.setEnabled(false);
		proxyPortText.setEnabled(false);
		timerList.setEnabled(false);
		randomDistList.setEnabled(false);
		randomDistDeltaText.setEnabled(false);
		randomDistDeviationText.setEnabled(false);
	}

	/** 
	 * Creates properties file if not exists in project
	 * The template used is in console plugin folder
	 */ 
	private void createPropertiesFile(){
		FileInputStream sourceFile;
		FileOutputStream destinationFile;
		Path locationPath = new Path(ExecutionContext.getBaseDir() + File.separator + ExecutionContext.DEFAULT_MAXQ_PROPERTY_FILE);
		try {
			propertiesFile.createNewFile();
			File templateFile= new File(locationPath.toOSString());
			sourceFile = new FileInputStream(templateFile);
			destinationFile = new FileOutputStream(propertiesFile);
			byte buffer[]=new byte[512*1024];
			int nbLecture;
			while((nbLecture = sourceFile.read(buffer)) != -1 ) {
				destinationFile.write(buffer, 0, nbLecture);
			} 	
		} catch (IOException e) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
			"Cannot create file maxq.properties");
		}
	}

	/** 
	 * Initialize all fields by reading maxq properties file
	 */ 
	private void readPropertiesFile(){
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(propertiesFile));
			String localProxyPort = properties.getProperty("local.proxy.port");
			if (localProxyPort==null){
				localProxyPort="";
			}
			if (localProxyPort.length()==0){
				localProxyPort=String.valueOf(DEFAULT_PORT);
			}
			portText.setText(localProxyPort);

			String remoteProxyHost = properties.getProperty("remote.proxy.host");
			if (remoteProxyHost==null){
				remoteProxyHost="";
			}
			String remoteProxyPort = properties.getProperty("remote.proxy.port");
			if (remoteProxyPort==null){
				remoteProxyPort="";
			}
			if (remoteProxyHost.length()!=0 && remoteProxyPort.length()!=0){
				useRemoteProxy.setSelection(true);
				proxyHostText.setEnabled(true);
				proxyHostText.setText(remoteProxyHost);
				proxyPortText.setEnabled(true);
				proxyPortText.setText(remoteProxyPort);
			}

			String timer = properties.getProperty("generator.isac.timer");
			if (timer==null){
				timer="";
			}
			if (timer.equals("null")){
				timerList.select(TIMER_NULL);
			}else if (timer.equals("Random")){
				timerList.select(TIMER_RANDOM);
				String randomDist = properties.getProperty("generator.isac.timer.random.dist");
				if (randomDist==null){
					randomDist="";
				}
				randomDistLabel.setVisible(true);
				randomDistList.setVisible(true);
				randomDistDeltaLabel.setVisible(true);
				randomDistDeltaText.setVisible(true);
				if (randomDist.equals("gaussian")){
					randomDistList.select(DIST_GAUSSIAN);
					randomDistDeviationLabel.setVisible(true);
					randomDistDeviationText.setVisible(true);
					String randomDelta = properties.getProperty("generator.isac.timer.random.delta");
					if (randomDelta==null){
						randomDelta="";
					}
					String randomDeviation = properties.getProperty("generator.isac.timer.random.deviation");
					if (randomDeviation==null){
						randomDeviation="";
					}
					randomDistDeltaText.setText(randomDelta);
					randomDistDeviationText.setText(randomDeviation);
				}else if (randomDist.equals("poisson")){
					randomDistList.select(DIST_POISSON);
					randomDistDeltaLabel.setText("&Unit:");
					String randomUnit = properties.getProperty("generator.isac.timer.random.unit");
					if (randomUnit==null){
						randomUnit="";
					}
					randomDistDeltaText.setText(randomUnit);
				}else if (randomDist.equals("negexpo")){
					randomDistList.select(DIST_NEGEXPO);
					String randomDelta = properties.getProperty("generator.isac.timer.random.delta");
					if (randomDelta==null){
						randomDelta="";
					}
					randomDistDeltaText.setText(randomDelta);
				}else{
					randomDistList.select(DIST_UNIFORM);
					String randomDelta = properties.getProperty("generator.isac.timer.random.delta");
					if (randomDelta==null){
						randomDelta="";
					}
					randomDistDeltaText.setText(randomDelta);
				}
			}else{
				timerList.select(TIMER_CONSTANT);
			}

		} catch (FileNotFoundException e) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
			"maxq.properties does not exist");
		} catch (IOException e) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
			"Cannot read maxq.properties file");
		}
	}

	/**
	 * Gets container name filled in text field
	 * @return String the container name 
	 */
	public String getContainerName() {
		return containerText.getText();
	}

	/**
	 * Gets result file name filled in text field
	 * @return String the file name 
	 */
	public String getFileName() {
		return fileText.getText();
	}

	/**
	 * Gets local port number filled in text field
	 * @return String the port number 
	 */
	public String getPortNumber() {
		return portText.getText();
	}

	/**
	 * Gets remote port number filled in text field
	 * @return String the port number 
	 */
	public String getProxyPortNumber() {
		return proxyPortText.getText();
	}

	/**
	 * Gets remote host filled in text field
	 * @return String the port number 
	 */
	public String getProxyHost() {
		return proxyHostText.getText();
	}

	/**
	 * Gets timer selected in combo
	 * @return String the timer
	 */
	public String getTimer() {
		return timerList.getText();
	}

	/**
	 * Gets maxQ properties file
	 * @return File the properties File 
	 */
	public File getPropertiesFile() {
		return propertiesFile;
	}

	/**
	 * Gets container path
	 * @return String the container path
	 */
	public String getContainerPath() {
		return containerPath.toOSString();
	}

	/**
	 * return true if remote proxy is used
	 * @return Boolean if remote proxy is used 
	 */
	public Boolean getUseRemoteProxy() {
		return useRemoteProxy.getSelection();
	}

	/**
	 * Gets random distribution filled in text field
	 * @return String the random distribution
	 */
	public String getRandomDist() {
		return randomDistList.getText();
	}

	/**
	 * Gets random delta or unit number filled in text field
	 * @return String the random delta or unit number
	 */
	public String getRandomDistDelta() {
		return randomDistDeltaText.getText();
	}

	/**
	 * Gets random deviation number filled in text field
	 * @return String the deviation number
	 */
	public String getRandomDistDeviation() {
		return randomDistDeviationText.getText();
	}
}