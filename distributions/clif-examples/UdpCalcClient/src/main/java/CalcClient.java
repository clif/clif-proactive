/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;


/**
 * Simple client for submitting requests to the
 * calculator UDP service. Useful just for troubleshooting.
 * 
 * @author Bruno Dillenseger
 */
abstract public class CalcClient
{
	private static final String CHARSET = "US-ASCII";
	private static final int PACKET_SIZE = 1024;

	/**
	 * Submit calculations to a calculator UDP server
	 * @param args
	 * <dl>
	 * <dt>args[0]</dt><dd>calculator server's IP address</dd>
	 * <dt>args[1]</dt><dd>calculator server's UDP port number</dd>
	 * <dt>args[2...]</dt><dd>operations to submit, such as 6*9 59-8</dd>
	 * </dl>
	 */
	static public void main(String args[])
	{
		try
		{
			DatagramSocket socket = new DatagramSocket();
			SocketAddress serverAddr = new InetSocketAddress(
				InetAddress.getByName(args[0]),
				Integer.parseInt(args[1]));
			socket.connect(serverAddr);
			for (int i=2 ; i<args.length ; ++i)
			{
				byte[] request = args[i].getBytes(CHARSET);
				DatagramPacket packet = new DatagramPacket(request, request.length);
				socket.send(packet);
				byte[] reply = new byte[PACKET_SIZE];
				packet = new DatagramPacket(reply, reply.length);
				socket.receive(packet);
				String result = new String(
					packet.getData(),
					packet.getOffset(),
					packet.getLength(),
					CHARSET);
				System.out.println(args[i] + result);
			}
			socket.close();
			System.exit(0);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-1);
		}
	}
}
