/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.calcinjector;

import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;

/**
 * Test for class SessionObject. Requires the calculator UDP server
 * to be running and listening to UDP port number 5432 on localhost
 * network interface.
 * 
 * @author Bruno Dillenseger
 */
public abstract class Test1
{
	/**
	 * Runs the test, which consist in submitting one
	 * operation to the 
	 * @param args arguments are ignored
	 */
	public static void main(String[] args)
	{
		Map<String,String> params = new HashMap<String,String>();

		// instantiation of one specimen object
		params.put(SessionObject.PLUGIN_RCVSIZE, "1024");
		params.put(SessionObject.PLUGIN_SNDSIZE, "1024");
		SessionObject specimen = new SessionObject(params);

		// get one session object by cloning the specimen object
		SessionObject session = (SessionObject)specimen.createNewSessionObject();

		// time-outs settings on the session object
		params.clear();
		params.put(SessionObject.CONTROL_SETTIMEOUT_TIMEOUT, "5000");
		session.doControl(SessionObject.CONTROL_SETTIMEOUT, params);

		// invoke the server with the session object and print the request report
		params.clear();
		params.put(SessionObject.SAMPLE_COMPUTE_HOST, "localhost");
		params.put(SessionObject.SAMPLE_COMPUTE_PORT, "5432");
		params.put(SessionObject.SAMPLE_COMPUTE_OPERATION, "5*3");
		System.out.println(session.doSample(SessionObject.SAMPLE_COMPUTE, params, new ActionEvent()).toString());
	}
}
