/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.calcinjector;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;

/**
 * Implementation of a session object for plugin ~CalcInjector~
 * This sample plug-in shows an alternative to using the UdpInjector
 * plug-in for testing the sample system under test provided by the
 * calculator UDP server. Instead of using generic UDP primitives,
 * the tester may write test scenarios using these higher-level
 * primitives based on the calculator's API.
 * 
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, SampleAction, DataProvider, ControlAction, TestAction
{
	static final String PLUGIN_SNDSIZE = "sndsize";
	static final String PLUGIN_RCVSIZE = "rcvsize";
	static final int TEST_SERVERFOUND = 2;
	static final int TEST_SERVERNOTFOUND = 3;
	static final int TEST_TIMEDOUT = 4;
	static final int TEST_NOTTIMEDOUT = 5;
	static final int TEST_GOTRESULT = 6;
	static final int TEST_GOTNORESULT = 7;
	// constants managed by the wizard
	static final int CONTROL_SETTIMEOUT = 1;
	static final String CONTROL_SETTIMEOUT_TIMEOUT = "timeout";
	static final int SAMPLE_COMPUTE = 0;
	static final String SAMPLE_COMPUTE_OPERATION = "operation";
	static final String SAMPLE_COMPUTE_PORT = "port";
	static final String SAMPLE_COMPUTE_HOST = "host";

	// required character encoding for the Calc server
	static final String CHARSET = "ASCII";

	// UDP socket
	private DatagramSocket socket;

 	// buffer size for sending and receiving UDP packets;
	private int rcvBufSize;
	private int sndBufSize;

	// computation result received from the Calc server
	private String result = null;
	private boolean serverFound = false;
	private boolean timedOut = false;

	/**
	 * Constructor for specimen objects (one per import, per scenario).
	 * Specimen objects are not used as is: they are just intended to
	 * get import parameters, and then to be cloned to run virtual users.
	 * Since there is no import parameter for this plug-in, there is
	 * nothing to do here.
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		rcvBufSize = Integer.parseInt(params.get(PLUGIN_RCVSIZE));
		sndBufSize = Integer.parseInt(params.get(PLUGIN_SNDSIZE));
	}

	/**
	 * Copy constructor (clones this specimen object to get a session
	 * object for one virtual user).
	 * Nothing to do but creating a UDP socket.
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		rcvBufSize = so.rcvBufSize;
		sndBufSize = so.sndBufSize;
		try
		{
			socket = new DatagramSocket();
			socket.setReceiveBufferSize(rcvBufSize);
			socket.setSendBufferSize(sndBufSize);
		}
		catch (SocketException ex)
		{
			throw new IsacRuntimeException("Can't create CalcInjector session object.", ex);
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * Clones this specimen session object for a new virtual user,
	 * by calling the copy constructor.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * Closes the UDP socket.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		socket.close();
		socket = null;
	}

	/**
	 * For session object recycling: closes current UDP socket and creates a
	 * new one. Discards a possible previous computation result.
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		socket.close();
		result = null;
		serverFound = false;
		timedOut = false;
		try
		{
			socket = new DatagramSocket();
			socket.setReceiveBufferSize(rcvBufSize);
			socket.setSendBufferSize(sndBufSize);
		}
		catch (SocketException ex)
		{
			throw new IsacRuntimeException("Can't reset CalcInjector session object.", ex);
		}
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_COMPUTE:
				// get the operation string
				String operation = params.get(SAMPLE_COMPUTE_OPERATION);
				// discard possible previous result
				result = null;
				serverFound = false;
				timedOut = true;
				// record request type
				report.type = "calc compute";
				// arbitrary comment (record submitted operation)
				report.comment = operation;
				// record request start date
				report.setDate(System.currentTimeMillis());
				try
				{
					// send the operation request to the server
					SocketAddress server = new InetSocketAddress(
						InetAddress.getByName(params.get(SAMPLE_COMPUTE_HOST)),
						Integer.parseInt(params.get(SAMPLE_COMPUTE_PORT)));
					byte[] data = operation.getBytes(CHARSET);
					DatagramPacket request = new DatagramPacket(data, data.length, server);
					socket.send(request);
					serverFound = true;
					// the server should send a response
					DatagramPacket reply = new DatagramPacket(new byte[rcvBufSize], rcvBufSize);
					socket.receive(reply);
					timedOut = false;
					// record request response time
					report.duration = (int) (System.currentTimeMillis() - report.getDate());
					// get the computation result from the UDP packet
					result = new String(reply.getData(), reply.getOffset(), reply.getLength(), CHARSET);
					if (result.startsWith("="))
					{
						// record the computation result, discarding the heading '=' character
						report.result = result.substring(1);
						// record request success
						report.successful = true;
					}
					else
					{
						// record the error message
						report.result = result;
						// record request failure
						report.successful = false;
					}
				}
				catch (Exception ex)
				{
					// record request response time
					report.duration = (int) (System.currentTimeMillis() - report.getDate());
					// record request failure
					report.successful = false;
					// record exception message as request result
					report.result = ex.getMessage();
				}
				// return the sample report
				return report;
			default:
				// should never occur (consistency problem between this plug-in and the scenario)
				throw new Error("Unable to find this sample in ~CalcInjector~ ISAC plugin: " + number);
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @return the latest computation result, if any, or an exception otherwise.
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 * @throws IsacRuntimeException if the latest computation failed or if no
	 * computation has been requested yet.
	 */
	public String doGet(String var)
	throws IsacRuntimeException
	{
		if (result == null)
		{
			// there is no result available
			throw new IsacRuntimeException(
				"No result available from CalcInjector. A successful compute sample is required.");
		}
		else
		{
			return result;
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
			case CONTROL_SETTIMEOUT:
				try
				{
					// sets the UDP socket time-out for packet reception
					socket.setSoTimeout(Integer.parseInt(params.get(CONTROL_SETTIMEOUT_TIMEOUT)));
				}
				catch (Exception ex)
				{
					// reports any failure for this control
					throw new IsacRuntimeException(
						"Invalid timeout for CalcInjector: " + params.get(CONTROL_SETTIMEOUT_TIMEOUT),
						ex);
				}
				break;
			default:
				// should never occur (consistency problem between this plug-in and the scenario)
				throw new Error(
					"Unable to find this control in ~CalcInjector~ ISAC plugin: "
					+ number);
		}
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params)
	{
		switch (number)
		{
			case TEST_GOTNORESULT:
				return result == null;
			case TEST_GOTRESULT:
				return result != null;
			case TEST_NOTTIMEDOUT:
				return ! timedOut;
			case TEST_TIMEDOUT:
				return timedOut;
			case TEST_SERVERNOTFOUND:
				return ! serverFound;
			case TEST_SERVERFOUND:
				return serverFound;
			default:
				// should never occur (consistency problem between this plug-in and the scenario)
				throw new Error(
					"Unable to find this test in ~CalcInjector~ ISAC plugin: "
					+ number);
		}
	}
}
