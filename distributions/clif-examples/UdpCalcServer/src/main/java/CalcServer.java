/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Random;

/**
 * Sample application for performance testing: calculator server
 * over UDP.
 * Listens on a UDP port and respond to requests containing a single
 * operations (+ * - /) with 2 integer operands, such as "12*4".
 * The answer is either an error message or the result of the operation,
 * prefixed by the = sign, such as "=48". The result is most of the time
 * correct, but it is periodically wrong.
 * The character encoding for both the request and the response is US-ASCII. 
 * 
 * @author Bruno Dillenseger
 */
public class CalcServer implements Runnable
{
	static final String CHARSET = "US-ASCII";
	// buffer size for both request input and response output
	static final int PACKET_SIZE = 1024;
	// computation error period (1 error each 10 requests)
	static final int ERROR_CYCLE = 10;
	// UDP socket for receiving request packets
	DatagramSocket sock;
	// buffer for putting requests' content
	byte[] buffer = new byte[PACKET_SIZE];

	/**
	 * Starts a calculator UDP server listening on all available
	 * network interfaces (0.0.0.0).
	 * @param args args[0] optional calculator server's UDP port number
	 */
	static public void main(String args[])
	{
		CalcServer server = null;
		try
		{
			if (args.length == 0)
			{
				server = new CalcServer();
			}
			else if (args.length == 1)
			{
				server = new CalcServer(Integer.parseInt(args[0]));
			}
		}
		catch (SocketException ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-1);
		}
		System.out.println("Calculator listening on UDP socket at address " + server.getAddress());
		new Thread(server).start();
	}

	/**
	 * Creates the calculator's UDP socket, bound to any
	 * available local port
	 * @throws SocketException
	 */
	CalcServer()
	throws SocketException
	{
		sock = new DatagramSocket();
	}

	/**
	 * Creates the calculator's UDP socket, bound to the given port
	 * @param port the UDP port to bind the server socket to
	 * @throws SocketException
	 */
	CalcServer(int port)
	throws SocketException
	{
		sock = new DatagramSocket(port);
	}

	/**
	 * Get the IP address and local port of the calculator's UDP socket
	 * @return the Inet socket address of the calculator's UDP socket
	 */
	public InetSocketAddress getAddress()
	{
		return new InetSocketAddress(sock.getLocalAddress(), sock.getLocalPort());
	}

	////////////////////////
	// Runnable interface //
	////////////////////////

	/**
	 * The calculator's activity consists in forever looping on waiting for
	 * requests and responding.
	 *  
	 */
	public void run()
	{
		try
		{
			sock.setReceiveBufferSize(buffer.length);
		}
		catch (SocketException ex)
		{
			ex.printStackTrace(System.err);
			System.exit(-2);
		}
		int calls = 0;
		while (true)
		{
			try
			{
				// get a request
				DatagramPacket request = new DatagramPacket(buffer, buffer.length);
				sock.receive(request);
				// analyze the request
				String operation = new String(request.getData(), request.getOffset(), request.getLength(), CHARSET);
				System.out.println("request from " + request.getSocketAddress() + ": " + operation);
				int plus = operation.lastIndexOf('+');
				int minus = operation.lastIndexOf('-');
				int multiply = operation.lastIndexOf('*');
				int divide = operation.lastIndexOf('/');
				int position = (plus != -1 ? plus : (minus != -1 ? minus : (multiply != -1 ? multiply : divide)));
				String replyString;
				if (position == -1)
				{
					replyString = "invalid operation";
				}
				else
				{
					// the request holds a valid operation => compute the result
					try
					{
						BigInteger result;
						BigInteger first = new BigInteger(operation.substring(0, position).trim());
						BigInteger second = new BigInteger(operation.substring(position + 1).trim());
						if (plus != -1)
						{
							result = first.add(second);
						}
						else if (minus != -1)
						{
							result = first.subtract(second);
						}
						else if (multiply != -1)
						{
							result = first.multiply(second);
						}
						else
						{
							result = first.divide(second);
						}
						// periodically forces an erroneous computation
						if (++calls == ERROR_CYCLE)
						{
							result = result.add(BigInteger.valueOf(ERROR_CYCLE));
							calls = 0;
						}
						replyString = "=" + result;
					}
					catch (NumberFormatException ex)
					{
						replyString = "invalid number";
					}
					catch (ArithmeticException ex)
					{
						replyString = "division by zero";
					}
				}
				byte[] replyData = replyString.getBytes(CHARSET);
				// perform some extra, unnecessary computation to increase and randomize response time
				BigInteger.probablePrime(64, new Random(System.currentTimeMillis()));
				// send the response
				DatagramPacket reply = new DatagramPacket(
					replyData,
					replyData.length,
					request.getSocketAddress());
				sock.send(reply);
				System.out.println(new String(reply.getData(), CHARSET));
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}
}
