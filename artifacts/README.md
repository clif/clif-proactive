# CLIF artifacts #

this directory contains all CLIF's artifacts which can be deployed in maven repository.

## profiles ##

2 profiles are defined for those artefacts:

* legacy : CLIF's legacy profile (from trunk)
* proactive : CLIF's ProActive profile (from ProActive branch)

artifacts built with "proactive" profile contain a "pa" classifier.
legacy artifacts contain no classifier.

## build ## 

to build and install all the artifacts, use the following command in "artifacts" directory:

```
#!script

mvn clean install -P legacy
mvn clean install -P proactive
```

