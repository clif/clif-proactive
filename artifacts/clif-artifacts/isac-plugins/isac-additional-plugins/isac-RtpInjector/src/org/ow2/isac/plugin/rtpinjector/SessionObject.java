/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even+
 *  the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.rtpinjector;

// CLIF imports
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.ow2.clif.probe.rtp.AppPacket;
import org.ow2.clif.probe.rtp.ByePacket;
import org.ow2.clif.probe.rtp.ParticipantStats;
import org.ow2.clif.probe.rtp.RTCPPacket;
import org.ow2.clif.probe.rtp.RTPInformation;
import org.ow2.clif.probe.rtp.RTPListener;
import org.ow2.clif.probe.rtp.RTPPacket;
import org.ow2.clif.probe.rtp.RTPSession;
import org.ow2.clif.probe.rtp.RTPStats;
import org.ow2.clif.probe.rtp.ReportPacket;
import org.ow2.clif.probe.rtp.SDESPacket;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;

/**
 * Implementation of a session object for plugin ~RtpInjector~
 * 
 * @author Rémi Druilhe
 */


public class SessionObject implements SessionObjectAction, DataProvider, TestAction, ControlAction, SampleAction
{
	static final int TEST_ISRTCPBYE = 22;
	static final String TEST_ISRTCPBYE_SESSION_ID = "Session ID";
	static final String TEST_ISRTCPBYE_NOTCHOICE = "notChoice";
	static final int CONTROL_SEND = 8;
	static final String CONTROL_SEND_SESSION_ID = "Session ID";
	static final String CONTROL_SEND_PAYLOADTYPE = "payloadType";
	static final String CONTROL_SEND_MARKER = "marker";
	static final String CONTROL_SEND_CSRCCOUNT = "csrcCount";
	static final String CONTROL_SEND_EXTENSION = "extension";
	static final String CONTROL_SEND_PADDING = "padding";
	static final String CONTROL_SEND_VERSION = "version";
	static final String CONTROL_SEND_TIME = "time";
	static final String CONTROL_SEND_CONTENT = "content";
	static final int CONTROL_FORWARD = 9;
	static final String CONTROL_FORWARD_SESSION_ID = "Session ID";
	static final String CONTROL_FORWARD_PACKETSIZE = "packetSize";
	static final String CONTROL_FORWARD_DURATION = "duration";
	static final String CONTROL_FORWARD_LISTENINGPORT = "listeningPort";
	static final int CONTROL_INITRTP = 11;
	static final String CONTROL_INITRTP_SESSION_ID = "Session ID";
	static final String CONTROL_INITRTP_DURATION = "duration";
	static final String CONTROL_INITRTP_TIMEOUT = "timeOut";
	static final String CONTROL_INITRTP_REMOTEPORT = "remotePort";
	static final String CONTROL_INITRTP_REMOTEADDRESS = "remoteAddress";
	static final String CONTROL_INITRTP_LOCALPORT = "localPort";
	static final String CONTROL_INITRTP_LOCALIPADDRESS = "localIpAddress";
	static final int CONTROL_DTMF = 12;
	static final String CONTROL_DTMF_SESSION_ID = "Session ID";
	static final String CONTROL_DTMF_PAYLOADTYPE = "payloadType";
	static final String CONTROL_DTMF_MARKER = "marker";
	static final String CONTROL_DTMF_CSRCCOUNT = "csrcCount";
	static final String CONTROL_DTMF_EXTENSION = "extension";
	static final String CONTROL_DTMF_PADDING = "padding";
	static final String CONTROL_DTMF_VERSION = "version";
	static final String CONTROL_DTMF_DTMFTYPE = "dtmfType";
	static final String CONTROL_DTMF_VOLUME = "volume";
	static final String CONTROL_DTMF_TIME = "time";
	static final String CONTROL_DTMF_DIGIT = "digit";
	static final int CONTROL_ENABLERTCP = 13;
	static final String CONTROL_ENABLERTCP_SESSION_ID = "Session ID";
	static final String CONTROL_ENABLERTCP_TIMEINTERVAL = "timeInterval";
	static final int CONTROL_ADDREPORT = 14;
	static final String CONTROL_ADDREPORT_SESSION_ID = "Session ID";
	static final int CONTROL_ADDSDES = 16;
	static final String CONTROL_ADDSDES_SESSION_ID = "Session ID";
	static final String CONTROL_ADDSDES_ITEMS = "items";
	static final int CONTROL_ADDBYE = 17;
	static final String CONTROL_ADDBYE_SESSION_ID = "Session ID";
	static final String CONTROL_ADDBYE_REASON = "reason";
	static final int CONTROL_ADDAPP = 18;
	static final String CONTROL_ADDAPP_SESSION_ID = "Session ID";
	static final String CONTROL_ADDAPP_SUBTYPE = "subtype";
	static final String CONTROL_ADDAPP_DATA = "data";
	static final String CONTROL_ADDAPP_NAME = "name";
	static final int CONTROL_RESETRTCPPACKET = 19;
	static final String CONTROL_RESETRTCPPACKET_SESSION_ID = "Session ID";
	static final int CONTROL_SENDBYE = 20;
	static final String CONTROL_SENDBYE_SESSION_ID = "Session ID";
	static final String CONTROL_SENDBYE_REASON = "reason";
	static final int CONTROL_SENDRECEIVERREPORT = 21;
	
	/*
	// General
	private InetAddress localIp;
	private Integer localPort;
	private InetAddress remoteIp;
	private Integer remotePort;
	private RTPListener rtpListener;
	
	//pierre
	private Integer sipSession;
	
	private RTPSession rtpSession;
	
	// RTP
	private Integer timeout;
	private Integer durationPacket = 20;
	
	// RTCP
	private RTPStats rtpStats;
	private boolean enableRtcp = false;
	private boolean enableBye = false;
	private String reason;
	private Long timeInterval = null;
	private LinkedList<RTCPPacket> rtcpTemplate = new LinkedList<RTCPPacket>();
	private Long lastRtcpTime = System.currentTimeMillis();*/
	
	static final String CONTROL_SENDRECEIVERREPORT_SESSION_ID = "Session ID";	
	
	private LinkedList<RTPInstance> rtpSessions = new LinkedList<RTPInstance>();
	
	public RTPListener rtpListener;
	public RTPStats rtpStats;
	
	private RTPInstance findMyInstance(String myInstanceId)
	{
		RTPInstance myInst = null;
		
		for (RTPInstance currentInst: rtpSessions)
		{
			if(currentInst.sipSession.compareTo(myInstanceId) == 0)
			{
				myInst = currentInst;
				
				break;
			}
		}
		
		return myInst;	
	}
	
	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable<String, String> params) {}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) {
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() 
	{
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() 
	{
		boolean rtpStatsInUse = false;
		
		for (RTPInstance currentInst: rtpSessions)
		{		
			currentInst.rtpSession.close();
			
			if (currentInst.enableRtcp)
				rtpStatsInUse = true;
		}
		
		if (rtpListener != null)
		{
			if(rtpStatsInUse)
				rtpStats.close();
			else
				//TODO pierre is there a bug? 
				rtpListener.close();
		}
		
		rtpSessions.clear();
		
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var) 
	{
		throw new IsacRuntimeException("Unknown parameter value in ~RtpInjector~ ISAC plugin: " + var);
	}

	
	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map params) 
	{
		boolean result = false;
		
		switch (number)
		{
			case TEST_ISRTCPBYE:
				RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.TEST_ISRTCPBYE_SESSION_ID));
				
				if(myInstance.enableRtcp)
				{
					if(((String) params.get(SessionObject.TEST_ISRTCPBYE_NOTCHOICE)).compareTo("") == 0)		
						result = isRtcpBye(myInstance);
					else
						result = isNotRtcpBye(myInstance);
				}
				break;
			default:
				throw new Error("Unable to find this test in ~RtpInjector~ ISAC plugin: " + number);
		}
		
		return result;
	}

	//TODO manage null case of myInstance
	public boolean isRtcpBye(RTPInstance myInstance)
	{
		
		ArrayList<ParticipantStats> byeList = rtpStats.getByeList();
		boolean result = false;
		
		for(int i=0; i<byeList.size(); i++)
		{	
			if(myInstance.remotePort.compareTo(byeList.get(i).getPort()) == 0)
				result = true;
		}
		
		return result;
	}

	//TODO manage null case of myInstance
	public boolean isNotRtcpBye(RTPInstance myInstance)
	{
		ArrayList<ParticipantStats> byeList = rtpStats.getByeList();
		boolean result = true;
		
		for(int i=0; i<byeList.size(); i++)
		{	
			if(myInstance.remotePort.compareTo(byeList.get(i).getPort()) == 0)
				result = false;
		}
		
		return result;
	}

	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map params)
	{
		switch (number) 
		{
			case CONTROL_SENDRECEIVERREPORT:
				sendReceiverReport(params);
				break;
			case CONTROL_SENDBYE:
				sendBye(params);
				break;
			case CONTROL_RESETRTCPPACKET:
				resetRtcp(params);
				break;
			case CONTROL_ADDAPP:
				addApp(params);
				break;
			case CONTROL_ADDBYE:
				addBye(params);
				break;
			case CONTROL_ADDSDES:
				addSdes(params);
				break;
			case CONTROL_ADDREPORT:
				addReport(params);
				break;
			case CONTROL_ENABLERTCP:
				enableRtcp(params);
				break;
			case CONTROL_DTMF:
				dtmf(params);
				break;
			case CONTROL_INITRTP:
				initRtp(params);
				break;
			case CONTROL_FORWARD:
				forwardStream(params);
				break;
			case CONTROL_SEND:
				sendFile(params);
				break;
			default:
				throw new Error("Unable to find this control in ~RtpInjector~ ISAC plugin: " + number);
		}
	}
	
	/**
	 * Initialize the RTP Stream.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void initRtp(Map params)
	{
		RTPInstance newInstance = new RTPInstance();
		
		if(((String) params.get(SessionObject.CONTROL_INITRTP_SESSION_ID)).compareTo("") != 0)
			newInstance.sipSession = (String) params.get(SessionObject.CONTROL_INITRTP_SESSION_ID);
		else
			newInstance.sipSession = null;	
		//TODO manage the null case
		
		try
		{
			if(((String) params.get(SessionObject.CONTROL_INITRTP_LOCALIPADDRESS)).compareTo("") != 0)
				newInstance.localIp = InetAddress.getByName((String) params.get(SessionObject.CONTROL_INITRTP_LOCALIPADDRESS));
			else
				newInstance.localIp = null;	
				
			newInstance.localPort = new Integer((String) params.get(SessionObject.CONTROL_INITRTP_LOCALPORT));
			
			newInstance.remoteIp = InetAddress.getByName((String) params.get(SessionObject.CONTROL_INITRTP_REMOTEADDRESS));
			newInstance.remotePort = new Integer((String) params.get(SessionObject.CONTROL_INITRTP_REMOTEPORT));
		}
		catch(UnknownHostException e)
		{
			throw new IsacRuntimeException("Unable to create InetAddress : " + e);
		}
		
		newInstance.timeout = new Integer((String) params.get(SessionObject.CONTROL_INITRTP_TIMEOUT));
		newInstance.durationPacket = new Integer((String) params.get(SessionObject.CONTROL_INITRTP_DURATION));

		ArrayList<Integer> ports = new ArrayList<Integer>();
		ports.add(newInstance.localPort);
		
		rtpListener = RTPListener.getInstance();
		
		rtpListener.openSocket(newInstance.localIp, ports);
		rtpListener.startListener();

		Double duration = new Double(newInstance.durationPacket);
		
		newInstance.rtpSession = new RTPSession(0, 0L, 0L, duration, newInstance.localPort);
		
		rtpSessions.add(newInstance);
	}
	
	/**
	 * Send a file.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void sendFile(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_SEND_SESSION_ID));
		
		// RTP packet
		Integer version = 2;
		Integer padding = 0;
		Integer extension = 0;
		Integer csrcCount = 1;
		Integer marker = 0;
		Integer payloadType = 0;
		
		if(((String) params.get(SessionObject.CONTROL_SEND_VERSION)).compareTo("") != 0)
			version = new Integer((String) params.get(SessionObject.CONTROL_SEND_VERSION));
		if(((String) params.get(SessionObject.CONTROL_SEND_PADDING)).compareTo("") != 0)
			padding = new Integer((String) params.get(SessionObject.CONTROL_SEND_PADDING));
		if(((String) params.get(SessionObject.CONTROL_SEND_EXTENSION)).compareTo("") != 0)
			extension = new Integer((String) params.get(SessionObject.CONTROL_SEND_EXTENSION));
		if(((String) params.get(SessionObject.CONTROL_SEND_CSRCCOUNT)).compareTo("") != 0)
			csrcCount = new Integer((String) params.get(SessionObject.CONTROL_SEND_CSRCCOUNT));
		if(((String) params.get(SessionObject.CONTROL_SEND_MARKER)).compareTo("") != 0)
			marker = new Integer((String) params.get(SessionObject.CONTROL_SEND_MARKER));
		if(((String) params.get(SessionObject.CONTROL_SEND_PAYLOADTYPE)).compareTo("") != 0)
			payloadType = new Integer((String) params.get(SessionObject.CONTROL_SEND_PAYLOADTYPE));
			
		// RTP Stream
		AudioInputStream audioInputStream = null;
		URL soundUrl;
		try {
			soundUrl = ClifClassLoader.getClassLoader().findResource((String) params.get(SessionObject.CONTROL_SEND_CONTENT));
		} catch (ClifException e1) {
			throw new IsacRuntimeException("ClifException : " + e1, e1);
		}
		
		Long startTime = System.currentTimeMillis();
		Long stopTime = new Long((String) params.get(SessionObject.CONTROL_SEND_TIME));
		int availableSize = 0;
		
		// RTCP Stats
		Long packetSum = 0L;
		Long octetSum = 0L;

		try
		{
			audioInputStream = AudioSystem.getAudioInputStream(soundUrl);
			availableSize = audioInputStream.available();
		}
		catch(IOException e) 
		{
			throw new IsacRuntimeException("IOException : " + e, e);
		}
		catch(UnsupportedAudioFileException e) 
		{
			throw new IsacRuntimeException("UnsupportedAudioFileException : " + e, e);
		}
		
		AudioFormat audioFormat = audioInputStream.getFormat();
		
		//System.out.println(audioInputStream.getFormat().toString());
		
		int size = new Double(myInstance.durationPacket * audioFormat.getSampleRate() * 0.001).intValue();

		byte[] buffer = new byte[size];
		
		myInstance.rtpSession.setSampling(audioFormat.getSampleRate());
		
		int packetNumber = availableSize / size;

		while(System.currentTimeMillis() - startTime < stopTime)
		{
			try
			{
				audioInputStream = AudioSystem.getAudioInputStream(soundUrl);
			}
			catch(IOException e) 
			{
				throw new IsacRuntimeException("IOException : " + e);
			}
			catch(UnsupportedAudioFileException e) 
			{
				throw new IsacRuntimeException("UnsupportedAudioFileException : " + e);
			}
			
			for(int i=0; i<=packetNumber; i++)
			{	
				if(System.currentTimeMillis() - startTime > stopTime)
					break;
				
				try
				{
					audioInputStream.read(buffer);
				}
				catch(IOException e)
				{
					throw new IsacRuntimeException("Unable to read file : " + e);
				}
				
				RTPPacket rtpPacket = new RTPPacket(payloadType, myInstance.rtpSession.getSequenceNumber(), 
						myInstance.rtpSession.getTimestamp(), myInstance.rtpSession.getSsrc(), buffer);
				
				myInstance.rtpSession.setPayloadType(payloadType);
				rtpPacket.setVersion(version);
				rtpPacket.setPadding(padding);
				rtpPacket.setExtension(extension);
				rtpPacket.setCsrcCount(csrcCount);
				rtpPacket.setMarker(marker);
				
				myInstance.sendRtpPacket(rtpListener,rtpPacket.createRtpPacket());
				//sendRtpPacket(rtpPacket.createRtpPacket(), myInstance.remoteIp, myInstance.remotePort, myInstance.localPort);
				packetSum++;
				
				octetSum = octetSum + buffer.length;

				if(myInstance.enableRtcp)
				{
					if(new Long(System.currentTimeMillis() - myInstance.lastRtcpTime).compareTo(myInstance.timeInterval) >=0)
					{
						LinkedList<byte[]> dataList = new LinkedList<byte[]>();
						Integer offset = 0;
						ArrayList<ParticipantStats> partStatTable = rtpStats.getParticipantsStats();
						
						for(int j=0; j<myInstance.rtcpTemplate.size(); j++)
						{
							RTCPPacket rtcpPacket = myInstance.rtcpTemplate.get(j);
							
							if(rtcpPacket.getPacketType().compareTo(201) == 0)
								((ReportPacket) rtcpPacket).receiverToSender();
							
							if(rtcpPacket.getPacketType().compareTo(200) == 0)
							{
								((ReportPacket) rtcpPacket).setNewValues(myInstance.rtpSession.getTimestamp(), packetSum, octetSum);
								
								for(int k=0; k<partStatTable.size(); k++)
								{
									if(myInstance.rtpSession.getPort().compareTo(partStatTable.get(k).getPort()) == 0)
									{
										Long dlsr = 0L;
										
										if(!partStatTable.get(k).getFirstReport())
											dlsr = System.currentTimeMillis() - partStatTable.get(k).getDlsr();
										
										((ReportPacket) rtcpPacket).addReportBlock(partStatTable.get(k).getSsrc(), 
												partStatTable.get(k).getFractionLost(),
												partStatTable.get(k).getCumulativePacketLost(),
												partStatTable.get(k).getCycle(),
												partStatTable.get(k).getSeqNumMax(),
												partStatTable.get(k).getTimeJitter(), 
												partStatTable.get(k).getLsr(),
												dlsr);
									}
								}
							}

							dataList.add(rtcpPacket.createPacket());
						}
						
						Integer length = 0;
						
						for(int j=0; j<dataList.size(); j++)
							length = length + dataList.get(j).length;
						
						byte[] data = new byte[length];
						
						while(!dataList.isEmpty())
						{
							byte[] tempData = dataList.removeFirst();
							
							for(int j=0; j<tempData.length; j++)
								data[offset + j] = tempData[j];
							
							offset = offset + tempData.length;
						}
						
						myInstance.sendRtpPacket(rtpListener,data);
						//myInstance.sendRtpPacket(data, myInstance.remoteIp, myInstance.remotePort + 1, myInstance.localPort + 1);
						myInstance.lastRtcpTime = System.currentTimeMillis();
					}
				}
				
				myInstance.rtpSession.incrementSequenceNumber();
				myInstance.rtpSession.incrementTimestamp();
			}
			
			if(((String) params.get(SessionObject.CONTROL_SEND_TIME)).compareTo("") == 0)
				break;
		}
		
		try
		{
			audioInputStream.close();
		}
		catch(IOException e)
		{
			throw new IsacRuntimeException("Unable to close input stream : " + e);
		}
		
		if(myInstance.enableBye)
		{
			ByePacket byePacket = new ByePacket();
			
			byePacket.addSsrc(myInstance.rtpSession.getSsrc(), myInstance.reason);
			
			myInstance.sendRtcpPacket(rtpListener,byePacket.createPacket());
			
			//sendRtpPacket(byePacket.createPacket(), myInstance.remoteIp, myInstance.remotePort+1, myInstance.localPort+1);
		}
	}
	
	/**
	 * Forward a stream.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void forwardStream(Map params)
	{
		//System.out.println("forward");
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_FORWARD_SESSION_ID));
		
		Integer listeningPort = new Integer((String) params.get(SessionObject.CONTROL_FORWARD_LISTENINGPORT));
		Integer packetSize = new Integer((String) params.get(SessionObject.CONTROL_FORWARD_PACKETSIZE));
		
		Long startTime = System.currentTimeMillis();
		Long stopTime = new Long((String) params.get(SessionObject.CONTROL_FORWARD_DURATION));

		ArrayList<Integer> ports = new ArrayList<Integer>();
		ports.add(listeningPort);
		
		rtpListener = RTPListener.getInstance();
		
		rtpListener.openSocket(myInstance.localIp, ports);
		rtpListener.startListener();

		while(System.currentTimeMillis() - startTime < stopTime)
		{	
			LinkedList<RTPInformation> rtpTable = rtpListener.getPackets(myInstance.timeout);
			LinkedList<DatagramPacket> dtgTable = new LinkedList<DatagramPacket>(); 
			
			while(!rtpTable.isEmpty())
				dtgTable.addLast(new DatagramPacket(rtpTable.removeFirst().getData(), packetSize));

			while(!dtgTable.isEmpty())
				myInstance.sendRtpPacket(rtpListener,dtgTable.removeFirst().getData());
				//sendRtpPacket(dtgTable.removeFirst().getData(), myInstance.remoteIp, myInstance.remotePort, myInstance.localPort);
		}
	}
	
	/**
	 * Send a DTMF packet over RTP.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void dtmf(Map params)
	{
		//System.out.println("dtmf");
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_DTMF_SESSION_ID));
		
		// RTP Packets
		Integer version = 2;
		Integer padding = 0;
		Integer extension = 0;
		Integer csrcCount = 1;
		Integer marker = -1;
		Integer payloadType = 101;
		
		if(((String) params.get(SessionObject.CONTROL_DTMF_VERSION)).compareTo("") != 0)
			version = new Integer((String) params.get(SessionObject.CONTROL_DTMF_VERSION));
		if(((String) params.get(SessionObject.CONTROL_DTMF_PADDING)).compareTo("") != 0)
			padding = new Integer((String) params.get(SessionObject.CONTROL_DTMF_PADDING));
		if(((String) params.get(SessionObject.CONTROL_DTMF_EXTENSION)).compareTo("") != 0)
			extension = new Integer((String) params.get(SessionObject.CONTROL_DTMF_EXTENSION));
		if(((String) params.get(SessionObject.CONTROL_DTMF_CSRCCOUNT)).compareTo("") != 0)
			csrcCount = new Integer((String) params.get(SessionObject.CONTROL_DTMF_CSRCCOUNT));
		if(((String) params.get(SessionObject.CONTROL_DTMF_MARKER)).compareTo("") != 0)
			marker = new Integer((String) params.get(SessionObject.CONTROL_DTMF_MARKER));
		if(((String) params.get(SessionObject.CONTROL_DTMF_PAYLOADTYPE)).compareTo("") != 0)
			payloadType = new Integer((String) params.get(SessionObject.CONTROL_DTMF_PAYLOADTYPE));
		
		Integer volume = 7;
		Integer end = 0;
		Double duration = 1200.0;
		Integer position = 0;
		
		String digit = "1";
		Integer durationEvent = 0;
		
		if(((String) params.get(SessionObject.CONTROL_DTMF_VOLUME)).compareTo("") != 0)
			volume = new Integer((String) params.get(SessionObject.CONTROL_DTMF_VOLUME));
		if(((String) params.get(SessionObject.CONTROL_DTMF_TIME)).compareTo("") != 0)
			duration = new Double((String) params.get(SessionObject.CONTROL_DTMF_TIME));
		if(((String) params.get(SessionObject.CONTROL_DTMF_DIGIT)).compareTo("") != 0)
			digit = (String) params.get(SessionObject.CONTROL_DTMF_DIGIT);
		
		Integer packetNumber = new Long(Math.round(duration / myInstance.durationPacket)).intValue();
		
		myInstance.rtpSession.setDuration(new Double(myInstance.durationPacket));
		myInstance.rtpSession.setSampling(8000.0F);
		
		for(int i=0; i<=packetNumber; i++)
		{
			if(((String) params.get(SessionObject.CONTROL_DTMF_DTMFTYPE)).compareTo("rfc2833") == 0)
			{
				byte[] data = new byte[4];
				
				if(i == packetNumber)
					end = 1;
				
				data = createRtpDtmf(digit, volume, durationEvent, end);
				
				durationEvent = durationEvent + myInstance.durationPacket;
				
				RTPPacket rtpPacket = new RTPPacket(payloadType, 
						myInstance.rtpSession.getSequenceNumber(), 
						myInstance.rtpSession.getTimestamp(), 
						myInstance.rtpSession.getSsrc(), 
						data);
				
				myInstance.rtpSession.setPayloadType(payloadType);
				rtpPacket.setVersion(version);
				rtpPacket.setPadding(padding);
				rtpPacket.setExtension(extension);
				rtpPacket.setCsrcCount(csrcCount);
				
				if(i == 0)
					rtpPacket.setMarker(1);
				
				if(marker.compareTo(-1) != 0)
					rtpPacket.setMarker(marker);
				
				//sendRtpPacket(rtpPacket.createRtpPacket(), myInstance.remoteIp, myInstance.remotePort, myInstance.localPort);
				myInstance.sendRtpPacket(rtpListener, rtpPacket.createRtpPacket());
			}
			/*else if(((String) params.get(SessionObject.CONTROL_DTMF_DTMFTYPE)).compareTo("frequency") == 0)
			{
				Integer size = RTPPacket.getSampling(payloadType).intValue() * durationPacket;
				byte[] data = new byte[size];
				
				data = generateTone(digit, durationPacket, payloadType, position);
				
				position = position + size;

				RTPPacket rtpPacket = new RTPPacket(payloadType, 
						rtpSession.getSequenceNumber(), 
						rtpSession.getTimestamp(), 
						rtpSession.getSsrc(), 
						data);
				
				sendRtpPacket(rtpPacket.createRtpPacket(), remoteIp, remotePort, localPort);
				
				rtpSession.incrementTimestamp();
			}*/
			
			myInstance.rtpSession.incrementSequenceNumber();
		}
	}
	
	/**
	 * Function on stand-by.
	 * 
	 * Generate DTMF tones using frequency but doesn't encode it to the selected payload type.
	 * 
	 * @param key : the digit to generate DTMF.
	 * @param time : the duration of the packet.
	 * @param payloadType : the payload type of the RTP packet in which you want to send DTMF.
	 * @param position : the last position on the trigonometric circle. It prevents from discontinued signal. 
	 * @return DTMF data for RTP packet.
	 */
	/*public byte[] generateTone(String key, Integer time, Integer payloadType, Integer position)
	{
		Double frequency = RTPPacket.getSampling(payloadType);
		
		byte[] buffer = new byte[new Float(time * frequency).intValue()];
		int[] lowFrequency = {697, 770, 852, 941};
		int[] highFrequency = {1209, 1336, 1477, 1633};
		String[][] positionKey = new String[][]{{"1", "2", "3", "A"},
												{"4", "5", "6", "B"},
												{"7", "8", "9", "C"},
												{"*", "0", "#", "D"}};
		
		int lowFreq = 697;
		int highFreq = 1209;

		for(int i=0; i<4; i++) 
		{
            for(int j=0; j<4; j++) 
            {
                if(positionKey[i][j].equalsIgnoreCase(key))
                {
                	lowFreq = lowFrequency[i];
                	highFreq = highFrequency[j];
                }
            }
        }

		double angle1 = (lowFreq / (frequency * 1000)) * (2.0 * Math.PI);
		double angle2 = (highFreq / (frequency * 1000)) * (2.0 * Math.PI);

		for(int i=0; i < time * frequency; i++)
			buffer[i] = new Integer(new Double(128 + 63 * Math.sin(angle1 * (i + position)) + 63 * Math.sin(angle2 * (i + position))).intValue()).byteValue();

		return buffer;
	}*/
	
	/**
	 * Create DTMF packet over RTP using RFC 4733/2833.
	 * 
	 * @param key : the digit to generate DTMF.
	 * @param volume : the volume of the DTMF.
	 * @param duration : the duration of the packet.
	 * @param end : if it is the end of the DTMF (Marker = 1 in the RTP packet).
	 * @return DTMF data for RTP packet.
	 */
	public byte[] createRtpDtmf(String key, int volume, int duration, int end)
	{
		byte[] dtmf = new byte[4];
		Integer header;
		Integer digit = 0;
		Integer reserved = 0;
		
		if(key.equalsIgnoreCase("*"))
			digit = 10;
		else if(key.equalsIgnoreCase("#"))
			digit = 11;
		else if(key.equalsIgnoreCase("A"))
			digit = 12;
		else if(key.equalsIgnoreCase("B"))
			digit = 13;
		else if(key.equalsIgnoreCase("C"))
			digit = 14;
		else if(key.equalsIgnoreCase("D"))
			digit = 15;
		else
			digit = new Integer(key);

		header = digit & 0xFF;
		dtmf[0] = header.byteValue();
		
		header = (end << 7) & 0x80;
		header |= (reserved << 6) & 0x40;
		header |= volume & 0x3F;
		dtmf[1] = header.byteValue();
		
		header = duration >> 8;
		dtmf[2] = header.byteValue();
		header = duration & 0xFF;
		dtmf[3] = header.byteValue();
		
		return dtmf;
	}

	/**
	 * Enable RTCP packets in the RTP stream.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void enableRtcp(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ENABLERTCP_SESSION_ID));
		
		myInstance.enableRtcp = true;
		
		if(((String) params.get(SessionObject.CONTROL_ENABLERTCP_TIMEINTERVAL)).compareTo("") != 0)
			myInstance.timeInterval = new Long((String) params.get(SessionObject.CONTROL_ENABLERTCP_TIMEINTERVAL));
		else
			myInstance.timeInterval = 5000L;
			
		ArrayList<Integer> rtcpPort = new ArrayList<Integer>();
		rtcpPort.add(myInstance.localPort+1);
		
		rtpStats = RTPStats.getInstance();
		rtpStats.openStats(rtcpPort);
		rtpStats.addRtcpPortToList(myInstance.localPort+1);
		rtpStats.startStats();
	}
	
	/**
	 * Add a report packet to the RTCP template.
	 * @param params 
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void addReport(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ADDREPORT_SESSION_ID));
		
		myInstance.rtcpTemplate.add(new ReportPacket(myInstance.rtpSession.getSsrc()));
	}
	
	/**
	 * Add a SDES packet to the RTCP template.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void addSdes(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ADDAPP_SESSION_ID));
		
		SDESPacket sdesPacket = new SDESPacket();
		
		List<List<String>> parametersTable = ParameterParser.getTable((String) params.get(SessionObject.CONTROL_ADDSDES_ITEMS));
		
		for(int i=0; i<parametersTable.size(); i++)
		{
			String[] line = parametersTable.get(i).toString().split(", ", 2);

			line[0] = line[0].substring(1, line[0].length());
			line[1] = line[1].substring(0, line[1].length() - 1);
			
			Integer itemType = sdesPacket.getItemType(line[0]);
			
			if(itemType.compareTo(-1) != 0)
				sdesPacket.addSdesItem(myInstance.rtpSession.getSsrc(), sdesPacket.createItem(itemType, line[1]));
		}

		myInstance.rtcpTemplate.add(sdesPacket);
	}
	
	/**
	 * Add a BYE packet to the RTCP template.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void addBye(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ADDBYE_SESSION_ID));
		
		myInstance.enableBye = true;
		myInstance.reason = (String) params.get(SessionObject.CONTROL_SENDBYE_REASON);
	}
	
	/**
	 * Add a APP packet to the RTCP template.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void addApp(Map params)
	{	
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ADDAPP_SESSION_ID));
		
		Integer subtype = new Integer((String) params.get(SessionObject.CONTROL_ADDAPP_SUBTYPE));
		String name = (String) params.get(SessionObject.CONTROL_ADDAPP_NAME);
		String data = (String) params.get(SessionObject.CONTROL_ADDAPP_DATA);
		
		AppPacket appPacket = new AppPacket(subtype, myInstance.rtpSession.getSsrc(), name, data);
		
		myInstance.rtcpTemplate.add(appPacket);
	}
	
	/**
	 * Reset RTCP template and disable RTCP.
	 * @param params 
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void resetRtcp(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_ADDAPP_SESSION_ID));
	
		myInstance.enableRtcp = false;
		myInstance.enableBye = false;
		myInstance.timeInterval = null;
		myInstance.rtcpTemplate.clear();
	}
	
	/**
	 * Send a RTCP BYE packet.
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void sendBye(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_SENDBYE_SESSION_ID));
		
		ByePacket byePacket = new ByePacket();
		
		byePacket.addSsrc(myInstance.rtpSession.getSsrc(), (String) params.get(SessionObject.CONTROL_SENDBYE_REASON));
		
		myInstance.sendRtcpPacket(rtpListener, byePacket.createPacket());
		//sendRtpPacket(byePacket.createPacket(), myInstance.remoteIp, myInstance.remotePort+1, myInstance.localPort+1);
	}
	
	/**
	 * Send a receiver report packet.
	 * @param params 
	 * 
	 * @param params : A map of the parameters of the control action.
	 */
	public void sendReceiverReport(Map params)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.CONTROL_SENDRECEIVERREPORT_SESSION_ID));
		
		Integer offset = 0;
		LinkedList<byte[]> dataList = new LinkedList<byte[]>();
		
		if(new Long(System.currentTimeMillis() - myInstance.lastRtcpTime).compareTo(myInstance.timeInterval) >= 0)
		{
			ArrayList<ParticipantStats> partStatTable = rtpStats.getParticipantsStats();
			
			for(int j=0; j<myInstance.rtcpTemplate.size(); j++)
			{
				RTCPPacket rtcpPacket = myInstance.rtcpTemplate.get(j);
				
				if(rtcpPacket.getPacketType().compareTo(200) == 0)
					((ReportPacket) rtcpPacket).senderToReceiver();
				
				if(rtcpPacket.getPacketType().compareTo(201) == 0)
				{	
					for(int k=0; k<partStatTable.size(); k++)
					{
						if(myInstance.rtpSession.getPort().compareTo(partStatTable.get(k).getPort()) == 0)
						{
							Long dlsr = 0L;
							
							if(!partStatTable.get(k).getFirstReport())
								dlsr = System.currentTimeMillis() - partStatTable.get(k).getDlsr();
							
							((ReportPacket) rtcpPacket).addReportBlock(partStatTable.get(k).getSsrc(), 
									partStatTable.get(k).getFractionLost(),
									partStatTable.get(k).getCumulativePacketLost(),
									partStatTable.get(k).getCycle(),
									partStatTable.get(k).getSeqNumMax(),
									partStatTable.get(k).getTimeJitter(), 
									partStatTable.get(k).getLsr(),
									dlsr);
						}
					}
				}
	
				dataList.add(rtcpPacket.createPacket());
			}
			
			Integer length = 0;
			
			for(int j=0; j<dataList.size(); j++)
				length = length + dataList.get(j).length;
			
			byte[] data = new byte[length];
			
			while(!dataList.isEmpty())
			{
				byte[] tempData = dataList.removeFirst();
				
				for(int j=0; j<tempData.length; j++)
					data[offset + j] = tempData[j];
				
				offset = offset + tempData.length;
			}

			myInstance.sendRtcpPacket(rtpListener, data);
			// sendRtpPacket(data, myInstance.remoteIp, myInstance.remotePort+1, myInstance.localPort+1);
			
			myInstance.lastRtcpTime = System.currentTimeMillis();
		}	
	}
	
	/**
	 * Send a RTP packet.
	 * 
	 * @param data : the packet.
	 * @param remoteAddress : InetAddress of the remote client.
	 * @param remotePort : the port of the remote client.
	 * @param localPort : the port on which the packet is sent.
	 */
	/*public void sendRtpPacket(byte[] data, InetAddress remoteAddress, int remotePort, int localPort)
	{
		RTPInstance myInstance = findMyInstance((String) params.get(SessionObject.));
		
		rtpListener.sendPacket(data, remoteAddress, remotePort, localPort);
		
		try
		{
			Thread.currentThread().sleep(myInstance.durationPacket);
		}
		catch(InterruptedException e)
		{
			throw new IsacRuntimeException("Unable to wait " + durationPacket + " ms : " + e);
		}
	}*/
	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map params, ActionEvent report)
	{	
		switch (number) 
		{
			default:
				throw new Error("Unable to find this sample in ~RtpInjector~ ISAC plugin: " + number);
		}
	}
}
	
