/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.sipinjector;

import java.util.LinkedList;

/**
 * @author Rémi Druilhe
 */
public class MySessionTable 
{
	private String id;
	private String callId;
	private String fromTag;
	private String toTag;
	private String branch;
	private LinkedList<Long> requestTime = new LinkedList<Long>(); 
	private LinkedList<String> callFlow = new LinkedList<String>();
	private long expires;
	
	/**
	 * Constructor.
	 */
	public MySessionTable()
	{
		id = "";
		callId = "";
		fromTag = "";
		toTag = "";
		expires = 0;
	}
	
	/**
	 * Clear MyResponseTable.
	 */
	public void clearMySessionTable()
	{
		id = null;
		callId = null;
		fromTag = "";
		toTag = "";
		requestTime.clear();
		callFlow.clear();
		expires = 0;
	}
	
	/**
	 * @return the ID.
	 */
	public String getMySessionId()
	{
		return id;
	}
	
	/**
	 * @return the Call-ID.
	 */
	public String getMySessionCallId()
	{
		return callId;
	}
	
	/**
	 * @return the tag from From.
	 */
	public String getMySessionFromTag()
	{
		return fromTag;
	}
	
	/**
	 * @return the tag from To.
	 */
	public String getMySessionToTag()
	{
		return toTag;
	}
	
	public String getMySessionBranch()
	{
		return branch;
	}
	
	/**
	 * @param method : the method of the request.
	 * @param responseCSeq : the CSeq of the response (must be the same as the request).
	 * @return the time until the last request of the session with the same CSeq.
	 */
	public Long getMySessionTime(String method, long responseCSeq)
	{
		long localCSeq = 1;
		long localTime = 0;
		int index = -1;

		if(callFlow.isEmpty() == false)
		{
			for(int i=0; i<callFlow.size(); i++)
			{
				if(callFlow.get(i).compareTo(method) == 0)
				{
					if(localCSeq == responseCSeq)
						index = i;
					
					localCSeq++;
				}
			}
		}

		if(index != -1)
			localTime = requestTime.get(index);

		return localTime;
	}
	
	/**
	 * @param method : the method which CSeq is needed.
	 * @return the next CSeq for the method of the session.
	 */
	public long getMySessionCSeq(String method)
	{
		long localCSeq = 1;

		if(callFlow.isEmpty() == false)
		{
			for(int i = 0; i<callFlow.size(); i++)
			{
				if(callFlow.get(i).compareTo(method) == 0)
					localCSeq++;
			}
		}

		return localCSeq;
	}
	
	/**
	 * @return the expires header.
	 */
	public long getMySessionExpires()
	{
		return expires;
	}
	
	/**
	 * @param localId : the ID to set.
	 */
	public void setMySessionId(String id)
	{
		this.id = id;
	}
	
	/**
	 * @param localCallId : the Call-ID to set.
	 */
	public void setMySessionCallId(String callId)
	{
		this.callId = callId;
	}
	
	/**
	 * @param localFromTag : the tag of From to set.
	 */
	public void setMySessionFromTag(String fromTag)
	{
		this.fromTag = fromTag;
	}
	
	/**
	 * @param localToTag : the tag of To to set.
	 */
	public void setMySessionToTag(String toTag)
	{
		this.toTag = toTag;
	}
	
	public void setMySessionBranch(String branch)
	{
		this.branch = branch;
	}
	
	/**
	 * @param method : the method to add to the callflow.
	 * @param localTime : the time to add to the callflow.
	 */
	public void setMySessionCallFlow(String method, long localTime)
	{
		callFlow.addLast(method);
		requestTime.addLast(localTime);
	}
	
	/**
	 * @param localExpires : the expires header to set.
	 */
	public void setMySessionExpires(long expires)
	{
		this.expires = expires;
	}
}
