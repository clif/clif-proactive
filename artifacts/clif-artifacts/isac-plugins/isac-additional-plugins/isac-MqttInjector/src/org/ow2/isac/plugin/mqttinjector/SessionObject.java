/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.ControlAction;


/**
 * Implementation of a session object for plugin ~MqttInjector~,
 * a ISAC plug-in providing MQTT traffic injection over MQTT 3.1.1
 * (@see http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/cos02/mqtt-v3.1.1-cos02.html)
 * 
 * Built over Eclipse Paho MQTT Java client library, released under
 * Eclipse Distribution License 1.0 and Eclipse Public License 1.0 
 */
public class SessionObject
	implements
		SessionObjectAction,
		SampleAction,
		TestAction,
		DataProvider,
		IMqttMessageListener, ControlAction
{
	static final String PLUGIN_CLIENT_ID_SIZE = "client id size";

	static final int SAMPLE_CONNECT = 0;
	static final String SAMPLE_CONNECT_ENABLE = "enable";
	static final String SAMPLE_CONNECT_FLAGS = "flags";
	static final String SAMPLE_CONNECT_TOPIC = "topic";
	static final String SAMPLE_CONNECT_QOS = "qos";
	static final String SAMPLE_CONNECT_MESSAGE = "message";
	static final String SAMPLE_CONNECT_TIMEOUT_S = "timeout_s";
	static final String SAMPLE_CONNECT_PASSWORD = "password";
	static final String SAMPLE_CONNECT_USERNAME = "username";
	static final String SAMPLE_CONNECT_CLIENTID = "clientid";
	static final String SAMPLE_CONNECT_COMMENT = "comment";
	static final String SAMPLE_CONNECT_ACTION_TYPE = "action type";
	static final String SAMPLE_CONNECT_PROTOCOL = "protocol";
	static final String SAMPLE_CONNECT_PORT = "port";
	static final String SAMPLE_CONNECT_HOST = "host";

	static final int SAMPLE_PUBLISH = 1;
	static final String SAMPLE_PUBLISH_FLAGS = "flags";
	static final String SAMPLE_PUBLISH_COMMENT = "comment";
	static final String SAMPLE_PUBLISH_ACTION_TYPE = "action type";
	static final String SAMPLE_PUBLISH_TOPIC = "topic";
	static final String SAMPLE_PUBLISH_QOS = "qos";
	static final String SAMPLE_PUBLISH_MESSAGE = "message";

	static final int SAMPLE_DISCONNECT = 4;
	static final String SAMPLE_DISCONNECT_COMMENT = "comment";
	static final String SAMPLE_DISCONNECT_ACTION_TYPE = "action type";

	static final int SAMPLE_SUBSCRIBE = 5;
	static final String SAMPLE_SUBSCRIBE_POLICY = "policy";
	static final String SAMPLE_SUBSCRIBE_SIZE = "size";
	static final String SAMPLE_SUBSCRIBE_COMMENT = "comment";
	static final String SAMPLE_SUBSCRIBE_ACTION_TYPE = "action type";
	static final String SAMPLE_SUBSCRIBE_QOS = "qos";
	static final String SAMPLE_SUBSCRIBE_TOPIC = "topic";

	static final int SAMPLE_UNSUBSCRIBE = 6;
	static final String SAMPLE_UNSUBSCRIBE_COMMENT = "comment";
	static final String SAMPLE_UNSUBSCRIBE_ACTION_TYPE = "action type";
	static final String SAMPLE_UNSUBSCRIBE_TOPIC = "topic";

	static final int SAMPLE_GETMESSAGE = 7;
	static final String SAMPLE_GETMESSAGE_MESSAGE_VAR = "message var";
	static final String SAMPLE_GETMESSAGE_TOPIC_VAR = "topic var";
	static final String SAMPLE_GETMESSAGE_COMMENT = "comment";
	static final String SAMPLE_GETMESSAGE_ACTION_TYPE = "action type";
	static final String SAMPLE_GETMESSAGE_TOPICS = "topics";
	static final String SAMPLE_GETMESSAGE_TIMEOUT = "timeout";

	static final int TEST_ISCONNECTED = 2;
	static final int TEST_ISNOTCONNECTED = 3;

	static final int TEST_ISDEFINED = 8;
	static final String TEST_ISDEFINED_VARIABLE = "variable";

	static final int TEST_ISNOTDEFINED = 9;
	static final String TEST_ISNOTDEFINED_VARIABLE = "variable";

	static final int CONTROL_CLEAR = 10;
	static final String CONTROL_CLEAR_VARIABLE = "variable";

	private MqttClient client;
	private String clientId;
	private int clientIdLength = 23;
	// FIFO queues for messages indexed by their subscription topic
	private Map<String,MessageQueue> messagesByTopic;
	// FIFO queue for all messages
	private MessageQueue allMessages;
	private Map<String,String> variables;

	/**
	 * Constructor for specimen object.
	 * Sets the length of generated client ids
	 * (defaults to 23, as a backward compliance
	 * with MQTT 3.1).
	 *
	 * @param params may contain the client id size setting
	 */
	public SessionObject(Map<String,String> params)
	{
		String idSizeStr = params.get(PLUGIN_CLIENT_ID_SIZE);
		if (idSizeStr != null)
		{
			try
			{
				clientIdLength = Integer.parseInt(idSizeStr);
			}
			catch (NumberFormatException ex)
			{
				throw new IsacRuntimeException("Invalid length for generated MQTT client ids: " + idSizeStr);
			}
		}
	}


	/**
	 * Copy constructor (clone specimen object to get session object).
	 * Session object state initialization.
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		client = null;
		clientId = null;
		clientIdLength = so.clientIdLength;
		messagesByTopic = new HashMap<String,MessageQueue>();
		allMessages = new MessageQueue();
		variables = new HashMap<String,String>();
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	@Override
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 * Forces disconnection of the MQTT client when connected.
	 * Clears the session object state to enable garbage collection.
	 */
	@Override
	public void close()
	{
		messagesByTopic = null;
		allMessages = null;
		variables = null;
		clientId = null;
		if (client != null && client.isConnected())
		{
			try
			{
				client.disconnectForcibly(0, 1000);
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("Warning: mqtt client failed to disconnect", ex);
			}
			finally
			{
				client = null;
			}
		}
	}


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 * Call {@link #close()} and initializes the session object state.
	 */
	@Override
	public void reset()
	{
		try
		{
			close();
		}
		finally
		{
			messagesByTopic = new HashMap<String,MessageQueue>();
			allMessages = new MessageQueue();
			variables = new HashMap<String,String>();
		}
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	@Override
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_GETMESSAGE:
				doSampleGetMessage(params, report);
				break;
			case SAMPLE_UNSUBSCRIBE:
				doSampleUnsubscribe(params, report);
				break;
			case SAMPLE_SUBSCRIBE:
				doSampleSubscribe(params, report);
				break;
			case SAMPLE_DISCONNECT:
				doSampleDisconnect(params, report);
				break;
			case SAMPLE_PUBLISH:
				doSamplePublish(params, report);
				break;
			case SAMPLE_CONNECT:
				doSampleConnect(params, report);
				break;
			default:
				throw new Error(
					"Unable to find this sample in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
		return report;
	}


	/**
	 * Instantiates an MQTT client object and opens a connection with an MQTT server.
	 * An MQTT client id is generated when none is provided
	 * (see {@link SessionObject#genClientId(int)}).
	 * @param params map giving MQTT server host and port, protocol (tcp or ssl), and
	 * possibly a client id as well as common samples' action type and optional comment.
	 * Optionally, a "last will and testament" message may be defined (together with a
	 * topic, QoS, and flags).
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT connection attempt.
	 * @throws IsacRuntimeException if this session object was already connected.
	 */
	private void doSampleConnect(Map<String, String> params, ActionEvent report)
	throws IsacRuntimeException
	{
		if (client == null || ! client.isConnected())
		{
			// processing of basic parameters
			String uri =
				ParameterParser.getCombo(params.get(SAMPLE_CONNECT_PROTOCOL))
				+ "://" + params.get(SAMPLE_CONNECT_HOST)
				+ ":" + params.get(SAMPLE_CONNECT_PORT);
			String id = params.get(SAMPLE_CONNECT_CLIENTID);
			String username = params.get(SAMPLE_CONNECT_USERNAME);
			String password = params.get(SAMPLE_CONNECT_PASSWORD);
			String timeout_sStr = params.get(SAMPLE_CONNECT_TIMEOUT_S);
			int timeout_s = 0;
			if (timeout_sStr != null)
			{
				try
				{
					timeout_s = Integer.parseInt(timeout_sStr);
				}
				catch (NumberFormatException ex)
				{
					throw new IsacRuntimeException("Invalid MQTT connection timeout: " + timeout_sStr);
				}
			}
			if (id == null || id.trim().isEmpty())
			{
				if (clientId == null)
				{
					clientId = genClientId((char)report.sessionId);
				}
				id = clientId;
			}
			// processing of optional "last will and testament" parameters
			boolean enableLWT = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_ENABLE)).equalsIgnoreCase("yes");
			String topic = params.get(SAMPLE_CONNECT_TOPIC);
			byte payload[] = null;
			int qos = -1;
			boolean retained = false;
			if (enableLWT)
			{
				String messageStr = params.get(SAMPLE_CONNECT_MESSAGE);
				if (messageStr != null)
				{
					try
					{
						payload = messageStr.getBytes("UTF-8"); 
					}
					catch (UnsupportedEncodingException ex)
					{
						throw new Error("Cannot set Last Will and Testament message", ex);
					}
				}
				String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_CONNECT_QOS));
				qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
				retained = ParameterParser.getCheckBox(params.get(SAMPLE_CONNECT_FLAGS)).contains("retain");
			}
			// sample execution
			report.setDate(System.currentTimeMillis());
			try
			{
				client = new MqttClient(uri, id, null);
				MqttConnectOptions opts = new MqttConnectOptions();
		        opts.setCleanSession(true);
		        if (username != null && !username.isEmpty())
		        {
		        	opts.setUserName(username);
		        }
		        if (password != null)
		        {
		        	opts.setPassword(password.toCharArray());
		        }
		        opts.setConnectionTimeout(timeout_s);
		        if (enableLWT)
		        {
		        	opts.setWill(topic, payload, qos, retained);
		        }
		        client.connect(opts);
				report.successful = true;
				report.result = "connected to " + uri;
			}
			catch (MqttException ex)
			{
				client = null;
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_CONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_CONNECT_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			} 
		}
		else
		{
			throw new IsacRuntimeException("Attempt to connect an already connected MQTT client.");
		}
	}


	/**
	 * Publishes a message over current MQTT connection.
	 * @param params map giving message topic, payload, qos value and retain flag
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT message publication attempt.
	 * @throws Error if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSamplePublish(Map<String, String> params, ActionEvent report)
	throws Error, IsacRuntimeException
	{
		if (client != null && client.isConnected())
		{
			String topic = params.get(SAMPLE_PUBLISH_TOPIC);
			byte payload[] = null;
			try
			{
				payload = params.get(SAMPLE_PUBLISH_MESSAGE).getBytes("UTF-8");
			}
			catch (UnsupportedEncodingException ex)
			{
				throw new Error("Cannot publish MQTT message", ex);
			}
			String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_PUBLISH_QOS));
			int qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
			boolean retain = ParameterParser.getCheckBox(params.get(SAMPLE_PUBLISH_FLAGS)).contains("retain");
			report.setDate(System.currentTimeMillis());
			try
			{
				client.publish(topic, payload, qos, retain);
				report.successful = true;
				report.result = "message published (" + payload.length + " bytes)";
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_PUBLISH_ACTION_TYPE);
			report.comment = params.get(SAMPLE_PUBLISH_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to publish a message from an unconnected MQTT client.");
		}
	}


	/**
	 * Closes current MQTT connection.
	 * @param params map giving common samples' parameters action type and
	 * optional comment.
	 * @param report initial action report, to be filled with all necessary
	 * information about the MQTT disconnection attempt.
	 * @throws IsacRuntimeException if this session object is not connected
	 * to an MQTT server.
	 */
	private void doSampleDisconnect(Map<String, String> params, ActionEvent report)
	{
		if (client != null && client.isConnected())
		{
			report.successful = true;
			report.setDate(System.currentTimeMillis());		
			try
			{
				client.disconnect();
				client = null;
				report.result = "disconnected";
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_DISCONNECT_ACTION_TYPE);
			report.comment = params.get(SAMPLE_DISCONNECT_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to disconnect an unconnected MQTT client.");
		}
	}


	/**
	 * Subscribes to a topic over an MQTT connection.
	 * @param params map giving subscription topic and qos value, maximum number of
	 * stored incoming messages received on this topic (can be zero to disable storing)
	 * and the message drop policy in case the storage capacity is reached (drop the
	 * oldest message or the incoming message),  
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT topic subscription attempt.
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSampleSubscribe(Map<String, String> params, ActionEvent report)
	{
		// check state and parameters validity
		if (client == null || !client.isConnected())
		{
			throw new IsacRuntimeException("Attempt to subscribe an unconnected MQTT client.");
		}
		String topic = params.get(SAMPLE_SUBSCRIBE_TOPIC);
		if (topic == null || topic.isEmpty())
		{
			throw new IsacRuntimeException("MQTT client can't subscribe to empty topic.");
		}
		String qosStr = ParameterParser.getRadioGroup(params.get(SAMPLE_SUBSCRIBE_QOS));
		if (qosStr == null || qosStr.isEmpty())
		{
			throw new IsacRuntimeException("Missing QoS specification for MQTT subscription.");
		}
		int qos;
		try
		{
			qos = Integer.parseInt(qosStr.split("\\D", 2)[0]);
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				"Bad QoS specification format for MQTT subscription: " + qosStr,
				ex);
		}
		String capacityStr = params.get(SAMPLE_SUBSCRIBE_SIZE);
		if (capacityStr == null || capacityStr.isEmpty())
		{
			throw new IsacRuntimeException(
				"Missing message buffer size while subscribing to MQTT topic " + topic);
		}
		int capacity;
		try
		{
			capacity = Integer.parseInt(capacityStr);
		}
		catch (Exception ex)
		{
			throw new IsacRuntimeException(
				"Bad message buffer size specification \"" + capacityStr + "\" while subscribing to MQTT topic "
				+ topic,
				ex);
		}
		String policyStr = ParameterParser.getCombo(params.get(SAMPLE_SUBSCRIBE_POLICY));
		if (policyStr == null || policyStr.isEmpty())
		{
			throw new IsacRuntimeException("Missing drop policy while subscribing to MQTT topic " + topic);
		}
		DropPolicy policy;
		if (policyStr.startsWith("new"))
		{
			policy = DropPolicy.NEWER_FIRST;
		}
		else if (policyStr.startsWith("old"))
		{
			policy = DropPolicy.OLDER_FIRST;
		}
		else
		{
			throw new IsacRuntimeException(
				"Bad drop policy specification \"" + policyStr + "\" while subscribing to MQTT topic "
				+ topic
				+ "\n=> expected " + DropPolicy.NEWER_FIRST + " or " + DropPolicy.OLDER_FIRST);
		}
		if (! messagesByTopic.containsKey(topic) && capacity > 0)
		{
			messagesByTopic.put(topic, new MessageQueue(capacity, policy));
		}
		report.setDate(System.currentTimeMillis());
		try
		{
			if (capacity > 0)
			{
				client.subscribe(topic, qos, this);
			}
			else
			{
				client.subscribe(topic, qos);
			}
			report.successful = true;
			report.result = "subscribed to " + topic + " with QoS " + qos;
		}
		catch (MqttException ex)
		{
			report.successful = false;
			report.result = ex.getReasonCode() + " " + ex.getMessage();
		}
		report.duration = (int) (System.currentTimeMillis() - report.getDate());
		report.type = params.get(SAMPLE_SUBSCRIBE_ACTION_TYPE);
		report.comment = params.get(SAMPLE_SUBSCRIBE_COMMENT);
		if (report.comment == null)
		{
			report.comment = "";
		}
	}


	/**
	 * Unsubscribes from a topic over an MQTT connection.
	 * Whatever the success status of this sample, the message store
	 * for incoming messages is discarded.
	 * @param params map giving topic to unsubscribe from,
	 * as well as common samples' action type and optional comment.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT topic unsubscription attempt.
	 * @throws IsacRuntimeException if this session object is not connected to an MQTT
	 * server.
	 */
	private void doSampleUnsubscribe(Map<String, String> params, ActionEvent report)
	{
		if (client != null && client.isConnected())
		{
			String topic = params.get(SAMPLE_UNSUBSCRIBE_TOPIC);
			report.setDate(System.currentTimeMillis());
			try
			{
				client.unsubscribe(topic);
				report.successful = true;
				report.result = "unsubscribed from " + topic;
			}
			catch (MqttException ex)
			{
				report.successful = false;
				report.result = ex.getReasonCode() + " " + ex.getMessage();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_UNSUBSCRIBE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_UNSUBSCRIBE_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
			MessageQueue mq = messagesByTopic.remove(topic);
			if (mq != null)
			{
				for (MessageWithTopic msg : mq)
				{
					allMessages.remove(msg);
				}
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to unsubscribe an unconnected MQTT client.");
		}
	}


	/**
	 * Gets next message from the message store associated to one, several or
	 * all current topic subscriptions, possibly waiting for a message to be available.
	 * The payload of the message, as well as the actual topic it was sent to, may be
	 * obtained as a plug-in variable (see {@link #doGet(String)}).
	 * @param params map giving the list of topics (empty string to read from all
	 * currently subscribed topics), a time-out (in ms) waiting for a message,
	 * an optional variable name to make the message payload available as a plug-in
	 * variable, an optional variable name for the actual topic it was sent to,
	 * as well as common samples' action type and optional comment. A negative timeout
	 * value means infinite time-out. A zero timeout means no waiting time.
	 * The only way to know if the timeout has been reached without getting a message
	 * is to set a variable name and to check if it has been defined
	 * (see {@link #doTest(int, Map)}.
	 * @param report initial action report, to be filled with all necessary information
	 * about the MQTT message reception attempt.
	 * @throws Error if current JVM does not support UTF-8 encoding
	 * @throws IsacRuntimeException if this session object has not subscribed to the
	 * given MQTT topic.
	 */
	private void doSampleGetMessage(Map<String, String> params, ActionEvent report)
	throws Error
	{
		List<String> topics = ParameterParser.getNField(params.get(SAMPLE_GETMESSAGE_TOPICS));
		MessageQueue queue = null;
		if (topics.isEmpty())
		{
			queue = allMessages;
		}
		else
		{
			Iterator<String> topicIter = topics.iterator();
			while (queue == null && topicIter.hasNext())
			{
				queue = messagesByTopic.get(topicIter.next());
			}
		}
		if (queue != null)
		{
			String msgVar = params.get(SAMPLE_GETMESSAGE_MESSAGE_VAR);
			String topicVar = params.get(SAMPLE_GETMESSAGE_TOPIC_VAR);
			String timeoutStr = params.get(SAMPLE_GETMESSAGE_TIMEOUT);
			int timeout_ms = 0;
			if (! timeoutStr.isEmpty())
			{
				timeout_ms = Integer.parseInt(timeoutStr);
			}
			MessageWithTopic message = null;
			if (msgVar != null && !msgVar.isEmpty())
			{
				variables.remove(msgVar);
			}
			if (topicVar != null && !topicVar.isEmpty())
			{
				variables.remove(topicVar);
			}
			report.setDate(System.currentTimeMillis());
			if (timeout_ms == 0)
			{
				message = queue.poll();
			}
			else
			{
				try
				{
					if (timeout_ms < 0)
					{
						message = queue.take();
					}
					else
					{
						message = queue.poll(timeout_ms, TimeUnit.MILLISECONDS);
					}
				}
				catch (InterruptedException ex)
				{
					message = null;
				}
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			report.type = params.get(SAMPLE_GETMESSAGE_ACTION_TYPE);
			report.comment = params.get(SAMPLE_GETMESSAGE_COMMENT);
			if (report.comment == null)
			{
				report.comment = "";
			}
			if (message != null)
			{
				report.successful = true;
				report.result = "got one message on topic " + message.topic;
				if (msgVar != null && !msgVar.isEmpty())
				{
					try
					{
						variables.put(msgVar, new String(message.message.getPayload(), "UTF-8"));
					}
					catch (UnsupportedEncodingException ex)
					{
						throw new Error("~MqttInjector~ requires UTF-8 charset encoding", ex);
					}
				}
				if (topicVar != null && !topicVar.isEmpty())
				{
					variables.put(topicVar, message.topic);
				}
				if (queue != allMessages)
				{
					allMessages.remove(message);
				}
				else
				{
					Iterator<Map.Entry<String,MessageQueue>> mqIter = messagesByTopic.entrySet().iterator();
					boolean found = false;
					while (!found && mqIter.hasNext())
					{
						Map.Entry<String,MessageQueue> mq = mqIter.next();
						if (MqttTopic.isMatched(mq.getKey(), message.topic))
						{
							mq.getValue().remove();
						}
					}
				}
			}
			else
			{
				report.successful = false;
				report.result = "got no message on topics " + topics.toString();
			}
		}
		else
		{
			throw new IsacRuntimeException("Attempt to get an MQTT message from unsubscribed topics " + topics.toString());
		}
	}


	/**
	 * Generates a client id compliant with MQTT 3.1.1 specifications,
	 * i.e. composed of a number of characters among 0-9 A-Z a-z.
	 * The number of such characters is defined by attribute
	 * {@link #clientIdLength}, making the number of possible client ids
	 * equal to 62^clientIdLength.
	 * The id is forged from the system time in nanoseconds, the given alea
	 * parameter, and the #{@link java.util.concurrent.ThreadLocalRandom}
	 * random generator.
	 * @param alea a contribution to the id randomness (suggestion: use the virtual
	 * user id allocated by the ISAC execution engine via the provided action event).
	 * @return the client id, which is very likely to be globally unique provided
	 * the client id length is sufficient compared to the number of clients.
	 */
	protected String genClientId(int alea)
	{
		StringBuilder idsb = new StringBuilder(clientIdLength + (clientIdLength % 23));
		char aleaChar = (char) (alea + System.nanoTime());
		byte[] randomBytes = new byte[23];
		while (idsb.length() < clientIdLength)
		{
			ThreadLocalRandom.current().nextBytes(randomBytes);
			idsb.append(new String(randomBytes));
		}
		idsb.setLength(clientIdLength);
		for (int i=0 ; i < clientIdLength ; ++i)
		{
			char character = (char)('0' + ((idsb.charAt(i) + aleaChar) % 62));
			if (character > '9')
			{
				character += 7;
			}
			if (character > 'Z')
			{
				character += 6;
			}
			if (character > 'z')
			{
				character -= '0';
			}
			idsb.setCharAt(i, character);
		}
		return idsb.toString();
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	@Override
	public boolean doTest(int number, Map<String, String> params)
	{
		switch (number)
		{
			case TEST_ISNOTDEFINED:
				return ! variables.containsKey(TEST_ISNOTDEFINED_VARIABLE);
			case TEST_ISDEFINED:
				return variables.containsKey(params.get(TEST_ISDEFINED_VARIABLE));
			case TEST_ISNOTCONNECTED:
				return client == null || ! client.isConnected();
			case TEST_ISCONNECTED:
				return client != null && client.isConnected();
			default:
				throw new Error(
					"Unable to find this test in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
	}


	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params)
	{
		switch (number)
		{
			case CONTROL_CLEAR:
				variables.remove(params.get(CONTROL_CLEAR_VARIABLE));
				break;
			default:
				throw new Error(
					"Unable to find this control in ~MqttInjector~ ISAC plugin: "
					+ number);
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	@Override
	public String doGet(String var)
	{
		if (var.equalsIgnoreCase("#"))
		{
			return clientId;
		}
		else if (variables.containsKey(var))
		{
			return variables.get(var);
		}
		throw new IsacRuntimeException("Unknown variable in ~MqttInjector~ ISAC plugin: " + var);
	}


	////////////////////////////////////
	// interface IMqttMessageListener //
	////////////////////////////////////


	/**
	 * Puts a message to the message store associated to the given topic,
	 * or does nothing if there is no store associated to the topic
	 * (since it means the session object has not subscribed to the topic).
	 * Note that the message store has a limited capacity and may drop a message
	 * in case this capacity is reached.
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message)
	{
		MessageQueue queue = messagesByTopic.get(topic);
		if (queue == null)
		{
			Iterator<Map.Entry<String,MessageQueue>> queueIter = messagesByTopic.entrySet().iterator();
			while (queue == null && queueIter.hasNext())
			{
				Map.Entry<String,MessageQueue> entry = queueIter.next();
				if (MqttTopic.isMatched(entry.getKey(), topic))
				{
					queue = entry.getValue();
				}
			}
		}
		if (queue != null)
		{
			MessageWithTopic msg = new MessageWithTopic(topic, message);
			queue.put(msg);
			allMessages.put(msg);
		}
	}


	/////////////////////////////////////////////////////////////////////
	// inner class for queuing incoming messages with limited capacity //
	/////////////////////////////////////////////////////////////////////


	public enum DropPolicy {
		OLDER_FIRST, // drop the message at the head of the queue
		NEWER_FIRST // drop incoming message
	};

	/**
	 * This queue implementation changes the behavior of its parent
	 * implementation by disabling any blocking put operation.
	 * Instead, when the queue capacity is reached, the put operation
	 * either drops the head element and puts the new element at the
	 * end (OLDER_FIRST drop policy), or just drops the new element
	 * (NEWER_FIRST drop policy).
	 */
	@SuppressWarnings("serial")
	class MessageQueue extends LinkedBlockingQueue<MessageWithTopic>
	{
		DropPolicy policy;

		/**
		 * Creates a message store for MQTT messages
		 * with maximum capacity and #{@link SessionObject.DropPolicy#NEWER_FIRST}
		 * drop policy
		 */
		MessageQueue()
		{
			super();
			policy = DropPolicy.NEWER_FIRST;
		}


		/**
		 * Creates a new message store for MQTT messages
		 * @param capacity maximum of number messages this store
		 * can hold before applying the drop policy
		 * @param policy message drop policy to be used when
		 * the message store is full
		 */
		MessageQueue(int capacity, DropPolicy policy)
		{
			super(capacity);
			this.policy = policy;
		}

		/**
		 * Puts the provided message at the end of the message store,
		 * possibly dropping the message at the head of the store if
		 * the store is full and the drop policy is OLDER_FIRST, or just
		 * drops it if the store is full and the drop policy is NEWER_FIRST 
		 */
		@Override
		public synchronized void put(MessageWithTopic message)
		{
			if (!offer(message) && policy.equals(DropPolicy.OLDER_FIRST))
			{
				poll();
				offer(message);
			}
		}
	}


	/**
	 * Inner class for associating an MQTT message with a topic
	 */
	class MessageWithTopic
	{
		final String topic;
		final MqttMessage message;

		MessageWithTopic(String topic, MqttMessage message)
		{
			this.topic = topic;
			this.message = message;
		}
	}
}
