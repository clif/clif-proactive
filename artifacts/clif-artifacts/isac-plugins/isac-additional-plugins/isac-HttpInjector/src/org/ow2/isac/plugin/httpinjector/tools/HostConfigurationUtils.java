/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.httpinjector.tools;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.ow2.isac.plugin.httpinjector.SessionObject;

/**
 * Created on august 17 2004
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public abstract class HostConfigurationUtils {

	/**
	 * Set the HostConfiguration that will be assigned to the httpClient. Holds
	 * String cookiesParameters = (String) params.get("cookieparams;
	 *
	 * all of the variables needed to describe an HTTP connection to a host.
	 * This includes remote host, port and protocol, proxy host and port, local
	 * address, and virtual host.
	 *
	 * @param params
	 *            Hashtable containing all the variable
	 * @return the built HostConfiguration
	 */
	public static HostConfiguration setHostConfiguration(
		SessionObject sessionObject, Map<String,String> params)
	throws URIException
	{
		HostConfiguration hostConfiguration = new HostConfiguration();
		String proxyHost = (String) params.get(ParameterConstants.PROXYHOST);
		String proxyPort = (String) params.get(ParameterConstants.PROXYPORT);
		String localAddress = (String) params.get(ParameterConstants.LOCALADDRESS);
		URI uri = new URI((String) params.get(ParameterConstants.URI), false);
		hostConfiguration.setHost(uri);

		// set the host and port for the proxy
		if (proxyHost != null && !proxyHost.equals(""))
		{
			int proxyPortNumber = (proxyPort != null && !proxyPort.equals(""))
				? new Integer(proxyPort).intValue()
				: ParameterConstants.PROXYDEFAULTPORT;
			hostConfiguration.setProxy(proxyHost, proxyPortNumber);
		}
		else
		{
			//	check if there is a default proxy configuration
			String defaultProxyHost = sessionObject.getProxyHost();
			String defaultProxyPort = sessionObject.getProxyPort();
			if (defaultProxyHost != null && !defaultProxyHost.equals("")
				&& defaultProxyPort != null && !defaultProxyPort.equals(""))
			{
				hostConfiguration.setProxy(defaultProxyHost, new Integer(
					defaultProxyPort).intValue());
			}
		}
		// set LocalAddress if it is defined
		if (localAddress != null && !localAddress.equals("")) {
			InetAddress inetAddress;
			try {
				inetAddress = InetAddress.getByName(localAddress);
				hostConfiguration.setLocalAddress(inetAddress);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		return hostConfiguration;
	}
}