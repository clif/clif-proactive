CLIF additional isac plugins

This project builds a bundle of jar files. It containes :
- all additional isac plugin jar files
- all jar files needed by plugins as dependencies.

The main goal is to obtain a simple ZIP assembly, used to build CLIF distribution packages.