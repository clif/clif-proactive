CLIF's isac plugins

This directory contains ISAC plugin artefacts for CLIF:
- clif-common-plugins : all common ISAC plugin maven projects
- clif-additional-plugins : all additional ISAC plugin maven project
- clif-commons : all common ISAC plugins in a zip file
- clif-additionals : all additional ISAC plugins in a zip file
