/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2006, 2008-2009, 2012 France Telecom
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.isac.plugin.stringhandler;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Base64;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.util.Random;
import org.ow2.clif.util.StringHelper;

/**
 * Implementation of a session object for plugin ~StringHandler~
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, DataProvider, TestAction, ControlAction
{
	static final String PLUGIN_DEFAULT = "default";
	static final String CONTROL_RANDOMSET_SIZE = "size";
	static final int CONTROL_SET = 0;
	static final String CONTROL_SET_VALUE = "value";
	static final int CONTROL_REPLACEALL = 1;
	static final String CONTROL_REPLACEALL_REPLACE = "replace";
	static final String CONTROL_REPLACEALL_SEARCH = "search";
	static final int CONTROL_RANDOMSET = 2;
	static final int CONTROL_SETPATTERN = 3;
	static final String CONTROL_SETPATTERN_PATTERN = "pattern";
	static final int CONTROL_RESETMATCHING = 4;
	static final int CONTROL_MATCH = 5;
	static final String CONTROL_MATCH_SKIP = "skip";
	static final int CONTROL_CAPTURE = 7;
	static final String CONTROL_CAPTURE_VARIABLE = "variable";
	static final String CONTROL_CAPTURE_LEVEL = "level";
	static final int CONTROL_CUT = 8;
	static final String CONTROL_CUT_TAIL = "tail";
	static final String CONTROL_CUT_HEAD = "head";
	static final int CONTROL_TRUNCATE = 11;
	static final String CONTROL_TRUNCATE_LENGTH = "length";
	static final int CONTROL_REPLACEALLMATCHES = 12;
	static final String CONTROL_REPLACEALLMATCHES_REPLACE = "replace";
	static final int CONTROL_WRITE = 13;
	static final String CONTROL_WRITE_STRING = "string";
	static final int CONTROL_APPEND = 14;
	static final String CONTROL_APPEND_STRING = "string";
	static final int CONTROL_INSERT = 15;
	static final String CONTROL_INSERT_STRING = "string";
	static final int CONTROL_SLICE = 16;
	static final String CONTROL_SLICE_OPTIONS = "options";
	static final int CONTROL_NEXTSLICE = 17;
	static final int CONTROL_BASE64DECODE = 20;
	static final String CONTROL_BASE64DECODE_URLSAFE = "urlsafe";
	static final String CONTROL_BASE64DECODE_CHARSET = "charset";
	static final int CONTROL_BASE64ENCODE = 21;
	static final String CONTROL_BASE64ENCODE_OPTIONS = "options";
	static final String CONTROL_BASE64ENCODE_CHARSET = "charset";
	static final int TEST_CONTAINS = 0;
	static final String TEST_CONTAINS_SEARCH = "search";
	static final int TEST_MATCHED = 6;
	static final int TEST_CONTAINSNOT = 9;
	static final String TEST_CONTAINSNOT_SEARCH = "search";
	static final int TEST_MATCHEDNOT = 10;
	static final int TEST_HASMORESLICE = 18;
	static final int TEST_HASNOMORESLICE = 19;
	static private final String GET_LENGTH = "#";
	static private final String GET_STRING = "";
	static private final String GET_SLICE = "slice";
	static private final String GET_SLICE_NUMBER = "slice#";
	static private final String GET_NEXT_SLICE = "slice++";

	static private Random random = new Random();

	private String defaultValue;
	private StringBuilder value;
	private Pattern pattern;
	private Matcher matcher;
	private Map<String,String> variables = new HashMap<String,String>();
	private String[] slices;
	private int sliceIndex;
	private boolean matched = false;


	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		defaultValue = params.get(PLUGIN_DEFAULT);
		if (defaultValue == null)
		{
			defaultValue = "";
		}
	}


	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		value = new StringBuilder(so.defaultValue);
		this.defaultValue = so.defaultValue;
	}


	/**
	 * Must be called each time the string value changes
	 * to update a number of dependencies.
	 */
	private void valueChanged()
	{
		// reset pattern matcher
		if (pattern != null)
		{
			matcher.reset(value);
			slices = null;
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close() {
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset() {
		value = new StringBuilder(defaultValue);
		pattern = null;
		matcher = null;
		variables = new HashMap<String,String>();
		matched = false;
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String arg)
	{
		String result = null;
		if (arg.equals(GET_STRING))
		{
			result = value.toString();
		}
		else if (arg.equals(GET_LENGTH))
		{
			result = Integer.toString(value.length());
		}
		else if (arg.equals(GET_SLICE_NUMBER))
		{
			if (slices != null)
			{
				result = String.valueOf(slices.length);
			}
		}
		else if (arg.startsWith(GET_SLICE_NUMBER))
		{
			if (slices != null)
			{
				int index = Integer.valueOf(arg.substring(GET_SLICE_NUMBER.length()));
				if (index >= 0 && index < slices.length)
				{
					result = slices[index];
				}
			}
		}
		else if (arg.equals(GET_SLICE))
		{
			if (slices != null && slices.length > 0 && sliceIndex >= 0 && sliceIndex < slices.length)
			{
				result = slices[sliceIndex];
			}
		}
		else if (arg.equals(GET_NEXT_SLICE))
		{
			doControl(CONTROL_NEXTSLICE, null);
			if (slices != null && slices.length > 0 && sliceIndex >= 0 && sliceIndex < slices.length)
			{
				result = slices[sliceIndex];
			}
		}
		else
		{
			 result = variables.get(arg);
		}
		if (result == null)
		{
			throw new IsacRuntimeException("Variable not available in ~StringHandler~ ISAC plugin: " + arg);
		}
		else
		{
			return result;
		}
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params)
	{
		switch (number)
		{
			case TEST_HASNOMORESLICE:
				return ! doTest(TEST_HASMORESLICE, params);
			case TEST_HASMORESLICE:
				if (slices == null)
				{
					throw new IsacRuntimeException("StringHandler's slice control must be called prior to testing slice availability.");
				}
				return sliceIndex+1 < slices.length;
			case TEST_MATCHEDNOT:
				return !matched;
			case TEST_CONTAINSNOT:
				return value.indexOf(params.get(TEST_CONTAINSNOT_SEARCH)) == -1; 
			case TEST_MATCHED:
				return matched;
			case TEST_CONTAINS:
				return value.indexOf(params.get(TEST_CONTAINS_SEARCH)) != -1;
			default:
				throw new Error("Unable to find this test in ~StringHandler~ ISAC plugin: " + number);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_BASE64ENCODE:
				try
				{
					String charset = params.get(CONTROL_BASE64ENCODE_CHARSET);
					List<String> options = ParameterParser.getCheckBox(params.get(CONTROL_BASE64ENCODE_OPTIONS));
					boolean urlSafe = options.contains("URL-safe");
					boolean discardPadding = options.contains("Discard padding");
					Base64.Encoder encoder;
					if (urlSafe)
					{
						encoder = Base64.getUrlEncoder();
					}
					else
					{
						encoder = Base64.getEncoder();
					}
					StringBuilder b64 = new StringBuilder(
						new String(
							encoder.encode(this.value.toString().getBytes(charset)),
							charset));
					if (discardPadding)
					{
						b64.setLength(b64.indexOf("="));
					}
					setValue(b64);
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform base64 encoding: unsupported charset "
							+ params.get(CONTROL_BASE64ENCODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_BASE64DECODE:
				try
				{
					String charset = params.get(CONTROL_BASE64DECODE_CHARSET);
					String urlSafeStr = params.get(CONTROL_BASE64DECODE_URLSAFE);
					Base64.Decoder decoder;
					if (urlSafeStr != null && StringHelper.isEnabled(urlSafeStr))
					{
						decoder = Base64.getUrlDecoder();
					}
					else
					{
						decoder = Base64.getDecoder();
					}
					setValue(
						new StringBuilder(
							new String(
								decoder.decode(this.value.toString().getBytes(charset)),
								charset)));
				}
				catch (IllegalArgumentException ex)
				{
					throw new IsacRuntimeException("Plugin ~StringHandler~ could not decode base64 input", ex);
				}
				catch (UnsupportedEncodingException ex)
				{
					throw new IsacRuntimeException(
						"Plugin ~StringHandler~ could not perform base64 decoding: unsupported charset "
							+ params.get(CONTROL_BASE64DECODE_CHARSET),
						ex);
				}
				break;
			case CONTROL_NEXTSLICE:
				if (slices != null && slices.length > 0 && sliceIndex < slices.length)
				{
					++sliceIndex;
				}
				else
				{
					throw new IsacRuntimeException("StringHandler has no next slice.");
				}
				break;
			case CONTROL_SLICE:
				if (pattern != null)
				{
					List<String> options = ParameterParser.getCheckBox(params.get(CONTROL_SLICE_OPTIONS));
					if (options.contains("Discard empty strings"))
					{
						slices = pattern.split(value, 0);
						List<String> realSlicesList = new ArrayList<String>(slices.length);
						for (String str : slices)
						{
							if (! str.isEmpty())
							{
								realSlicesList.add(str);
							}
						}
						slices = realSlicesList.toArray(new String[realSlicesList.size()]);
					}
					else
					{
						slices = pattern.split(value, -1);
					}
					sliceIndex = -1;
				}
				else
				{
					throw new IsacRuntimeException("StringHandler can't slice when no pattern is set.");
				}
				break;
			case CONTROL_INSERT:
				value.insert(0, params.get(CONTROL_INSERT_STRING));
				valueChanged();
				break;
			case CONTROL_APPEND:
				value.append(params.get(CONTROL_APPEND_STRING));
				valueChanged();
				break;
			case CONTROL_WRITE:
				String overwrite = params.get(CONTROL_WRITE_STRING);
				value.replace(0, overwrite.length(), overwrite);
				valueChanged();
				break;
			case CONTROL_REPLACEALLMATCHES:
				if (pattern != null)
				{
					value = new StringBuilder(matcher.replaceAll(params.get(CONTROL_REPLACEALLMATCHES_REPLACE)));
					valueChanged();
				}
				else
				{
					throw new IsacRuntimeException("StringHandler can't replace matches when no pattern is set.");
				}
				break;
			case CONTROL_TRUNCATE:
				int length = Integer.parseInt(params.get(CONTROL_TRUNCATE_LENGTH));
				if (length < value.length())
				{
					value.delete(length, value.length());
				}
				valueChanged();
				break;
			case CONTROL_CUT:
				int head = Integer.parseInt(params.get(CONTROL_CUT_HEAD));
				int tail = Integer.parseInt(params.get(CONTROL_CUT_TAIL));
				if (head > 0)
				{
					value.delete(0, head);
				}
				if (tail > 0)
				{
					value.delete(value.length()-tail, value.length());
				}
				valueChanged();
				break;
			case CONTROL_CAPTURE:
				if (matched)
				{
					variables.put(
						params.get(CONTROL_CAPTURE_VARIABLE),
						matcher.group(
							Integer.parseInt(params.get(CONTROL_CAPTURE_LEVEL))));
				}
				else
				{
					variables.remove(params.get(CONTROL_CAPTURE_VARIABLE));
					throw new IsacRuntimeException("StringHandler plug-in can't capture an unmatched pattern.");
				}
				break;
			case CONTROL_MATCH:
				int i = Integer.parseInt(params.get(CONTROL_MATCH_SKIP));
				matched = false;
				while (i > 0 && matcher.find())
				{
					--i;
				}
				if (i == 0)
				{
					matched = matcher.find();
				}
				break;
			case CONTROL_RESETMATCHING:
				matcher.reset();
				matched = false;
				break;
			case CONTROL_SETPATTERN:
				pattern = Pattern.compile(params.get(CONTROL_SETPATTERN_PATTERN));
				matcher = pattern.matcher(value);
				matched = false;
				break;
			case CONTROL_RANDOMSET:
				setValue(random.nextStringBuilder(Integer.parseInt(params.get(CONTROL_RANDOMSET_SIZE))));
				break;
			case CONTROL_REPLACEALL:
				String search = params.get(CONTROL_REPLACEALL_SEARCH);
				String replace = params.get(CONTROL_REPLACEALL_REPLACE);
				int pos = 0;
				while (pos < value.length() && (pos = value.indexOf(search, pos)) != -1)
				{
					value.replace(pos, pos + search.length(), replace);
					pos += replace.length();
				}
				valueChanged();
				break;
			case CONTROL_SET:
				setValue(new StringBuilder((params.get(CONTROL_SET_VALUE))));
				break;
			default:
				throw new Error("Unable to find this control in ~StringHandler~ ISAC plugin: " + number);
		}
	}

	private void setValue(StringBuilder newValue)
	{
		value = newValue;
		valueChanged();
	}
}
