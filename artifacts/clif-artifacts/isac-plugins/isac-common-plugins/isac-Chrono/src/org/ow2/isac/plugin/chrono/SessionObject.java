package org.ow2.isac.plugin.chrono;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Hashtable;
import java.lang.Error;
import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.util.ParameterParser;

/**
 * Implementation of a session object for plugin ~Chrono~
 */
public class SessionObject
	implements SessionObjectAction, TestAction, ControlAction, SampleAction, DataProvider
{
	static private final int STOPPED = -1;
	static final String TYPE_STOP = "Chrono stop";
	static final String TYPE_SPLIT = "Chrono split";

	static final int SAMPLE_SPLIT = 10;
	static final String SAMPLE_SPLIT_RESULT = "result";
	static final String SAMPLE_SPLIT_COMMENT = "comment";
	static final String SAMPLE_SPLIT_SUCCESSFUL = "successful";
	static final int SAMPLE_STOP = 11;
	static final String SAMPLE_STOP_RESULT = "result";
	static final String SAMPLE_STOP_COMMENT = "comment";
	static final String SAMPLE_STOP_SUCCESSFUL = "successful";
	static final int CONTROL_START = 7;
	static final int CONTROL_SUSPEND = 8;
	static final int CONTROL_RESUME = 9;
	static final int CONTROL_DROP = 12;
	static final int TEST_IS_GT = 0;
	static final String TEST_IS_GT_VALUE = "value";
	static final int TEST_IS_GTE = 1;
	static final String TEST_IS_GTE_VALUE = "value";
	static final int TEST_IS_LT = 2;
	static final String TEST_IS_LT_VALUE = "value";
	static final int TEST_IS_LTE = 3;
	static final String TEST_IS_LTE_VALUE = "value";
	static final int TEST_IS_ON = 4;
	static final int TEST_IS_OFF = 5;
	static final int TEST_IS_SUSPENDED = 6;
	
	private long startTime = STOPPED;
	private long chrono = STOPPED;
	private boolean suspended = false;
	private long suspendTime = 0;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
	}


	/**
	 * Computes the actual chrono value
	 * @return the actual chrono value if the chrono has been started,
	 * or STOPPED constant is it is currently stopped
	 */
	private int getActualTime()
	{
		if (chrono == STOPPED)
		{
			return STOPPED;
		}
		else if (suspended)
		{
			return (int) (suspendTime - chrono);
		}
		else
		{
			return (int) (System.currentTimeMillis() - chrono);
		}
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		chrono = startTime = STOPPED;
		suspended = false;
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map params)
	{
		switch (number) {
			case TEST_IS_SUSPENDED:
				return suspended && chrono != STOPPED;
			case TEST_IS_OFF:
				return chrono == STOPPED;
			case TEST_IS_ON:
				return chrono != STOPPED;
			case TEST_IS_LTE:
				if (chrono == STOPPED)
				{
					throw new IsacRuntimeException("Illegal attempt to get a stopped chrono's value");
				}
				return getActualTime() <= Long.parseLong((String)params.get(TEST_IS_LTE_VALUE));
			case TEST_IS_LT:
				if (chrono == STOPPED)
				{
					throw new IsacRuntimeException("Illegal attempt to get a stopped chrono's value");
				}
				return getActualTime() < Long.parseLong((String)params.get(TEST_IS_LT_VALUE));
			case TEST_IS_GTE:
				if (chrono == STOPPED)
				{
					throw new IsacRuntimeException("Illegal attempt to get a stopped chrono's value");
				}
				return getActualTime() >= Long.parseLong((String)params.get(TEST_IS_GTE_VALUE));
			case TEST_IS_GT:
				if (chrono == STOPPED)
				{
					throw new IsacRuntimeException("Illegal attempt to get a stopped chrono's value");
				}
				return getActualTime() > Long.parseLong((String)params.get(TEST_IS_GT_VALUE));
			default:
				throw new Error("Unable to find this test in ~Chrono~ ISAC plugin: " + number);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map params)
	{
		switch (number)
		{
			case CONTROL_DROP:
				if (chrono != STOPPED)
				{
					chrono = STOPPED;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to drop a stopped chrono.");
				}
				break;
			case CONTROL_RESUME:
				if (chrono != STOPPED && suspended)
				{
					chrono += System.currentTimeMillis() - suspendTime;
					suspendTime = 0;
					suspended = false;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to resume a chrono that is not suspended.");
				}
				break;
			case CONTROL_SUSPEND:
				if (chrono != STOPPED && !suspended)
				{
					suspendTime = System.currentTimeMillis();
					suspended = true;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to suspend an inactive chrono.");
				}
				break;
			case CONTROL_START:
				if (chrono == STOPPED)
				{
					chrono = startTime = System.currentTimeMillis();
					suspended = false;
					suspendTime = 0;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to start an already started chrono.");
				}
				break;
			default:
				throw new Error("Unable to find this control in ~Chrono~ ISAC plugin: " + number);
		}
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_STOP:
				if (chrono != STOPPED)
				{
					report.duration = getActualTime();
					report.comment = (String)params.get(SAMPLE_STOP_COMMENT);
					report.result = (String)params.get(SAMPLE_STOP_RESULT);
					String successStr = (String)params.get(SAMPLE_STOP_SUCCESSFUL);
					report.successful = ParameterParser.getCheckBox(successStr).contains(SAMPLE_STOP_SUCCESSFUL);
					report.type = TYPE_STOP;
					report.setDate(startTime);
					chrono = startTime = STOPPED;
					suspended = false;
					return report;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to stop an inactive chrono.");
				}
			case SAMPLE_SPLIT:
				if (chrono != STOPPED)
				{
					report.duration = getActualTime();
					report.comment = (String)params.get(SAMPLE_SPLIT_COMMENT);
					report.result = (String)params.get(SAMPLE_SPLIT_RESULT);
					String successStr = (String)params.get(SAMPLE_SPLIT_SUCCESSFUL);
					report.successful = ParameterParser.getCheckBox(successStr).contains(SAMPLE_SPLIT_SUCCESSFUL);
					report.type = TYPE_SPLIT;
					report.setDate(startTime);
					return report;
				}
				else
				{
					throw new IsacRuntimeException("Illegal attempt to split an inactive chrono.");
				}
			default:
				throw new Error("Unable to find this sample in ~Chrono~ ISAC plugin: " + number);
		}
	}

	
	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		if (chrono == STOPPED)
		{
			throw new IsacRuntimeException("Illegal attempt to get a stopped chrono's value.");
		}
		else
		{
			return String.valueOf(getActualTime());
		}
	}
}
