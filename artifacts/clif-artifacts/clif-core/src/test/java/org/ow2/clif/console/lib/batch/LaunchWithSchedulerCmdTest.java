/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.batch;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests related to {@link LaunchWithSchedulerCmd}.
 *
 * @author Laurent Pellegrino
 */
public class LaunchWithSchedulerCmdTest {

    private LaunchWithSchedulerCmd launchWithSchedulerCmd;

    @Before
    public void setUp() {
        launchWithSchedulerCmd = new LaunchWithSchedulerCmd(
                "http://localhost:8080/rest", null, "admin", "admin", "dummy", "dummy.ctp", "dummy0", null);
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsValid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("0")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsValid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsValid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("99")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsInvalid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("-1")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsInvalid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":0")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_SecondsInvalid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsValid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("0:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsValid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("42:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsValid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("99:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsInvalid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("-1:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsInvalid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":0:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_MinutesSecondsInvalid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":42:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsValid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("0:42:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsValid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("42:42:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsValid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("99:42:42")).isTrue();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsInvalid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("-1:42:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsInvalid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":0:42:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_HoursMinutesSecondsInvalid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":42:42:42")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_Invalid1() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("abcdef")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_Invalid2() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid("")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_Invalid3() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(" ")).isFalse();
    }

    @Test
    public void testIsWalltimeExpressionValid_Invalid4() {
        assertThat(launchWithSchedulerCmd.isWalltimeExpressionValid(":::")).isFalse();
    }

    @Test
    public void testGetWalltime() {
        String walltimeValue = "1:2:3";

        System.setProperty(LaunchWithSchedulerCmd.PROPERTY_TASK_WALLTIME, walltimeValue);

        assertThat(launchWithSchedulerCmd.getWalltime()).isEqualTo(walltimeValue);
    }

}