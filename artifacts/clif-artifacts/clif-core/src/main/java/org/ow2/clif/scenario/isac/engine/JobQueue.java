/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2010 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.ArrayList;
import java.util.List;

import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.storage.api.AlarmEvent;

/**
 * Implementation of JobQueue using a heap.
 * 
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
class JobQueue {
	private final Clock		clock;
	private final List<Job>	heap	= new ArrayList<Job>(1000);
	private volatile long	delay;
	private BladeInsertResponse insertResponse;

	protected JobQueue(
		Clock clock,
		long delay,
		BladeInsertResponse insertResponse)
	{
		this.clock = clock;
		this.delay = delay;
		this.insertResponse = insertResponse;
	}

	private void swap(int index1, int index2) {
		Job tmp;

		tmp = heap.get(index1);
		heap.set(index1, heap.get(index2));
		heap.set(index2, tmp);
	}

	private void upHeap(int index) {
		int tmp;

		if (index == 0)
			return;
		tmp = (index - 1) >> 1;
		if (heap.get(tmp).alarm <= heap.get(index).alarm)
			return;
		swap(tmp, index);
		upHeap(tmp);
	}

	private void downHeap(int index) {
		int size, tmp;
		long alarm0, alarm1, alarm2;

		size = heap.size();
		tmp = (index << 1) + 1;
		if (tmp >= size)
			return;
		alarm0 = heap.get(index).alarm;
		alarm1 = heap.get(tmp).alarm;
		if (tmp + 1 < size) {
			alarm2 = heap.get(tmp + 1).alarm;
			if (alarm0 <= alarm1 && alarm0 <= alarm2)
				return;
			if (alarm1 > alarm2)
				tmp++;
		}
		else if (alarm0 <= alarm1)
			return;
		swap(tmp, index);
		downHeap(tmp);
	}

	protected synchronized void insert(Job job) {
		heap.add(job);
		upHeap(heap.size() - 1);
	}

	private void remove(int index) {
		int lastIndex = heap.size() - 1;

		heap.set(index, heap.get(lastIndex));
		heap.remove(lastIndex);
		downHeap(index);
	}

	protected synchronized void remove(Group group, int n) {
		Job j;

		for (int i = heap.size() - 1; i >= 0 && n > 0; i--)
			if ((j = heap.get(i)).group == group) {
				remove(i);
				j.free();
				n--;
			}
	}

	/**
	 * Gets a job that must be executed now
	 * (i.e. whose deadline has been reached).
	 * If the elected job's deadline is violated for more
	 * than the "job delay" threshold, then an alarm is generated.  
	 * @return a job, or null if no job is to be executed at the moment.
	 */
	protected synchronized Job pop() {
		Job todo = null;

		if (heap.size() > 0)
		{
			Job j = heap.get(0);
			long late = clock.getDate() - j.alarm;
			if (late >= 0)
			{
				todo = j;
				if (delay >= 0 && late > delay)
				{
					try
					{
						insertResponse.alarm(
							new AlarmEvent(
								System.currentTimeMillis(),
								AlarmEvent.WARNING,
								new StringBuilder("Virtual user ")
									.append(j.id)
									.append(" is more than ")
									.append(late).append("ms late").toString()));
					}
					catch (Exception ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				remove(0);
			}
		}
		return todo;
	}

	protected void setDelay(long delay) {
		this.delay = delay;
	}

	protected synchronized void free() {
		for (int i = 0, size = heap.size(); i < size; i++)
			heap.get(i).free();
		heap.clear();
	}
}
