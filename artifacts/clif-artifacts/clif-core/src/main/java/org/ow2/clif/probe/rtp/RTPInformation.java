/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

/**
 * Class to keep informations about RTP packets.
 * 
 * @author Rémi Druilhe
 */
public class RTPInformation 
{
	private byte[] data;
	private Long time;
	private Integer port;
	
	public RTPInformation() {}
	
	public RTPInformation(byte[] data, Long time, Integer port) 
	{
		this.data = data;
		this.time = time;
		this.port = port;
	}

	// Get methods
	public byte[] getData()
	{
		return data;
	}
	
	public Long getTime()
	{
		return time;
	}
	
	public Integer getPort()
	{
		return port;
	}
	
	// Set methods
	public void setData(byte[] data)
	{
		this.data = data;
	}
	
	public void setTime(Long time)
	{
		this.time = time;
	}
	
	public void setPort(Integer port)
	{
		this.port = port;
	}
}
