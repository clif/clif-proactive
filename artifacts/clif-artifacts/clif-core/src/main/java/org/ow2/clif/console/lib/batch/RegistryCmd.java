/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.util.CodeServer;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.PACodeServer;

/**
 * Batch command to run the Clif Registry where servers and ClifApplications will be bound.
 * Finally, in case of shared code server configuration, a code server is launched for all
 * further deployments.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class RegistryCmd
{
	/**
	 * Runs the Clif Registry and possibly a shared code server.
	 * @param args - (no argument)
	 */
	public static void main(String[] args)
	{
		try
		{
			ExecutionContext.init("./");
			ClifRegistry registry = ClifRegistry.getInstance(true);
			System.out.println("ProActive CLIF registry successfully created on: " + registry);
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
			System.err.println("Maybe another registry is already running on this host");
			System.exit(BatchUtil.ERR_REGISTRY);
		}
		if (ExecutionContext.codeServerIsShared())
		{
			try
			{
				if (ExecutionContext.getCLIFPACodeServer() == null)
				{
					CodeServer.launch(
			        	null,
			        	Integer.parseInt(System.getProperty("clif.codeserver.port", "1357")),
			        	ExecutionContext.getBaseDir() + "lib/ext/",
			        	System.getProperty("clif.codeserver.path", "."));
				}
				else
				{
					PACodeServer.launch(
						ExecutionContext.getBaseDir() + "lib/ext/",
			        	System.getProperty("clif.codeserver.path", "."));
				}
			}
			catch (Exception e)
			{
				e.printStackTrace(System.err);
				System.err.println("Maybe another code server is already running on this host");
				System.exit(BatchUtil.ERR_CODESERVER);
			}
		}
	}
}
