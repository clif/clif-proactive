/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

/**
 * Math extension more specifically tuned to deal with statistical analysis
 * of time measures
 * @author Guy Vachet
 */
public final class Math4Long {

    private static final double LOG_10 = Math.log(10);

    // Can't subclass final class Math so
    // redefine the methods for private use in the class
    private static double ceil(double a) {
        return Math.ceil(a);
    }

    private static double log(double a) {
        return Math.log(a);
    }

    private static double log10(double a) {
        return log(a) / LOG_10;
    }

    private static double pow(double a, double b) {
        return Math.pow(a, b);
    }

    private static double rint(double a) {
        return Math.rint(a);
    }

    /**
     * fix the number of significant digits
     */
    private static int numberOfDigit(double a) {
        int dn = 1;
        if (a > 10)
            dn += log10(a);
        if (a < 0) {
            dn += log10(-a);
        }
        return dn;
    }

    /**
     * refines the number of significant digits, considering the precision is
     * square root dependent on number of measures.
     */
    private static int numberOfDigit(double a, int size) {
        int dn = numberOfDigit(a);
        if (size > 10)
            dn += Math.round((log10(Math.sqrt(size))));
        return dn;
    }

    /**
     * rounds a double keeping only "n" significant digits
     */
    private static double roundValue(double d, int precision) {
        double a;
        if (d == 0 || precision <= 0)
            return d;
        if (d > 0)
            a = pow(10, ceil(log10(d)) - precision);
        else
            a = pow(10, ceil(log10(-d)) - precision);
        if (a >= 1) {
            a = rint(a);
            a = rint(d / a) * a;
        } else {
            a = rint(1 / a);
            a = rint(d * a) / a;
        }
        return a;
    }

    /**
     * rounds a double keeping only "n" digits (default way) considering this
     * double is representing a time measurement.
     * @param d the double to be rounded
     * @return the rounded value of this double
     */
    public static double round(double d) {
        return roundValue(d, numberOfDigit(d));
    }

    /**
     * refines the rounding of a double keeping only "n" digits considering the
     * number of measures used to calculate this double (typical of average..)
     * @param d the double to be rounded
     * @param size is the number of measures to compute this double
     * @return the rounded value of this double considering the measure number
     */
    public static double round(double d, int size) {
        return roundValue(d, numberOfDigit(d, size));
    }

    /**
     */
    public static double displayDouble(double nb, int digitNb) {
        double factor = 1.0;
        for (int i = 0; i < digitNb; i++)
            factor = 10 * factor;
        return Math.round(nb * factor) / factor;
    }

    public static double displayDouble(double nb) {
        if (Math.abs(nb) < 1)
            return displayDouble(nb, 3);
        else if (Math.abs(nb) < 10)
            return displayDouble(nb, 2);
        else if (Math.abs(nb) < 100)
            return displayDouble(nb, 1);
        else
            return displayDouble(nb, 0);
    }
    /**
     * @param t time in millisecond to be displayed
     * @return time in human readable display (by using convenient unit)
     */
    public static String displayMillisecTime(long t) {
        if (t < 1000)
            return new StringBuffer().append(t).append(" milliseconds").toString();
        else if (t < 60000)
            return new StringBuffer().append(displayDouble(t / 1000.0)).append(" seconds").toString();
        else if (t < 3600000)
            return new StringBuffer().append(displayDouble(t / 60000.0)).append(" minutes").toString();
        else
            return new StringBuffer().append(displayDouble(t / 3600000.0)).append(" hours").toString();
    }

    /**
     * @param t time in second to be displayed
     * @return time in human readable display (by using convenient unit)
     */
    public static String displaySecondTime(long t) {
        if (t < 60)
            return new StringBuffer().append(t).append(" seconds").toString();
        else if (t < 3600)
            return new StringBuffer().append(displayDouble(t / 60.0)).append(" minutes").toString();
        else
            return new StringBuffer().append(displayDouble(t / 3600.0)).append(" hours").toString();
    }


}
