<!-- A scenario is composed of two parts :-->
<!--  - behaviors, to define some behavior...-->
<!--  - load, to define the load repartition...-->
<!ELEMENT scenario (behaviors,loadprofile)>
<!-- In the part behaviors, we must define the plugins that will be used in behaviors-->
<!ELEMENT behaviors (plugins,behavior+)>
<!-- For each plugin we define the plugin with the use tag-->
<!ELEMENT plugins (use*)>
<!-- We can add some parameters if it's needed-->
<!ELEMENT use (params?)>
<!-- We define an id which can be used in the next parts, to reference the plugin used-->
<!-- The name is the name of the plugin that will be used-->
<!ATTLIST use
  id        ID     #REQUIRED
  name      CDATA  #REQUIRED
>
<!-- Now we can define the behaviors-->
<!-- a behavior begin with the behavior tag, and can be composed of: -->
<!--   - A sample : reference to a specified sample plugin... -->
<!--   - A timer : it's a reference to a timer plugin... -->
<!--   - A while controller : it's a while loop... -->
<!--   - A preemptive : it's a controller adding a preemptive for all it children... -->
<!--   - An if controller : it's a controller doing the if / then /else task... -->
<!--   - A nchoice controller : it's a controller which permits doing random choices between some sub-behaviors with a weight factor -->
<!ELEMENT behavior (sample|timer|control|while|preemptive|if|nchoice)*>
<!-- When we define a behavior we must define the id parameter too, -->
<!-- it will be used to reference behavior in load part-->
<!ATTLIST behavior
  id        ID     #REQUIRED
>
<!-- A sample element could need some parameters-->
<!-- the parameters needed are defined in the plugin, which will be used, definition file-->
<!ELEMENT sample (params?)>
<!-- A sample element have for parameter : -->
<!--  - use : the id of the plugin that will be used for this sample-->
<!--          the id of this plugin must be defined into the plugins part-->
<!--  - name : the name of the action that is referenced by the sample tag-->
<!--           this action name must be specified in the plugin, which is used, definition file--> 
<!ATTLIST sample
  use       CDATA  #REQUIRED
  name      CDATA  #REQUIRED
>
<!-- A timer element could need some parameters--> 
<!-- the parameters needed are defined in the plugin, which will be used, definition file-->
<!ELEMENT timer (params?)>
<!-- The timer have got the same parameters of a sample element-->
<!ATTLIST timer
  use       CDATA  #REQUIRED
  name      CDATA  #REQUIRED
>
<!ELEMENT control (params?)>
<!ATTLIST control
  use       CDATA  #REQUIRED
  name      CDATA  #REQUIRED
>
<!-- A while controller must contain a condition and a sub-behavior--> 
<!ELEMENT while (condition,(sample|timer|control|while|preemptive|if|nchoice)*)>
<!-- A condition is a reference to a test of a specified plugin-->
<!-- it could need some parameters-->
<!ELEMENT condition (params?)>
<!-- we need specified as parameters for this tag, the plugin used and the name of the test, like sample or timer tag-->
<!ATTLIST condition 
  use       CDATA  #REQUIRED
  name      CDATA  #REQUIRED
>
<!-- A preemptive element is defined as a while element, the difference is in the execution process-->
<!-- For a while we evaluate the condition before each loop, in a preemptive before each action...-->
<!ELEMENT preemptive (condition,(sample|timer|control|while|preemptive|if|nchoice)*)>
<!-- An if controller must contains a condition and a sub-behavior ('then' tag)-->
<!-- And optionally it could contain another sub-behavior ('else' tag)-->
<!ELEMENT if (condition,then,else?)>
<!-- A then tag delimited the sub-behavior that will be executed if the condition is true-->
<!ELEMENT then (sample|timer|control|while|preemptive|if|nchoice)*>
<!-- A else element contains a sub-behavior too-->
<!ELEMENT else (sample|timer|control|while|preemptive|if|nchoice)*>
<!-- A nchoice plugin contains n sub-behavior, each sub-behavior have a probability to be executed-->
<!ELEMENT nchoice (choice+)>
<!-- An choice element contain a sub-behavior-->
<!ELEMENT choice (sample|timer|control|while|preemptive|if|nchoice)*>
<!-- And this element take for parameter a probability-->
<!ATTLIST choice 
  proba    CDATA  #REQUIRED
>
<!-- Now we define the params element, this element begin the part to define parameters for the parent element-->
<!ELEMENT params (param+)>
<!-- For each param we need to define it with the param tag-->
<!ELEMENT param EMPTY>
<!-- This tag take for parameters the name of the parameter and it value-->
<!ATTLIST param
  name      CDATA  #REQUIRED
  value     CDATA  #REQUIRED
>
<!-- Now let's define the load part, this part is used to define the ramps, each ramps represent the load for a behavior-->
<!-- We can define some ramps together in a group element, this element is used to launch several behaviors in the same time-->
<!ELEMENT loadprofile (group*)>
<!-- A group is a composition of 'ramp' elements-->
<!ELEMENT group (ramp+)>
<!-- We need define the behavior id of the group and optionally -->
<!-- the force stop mode, default is true -->
<!ATTLIST group
  behavior        CDATA        #REQUIRED
  forceStop		  (true|false) "true"
>
<!-- each ramp could take some parameters-->
<!ELEMENT ramp (points)>
<!-- For a ramp we must define the style of the ramp, which will be used-->
<!ATTLIST ramp
  style     CDATA  #REQUIRED
>
<!ELEMENT points (point,point)>
<!ELEMENT point EMPTY>
<!-- For a ramp we must define the style of the ramp and the reference of the behavior, which will be used-->
<!ATTLIST point
  x     CDATA  #REQUIRED
  y     CDATA  #REQUIRED
>