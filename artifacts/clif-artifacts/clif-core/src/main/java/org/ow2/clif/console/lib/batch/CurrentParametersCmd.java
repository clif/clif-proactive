/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005-2006, 2011 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.io.Serializable;
import java.util.Map;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.util.ExecutionContext;


/**
 * Command to get the list of parameters and their associated values of a blade.
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class CurrentParametersCmd
{
	/**
	 * Displays the list of a blade's parameters and associated values.
	 * @param args args[0] is the name of the target deployed test plan,
	 * args[1] is the identifier of the target blade in the given test plan
	 */
	public static void main(String[] args)
	{
		if (args.length < 2)
		{
			BatchUtil.usage("arguments expected: <name of deployed test plan> <blade identifier>");
		}
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], args[1]));
	}


	/**
	 * Displays the list of a blade's parameters and associated values.
	 * @param testPlan name of the target deployed testplan
	 * @param bladeId identifier of the target blade in the given test plan
	 * @return command execution status code (@see BatchUtil)
	 */
	static public int run(String testPlan, String bladeId)
	{
		try
		{
			ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlan);
			Map<String,Serializable> res = clifApp.getCurrentParameters(bladeId);
			if (res == null)
			{
				System.out.println("No parameter for this blade");
			}
			else
			{
				for (Map.Entry<String,Serializable> entry : res.entrySet())
				{
					System.out.println(entry.getKey() + " = " + entry.getValue());
				}
			}
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.err.println("Error occured while getting parameters");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
