/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * This class implements a generic serializable observer that
 * delegates calls to update() to another observer implementation
 * that is typically non serializable.
 * On instantiation, the new SerializableObserver instance is
 * added to a static Map with its hashcode as key.
 * Then, on deserialization, it is replaced by its original
 * instance, picked from the static Map, holding the reference to
 * the delegate observer. Finally, calls to update() are forwarded
 * to the delegate observer, who did not undergo serialization-
 * deserialization.
 * The serializable observer is removed from the instances Map
 * on garbage collection.
 * 
 * @author Bruno Dillenseger
 */
public class SerializableObserver implements Serializable, Observer
{
	private static final long serialVersionUID = -7115137732510723363L;
	/** Registers "active" instances */
	private static Map<Integer,SerializableObserver> observerInstances =
		new HashMap<Integer,SerializableObserver>();

	/** a statically unique identifier for this instance (Object hashcode) */
	private int suid;
	/** the delegate observer (not supposed to be serializable) */
	private transient Observer delegate;

	/**
	 * Creates a new serializable observer wrapping a non-serializable observer.
	 * @param delegate the observer to which observations will be forwarded.
	 */
	public SerializableObserver(Observer delegate)
	{
		this.delegate = delegate;
		suid = hashCode();
		observerInstances.put(suid, this);
	}

	/**
	 * Forward call to the delegate observer.
	 * @see Observer#update(Observable, Object)
	 */
	@Override
	public void update(Observable o, Object arg)
	{
		delegate.update(o, arg);
	}

	/**
	 * Enforces that the original object is obtained on deserialization,
	 * instead of a copy (picked from the instances Map using this
	 * instance suid as key). This restores the delegate attribute.
	 * @return the original observer instance that was serialized.
	 */
	private Object readResolve()
	{
		return observerInstances.get(suid);
	}

	/**
	 * Removes this observer from the set of observers on garbage collection.
	 */
	@Override
	public void finalize()
	{
		observerInstances.remove(suid);
	}
}
