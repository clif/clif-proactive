/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.analyze.statistics.profiling.ProbeDatum;
import org.ow2.clif.analyze.statistics.util.data.LongStatistics;
import org.ow2.clif.analyze.statistics.util.data.Math4Long;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.supervisor.api.ClifException;

import java.util.*;

/**
 *  Analyze JVM probes of CLIF
 * 
 * @author Guy Vachet
 */
public class JvmAnalyst extends ProbeAnalyst {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	public static final String TYPE_LABEL = JVM_EVENT_TYPE_LABEL;
	public static final String FIELD_LABEL = FREE_MEMORY_EVENT_FIELD_LABEL;
//	other available fields: USED_MEMORY_EVENT_FIELD_LABEL
//							FREE_USABLE_MEMORY_EVENT_FIELD_LABEL
	// specific companion of JVM measure
	private Map<String, List<AlarmEvent>> bladeGCEvents;

	/**
	 * constructor
	 */
	public JvmAnalyst() {
		setLabel(TYPE_LABEL);
		bladeGCEvents = new TreeMap<String, List<AlarmEvent>>();
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis:"));
	}

	public JvmAnalyst(String analyzeRange) {
		setLabel(TYPE_LABEL);
		bladeGCEvents = new TreeMap<String, List<AlarmEvent>>();
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis").append(analyzeRange));
	}

	@Override
	public void addProfilingData(BladeStoreReader reader, EventFilter filter)
			throws ClifException {
		String bladeId = reader.getBladeDescriptor().getId();
		List<Datum> jvmData = new ArrayList<Datum>();
		List<BladeEvent> alarmData = new ArrayList<BladeEvent>();
		List<AlarmEvent> gCAlarms = new ArrayList<AlarmEvent>();
		AlarmEvent alarmEvent;
		long minTime = 0;
		String arg;
		jvmData = new JvmReader(reader).getProfilingData(filter);
		addBladeData(bladeId, jvmData);
		if (VERBOSE) {
			System.out.println("Get " + jvmData.size()
					+ " JVM data from blade # " + bladeId);
		}
		// specific data recovering because of non-number result (here is
		// String argument for AlarmEvent). Doesn't matter if CLIF event
		// recovering due to small number of this kind of event (hope so).
		if (filter instanceof DateFilter) {
			minTime = ((DateFilter) filter).getMinTime();
			if (VERBOSE)
				System.out.println("Time offset for JVM data = " + minTime);
		}
		alarmData = reader.getEvents(ALARM_EVENT_TYPE_LABEL, filter);
		for (Iterator<BladeEvent> iterator = alarmData.iterator(); iterator
				.hasNext();) {
			alarmEvent = (AlarmEvent) iterator.next();
			alarmEvent.setDate(alarmEvent.getDate() - minTime);
			arg = (String) alarmEvent.getFieldValue(AlarmEvent.ARGUMENT_FIELD);
			if (arg.equalsIgnoreCase(GC_ARGUMENT)) {
				gCAlarms.add(alarmEvent);
			}
		}
		Collections.sort(gCAlarms);
		bladeGCEvents.put(bladeId, gCAlarms);
		if (VERBOSE)
			System.out.println("Get " + gCAlarms.size()
					+ " JVM alarm from blade # " + bladeId);
	}

	private void outputGCAnalysis(boolean isDetailed) {
		String[] probeIds = getBladeIdentifiers();
		LongStatistics periodsOfGC;
		long previousTimeOfGC, timeOfGC;
		List<AlarmEvent> alarmData;
		int size, maxGCEventNb = 0;
		StringBuffer sb, meanDisplay, stdDisplay;
		// first of all, compute GC periods and set the max number of GC events
		sb = new StringBuffer("\nPeriods (in seconds) of ");
		sb.append(GC_ARGUMENT).append("\nProbeId");
		for (int i = 0; i < probeIds.length; i++)
			sb.append("\t").append(probeIds[i]);
		meanDisplay = new StringBuffer("\nMean");
		stdDisplay = new StringBuffer("\nStd");
		for (int i = 0; i < probeIds.length; i++) {
			alarmData = bladeGCEvents.get(probeIds[i]);
			size = alarmData.size();
			if (size > maxGCEventNb)
				maxGCEventNb = size;
			previousTimeOfGC = 0;
			periodsOfGC = new LongStatistics(size);
			for (Iterator<AlarmEvent> it = alarmData.iterator(); it.hasNext();) {
				timeOfGC = (it.next()).getDate();
				periodsOfGC.addLong(timeOfGC - previousTimeOfGC);
				previousTimeOfGC = timeOfGC;
			}
			meanDisplay.append("\t").append(
					Math4Long.round(periodsOfGC.getStatSortMean() / 1000, size));
			stdDisplay.append("\t").append(
					Math4Long.round(periodsOfGC.getStatSortStd() / 1000, size));
		}
		sb.append(meanDisplay).append(stdDisplay);
		if (isDetailed) {
			sb.append("\nTime (in seconds) of ").append(GC_ARGUMENT).append(
					"\nProbeId");
			for (int i = 0; i < probeIds.length; i++)
				sb.append("\t").append(probeIds[i]);
			for (int j = 0; j < maxGCEventNb; j++) {
				sb.append("\nGC:");
				for (int i = 0; i < probeIds.length; i++) {
					alarmData = bladeGCEvents.get(probeIds[i]);
					if (j < alarmData.size()) {
						timeOfGC = (alarmData.get(j)).getDate();
						sb.append("\t").append(Math.round(timeOfGC / 1000.0));
					} else
						sb.append("\t").append("-");
				}
			}
		}
		System.out.println(sb.append("\n"));
	}

	/**
	 * 
	 * @param isDetailed
	 *            if true display more analysis
	 * @param sliceSize
	 *            size of elapsed time in order to analyze sub-population
	 */
	@Override
	public void outputAnalysis(boolean isDetailed, long sliceSize) {
		super.outputAnalysis(isDetailed, sliceSize);
		if (!isEmpty()) {
			outputGCAnalysis(isDetailed);
		}
	}

	/**
	 */
	class JvmReader extends BladeDatumReader {

		public JvmReader(BladeStoreReader bladeReader) {
			setBladeStoreReader(bladeReader);
			setEventTypeLabel(TYPE_LABEL);
		}

		/**
		 * @param event
		 * @param minTime
		 * @return
		 * @see org.ow2.clif.probe.jvm.JVMEvent
		 */
		@Override
		public Datum convert2Datum(BladeEvent event, long minTime) {
			return new ProbeDatum(event.getDate() - minTime, ((Long) event
					.getFieldValue(FIELD_LABEL)).longValue());
		}

	}

}
