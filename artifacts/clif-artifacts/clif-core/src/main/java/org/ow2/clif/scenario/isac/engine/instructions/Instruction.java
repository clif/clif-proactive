/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.instructions;

/**
 * @author Emmanuel Varoquaux
 */
public abstract class Instruction {

	/* Types of instructions */
	public static final int	SAMPLE	= 1;
	public static final int	TIMER	= 2;
	public static final int	CONTROL	= 3;
	public static final int	TEST	= 4;
	public static final int	GOTO	= 5;
	public static final int	NCHOICE	= 6;
	public static final int	EXIT	= 7;

	public abstract int getType();
}
