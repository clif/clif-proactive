/*
 * Copyright (C) 2005 - France Telecom R&D
 */
package org.ow2.clif.analyze.statistics.profiling;

/**
 * This object stores the date and measurement carried out by
 * a probe of CLIF. This extracted result is that of the field whose
 * label defined by "event field label".
 * 
 * @author Guy Vachet
 * @see org.ow2.clif.storage.api.ProbeEvent
 */
public class ProbeDatum extends Datum {
	private long result = -1;

	public ProbeDatum(long timestamp, long result) {
		setDate(timestamp);
		this.result = result;
	}

	@Override
	public long getResult() {
		return result;
	}

}
