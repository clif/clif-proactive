/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2014, 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.report;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.commons.math3.stat.StatUtils;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.ow2.clif.analyze.lib.report.Dataset.ChartType;
import org.ow2.clif.analyze.lib.report.Report.imageFormats;
import org.ow2.clif.analyze.lib.report.Section.ChartRepresentation;
import org.ow2.clif.analyze.statistics.util.data.ListOfLong;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 */

public class Chart
{
	static final Color[] COLORS = new Color[] {
		ChartColor.LIGHT_BLUE,
		Color.RED,
		Color.PINK,
		Color.GRAY,
		Color.MAGENTA,
		Color.ORANGE,
		ChartColor.DARK_GREEN,
		ChartColor.DARK_MAGENTA,
		ChartColor.DARK_RED,
		ChartColor.LIGHT_GREEN,
		ChartColor.LIGHT_MAGENTA,
		ChartColor.LIGHT_RED,
		Color.BLUE,
		ChartColor.VERY_DARK_GREEN,
		ChartColor.VERY_DARK_MAGENTA,
		ChartColor.VERY_DARK_RED,
	};

	ChartRepresentation representation;
	String title;
	private long startTime;
	private long endTime;
	private int numberOfSlices;
	private int numberOfQuantiles;
	private JFreeChart jfChart;
	private List<ChartDataValues> chartDataValuesList;
	private Section section;

	// constructors
	public Chart(Section section, String title, ChartRepresentation representation, long startTime,
			long endTime, int numberOfSlices, int quantilesNumber, JFreeChart jfChart) {
		super();
		LogAndDebug.tracep("(" + section + ", "+title + ", " + representation + ", " + startTime +
				", " + endTime + ", " + numberOfSlices + ", " + quantilesNumber + ", ...)");
		this.section = section;
		this.representation = representation;
		this.title = title;
		this.startTime = startTime;
		this.endTime = endTime;
		this.numberOfSlices = numberOfSlices;
		this.numberOfQuantiles = quantilesNumber;
		this.jfChart = jfChart;

		chartDataValuesList = new ArrayList<ChartDataValues>();
		LogAndDebug.tracem("(...)");
	}


	public Chart(Section section, String title, ChartRepresentation representation, long startTime,
			long endTime, int numberOfSlices, int quantilesNumber) {
		this(section, title, representation, startTime, endTime, numberOfSlices, quantilesNumber, null);
	}


	public Chart(String title, ChartRepresentation representation) {
		this(null, title, representation, -1, -1, 10, 10, null);
	}

	public Chart(Section section, String title, ChartRepresentation representation) {
		this(section, title, representation, -1, -1, 10, 10, null);
	}


	//////////////////////////////////////////
	// graphics generation methods
	//////////////////////////////////////////


	// generate graphic from associated ChartDataValues list
	public JFreeChart generateGraphics(){
		try
		{
			switch (representation){
				case TIME_BASED:
					generateTimeBasedGraphics();
					break;
				case HISTOGRAM:
					generateHistogramGraphics();
					break;
				case QUANTILE:
					generateQuantileGraphics();
					break;
			}
		}
		catch (Throwable thrw)
		{
			thrw.printStackTrace(System.err);
			jfChart = null;
		}
		return jfChart;
	}


	private void generateTimeBasedGraphics()
	{
		double initialTime = getInitialTime();
		double finalTime = getFinalTime();
		Map<String,XYSeriesCollection> rawDatasetsByScale = createDatasetRaw(initialTime, finalTime);
		Map<String,XYSeriesCollection> movingDatasetsByScale = createDatasetMoving(initialTime, finalTime);
		XYSeriesCollection throughputDataset = createDatasetThroughtput(
			initialTime,
			finalTime);
		XYPlot plot = null;
		Integer collectionIndex = 0;
		if (throughputDataset.getSeriesCount()
			+ rawDatasetsByScale.size()
			+ movingDatasetsByScale.size() == 0)
		{
			// nothing to plot
			jfChart = null;
		}
		else
		{
			plot = new XYPlot();
			plot.setDrawingSupplier(
				new DefaultDrawingSupplier(
					COLORS,
					DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
					DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
					DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
					DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE));
			Map<String,Integer> axisMap = new HashMap<String,Integer>();
			for (Map.Entry<String,XYSeriesCollection> collection : rawDatasetsByScale.entrySet())
			{
				XYDotRenderer dotRenderer = new XYDotRenderer();
				dotRenderer.setDotHeight(3);
				dotRenderer.setDotWidth(3);
				plot.setDataset(collectionIndex, collection.getValue());
				plot.setRenderer(collectionIndex, dotRenderer);
				plot.setRangeAxis(collectionIndex, new NumberAxis(collection.getKey()));
				plot.mapDatasetToRangeAxis(collectionIndex, collectionIndex);
				axisMap.put(collection.getKey(), collectionIndex);
				++collectionIndex;
			}
			for (Map.Entry<String,XYSeriesCollection> collection : movingDatasetsByScale.entrySet())
			{
				XYItemRenderer lineRenderer = new StandardXYItemRenderer();
				plot.setDataset(collectionIndex, collection.getValue());
				plot.setRenderer(collectionIndex, lineRenderer);
				if (axisMap.containsKey(collection.getKey()))
				{
					plot.mapDatasetToRangeAxis(collectionIndex, axisMap.get(collection.getKey()));
				}
				else
				{
					plot.setRangeAxis(collectionIndex, new NumberAxis(collection.getKey()));
					plot.mapDatasetToRangeAxis(collectionIndex, collectionIndex);
				}
				++collectionIndex;
			}
			if (throughputDataset.getSeriesCount() > 0)
			{
				XYItemRenderer lineRenderer = new StandardXYItemRenderer();
				plot.setDataset(collectionIndex, throughputDataset);
				plot.setRenderer(collectionIndex, lineRenderer);
				NumberAxis throughputAxis = new NumberAxis("Throughput (1/second)");
				throughputAxis.setAutoRangeIncludesZero(true);
				plot.setRangeAxis(collectionIndex, throughputAxis);
				plot.mapDatasetToRangeAxis(collectionIndex, collectionIndex);
				++collectionIndex;
			}
			NumberAxis timeAxis = new NumberAxis("Test time (ms)");
			timeAxis.setAutoRange(true);
			timeAxis.setAutoRangeIncludesZero(false);
			plot.setDomainAxis(timeAxis);
			plot.setDomainCrosshairVisible(true);
			plot.setRangeCrosshairVisible(true);
			jfChart = new JFreeChart(plot);
			jfChart.setBackgroundPaint(Color.WHITE);
		}
	}


	private void generateHistogramGraphics()
	{
		this.jfChart = null;
		if (chartDataValuesList != null && ! chartDataValuesList.isEmpty())
		{
			HistogramDataset histogramDataset = createHistogramDataset();
			if (histogramDataset.getSeriesCount() > 0)
			{
				PlotOrientation orientation = PlotOrientation.VERTICAL;
				boolean legend = true;
				boolean tooltips = true;
				boolean urls = true;
				JFreeChart chart = ChartFactory.createHistogram(
					"Distribution (" + numberOfSlices + " slices)",
					chartDataValuesList.get(0).yLabel,
					"occurrences",
					histogramDataset,
					orientation,
					legend,
					tooltips,
					urls);
				chart.getPlot().setForegroundAlpha(1f / histogramDataset.getSeriesCount());
				//save data for this current chart data
				this.jfChart = chart;
			}
		}
	}


	private void generateQuantileGraphics()
	{
		this.jfChart = null;
		if (chartDataValuesList != null && ! chartDataValuesList.isEmpty())
		{
			DefaultCategoryDataset quantileDataset = createQuantileDataset();
			if (quantileDataset.getRowCount() > 0)
			{
				JFreeChart chart = ChartFactory.createBarChart(
					null,         				// no title
					"Quantiles",				// quantiles axis label
					chartDataValuesList.get(0).yLabel, // domain axis label
					quantileDataset,			// data
					PlotOrientation.HORIZONTAL, // orientation
					true,                      // include legend
					true,
					false);
				CategoryPlot plot = (CategoryPlot) chart.getPlot();
				plot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
				plot.setRangePannable(true);
				BarRenderer renderer = (BarRenderer) plot.getRenderer();
				renderer.setItemLabelAnchorOffset(9.0);
				renderer.setBaseItemLabelsVisible(true);
				renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
				CategoryAxis categoryAxis = plot.getDomainAxis();
				categoryAxis.setCategoryMargin(0.25);
				categoryAxis.setUpperMargin(0.02);
				categoryAxis.setLowerMargin(0.02);
				NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
				rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
				rangeAxis.setUpperMargin(0.10);
				ChartUtilities.applyCurrentTheme(chart);
				this.jfChart = chart;
			}
		}
	}


	private DefaultCategoryDataset createQuantileDataset()
	{
		DefaultCategoryDataset ret = new DefaultCategoryDataset();
		double[] quantilesValues;
		double[] yValues = null;

		for (ChartDataValues currentData: chartDataValuesList)
		{
			int quantile = 100 / numberOfQuantiles;
			int quantilePercent = 0;
			yValues = toDoubleArray(currentData.getStol().getData());
			quantilesValues = quantiles(numberOfQuantiles-1, yValues);
			for (int i = 0; i < quantilesValues.length; i++)
			{
				quantilePercent += quantile;
				String columnKey = String.valueOf(quantilePercent);
				ret.addValue(
					quantilesValues[i],
					currentData.plotName,
					columnKey+" %");
			}
		}
		return ret;
	}


	/**
	 * Get all time-based XYSeries collections (both raw and moving statistics series)
	 * indexed by their y-axis label
	 * @return a map with y-axis labels as keys and XYSeries collections as values 
	 */
	private Map<String,XYSeriesCollection> createDatasetRaw(double initialTime, double finalTime)
	{
		int indexTimeStart;
		int indexTimeEnd;
		ListOfLong datesList;
		Map<String,XYSeriesCollection> result = new HashMap<String,XYSeriesCollection>();
		String yLabel;
		XYSeriesCollection collection;
		double[] dates;
		double[] values;

		for (ChartDataValues chartData: chartDataValuesList)
		{
			yLabel = chartData.getyLabel();
			collection = result.get(yLabel);
			if (collection == null)
			{
				collection = new XYSeriesCollection();
			}
			//create a XYDataset for these values
			datesList = chartData.getStolDate().getData();
			initialTime = getInitialTime();
			indexTimeStart = getStartTimeIndex(datesList);
			indexTimeEnd = getEndTimeIndex(datesList);
			dates = toDoubleArray(
				chartData.getStolDate().getData(),
				indexTimeStart,
				indexTimeEnd,
				chartData.getMaxPointsNb());
			values = toDoubleArray(
				chartData.getStol().getData(),
				indexTimeStart,
				indexTimeEnd,
				chartData.getMaxPointsNb());
			if (dates.length > 0)
			{
				if (chartData.chartTypesSet.contains(ChartType.RAW))
				{
					double[] xValues = dates; 
					double[] yValues = values; 
					XYSeries serieRaw = new XYSeries(chartData.plotName);
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length; i++)
						{
							serieRaw.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieRaw);
				}
			}
			if (collection.getSeriesCount() > 0 && !result.containsKey(yLabel))
			{
				result.put(yLabel, collection);
			}
		}
		return result;
	}


	/**
	 * Get all time-based XYSeries collections (both raw and moving statistics series)
	 * indexed by their y-axis label
	 * @return a map with y-axis labels as keys and XYSeries collections as values 
	 */
	private Map<String,XYSeriesCollection> createDatasetMoving(double initialTime, double finalTime)
	{
		int indexTimeStart;
		int indexTimeEnd;
		ListOfLong datesList;
		Map<String,XYSeriesCollection> result = new HashMap<String,XYSeriesCollection>();
		String yLabel;
		XYSeriesCollection collection;
		double[] dates;
		double[] values;
		int[] splitTime;

		for (ChartDataValues chartData: chartDataValuesList)
		{
			yLabel = chartData.getyLabel();
			collection = result.get(yLabel);
			if (collection == null)
			{
				collection = new XYSeriesCollection();
			}
			//create a XYDataset for these values
			datesList = chartData.getStolDate().getData();
			initialTime = getInitialTime();
			indexTimeStart = getStartTimeIndex(datesList);
			indexTimeEnd = getEndTimeIndex(datesList);
			dates = toDoubleArray(
				chartData.getStolDate().getData(),
				indexTimeStart,
				indexTimeEnd,
				chartData.getMaxPointsNb());
			values = toDoubleArray(
				chartData.getStol().getData(),
				indexTimeStart,
				indexTimeEnd,
				chartData.getMaxPointsNb());
			// date intervals for moving statistics
			splitTime = split(
				initialTime,
				finalTime,
				dates,
				chartData.getTimeWindow(),
				chartData.getStep());
			if (dates.length > 0)
			{
				if (chartData.chartTypesSet.contains(ChartType.MOVING_MIN))
				{
					double[][] xyValues = getMovingMinimum(
						getInitialTime(),
						values,
						splitTime,
						chartData.getTimeWindow(),
						chartData.getStep());
					double[] xValues = xyValues[0];
					double[] yValues = xyValues[1];
					XYSeries serieMovingMin = new XYSeries(chartData.plotName + ": min");
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length; i++)
						{
							serieMovingMin.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieMovingMin);
				}
				if(chartData.chartTypesSet.contains(ChartType.MOVING_MAX))
				{
					double[][] xyValues = getMovingMaximum(
						getInitialTime(),
						values,
						splitTime,
						chartData.getTimeWindow(),
						chartData.getStep()); 
					double[] xValues = xyValues[0];
					double[] yValues = xyValues[1];
					XYSeries serieMovingMax =new XYSeries(chartData.plotName + ": max");
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length; i++)
						{
							serieMovingMax.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieMovingMax);
				}
				if(chartData.chartTypesSet.contains(ChartType.MOVING_AVERAGE))
				{
					double[][] xyValues = getMovingAverages(
						getInitialTime(),
						values,
						splitTime,
						chartData.getTimeWindow(),
						chartData.getStep()); 
					double[] xValues = xyValues[0];
					double[] yValues = xyValues[1];
					XYSeries serieMovingAverage =new XYSeries(chartData.plotName + ": average");
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length; i++)
						{
							serieMovingAverage.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieMovingAverage);
				}
				if(chartData.chartTypesSet.contains(ChartType.MOVING_MEDIAN))
				{
					double[][] xyValues = getMovingMedians(
						getInitialTime(),
						values,
						splitTime,
						chartData.getTimeWindow(),
						chartData.getStep());
					double[] xValues = xyValues[0];
					double[] yValues = xyValues[1];
					XYSeries serieMovingMedian =new XYSeries(chartData.plotName + ": median");
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length; i++)
						{
							serieMovingMedian.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieMovingMedian);
				}
				if(chartData.chartTypesSet.contains(ChartType.MOVING_STDDEV))
				{
					double[][] xyValues = getMovingStandardDeviations(
						getInitialTime(),
						values,
						splitTime,
						chartData.getTimeWindow(),
						chartData.getStep());
					double[] xValues = xyValues[0];
					double[] yValues = xyValues[1];
					XYSeries serieMovingStandardDeviation = new XYSeries(chartData.plotName + ": standard deviation");
					if( xValues.length == yValues.length)
					{
						for (int i = 0; i < xValues.length;i++)
						{
							serieMovingStandardDeviation.add(xValues[i], yValues[i]);
						}
					}
					collection.addSeries(serieMovingStandardDeviation);
				}
			}
			if (collection.getSeriesCount() > 0 && !result.containsKey(yLabel))
			{
				result.put(yLabel, collection);
			}
		}
		return result;
	}


	private XYSeriesCollection createDatasetThroughtput(double initialTime, double finalTime)
	{
		XYSeriesCollection movingThroughputDataset = new XYSeriesCollection();
		int indexTimeStart;
		int indexTimeEnd;
		for (ChartDataValues chartData: chartDataValuesList)
		{
			ListOfLong datesList = chartData.getStolDate().getData();
			indexTimeStart = getStartTimeIndex(datesList);
			indexTimeEnd = getEndTimeIndex(datesList);
			double[] dates = toDoubleArray(
				chartData.getStolDate().getData(),
				indexTimeStart,
				indexTimeEnd,
				chartData.getMaxPointsNb());
			// date intervals for moving statistics
			int[] splitTime = split(
				initialTime,
				finalTime,
				dates,
				chartData.getTimeWindow(),
				chartData.getStep());
			//create a XYDataset for these values
			String plotName =  chartData.plotName;
			if(chartData.chartTypesSet.contains(ChartType.MOVINGTHROUGHPUT))
			{
				double[][] xyValues = getMovingThroughput(
					getInitialTime(),
					splitTime,
					chartData.getTimeWindow(),
					chartData.getStep());
				double[] xValues = xyValues[0];
				double[] yValues = xyValues[1];
				XYSeries serieMovingThroughput = new XYSeries(plotName + ": Throughput");
				if( xValues.length == yValues.length){
					for (int i = 0; i < xValues.length-1; i++){
						serieMovingThroughput.add(xValues[i], yValues[i]);
					}
				}
				movingThroughputDataset.addSeries(serieMovingThroughput);
			}
		}
		return movingThroughputDataset;
	}


	private HistogramDataset createHistogramDataset()
	{
		HistogramDataset ret = new HistogramDataset();
		for (ChartDataValues currentData : chartDataValuesList)
		{
			double[] yValues = null;
			if (currentData.getDataset().isPerformDraw())
			{
				yValues = toDoubleArray(currentData.getStol().getData());
				if (yValues.length > 0)
				{
					ret.addSeries(currentData.plotName, yValues, numberOfSlices);
				}
			}
		}
		return ret;
	}


	/**
	 * Get quantiles
	 * @param n
	 * @param values
	 * @return array that contains the quantiles
	 */
	public double[] quantiles(int n, double[] values)
	{
		if (values.length > 0)
		{
			values = (double[])values.clone();
			Arrays.sort(values);
			double[] qtls = new double[n+1];
			for ( int i=0; i<=n; ++i ) {
				qtls[i] = values[((values.length-1)*i)/n];
			}
			return qtls;
		}
		else
		{
			return new double[0];
		}
	}


	/**
	 * Computes the moving minimum of values, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param values array of all values on which moving statistics are computed
	 * @param splitTime indexes of starting/ending values for each step.
	 * For example, the first value in step i is located at index splitTime[2*i] in the values parameter,
	 * and the last one is located at index splitTime[2*i+1].
	 * @param timeWindow the width (in ms) of the time window over which moving statistics are computed 
	 * @param step the progression step (in ms) for moving statistics
	 * @return moving minimum of the given values, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in ms),
	 * defined for each step by the middle date between i*step and i*step + time window
	 * [1] contains the array of values minimum over the time window through all steps
	 */
	public double[][] getMovingMinimum(double initialTime, double[] values, int[] splitTime, int timeWindow, int step)
	{
		double[][] result = new double[2][];
		double[] minValues = new double[splitTime.length / 2];
		double[] timeValues = new double[splitTime.length / 2];
		int resultLength = 0;
		for (int i = 0 ; i < splitTime.length ; i += 2)
		{
			if (splitTime[i] != -1)
			{
				timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
				minValues[resultLength] = StatUtils.min(
					values,
					splitTime[i],
					1 + splitTime[i+1] - splitTime[i]);
				++resultLength;
			}
		}
		result[0] = Arrays.copyOf(timeValues, resultLength);
		result[1] = Arrays.copyOf(minValues, resultLength);
		return result;
	}


	/**
	 * Computes the moving maximum of values, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param values array of all values on which moving statistics are computed
	 * @param splitTime indexes of starting/ending values for each step.
	 * For example, the first value in step i is located at index splitTime[2*i] in the values parameter,
	 * and the last one is located at index splitTime[2*i+1].
	 * @param timeWindow the width (in ms) of the time window over which moving statistics are computed 
	 * @param step the progression step (in ms) for moving statistics
	 * @return moving maximum of the given values, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in ms),
	 * defined for each step by the middle date between i*step and i*step + time window
	 * [1] contains the array of values maximum over the time window through all steps
	 */
	public double[][] getMovingMaximum(double initialTime, double[] values, int[] splitTime, int timeWindow, int step)
	{
		double[][] result = new double[2][];
		double[] maxValues = new double[splitTime.length / 2];
		double[] timeValues = new double[splitTime.length / 2];
		int resultLength = 0;
		for (int i = 0; i < splitTime.length ; i += 2)
		{
			if (splitTime[i] != -1)
			{
				timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
				maxValues[resultLength] = StatUtils.max(
					values,
					splitTime[i],
					1 + splitTime[i+1] - splitTime[i]);
				++resultLength;
			}
		}
		result[0] = Arrays.copyOf(timeValues, resultLength);
		result[1] = Arrays.copyOf(maxValues, resultLength);
		return result;
	}


	/**
	 * Computes the moving average of values, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param values array of all values on which moving statistics are computed
	 * @param splitTime indexes of starting/ending values for each step.
	 * For example, the first value in step i is located at index splitTime[2*i] in the values parameter,
	 * and the last one is located at index splitTime[2*i+1].
	 * @param timeWindow the width (in ms) of the time window over which moving statistics are computed 
	 * @param step the progression step (in ms) for moving statistics
	 * @return moving average of the given values, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in ms),
	 * defined for each step by the middle date between i*step and i*step + time window
	 * [1] contains the array of values average over the time window through all steps
	 */
	public double[][] getMovingAverages(double initialTime, double[] values, int[] splitTime, int timeWindow, int step)
	{
		double[][] result = new double[2][];
		double[] meanValues = new double[splitTime.length / 2];
		double[] timeValues = new double[splitTime.length / 2];
		int resultLength = 0;
		for (int i = 0; i < splitTime.length ; i += 2)
		{
			if (splitTime[i] != -1)
			{
				timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
				meanValues[resultLength] = StatUtils.mean(
					values,
					splitTime[i],
					1 + splitTime[i+1] - splitTime[i]);
				++resultLength;
			}
		}
		result[0] = Arrays.copyOf(timeValues, resultLength);
		result[1] = Arrays.copyOf(meanValues, resultLength);
		return result;
	}


	/**
	 * Computes the moving median of values, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param values array of all values on which moving statistics are computed
	 * @param splitTime indexes of starting/ending values for each step.
	 * For example, the first value in step i is located at index splitTime[2*i] in the values parameter,
	 * and the last one is located at index splitTime[2*i+1].
	 * @param timeWindow the width (in ms) of the time window over which moving statistics are computed 
	 * @param step the progression step (in ms) for moving statistics
	 * @return moving median of the given values, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in ms),
	 * defined for each step by the middle date between i*step and i*step + time window
	 * [1] contains the array of values median over the time window through all steps
	 */
	public double[][] getMovingMedians(double initialTime, double[] values, int[] splitTime, int timeWindow, int step)
	{
		double[][] result = new double[2][];
		double[] medianValues = new double[splitTime.length];
		double[] timeValues = new double[splitTime.length];
		int resultLength = 0;
		for (int i = 0; i < splitTime.length ; i += 2)
		{
			if (splitTime[i] != -1)
			{
				timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
				medianValues[resultLength] = getMovingMedians(
					values,
					splitTime[i],
					splitTime[i+1]);
				++resultLength;
			}
		}
		result[0] = Arrays.copyOf(timeValues, resultLength);
		result[1] = Arrays.copyOf(medianValues, resultLength);
		return result;
	}


	/**
	 * Get the median of a portion of an array of values
	 * @param values the full array of values
	 * @param beginIndex the starting index (starting from 0) of the portion
	 * @param endIndex the ending index (inclusive) of the portion
	 * @return the values median of the portion
	 */
	private double getMovingMedians(double[] values, int beginIndex, int endIndex)
	{
		assert beginIndex >= 0 : "begin index must be greater than or equal to zero";
		assert endIndex >= beginIndex : "end index must be greater or equal to begin index";
		assert endIndex < values.length : "end index must be less than the size of the list of values";
		// get the portion
		double[] portion = Arrays.copyOfRange(values, beginIndex, endIndex + 1);
		// sort the portion
		Arrays.sort(portion);
		// get the value at the middle of the portion
		if (portion.length % 2 == 0)
		{
			return (values[portion.length / 2 - 1] + values[portion.length / 2]) / 2;
		}
		else
		{
			return values[portion.length / 2];
		}
	}


	/**
	 * Computes the moving standard deviation of values, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param values array of all values on which moving statistics are computed
	 * @param splitTime indexes of starting/ending values for each step.
	 * For example, the first value in step i is located at index splitTime[2*i] in the values parameter,
	 * and the last one is located at index splitTime[2*i+1].
	 * @param timeWindow the width (in ms) of the time window over which moving statistics are computed 
	 * @param step the progression step (in ms) for moving statistics
	 * @return moving standard deviation of the given values, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in ms),
	 * defined for each step by the middle date between i*step and i*step + time window
	 * [1] contains the array of values standard deviation over the time window through all steps
	 */
	public double[][] getMovingStandardDeviations(double initialTime, double[] values, int[] splitTime, int timeWindow, int step)
	{
		double[][] result = new double[2][];
		double[] devValues = new double[splitTime.length / 2];
		double[] timeValues = new double[splitTime.length / 2];
		int resultLength = 0;
		for (int i = 0; i < splitTime.length ; i += 2)
		{
			if (splitTime[i] != -1)
			{
				timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
				devValues[resultLength] = Math.sqrt(
					StatUtils.populationVariance(
						values,
						splitTime[i],
						1 + splitTime[i+1] - splitTime[i]));
				++resultLength;
			}
		}
		result[0] = Arrays.copyOf(timeValues, resultLength);
		result[1] = Arrays.copyOf(devValues, resultLength);
		return result;
	}


	/**
	 * Computes the moving throughput of event occurrences, according to a given time window and progression step.
	 * @param initialTime starting time (in ms) of the first time step
	 * @param splitTime indexes of starting/ending event occurrences for each step.
	 * For example, the first occurrence in step i is occurrence number splitTime[2*i],
	 * and the last one is occurrence number splitTime[2*i+1].
	 * @param timeWindow the width (in milliseconds) of the time window over which moving statistics are computed 
	 * @param step the progression step (in milliseconds) for moving statistics
	 * @return moving throughput of the given measurements, as an array of two series of numbers:
	 * [0] contains the array of dates (of type int, in milliseconds),
	 * defined by the middle dates between i*step and i*step + time window
	 * [1] contains the array of event throughput over the time window through all steps
	 */
	public double[][] getMovingThroughput(double initialTime, int[] splitTime, int timeWindow, int step) 
	{
		double[][] result = new double[2][];
		double[] thrpValues = new double[splitTime.length / 2];
		double[] timeValues = new double[splitTime.length / 2];
		int resultLength = 0;
		for (int i = 0; i < splitTime.length ; i += 2)
		{
			timeValues[resultLength] = initialTime + (timeWindow + i * step) / 2.0;
			if (splitTime[i] == -1)
			{
				thrpValues[resultLength] = 0.0;
			}
			else
			{
				thrpValues[resultLength] = 1000.0 * (1 + splitTime[i + 1] - splitTime[i]) / timeWindow;
			}
			++resultLength;
		}
		result[0] = timeValues;
		result[1] = thrpValues;
		return result;
	}


	////////////////////////////////////
	//  Numeric types conversions
	////////////////////////////////////


	static private double[] toDoubleArray(final ListOfLong longList)
	{
		return toDoubleArray(longList, 0, longList.size()-1, -1);
	}

	
	/**
	 * Converts a list of Long to an array of doubles, optionally with a maximum
	 * number of elements (thus ignoring trailing elements).
	 *
	 * @param longList the given list of Long objects
	 * @param maxPoints the maximum number of elements to convert and copy to
	 * the resulting array of doubles, or -1 for no limit
	 * @return the resulting array of doubles
	 */
	static private double[] toDoubleArray(final ListOfLong longList, int fromIndex, int toIndex, final int maxPoints)
	{
		int size = 1 + toIndex - fromIndex; 
		if (longList.size() < size)
		{
			size = longList.size();
		}
		if (maxPoints != -1 && maxPoints < size)
		{
			size = maxPoints;
		}
		if (size < 0)
		{
			size = 0;
		}
		double[] result = new double[size];
		if (size > 0)
		{
			Iterator<Long> iter = longList.subList(fromIndex, fromIndex+size).iterator();
			for (int i = 0 ; i < size ; ++i)
			{
				result[i] = iter.next();
			}
		}
		return result;
	}


	/**
	 * Gets the index of the first value from the given list
	 * greater or equal to startTime.
	 *
	 * @param dates ascending ordered list of time values
	 * @return the index of the first value greater than or equal
	 * to startTime, or the array size if dates contains no value
	 * greater than or equal to startTime. When dates is null or
	 * empty, the return value is 0.
	 */
	private int getStartTimeIndex(ListOfLong dates)
	{
		int i = 0;
		if (startTime != -1 && dates != null)
		{
			Iterator<Long> iter = dates.iterator();
			while (iter.hasNext() && iter.next() < startTime)
			{
				++i;
			}
		}
		return i;
	}


	/**
	 * Gets the index of the last/greater value from the given list
	 * less or equal to endTime.
	 *
	 * @param dates ascending ordered list of time values
	 * @return the index of the last value less than or equal to
	 * endTime, or -1 if dates contains no value less than or equal
	 * to endTime. When dates is null or empty, the return value is -1.
	 */
	private int getEndTimeIndex(ListOfLong dates)
	{
		int i = (dates == null ? -1 : dates.size() - 1);
		if (endTime != -1 && dates != null)
		{
			ListIterator<Long> iter = dates.listIterator(dates.size());
			while (iter.hasPrevious() && iter.previous() > endTime)
			{
				--i;
			}
		}
		return i;
	}


	/**
	 * Utility method for moving statistics:
	 * computes a sequence of measurement indexes corresponding to time windows [i*step, i*step+timeWindow[.
	 * For step #i, the computed array elements consists of a pair of int values,
	 * located in the array at indexes i*2 and i*2+1
	 * respectively giving the index of the first measurement in the current time window,
	 * and the index of the last measurement in the current time window;
	 * when there is no measurement in the current time window, this pair of indexes is set to (-1, -1).
	 * Note: assumes that the provided list of dates is immutable so that some valuable caching can be performed
	 * based on the object reference of the list object.
	 * @param datesList the list of dates of measurements (assumed to be in chronological/growing order)
	 * @param timeWindow the width (in milliseconds) of the time window over which moving statistics are computed 
	 * @param step the progression step (in milliseconds) for moving statistics
	 * @return the sequence of measurement starting/ending pair of indexes corresponding to time intervals defined
	 * by the given time window and progression step.
	 */
	private int[] split(double initialTime, double finalTime, double[] dates, int timeWindow, int step)
	{
		// the array of dates is supposed to be sorted in growing order, so the biggest value is at the end
		int[] result = new int[(int) (1 + (finalTime - initialTime) / step) * 2];
		// -1 means there is no value for the corresponding step
		Arrays.fill(result, -1);

		for (int i=0 ; i<dates.length ; ++i)
		{
			int stepIndex = (int)((dates[i] - initialTime) / step);
			for (int j = stepIndex - (timeWindow / step) ; j < stepIndex + (timeWindow / step) ; ++j)
			{
				if (j >= 0 && j < result.length/2 && dates[i] >= j * step + initialTime && dates[i] < j * step + timeWindow + initialTime)
				{
					int resultIndex = j * 2;
					if(result[resultIndex] == -1)
					{
						result[resultIndex] = i;
						result[resultIndex + 1] = i;
					}
					else
					{
						if (result[resultIndex] > i)
						{
							result[resultIndex] = i;
						}
						if (result[resultIndex + 1] < i)
						{
							result[resultIndex + 1] = i;
						}
					}
				}
			}
		}
		return result;
	}


	////////////////////////////////////
	// getters and setters
	////////////////////////////////////

	public JFreeChart getJfChart() {
		return jfChart;
	}

	public void setJfChart(JFreeChart jfChart) {
		this.jfChart = jfChart;
	}

	public ChartRepresentation getRepresentation() {
		return representation;
	}

	public void setRepresentation(ChartRepresentation representation) {
		this.representation = representation;
	}


	public void addChartDataValues(List<ChartDataValues> values)
	{
		if (values != null)
		{
			chartDataValuesList.addAll(values);
		}
	}


	public void clearChartDataValues()
	{
		chartDataValuesList.clear();
	}

	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * Gets the forced chart starting time.
	 * @return -1 if there is no forced starting time,
	 * or the forced chart starting time in ms. 
	 */
	public long getStartTime() {
		return startTime;
	}


	/**
	 * Forces the chart starting time, or leaves it to its default
	 * @param startTime a positive chart starting time in ms for all
	 * datasets, or -1 to use the default starting time.
	 */
	public void setStartTime(long startTime)
	{
		this.startTime = startTime;
	}


	/**
	 * Gets the forced chart ending time.
	 * @return -1 if there is no forced ending time,
	 * or the forced chart ending time in ms. 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * Forces the chart ending time, or leaves it to its default
	 * @param endTime a positive chart ending time in ms for all
	 * datasets, or -1 to use the default ending time.
	 */
	public void setEndTime(long endTime)
	{
		this.endTime = endTime;
	}


	/**
	 * Gets the default chart starting time, i.e. the minimum
	 * time through all values of all datasets with a time-based
	 * chart enabled (scatter plot and/or moving statistics).
	 * @return the default chart starting time, or -1 if there is
	 * no time-based chart at all.
	 */
	public long getDefaultStartTime()
	{
		long defaultStartTime = Long.MAX_VALUE;
		for (ChartDataValues chartData : chartDataValuesList)
		{
			Dataset dataset = chartData.getDataset();
			if (dataset != null
				&& dataset.isPerformDraw()
				&& dataset.isPerformTimeChart())
			{
				long candidate = dataset.getFirstEventTimeAfterFilter();
				if (candidate != -1 && candidate < defaultStartTime)
				{
					defaultStartTime = candidate;
				}
			}
		}
		if (defaultStartTime == Long.MAX_VALUE)
		{
			defaultStartTime = -1;
		}
		return defaultStartTime;
	}


	/**
	 * Gets the default chart ending time, i.e. the maximum
	 * time through all values of all datasets with a time-based
	 * chart enabled (scatter plot and/or moving statistics).
	 * @return the default chart ending time, or -1 if there is
	 * no time-based chart at all.
	 */
	public long getDefaultEndTime()
	{
		long defaultEndTime = Long.MIN_VALUE;
		for (ChartDataValues chartData : chartDataValuesList)
		{
			Dataset dataset = chartData.getDataset();
			if (dataset != null
				&& dataset.isPerformDraw()
				&& dataset.isPerformTimeChart())
			{
				long candidate = dataset.getLastEventTimeAfterFilter();
				if (candidate != -1 && candidate > defaultEndTime)
				{
					defaultEndTime = candidate;
				}
			}
		}
		if (defaultEndTime == Long.MIN_VALUE)
		{
			defaultEndTime = -1;
		}
		return defaultEndTime;
	}


	public long getInitialTime()
	{
		if (startTime == -1)
		{
			return getDefaultStartTime();
		}
		else
		{
			return startTime;
		}
	}


	public long getFinalTime()
	{
		if (endTime == -1)
		{
			return getDefaultEndTime();
		}
		else
		{
			return endTime;
		}
	}


	public int getNumberOfSlices() {
		return numberOfSlices;
	}


	public void setNumberOfSlices(int numberOfSlices) {
		this.numberOfSlices = numberOfSlices;
	}



	public int getNumberOfQuantils() {
		return numberOfQuantiles;
	}



	public void setNumberOfQuantils(int i) {
		this.numberOfQuantiles = i;
	}



	public String exportToTxt(int nbTabs, File directory, String sectionName, imageFormats imageFormat) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer(tabs + "Chart\n");
		text.append(tabs + "  title: " + title + "\n");

		switch (representation){
		case TIME_BASED:
			text.append(tabs + "  TIME_BASED\n");
			break;
		case HISTOGRAM:
			text.append(tabs + "  HISTOGRAM "+ numberOfSlices + " slices.\n");
			break;
		case QUANTILE:
			text.append(tabs + "  QUANTILE "+ numberOfQuantiles + " quantiles.\n");
			break;
		default:
			;
		}

		text.append(tabs + "  startTime:   " + startTime + " ms\n");
		text.append(tabs + "  endTime: "     + endTime   + " ms\n");
/* disables the output of chart's points coordinates - most probably too verbose!
		text.append(tabs + "  ChartDataValues:\n");
		for (ChartDataValues cdv : chartDataValuesList){
			text.append(cdv.exportToTxt(nbTabs+1));
		}
*/
		String fileName = generateChartFile(sectionName, imageFormat, directory);
		if (fileName == null)
		{
			text.append(tabs).append("  empty chart.\n");
		}
		else
		{
			text.append(tabs + "  chart file: \"" + directory.getName() + File.separator + fileName + "\"\n");
		}
		return text.toString();
	}






/*	
 * Tomas version
 * 
 * public String exportToXml(int nbTabs, File directory, String sectionName, imageFormats imageFormat) {
	
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		StringBuffer text = new StringBuffer(tabs + "Chart\n");
		text.append(tabs + "  title: " + title + "\n");

		switch (representation){
		case TIME_BASED:
			text.append(tabs + "  TIME_BASED\n");
			break;
		case HISTOGRAM:
			text.append(tabs + "  HISTOGRAM "+ numberOfSlices + " slices.\n");
			break;
		case QUANTILE:
			text.append(tabs + "  QUANTILE "+ numberOfQuantiles + " quantiles.\n");
			break;
		case BOXPLOT:
			text.append(tabs + "  BOXPLOT\n");
			break;
		default:
			;
		}

		text.append(tabs + "  startTime:   " + startTime + " ms\n");
		text.append(tabs + "  endTime: "     + endTime   + " ms\n");

		text.append(tabs + "  ChartDataValues:\n");
		for (ChartDataValues cdv : chartDataValuesList){
			text.append(cdv.exportToXml(nbTabs+1));
		}

		String fileName = generateChartFile(sectionName, imageFormat, directory);
		text.append(tabs + "  chart file: \"" + directory.getName() + File.separator + fileName + "\"\n");

		return text.toString();
	}
*/

	public String exportToHtml(int tabsMem, File directory, String sectionName, imageFormats imageFormat) {
		StringBuffer htmlText = new StringBuffer("");
		String fileName = generateChartFile(sectionName, imageFormat, directory);
		if (fileName == null)
		{
			LogAndDebug.htmlWrite(htmlText, "<span class=\"notice\">Empty chart</span>");
		}
		else
		{
			LogAndDebug.htmlWriteNP(htmlText, "<figure>");
			LogAndDebug.htmlTabsp();
			LogAndDebug.htmlWriteNP(htmlText, "<figcaption>" + getTitle() + ": ");
			LogAndDebug.htmlTabsp();
			switch (getRepresentation())
			{
				case TIME_BASED:
					LogAndDebug.htmlWriteNP(htmlText, "time-based chart.");
					break;
				case HISTOGRAM:
					LogAndDebug.htmlWriteNP(htmlText, "histogram with " + getNumberOfSlices() + " slices.");
					break;
				case QUANTILE:
					LogAndDebug.htmlWriteNP(htmlText, getNumberOfQuantils() + " quantiles chart.");
					break;
			}
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</figcaption>");
			LogAndDebug.htmlWriteNP(htmlText, "<img");
			LogAndDebug.htmlTabsp();
			LogAndDebug.htmlWriteNP(htmlText, "src=\"" + directory.getName() + "/" + fileName + "\"");
			LogAndDebug.htmlWriteNP(htmlText, "alt=\"" + sectionName + "\"");
			LogAndDebug.htmlWriteNP(htmlText, "title=\"" + sectionName + "\"");
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "/>");
			LogAndDebug.htmlTabsm();
			LogAndDebug.htmlWriteNP(htmlText, "</figure>");
		}
		return htmlText.toString();
	}


	public  void exportToXML(int tabsMem, File directory, String sectionName, imageFormats imageFormat) {
//		StringBuffer htmlText = new StringBuffer("");
		String fileName = generateChartFile(sectionName, imageFormat, directory);
		if (fileName == null)
		{
			System.out.println("  empty chart.\n");
		}
		else
		{
			System.out.println(tabsMem + "  chart file: \"" + directory.getName() + File.separator + fileName + "\"\n");
		}
	}


	private String generateChartFile(String sectionName, imageFormats imageFormat, File directory) {
		// generate chart file
		String label = null;
		if (! chartDataValuesList.isEmpty() && jfChart != null)
		{
			String imageSuffix;
			switch (imageFormat) {
			case PNG_FORMAT:
				imageSuffix = "png";
				break;
			case JPG_FORMAT:
				imageSuffix = "jpg";
				break;
			case SVG_FORMAT:
				imageSuffix = "svg";
				break;
			default:
				imageSuffix = "png";
				break;
			}
			label =
				sectionName
				+ "-"
				+ chartDataValuesList.get(0).getyLabel()
				+ "." + imageSuffix;
			writeChartImage(new File(directory, label), imageSuffix, jfChart);
		}
		return label;
	}


	public String toText(double[] vect){
		String ret = "[";
		String pre = "";
		String post = "";
		for (double d : vect){
			ret += pre + Double.toString(d);
			pre = ", ";
			post = "]";
		}
		return ret + post;
	}


	public Section getSection(){
		return section;
	}


	public String dump() {
		StringBuffer text = new StringBuffer(""); 
		String tabs = "\t\t\t";
		text.append(tabs + "  title: \"" + title + "\"\n");
		text.append(tabs + "  section: \"" + LogAndDebug.toText(section) + "\"\n");
		text.append(tabs + "  startTime: " + startTime + "\n");
		text.append(tabs + "  endTime: " + endTime + "\n");
		text.append(tabs + "  numberOfSlices: " + numberOfSlices + "\n");
		text.append(tabs + "  numberOfQuantiles: " + numberOfQuantiles + "\n");
		text.append(tabs + "  jfChart: " + jfChart + "\n");
		text.append("\n");
		return text.toString();
	}




	/**
	 *  Write chart image.
	 * 
	 * @param fileName
	 *            the file name
	 * @param imageFormat
	 *            the image format
	 * @param chartId
	 *            the chart id
	 * 
	 * @return true, if successful
	 */
	private boolean writeChartImage(File target, String imageFormat, JFreeChart jFreeChart)
	{
		assert jFreeChart != null : "this method should not be called with a null JFreeChart argument";
		BufferedImage img = jFreeChart.createBufferedImage(700, 400);
		try
		{
			FileOutputStream output = new FileOutputStream(target);
			if (imageFormat.equalsIgnoreCase("PNG"))
			{
				ChartUtilities.writeBufferedImageAsPNG(output, img);
				output.close();
				return true;
			} else if (imageFormat.equalsIgnoreCase("JPG"))
			{
				ChartUtilities.writeBufferedImageAsJPEG(output, img);
				output.close();
				return true;
			} else if (imageFormat.equalsIgnoreCase("JPEG"))
			{
				ChartUtilities.writeBufferedImageAsJPEG(output, img);
				output.close();
				return true;
			} else if (imageFormat.equalsIgnoreCase("SVG"))
			{
				DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
				Document document = domImpl.createDocument(null, "svg", null);
				SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
				Writer out = new OutputStreamWriter(output, "UTF-8");

				jFreeChart.draw(svgGenerator, new Rectangle2D.Double(0, 0, 400, 300), null);

				svgGenerator.stream(out, true);
				output.close();
				return true;
			}
			output.close();
			return false;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
