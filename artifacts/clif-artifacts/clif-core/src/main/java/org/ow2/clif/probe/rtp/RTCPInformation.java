/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

/**
 * Class to keep informations about RTCP packets. 
 * 
 * @author Rémi Druilhe
 */
public class RTCPInformation 
{
	private Integer packetType;
	private Integer count;
	private Integer length;
	private Long time;
	
	public RTCPInformation(Integer packetType, Integer count, Integer length, Long time)
	{
		this.packetType = packetType;
		this.count = count;
		this.length = length;
		this.time = time;
	}
	
	// GET methods
	public Integer getPacketType()
	{
		return packetType;
	}
	
	public Integer getCount()
	{
		return count;
	}
	
	public Integer getLength()
	{
		return length;
	}
	
	public Long getTime()
	{
		return time;
	}
	
	// SET methods
	public void setPacketType(Integer packetType)
	{
		this.packetType = packetType;
	}
	
	public void setCount(Integer count)
	{
		this.count = count;
	}
	
	public void setLength(Integer length)
	{
		this.length = length;
	}
	
	public void setTime(Long time)
	{
		this.time = time;
	}
}
