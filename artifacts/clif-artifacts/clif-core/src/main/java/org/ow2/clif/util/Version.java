/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * Gives CLIF's version (including compilation date and time)
 * @author Bruno Dillenseger
 */
public class Version
{
	static public final String version_filename = "compile.timestamp";
	static private String timestamp;


	static
	{
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(
				new InputStreamReader(
					Version.class.getClassLoader().getResourceAsStream(version_filename)));
			timestamp = reader.readLine();
		}
		catch (IOException ex)
		{
			ex.printStackTrace(System.err);
			timestamp = "(unable to get compilation timestamp)";
		}
		if (reader != null)
		{
			try
			{
				reader.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}


	static public String getVersion()
	{
		return timestamp;
	}


	static public void main(String[] args)
	{
		System.out.println(timestamp);
	}
}
