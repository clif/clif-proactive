/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.lib;

import java.util.Map;
import org.etsi.uri.gcm.api.type.GCMTypeFactory;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.proactive.core.component.Constants;
import org.objectweb.proactive.core.component.ContentDescription;
import org.objectweb.proactive.core.component.ControllerDescription;
import org.objectweb.proactive.core.component.Utils;
import org.objectweb.proactive.core.component.factory.PAGenericFactory;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.server.api.BladeType;
import org.ow2.clif.server.api.Synchronizer;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.api.StorageWrite;
import org.ow2.clif.storage.lib.filestorage.FileStorageProxyImpl;
import org.ow2.clif.supervisor.api.SupervisorInfo;

public class Blade implements BladeType{

	public Blade(){}

	public Component createBlade(String bladeId, Map<String, Object> adlParams) throws InstantiationException, NoSuchInterfaceException, IllegalContentException, IllegalLifeCycleException, IllegalBindingException {
        Component boot = Utils.getBootstrapComponent();
        GCMTypeFactory tf = GCM.getGCMTypeFactory(boot);
        PAGenericFactory gf = Utils.getPAGenericFactory(boot);
        //Creating the composite (blade) component
        Type bladeType = tf.createFcType(new InterfaceType[]{
			tf.createFcItfType(StorageProxyAdmin.STORAGEPROXY_ADMIN, StorageProxyAdmin.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(DataCollectorAdmin.DATA_COLLECTOR_ADMIN, DataCollectorAdmin.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(BladeControl.BLADE_CONTROL, BladeControl.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(SupervisorInfo.SUPERVISOR_INFO, SupervisorInfo.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE)//TODO mandatory
        });
		Component blade = gf.newFcInstance(bladeType, new ControllerDescription(bladeId, Constants.COMPOSITE),
                new ContentDescription(CFractiveComposite.class.getName(), new Object[] {}));

		//Creating the insert-adapter
        Type insertAdapterType = tf.createFcType(new InterfaceType[]{
			tf.createFcItfType(BladeControl.BLADE_CONTROL, BladeControl.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(BladeInsertResponse.BLADE_INSERT_RESPONSE, BladeInsertResponse.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(StorageProxyAdmin.STORAGEPROXY_ADMIN, StorageProxyAdmin.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(SupervisorInfo.SUPERVISOR_INFO, SupervisorInfo.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),//TODO Mandatory!!
			tf.createFcItfType(BladeControl.BLADE_INSERT_CONTROL, BladeControl.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(DataCollectorWrite.DATA_COLLECTOR_WRITE, DataCollectorWrite.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
        });
		Component insertAdapter = gf.newFcInstance(insertAdapterType, new ControllerDescription("insert-adapter", Constants.PRIMITIVE),
				new ContentDescription(BladeInsertAdapterImpl.class.getName(), new Object[] {}));

		//creating insert
		Type insertType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType(BladeControl.BLADE_INSERT_CONTROL, BladeControl.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType(Synchronizer.SYNCHRONIZER, Synchronizer.class.getName(), TypeFactory.SERVER, TypeFactory.OPTIONAL, TypeFactory.SINGLE),
			tf.createFcItfType(DataCollectorWrite.DATA_COLLECTOR_WRITE, DataCollectorWrite.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(BladeInsertResponse.BLADE_INSERT_RESPONSE, BladeInsertResponse.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
		});
		Component insert = gf.newFcInstance(insertType, new ControllerDescription("insert", Constants.PRIMITIVE),
				new ContentDescription((String)adlParams.get("insert"), new Object[] {}));

		//creating data-collector
		Type dataCollectorType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType(DataCollectorAdmin.DATA_COLLECTOR_ADMIN, DataCollectorAdmin.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(DataCollectorWrite.DATA_COLLECTOR_WRITE, DataCollectorWrite.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(StorageWrite.STORAGE_WRITE, StorageWrite.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
		});
		Component dataCollector = gf.newFcInstance(dataCollectorType, new ControllerDescription("data-collector", Constants.PRIMITIVE),
				new ContentDescription((String)adlParams.get("datacollector"), new Object[] {}));

		//creating storage-proxy
		Type storageProxyType = tf.createFcType(new InterfaceType[] {
				tf.createFcItfType(StorageProxyAdmin.STORAGEPROXY_ADMIN, StorageProxyAdmin.class.getName(), TypeFactory.SERVER, TypeFactory.OPTIONAL, TypeFactory.SINGLE),
				tf.createFcItfType(StorageWrite.STORAGE_WRITE, StorageWrite.class.getName(), TypeFactory.SERVER, TypeFactory.OPTIONAL, TypeFactory.SINGLE),
		});
		Component storageProxy = gf.newFcInstance(storageProxyType, new ControllerDescription("storage-proxy", Constants.PRIMITIVE),
				new ContentDescription(FileStorageProxyImpl.class.getName(), new Object[] {}));

		//bindings
		ContentController cc = GCM.getContentController(blade);
		cc.addFcSubComponent(insertAdapter);
		cc.addFcSubComponent(insert);
		cc.addFcSubComponent(dataCollector);
		cc.addFcSubComponent(storageProxy);
		BindingController bcBlade = GCM.getBindingController(blade);
        BindingController bcInsertAdapter = GCM.getBindingController(insertAdapter);
        BindingController bcInsert = GCM.getBindingController(insert);
        BindingController bcDataCollector = GCM.getBindingController(dataCollector);
        bcBlade.bindFc(BladeControl.BLADE_CONTROL, insertAdapter.getFcInterface(BladeControl.BLADE_CONTROL));
        bcBlade.bindFc(StorageProxyAdmin.STORAGEPROXY_ADMIN, storageProxy.getFcInterface(StorageProxyAdmin.STORAGEPROXY_ADMIN));
        bcBlade.bindFc(DataCollectorAdmin.DATA_COLLECTOR_ADMIN, dataCollector.getFcInterface(DataCollectorAdmin.DATA_COLLECTOR_ADMIN));
        bcInsertAdapter.bindFc(BladeControl.BLADE_INSERT_CONTROL, insert.getFcInterface(BladeControl.BLADE_INSERT_CONTROL));
        bcInsertAdapter.bindFc(DataCollectorWrite.DATA_COLLECTOR_WRITE, dataCollector.getFcInterface(DataCollectorWrite.DATA_COLLECTOR_WRITE));
        bcInsertAdapter.bindFc(StorageProxyAdmin.STORAGEPROXY_ADMIN, storageProxy.getFcInterface(StorageProxyAdmin.STORAGEPROXY_ADMIN));
        bcInsertAdapter.bindFc(SupervisorInfo.SUPERVISOR_INFO, blade.getFcInterface(SupervisorInfo.SUPERVISOR_INFO));
        bcInsert.bindFc(BladeInsertResponse.BLADE_INSERT_RESPONSE, insertAdapter.getFcInterface(BladeInsertResponse.BLADE_INSERT_RESPONSE));
        bcInsert.bindFc(DataCollectorWrite.DATA_COLLECTOR_WRITE, dataCollector.getFcInterface(DataCollectorWrite.DATA_COLLECTOR_WRITE));
        bcDataCollector.bindFc(StorageWrite.STORAGE_WRITE, storageProxy.getFcInterface(StorageWrite.STORAGE_WRITE));

        return blade;
	}

}
