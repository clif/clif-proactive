/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004-2005, 2011-2012 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib;

import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Map;
import org.ow2.clif.util.StringHelper;

/**
 * Utility class for writing a test plan as a property file.
 * @author Bruno Dillenseger
 */
public class TestPlanWriter
{
	static public void write2prop(OutputStream out, Map<String,ClifDeployDefinition> testPlan)
		throws IOException
	{
		TestPlanWriter writer = new TestPlanWriter(out);
		writer.write2prop(testPlan);
		writer.close();
	}


	OutputStream out;


	public TestPlanWriter(OutputStream out)
	{
		this.out = out;
	}


	public void close()
		throws IOException
	{
		out.close();
	}


	public void flush()
		throws IOException
	{
		out.flush();
	}


	public void write2prop(Map<String,ClifDeployDefinition> testPlan)
		throws IOException
	{
		int n = 0;
		PrintStream ps = new PrintStream(out, false, "ISO-8859-1");

		ps.println("#CLIF test plan");
		ps.println("#" + new Date());
		for (Map.Entry<String,ClifDeployDefinition> entry : testPlan.entrySet())
		{
			ClifDeployDefinition def = entry.getValue();
			String prefix = TestPlanReader.BLADE_PROP + "." + n++ + ".";
			ps.println();
			// blade identifier
			ps.println(
				prefix + TestPlanReader.ID_PROP
				+ "=" + StringHelper.escapeProperty(entry.getKey()));
			// blade insert class
			if (def.isProbe())
			{
				ps.println(
					prefix + TestPlanReader.PROBE_PROP
					+ "=" + StringHelper.escapeProperty(def.getContext().get("insert")));
			}
			else
			{
				ps.println(
					prefix + TestPlanReader.INJECTOR_PROP
					+ "=" + StringHelper.escapeProperty(def.getContext().get("insert")));
			}
			// server name
			ps.println(
				prefix + TestPlanReader.SERVER_PROP
				+ "=" + StringHelper.escapeProperty(def.getServerName()));
			// argument line
			ps.println(
				prefix + TestPlanReader.ARGUMENT_PROP
				+ "=" + StringHelper.escapeProperty(def.getArgument()));
			// comment
			ps.println(
				prefix + TestPlanReader.COMMENT_PROP
				+ "=" + StringHelper.escapeProperty(def.getComment()));
		}
	}
}
