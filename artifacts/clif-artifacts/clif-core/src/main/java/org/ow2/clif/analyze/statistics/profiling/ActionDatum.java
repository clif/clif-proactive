/*
 * Copyright (C) 2005 - France Telecom R&D
 */
package org.ow2.clif.analyze.statistics.profiling;

/**
 * This object materializes the date, the duration and the success of
 * each request injected by CLIF.
 * 
 * @author Guy Vachet
 * @see org.ow2.clif.storage.api.ActionEvent
 */
public class ActionDatum extends Datum {
	private long duration;
	private boolean ctrl;

	public ActionDatum(long timestamp, long duration, boolean ctrl) {
		setDate(timestamp);
		this.duration = duration;
		this.ctrl = ctrl;
	}

	@Override
	public long getResult() {
		return duration;
	}

	public boolean isOk() {
		return ctrl;
	}

}
