/*
* CLIF is a Load Injection Framework
* Copyright (C) 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.engine;

import java.util.LinkedList;
import java.util.List;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.storage.api.AlarmEvent;

/**
 * Wrapper for asynchronous management of alarms sent to
 * BladeInsertResponse interface.
 * 
 * @author Bruno Dillenseger
 */
public class AsyncAlarmForwarder extends Thread implements BladeInsertResponse
{
	private List<AlarmEvent> alarmQ = new LinkedList<AlarmEvent>();
	private BladeInsertResponse target;
	private boolean interrupted = false;

	/**
	 * Creates a front-end for a target BladeInsertResponse object.
	 * Alarms are asynchronously forwarded to the target, while 
	 * other BladeInsertResponse calls are synchronously redirected
	 * to the target.
	 * @param group the thread group this thread will belong to
	 * @param target the target BladeInsertResponse object
	 */
	public AsyncAlarmForwarder(ThreadGroup group, BladeInsertResponse target)
	{
		super(group, "Asynchronous ISAC alarm forwarder");
		this.target = target;
		start();
	}

	/**
	 * Discards all alarms pending in the queue.
	 */
	public synchronized void clear()
	{
		alarmQ.clear();
	}

	///////////////////////////////////
	// interface BladeInsertResponse //
	///////////////////////////////////

	/**
	 * Put the provided alarm in a queue for asynchronous forwarding
	 * to the target.
	 * @param alarm the alarm to forward to the target
	 */
	public synchronized void alarm(AlarmEvent alarm)
	{
		alarmQ.add(alarm);
		notify();
	}

	/**
	 * Direct/synchronous "aborted()" call on the target
	 */
	public synchronized void aborted()
	{
		target.aborted();
	}

	/**
	 * Direct/synchronous "completed()" call on the target
	 */
	public synchronized void completed()
	{
		target.completed();
	}

	//////////////////////
	// Thread overrides //
	//////////////////////

	/**
	 * Thread activity: get alarms from the queue and forward them
	 * until the thread is interrupted.
	 */
	public void run()
	{
		while (! interrupted)
		{
			AlarmEvent alarm = null;
			synchronized(this)
			{
				if (alarmQ.isEmpty())
				{
					try
					{
						wait();
					}
					catch (InterruptedException ex)
					{
					}
				}
				else
				{
					alarm = alarmQ.remove(0);
				}
			}
			if (alarm != null)
			{
				target.alarm(alarm);
			}
		}
	}

	/**
	 * Interruption override: traps interrupt for thread
	 * termination without generating InterruptedException
	 * in possibly active blocking operations (network I/O).
	 * Uses notifyAll() to exit from pending wait() calls.
	 */
	public synchronized void interrupt()
	{
		interrupted = true;
		notifyAll();
	}
}
