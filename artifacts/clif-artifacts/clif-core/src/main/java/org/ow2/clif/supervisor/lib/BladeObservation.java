/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.supervisor.lib;

import java.io.Serializable;
import org.ow2.clif.supervisor.api.BladeState;


/**
 * Used by the Observable interface: Observer objects will
 * be invoked with an instance of this class as 2nd argument whenever
 * a Blade state event occurs
 * @see java.util.Observer#update(Observable,Object)
 * @author Bruno Dillenseger
 */
public class BladeObservation implements Serializable
{
	private static final long serialVersionUID = 7410089422807090794L;
	String bladeId;
	BladeState state;


	public BladeObservation(String bladeId, BladeState state)
	{
		this.bladeId = bladeId;
		this.state = state;
	}


	public String getBladeId()
	{
		return bladeId;
	}


	public BladeState getState()
	{
		return state;
	}


	public String toString()
	{
		return "blade " + bladeId + " switched to state " + state; 
	}
}
