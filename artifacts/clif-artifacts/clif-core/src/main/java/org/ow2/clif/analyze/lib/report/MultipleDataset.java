package org.ow2.clif.analyze.lib.report;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 */

/**
 * @author etpo5374
 *
 */
public class MultipleDataset extends Dataset {
	
	static DatasetType datasetType = DatasetType.MULTIPLE_DATASET;


	/**
	 * @param testName
	 * @param bladeId
	 * @param eventType
	 */
	public MultipleDataset(String testName, String bladeId, String eventType) {
		super("", testName, bladeId, eventType);
		
	}

	public MultipleDataset(String testName, String bladeId) {
		this(testName, bladeId, ""); 
	}

	public MultipleDataset() {
		this("", "", "");
	}

	@Override
	public List<Statistics> getStatistics()
	{
		List<Statistics> ret = new ArrayList<Statistics>();
		for(Datasource datasource : datasources){
			ret.add(datasource.getStatistics());
		}
		return ret;
	}

	@Override
	public void setStatistics(List<Statistics> _statistics){
		this.statistics = null;		// sould never acces this
		int i = 0;
		if (_statistics.size() != datasources.size()){
			System.err.println("ERROR MultipleDataset.setStatistics: given statistics have not correct size. "
					+ _statistics.size() + " instead of " + datasources.size() + ".");

		}
		for (Statistics stat : _statistics){
			Datasource datasource = datasources.get(i++);
			datasource.setStatistics(stat);
		}
	}

}
