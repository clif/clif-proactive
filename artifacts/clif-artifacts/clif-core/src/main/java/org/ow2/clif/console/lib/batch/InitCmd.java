/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.ThrowableHelper;

/**
 * Batch command to initialize all blades of a given deployed test plan.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class InitCmd
{
	/**
	 * @see #run(String, String)
	 * @param args shall contain 2 arguments: args[0] is the name of the deployed test plan to initialize,
	 * args[1] is an arbitrary identifier string for this new test run
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length != 2)
		{
		    BatchUtil.usage("2 arguments expected: <deployed test plan name> <new test run identifier>");
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], args[1]));
	}


	/**
	 * Initializes all the blades of the given deployed test plan to prepare for a new execution.
	 * According to the blade lifecycle, initialization may occur just after deployment or after
	 * execution termination (i.e. in aborted, completed or stopped states). 
	 * @param testPlanName the name of the deployed test plan to initialize
	 * @param testRunId an arbitrary identifier string for this new test run
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String testPlanName, String testRunId)
	{		
		try
		{
		    ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlanName);
		    if(clifApp == null)
		    {
		        System.err.println("Error: unable to find such deployed test plan");
		        return BatchUtil.ERR_DEPLOY;
		    }
		    System.out.println("Initializing");
	    	clifApp.init(testRunId);
// fixed            int res = clifApp.waitForInitializedState(null);
			int res = clifApp.waitForState(null, BladeState.INITIALIZED);
            if(res == BatchUtil.SUCCESS)
            {
                System.out.println("Initialized");
                return BatchUtil.SUCCESS;
            }
		}
		catch (Exception ex)
		{
		    System.err.println("Initialization failed:\n" + ThrowableHelper.getStackTrace(ex));
		    if (ex instanceof ClifException && ex.getCause() == null)
		    {
		    	return BatchUtil.ERR_LIFECYCLE;
		    }
		}
		return BatchUtil.ERR_EXEC;
	}
}
