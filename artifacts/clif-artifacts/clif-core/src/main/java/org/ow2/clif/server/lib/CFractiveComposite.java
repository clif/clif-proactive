/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.server.lib;

import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.component.body.ComponentInitActive;

public class CFractiveComposite implements AttributeController, ComponentInitActive {//Composite not mandatory

	public void initComponentActivity(Body body) {
		PAActiveObject.setImmediateService("getAllSubComponents");
		PAActiveObject.setImmediateService("initCollect");
		PAActiveObject.setImmediateService("getCollectSize");
		PAActiveObject.setImmediateService("getBladeId");
		PAActiveObject.setImmediateService("getFcSuperComponents");
		PAActiveObject.setImmediateService("getFcName");
		PAActiveObject.setImmediateService("setArgument");
		PAActiveObject.setImmediateService("getFcSubComponents");
		PAActiveObject.setImmediateService("setId");
		PAActiveObject.setImmediateService("getId");
		PAActiveObject.setImmediateService("changeParameter");
		PAActiveObject.setImmediateService("getCurrentParameters");
		PAActiveObject.setImmediateService("setBladeState");
		PAActiveObject.setImmediateService("waitForStateInitialized");
		PAActiveObject.setImmediateService("waitForStateRunning");
		PAActiveObject.setImmediateService("getDefinitions");
		PAActiveObject.setImmediateService("setDefinitions");
		PAActiveObject.setImmediateService("waitEndOfRun");
		PAActiveObject.setImmediateService("getGlobalState");
		PAActiveObject.setImmediateService("waitForState");
		PAActiveObject.setImmediateService("waitStationaryState");
		PAActiveObject.setImmediateService("alarm");
		PAActiveObject.setImmediateService("join");
		PAActiveObject.setImmediateService("resume");
		PAActiveObject.setImmediateService("suspend");
		PAActiveObject.setImmediateService("stop");
		PAActiveObject.setImmediateService("start");
		PAActiveObject.setImmediateService("do_start");
		PAActiveObject.setImmediateService("init");
		PAActiveObject.setImmediateService("do_init");
		PAActiveObject.setImmediateService("collect");
		PAActiveObject.setImmediateService("changeParameter");
		PAActiveObject.setImmediateService("getCurrentParameters");
		PAActiveObject.setImmediateService("getStatLabels");
		PAActiveObject.setImmediateService("getStats");
		PAActiveObject.setImmediateService("listFc");
		PAActiveObject.setImmediateService("unbindFc");
		PAActiveObject.setImmediateService("bindFc");
		PAActiveObject.setImmediateService("lookupFc");
		PAActiveObject.setImmediateService("getFcState");
		PAActiveObject.setImmediateService("stopFc");
		PAActiveObject.setImmediateService("startFc");
		PAActiveObject.setImmediateService("getComponentParameters");
		PAActiveObject.setImmediateService("migrateControllersDependentActiveObjectsTo");
		PAActiveObject.setImmediateService("getOutputInterceptors");
		PAActiveObject.setImmediateService("getInputInterceptors");
		PAActiveObject.setImmediateService("toString");
		PAActiveObject.setImmediateService("getRepresentativeOnThis");
		PAActiveObject.setImmediateService("setControllerObject");
		PAActiveObject.setImmediateService("getID");
		PAActiveObject.setImmediateService("getBody");
		PAActiveObject.setImmediateService("getReferenceOnBaseObject");
		PAActiveObject.setImmediateService("isFcInternalItf");
		PAActiveObject.setImmediateService("getFcItfType");
		PAActiveObject.setImmediateService("getFcItfOwner");
		PAActiveObject.setImmediateService("getFcItfName");
		PAActiveObject.setImmediateService("getNFType");
		PAActiveObject.setImmediateService("getFcType");
		PAActiveObject.setImmediateService("getFcInterfaces");
		PAActiveObject.setImmediateService("getFcInterface");

		PAActiveObject.setImmediateService("migrateDependentActiveObjectsTo");
		PAActiveObject.setImmediateService("isComposite");
		PAActiveObject.setImmediateService("isPrimitive");
		PAActiveObject.setImmediateService("getHierarchicalType");
		PAActiveObject.setImmediateService("setControllerItfType");
		PAActiveObject.setImmediateService("setItfType");
		PAActiveObject.setImmediateService("checkLifeCycleIsStopped");
		PAActiveObject.setImmediateService("getFcItfOwner");
		PAActiveObject.setImmediateService("getFcItfType");
		PAActiveObject.setImmediateService("getFcItfName");
		PAActiveObject.setImmediateService("isFcInternalItf");
		PAActiveObject.setImmediateService("initController");
		PAActiveObject.setImmediateService("getState");
		PAActiveObject.setImmediateService("duplicateController");
		PAActiveObject.setImmediateService("removeFcSubComponent");
		PAActiveObject.setImmediateService("addFcSubComponent");
		PAActiveObject.setImmediateService("removeFcSubComponent");
		PAActiveObject.setImmediateService("addFcSubComponent");
		PAActiveObject.setImmediateService("isSubComponent");
		PAActiveObject.setImmediateService("getFcSubComponents");
		PAActiveObject.setImmediateService("getFcInternalInterfaces");
		PAActiveObject.setImmediateService("setControllerItfType");

		PAActiveObject.setImmediateService("equals");
		PAActiveObject.setImmediateService("hashCode");
		PAActiveObject.setImmediateService("isInternal");
		PAActiveObject.setImmediateService("isStreamItf");
		PAActiveObject.setImmediateService("isGCMCollectiveItf");
		PAActiveObject.setImmediateService("isGCMMulticastItf");
		PAActiveObject.setImmediateService("isGCMGathercastItf");
		PAActiveObject.setImmediateService("isGCMCollectionItf");
		PAActiveObject.setImmediateService("isGCMSingletonItf");
		PAActiveObject.setImmediateService("getGCMCardinality");
		PAActiveObject.setImmediateService("isFcSubTypeOf");
		PAActiveObject.setImmediateService("isFcOptionalItf");
		PAActiveObject.setImmediateService("isFcCollectionItf");
		PAActiveObject.setImmediateService("isFcClientItf");
		PAActiveObject.setImmediateService("getFcItfSignature");
		PAActiveObject.setImmediateService("getFcItfName");

		/*TestControl*/
		PAActiveObject.setImmediateService("getStats");
		PAActiveObject.setImmediateService("getStatLabels");
		PAActiveObject.setImmediateService("getCurrentParameters");
		PAActiveObject.setImmediateService("addObserver");
		PAActiveObject.setImmediateService("deleteObservers");
		PAActiveObject.setImmediateService("changeParameter");

		/*SupervisorInfo*/
		PAActiveObject.setImmediateService("setBladeState");
		PAActiveObject.setImmediateService("setDefinitions");
		PAActiveObject.setImmediateService("getDefinitions");
		PAActiveObject.setImmediateService("alarm");
		PAActiveObject.setImmediateService("getGlobalState");
		PAActiveObject.setImmediateService("waitStationaryState");
		PAActiveObject.setImmediateService("waitEndOfRun");
		PAActiveObject.setImmediateService("waitForState");
		PAActiveObject.setImmediateService("waitForStateInitialized");
		PAActiveObject.setImmediateService("waitForStateRunning");

		/*DataCollectorAdmin*/
		PAActiveObject.setImmediateService("getStat");
		PAActiveObject.setImmediateService("getLabels");

		/*BladeControl*/
		PAActiveObject.setImmediateService("setArgument");
		PAActiveObject.setImmediateService("setId");
		PAActiveObject.setImmediateService("getId");
		PAActiveObject.setImmediateService("changeParameter");
		PAActiveObject.setImmediateService("getCurrentParameters");

		/*StorageAdmin*/
		PAActiveObject.setImmediateService("terminate");
		PAActiveObject.setImmediateService("collect");
		PAActiveObject.setImmediateService("newTest");

		/*StorageRead*/
		PAActiveObject.setImmediateService("getTests");
		PAActiveObject.setImmediateService("getTestPlan");
		PAActiveObject.setImmediateService("getBladeProperties");
		PAActiveObject.setImmediateService("getEventFieldLabels");
		PAActiveObject.setImmediateService("getEventIterator");
		PAActiveObject.setImmediateService("getNextEvents");
		PAActiveObject.setImmediateService("closeEventIterator");
		PAActiveObject.setImmediateService("getEvents");
		PAActiveObject.setImmediateService("countEvents");

		/*StorageProxyAdmin*/
		PAActiveObject.setImmediateService("init");
		PAActiveObject.setImmediateService("newTest");
		PAActiveObject.setImmediateService("closeTest");
		PAActiveObject.setImmediateService("getBladeId");
		PAActiveObject.setImmediateService("initCollect");
		PAActiveObject.setImmediateService("collect");
		PAActiveObject.setImmediateService("getCollectSize");
		PAActiveObject.setImmediateService("closeCollect");
	}

}
