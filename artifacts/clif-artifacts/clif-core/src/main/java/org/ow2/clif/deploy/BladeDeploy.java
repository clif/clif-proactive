/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2010-2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.server.api.ClifServerControl;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.Future;

/**
 * Asynchronously and remotely instantiates Blade components in one CLIF server
 * and sets their arguments.
 * 
 * @author Bruno Dillenseger
 */
public class BladeDeploy extends Future
{
	protected Interface supervisorInfo;
	protected Component serverComp;
	protected String serverName;
	protected String argument;
	protected Collection<Map.Entry<String,ClifDeployDefinition>> bladeDefs;


	/**
	 * Creates a thread dedicated to deploying a collection of blades
	 * in a given CLIF server. This thread must be started in order
	 * deployment to actually start. Then, the get() method will deliver
	 * the deployment results.
	 * @param serverComp the target Clif server component
	 * @param serverName the name of the target Clif server
	 * @param definitions the collection of blade deployment definitions
	 * @param lock a lock object that will be notified once deployment process is complete.
	 * May be null if this synchronization feature is not wanted.
	 * @see #get()
	 */
	public BladeDeploy(
		String serverName,
		Component serverComp,
		Collection<Map.Entry<String,ClifDeployDefinition>> definitions,
		Object lock)
	{
		super("Blades deployment on " + serverName, lock);
		this.serverComp = serverComp;
		this.serverName = serverName;
		this.bladeDefs = definitions;
	}


	/**
	 * Actually performs the blades deployment.
	 */
	@Override
	public Object futureExecution()
	throws Exception
	{
		Map<String,Object> result = new HashMap<String,Object>();
		ClifServerControl serverCtrl = null;
		try
		{
			// get ClifServerControl interface, possibly with multiple attempts
			final int retries = Integer.parseInt(
				System.getProperty(
					ExecutionContext.DEPLOY_RETRIES_PROP_NAME,
					ExecutionContext.DEPLOY_RETRIES_PROP_DEFAULT));
			final int retryDelay_ms = Integer.parseInt(
				System.getProperty(
					ExecutionContext.DEPLOY_RETRYDELAY_PROP_NAME,
					ExecutionContext.DEPLOY_RETRYDELAY_PROP_DEFAULT));
			int attempts = 0;
			while (serverCtrl == null && attempts <= retries)
			{
				++attempts;
				try
				{
					serverCtrl = (ClifServerControl)serverComp.getFcInterface(
						ClifServerControl.CLIF_SERVER_CONTROL);
				}
				catch (Exception ex)
				{
					System.out.println(
						"Deployment attempt #" + attempts + " of " + (retries + 1) + " failed on server " + serverName + ".\n"
						+ ex);
					if (attempts > retries)
					{
						throw ex;
					}
					else
					{
						Thread.sleep(retryDelay_ms);
					}
				}
			}
			// remove all blades from the target CLIF server
			serverCtrl.removeAllBlades();
			// install new blades
			result = new HashMap<String,Object>();
			for (Map.Entry<String,ClifDeployDefinition> entry : bladeDefs)
			{
				try
				{
					Component blade = serverCtrl.addBlade(
						entry.getValue().getAdlDefinition(),
						entry.getValue().getContext(),
						entry.getKey(),
						entry.getValue().getArgument());
					result.put(
						entry.getKey(),
						blade);
					System.out.println(entry.getValue().getClassName() + " deployed on server " + serverName);
				}
				catch (ClifException ex)
				{
					result.put(entry.getKey(), ex);
				}
			}
		}
		catch (Throwable th)
		{
			th.printStackTrace(System.err);
			throw new ClifException(
				"Exception in blade deployment on CLIF server " + serverName,
				th);
		}
		return result;
	}


	/**
	 * Gets the blade components (the call is blocked waiting for the blades to be
	 * actually deployed)
	 * @return a Map of blade id / blade component or exception entries
	 */
	@SuppressWarnings("unchecked")
	public Map<String,Object> get()
	throws Exception
	{
		return (Map<String,Object>)futureGet();
	}


	/**
	 * Gets the collection of blade definitions this instance is supposed to deploy.
	 * @return the blade definitions, as given at this object creation
	 */
	public Collection<Map.Entry<String,ClifDeployDefinition>>getDefinitions()
	{
		return bladeDefs;
	}
}
