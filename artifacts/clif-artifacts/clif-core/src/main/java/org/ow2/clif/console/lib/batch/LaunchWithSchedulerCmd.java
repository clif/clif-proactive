/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.batch;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ow2.clif.deploy.ProActiveSchedulerClient;
import org.ow2.clif.storage.lib.filestorage.FileStorageCommons;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;


/**
 * Batch command to deploy and to run a test plan using ProActive Workflow and Scheduling.
 * <p>
 * The deployed test plan is given a name (registered in the Registry) for
 * further reference with other batch commands.
 *
 * @author Laurent Pellegrino
 * @see ProActiveSchedulerClient
 */
public class LaunchWithSchedulerCmd {

    private static final String DEFAULT_CLIF_DISTRIBUTION_UPDATE_STRATEGY = "auto";

    private static final String DEFAULT_CLIF_DISTRIBUTION_REPOSITORY_URL = "http://localhost/PA-CLIF/";

    private static final long DEFAULT_POLLING_PERIOD_IN_MS = 5000;

    private static final String DEFAULT_WORKFLOW_NAME = "test-plan-executor.xml";

    /**
     * Property used to say which update strategy to apply on ProActive Nodes for the Clif distribution.
     * The default value is {@code auto}. Accepted values are:
     * <ol>
     * <li>auto: the download of a new Clif distribution depends of the meta information fetched
     * by using {@link LaunchWithSchedulerCmd}</li>
     * <li>force: the download of the Clif distribution is forced whatever meta information value is</li>
     * </ol>
     *
     * @see LaunchWithSchedulerCmd#PROPERTY_CLIF_DISTRIBUTION_REPOSITORY_URL
     */
    public static final String PROPERTY_CLIF_DISTRIBUTION_UPDATE_STRATEGY = "clif.distribution.update";

    /**
     * Defines the URL of the Clif repository used to download the latest Clif archive and ProActive addons.
     * The default value is {@code http://clif.rd.francetelecom.fr/PA-CLIF/}. It is assumed that a file
     * named "latest-release.properties" is accessible from this location.
     */
    public static final String PROPERTY_CLIF_DISTRIBUTION_REPOSITORY_URL = "clif.distribution.repository.url";

    /**
     * If the property value is {@code true}, then data spaces
     * are not cleaned up and some debug information are print.
     */
    public static final String PROPERTY_SCHEDULER_DEBUG = "sched.debug";

    /**
     * The URL to the Scheduler REST API used to deploy and run a test plan.
     */
    public static final String PROPERTY_SCHEDULER_REST_URL = "sched.url";

    /**
     * Polling period in ms.
     */
    public static final String PROPERTY_SCHEDULER_POLLING_PERIOD = "sched.polling.period";

    /**
     * The path to a Scheduler credentials file used for authentication.
     * This property has priority over {@link #PROPERTY_SCHEDULER_USERNAME} and
     * {@link #PROPERTY_SCHEDULER_PASSWORD}.
     */
    public static final String PROPERTY_SCHEDULER_CREDENTIALS = "sched.credentials";

    /**
     * The username to use for authenticating to the Scheduler REST API.
     */
    public static final String PROPERTY_SCHEDULER_USERNAME = "sched.username";

    /**
     * The password to use for authenticating to the Scheduler REST API. If this property is defined,
     * then it is also required to define {@link #PROPERTY_SCHEDULER_USERNAME}. Otherwise, the property
     * is optional.
     */
    public static final String PROPERTY_SCHEDULER_PASSWORD = "sched.password";

    /**
     * Walltime set to the main task of the Workflow.
     * The task is aborted if it runs longer than the delay specified in the form "[[HH:]MM:]SS".
     */
    public static final String PROPERTY_TASK_WALLTIME = "task.walltime";

    // -----------------------------------------------

    private final Set<String> fileNamesExcludedFromInputTransfer;

    private final String schedulerRestUrl;

    private final String deployName;

    private final String testPlanFileName;

    private final String testRunId;

    private final File proactiveWorkflowFile;

    private final ProActiveSchedulerClient proactiveClient;

    private final long pollingPeriod;

    private final String reportDirName;

    private final String updateStrategy;

    private final String walltimeExpression;

    public LaunchWithSchedulerCmd(String schedulerRestUrl, String schedulerCredentialsFile,
            String schedulerUsername, String schedulerPassword, String deployName, String testPlanFileName,
            String testRunId, String proactiveWorkflowFile) {

        this.schedulerRestUrl = schedulerRestUrl;
        this.deployName = deployName;
        this.testPlanFileName = testPlanFileName;
        this.testRunId = testRunId;

        this.proactiveWorkflowFile = toFile(proactiveWorkflowFile);

        this.pollingPeriod = getPollingPeriod();
        this.walltimeExpression = getWalltime();
        this.updateStrategy = getClifDistributionUpdateStrategy();

        if (walltimeExpression != null && !isWalltimeExpressionValid(walltimeExpression)) {
            System.exit(
                    printError("Invalid value for property '"
                            + PROPERTY_TASK_WALLTIME + "': " + walltimeExpression
                            + ". Expected expression in form of '[[HH:]MM:]SS'", 2));
        }

        this.reportDirName = getReportDirName();

        this.fileNamesExcludedFromInputTransfer = ImmutableSet.of("stats", reportDirName);

        this.proactiveClient =
                new ProActiveSchedulerClient(
                        schedulerRestUrl, toFile(schedulerCredentialsFile),
                        schedulerUsername, schedulerPassword);
    }

    protected String getClifDistributionUpdateStrategy() {
        String result = System.getProperty(
                PROPERTY_CLIF_DISTRIBUTION_UPDATE_STRATEGY,
                DEFAULT_CLIF_DISTRIBUTION_UPDATE_STRATEGY);

        if (!result.equalsIgnoreCase("force") && !result.equalsIgnoreCase("auto")) {
            System.exit(
                    printError("Unknown strategy '" + result + "' for property '"
                            + PROPERTY_CLIF_DISTRIBUTION_UPDATE_STRATEGY
                            + "'. Accepted values are 'auto' or 'force'.", 0));
        }

        return result;
    }

    protected String getClifDistributionRepositoryUrl() {
        String result = System.getProperty(PROPERTY_CLIF_DISTRIBUTION_REPOSITORY_URL,
                DEFAULT_CLIF_DISTRIBUTION_REPOSITORY_URL);

        if (!result.endsWith("/")) {
            result = result + '/';
        }

        return result;
    }

    protected long getPollingPeriod() {
        try {
            return Long.parseLong(
                    System.getProperty(
                            PROPERTY_SCHEDULER_POLLING_PERIOD,
                            String.valueOf(DEFAULT_POLLING_PERIOD_IN_MS)));
        } catch (NumberFormatException e) {
            return DEFAULT_POLLING_PERIOD_IN_MS;
        }
    }

    protected String getReportDirName() {
        String value =
                System.getProperty(
                        FileStorageCommons.REPORT_DIR_PROP,
                        FileStorageCommons.REPORT_DIR_DEFAULT);

        return new File(value).getName();
    }

    protected String getWalltime() {
        return System.getProperty(PROPERTY_TASK_WALLTIME, null);
    }

    protected boolean isWalltimeExpressionValid(String expression) {
        Pattern pattern = Pattern.compile("([0-9]{1,2})([:][0-9]{1,2}){0,2}");
        Matcher matcher = pattern.matcher(expression);
        return matcher.matches();
    }

    protected File toFile(String filePath) {
        if (filePath != null) {
            File file = new File(filePath);

            if (!file.exists()) {
                System.exit(printError("File does not exists: " + filePath, 1));
            }

            return file;
        }

        return null;
    }

    /**
     * Command entry point.
     *
     * @param args application arguments.
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            BatchUtil.usage("expected arguments: <deployed test plan name> " +
                    "<test plan definition file> <test run identifier> [proactive workflow file]");
        }

        String schedulerRestUrl = getProperty(PROPERTY_SCHEDULER_REST_URL, true);
        String schedulerCredentialsFile = getProperty(PROPERTY_SCHEDULER_CREDENTIALS, false);
        String schedulerUsername = null;
        String schedulerPassword = null;

        if (schedulerCredentialsFile == null) {
            schedulerUsername = getProperty(PROPERTY_SCHEDULER_USERNAME, true);
            schedulerPassword = getProperty(PROPERTY_SCHEDULER_PASSWORD, false);

            if (schedulerPassword == null) {
                System.out.println(
                        "Please enter the password for user '" + schedulerUsername + "':");
                schedulerPassword = new String(System.console().readPassword());
            }
        }

        String workflowFilePath = null;

        if (args.length == 4) {
            workflowFilePath = args[3];
        }

        LaunchWithSchedulerCmd launchWithSchedulerCmd =
                new LaunchWithSchedulerCmd(
                        schedulerRestUrl, schedulerCredentialsFile, schedulerUsername, schedulerPassword,
                        args[0], args[1], args[2], workflowFilePath);

        System.exit(launchWithSchedulerCmd.run());
    }

    private static String getProperty(String propertyName, boolean throwExceptionIfNotDefined) {
        String value = System.getProperty(propertyName);

        if (value == null && throwExceptionIfNotDefined) {
            System.exit(printError("Missing a required property '" + propertyName + "'", 0));
        }

        return value;
    }

    /**
     * Deploys a test plan (i.e. a set of blades among CLIF servers) according to the given test plan definition.
     *
     * @return command status code (@see BatchUtil)
     */
    public int run() {
        printSchedulerVersion(proactiveClient);

        String dataSpaceDirName = "clif-" + UUID.randomUUID().toString();
        String jobId = null;
        String sessionId = null;

        try {
            sessionId = proactiveClient.login();

            File zipFile = proactiveClient.pack(new File("."), Files.createTempDirectory("clif").toFile(),
                    fileNamesExcludedFromInputTransfer);

            final File finalZipFile = zipFile;
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    finalZipFile.delete();
                }
            }));

            if (isDebugEnabled()) {
                System.out.println(
                        "Pushing configuration files on user data space in remote folder " + dataSpaceDirName);
            }

            proactiveClient.dataSpacePush(sessionId, "user", zipFile, dataSpaceDirName);

            System.out.println("Configuration files have been pushed to the Scheduler");

            ImmutableMap.Builder<String, String> variables =
                    createVariables(
                            proactiveClient, dataSpaceDirName, deployName, testPlanFileName, testRunId);

            jobId = proactiveClient.submit(sessionId, getWorkflowInputStream(), variables.build());

            if (jobId != null) {
                System.out.println("Test plan submitted for execution");
                System.out.println("Waiting for completion of Job " + jobId);

                String jobStatus = proactiveClient.getJobStatus(sessionId, jobId);
                printJobStatus(jobId, jobStatus);

                jobStatus = waitForJobTermination(proactiveClient, sessionId, jobId, jobStatus);

                if (jobStatus.equals("CANCELED")) {
                    return printError("Job terminated with error", 4);
                }

                if (jobStatus.equals("FINISHED")) {
                    proactiveClient.dataSpaceFetch(
                            sessionId, "user", dataSpaceDirName, new File("."), "");
                }
            } else {
                return printError("Error while submitting workflow to execute test plan", 2);
            }
        } catch (IOException e) {
            if (isDebugEnabled()) {
                e.printStackTrace();
            }

            return printError("Scheduler REST API is not reachable: " + schedulerRestUrl, 3);
        } finally {
            if (!isDebugEnabled() && sessionId != null) {
                try {
                    proactiveClient.dataSpaceDelete(sessionId, "user", dataSpaceDirName, "");

                    if (jobId != null) {
                        proactiveClient.dataSpaceDelete(
                                sessionId, "user", "TaskLogs-" + jobId + "-0.log", "");
                    }

                    System.out.println("Data spaces on Scheduler have been cleaned up");
                } catch (IOException e) {
                    System.err.println("Error while trying to delete files on data space");
                }
            } else {
                System.out.println("Data spaces clean up skipped because debug mode is enabled");
            }
        }

        return 0;
    }

    private InputStream getWorkflowInputStream() throws FileNotFoundException {
        if (proactiveWorkflowFile == null) {
            return LaunchWithSchedulerCmd.class.getResourceAsStream("/" + DEFAULT_WORKFLOW_NAME);
        }

        return new BufferedInputStream(new FileInputStream(proactiveWorkflowFile));
    }

    protected boolean isDebugEnabled() {
        return System.getProperty(PROPERTY_SCHEDULER_DEBUG, "false").equalsIgnoreCase("true");
    }

    protected String waitForJobTermination(ProActiveSchedulerClient proactiveClient, String sessionId,
            String jobId, String jobStatus) throws IOException {

        while (isJobNotTerminated(jobStatus)) {
            try {
                Thread.sleep(pollingPeriod);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            String newJobStatus = proactiveClient.getJobStatus(sessionId, jobId);

            if (!newJobStatus.equals(jobStatus)) {
                printJobStatus(jobId, newJobStatus);
            }

            jobStatus = newJobStatus;
        }

        return jobStatus;
    }

    protected boolean isJobNotTerminated(String jobStatus) {
        return jobStatus.equals("PENDING") || jobStatus.equals("RUNNING")
                || jobStatus.equals("STALLED") || jobStatus.equals("PAUSED");
    }

    protected ImmutableMap.Builder<String, String> createVariables(
            ProActiveSchedulerClient proactiveClient, String dataSpaceDirName, String deployName,
            String testPlanFileName, String testRunId) throws IOException {

        ImmutableMap.Builder<String, String> variables = ImmutableMap.builder();

        variables.put("CLIF_DISTRIBUTION_REPOSITORY_URL", getClifDistributionRepositoryUrl());
        variables.put("CLIF_DISTRIBUTION_UPDATE", updateStrategy);
        variables.put("REPORT_DIR_NAME", reportDirName);
        variables.put("SYNCHRONIZATION_SERVICE_URL", proactiveClient.getSynchronizationServiceUrl());
        variables.put("TEST_PLAN_DIRECTORY", dataSpaceDirName);
        variables.put("TEST_PLAN_FILE_NAME", testPlanFileName);
        variables.put("TEST_PLAN_NAME", deployName);
        variables.put("TEST_RUN_ID", testRunId);

        if (walltimeExpression != null) {
            variables.put("TEST_WALLTIME", walltimeExpression);
        }

        return variables;
    }

    private void printJobStatus(String jobId, String newJobStatus) {
        System.out.println("Job " + jobId + " has status " + newJobStatus);
    }

    private void printSchedulerVersion(ProActiveSchedulerClient proactiveClient) {
        String schedulerVersion = proactiveClient.getSchedulerVersion();

        if (schedulerVersion != null) {
            System.out.println("Using ProActive Workflows and Scheduling in version " + schedulerVersion);
        }
    }

    private static int printError(String message, int exitCode) {
        System.err.println(message);
        return exitCode;
    }

}
