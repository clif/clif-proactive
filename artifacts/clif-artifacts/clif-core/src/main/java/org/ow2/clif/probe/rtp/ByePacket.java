/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Create a RTCP BYE packet.
 * 
 *        0                   1                   2                   3
 *        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       |V=2|P|   SC    | PT=BYE=203  |              length             |
 *       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       |                          SSRC/CSRC                            |
 *       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *       :                             ...                               :
 *       +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * (opt) |      length   |               reason for leaving            ...
 *       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * Cf. RFC 3550 for details about construction of BYE packet.
 *
 * @author Rémi Druilhe
 */
public class ByePacket extends RTCPPacket
{
	private Integer version = 2;		// Version (-) : 2 bits
	private Integer padding = 0;		// Padding (-) : 1 bit
	private Integer packetType = 203;	// Packet Type (-) : 8 bits
	
	private LinkedList<byte[]> byeBlock = new LinkedList<byte[]>();
	
	/**
	 * Contructor
	 */
	public ByePacket() {}
	
	/**
	 * Method to create a BYE packet.
	 * 
	 * @return the BYE packet in bytes.
	 */
	@Override
	public byte[] createPacket()
	{
		Integer length = 4;
		Integer offset = 4;
		
		for(int i=0; i<byeBlock.size(); i++)
		{
			length = length + byeBlock.get(i).length;
		}

		byte[] bye = new byte[length];
		
		Integer header = (version << 6) & 0xC0;
		header |= (padding << 5) & 0x20;
		
		Integer count = byeBlock.size();
		
		header |= count & 0x1F;
		bye[0] = header.byteValue();

		header = packetType & 0xFF;
		bye[1] = header.byteValue();
		
		Integer tempLength = (length >> 2) - 1;
		
		header = tempLength >> 8;
		bye[2] = header.byteValue();
		header = tempLength & 0xFF;
		bye[3] = header.byteValue();

		while(!byeBlock.isEmpty())
		{
			byte[] tempReport = byeBlock.removeFirst();
			
			for(int i=0; i<tempReport.length; i++)
			{
				bye[i+offset] = tempReport[i];
			}
			
			offset = offset + tempReport.length; 
		}
		
		return bye;
	}
	
	/**
	 * Create an element of BYE packet.
	 * 
	 * @param ssrc : the SSRC associated to the report. 
	 * @param reason : the reason for leaving the session (optional). Can be empty.
	 */
	public void addSsrc(Long ssrc, String reason)
	{
		Integer length = 4;
		
		if(reason.compareTo("") != 0)
		{
			Integer boundary = new Double(Math.ceil((new Double(reason.length()) + 1.0 + 1.0) / 4.0)).intValue();
			
			length = length + 4 * boundary;
		}
		
		byte[] block = new byte[length];
		
		block[0] = new Long(ssrc >> 24).byteValue();
		block[1] = new Long((ssrc >> 16) & 0xFF).byteValue();
		block[2] = new Long((ssrc >> 8) & 0xFF).byteValue();
		block[3] = new Long(ssrc & 0xFF).byteValue();

		if(reason.compareTo("") != 0)
		{
			byte[] byteReason = reason.getBytes();
			
			Integer boundary = new Double(Math.ceil((new Double(reason.length()) + 1.0 + 1.0) / 4.0)).intValue();
			
			block[4] = new Long(reason.length()).byteValue();

			for(int i=0; i<(4*boundary)-1; i++)
			{
				if(i < reason.length())
					block[i+5] = byteReason[i];
				else
					block[i+5] = new Integer(0).byteValue();
			}
		}
		
		byeBlock.add(block);
	}
	
	/**
	 * Returns the packet type. Here it is 203.
	 */
	@Override
	public Integer getPacketType()
	{
		return packetType;
	}
	
	/**
	 * List all SSRC present in a RTCP BYE.
	 * 
	 * @param data : the data to decode.
	 * @param offset : the beginning of the report.
	 * @return a list of all SSRC.
	 */
	public static ArrayList<Long> decodeSsrc(byte[] data, Integer offset)
	{
		ArrayList<Long> ssrcList = new ArrayList<Long>();
		Integer sourceCount = RTCPPacket.decodeCount(data, offset);
		
		for(int i=0; i<sourceCount; i++)
		{
			Long ssrc = new Long((decodeTwoComplement(new Byte(data[i*4 + offset + 4]).longValue()) << 24)
				+ (decodeTwoComplement(new Byte(data[i*4 + offset + 5]).longValue()) << 16)
				+ (decodeTwoComplement(new Byte(data[i*4 + offset + 6]).longValue()) << 8) 
				+ decodeTwoComplement(new Byte(data[i*4 + offset + 7]).longValue()));
			
			ssrcList.add(ssrc);
		}
		
		return ssrcList;
	}
}
