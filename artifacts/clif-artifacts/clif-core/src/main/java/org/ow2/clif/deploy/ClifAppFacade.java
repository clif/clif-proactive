/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009-2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.locks.ReentrantLock;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.proactive.annotation.ImmediateService;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.batch.BatchUtil;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.lib.filestorage.FileStorageCollect;
import org.ow2.clif.storage.lib.filestorage.server.FileServer;
import org.ow2.clif.storage.lib.filestorage.server.SocketBasedFileServer;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.SupervisorInfo;
import org.ow2.clif.supervisor.api.TestControl;
import org.ow2.clif.supervisor.lib.BladeObservation;
import org.ow2.clif.util.CodeServer;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.ItfName;
import org.ow2.clif.util.PACodeServer;
import org.ow2.clif.util.SerializableObserver;


/**
 * This class is responsible for creating the initial Clif application. Then, it provides test
 * plan deployment.
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class ClifAppFacade extends Observable implements Observer
{
	private ReentrantLock deployLock = new ReentrantLock();
	private volatile boolean deployed = false;
	protected String clifAppDefinition;
	protected String clifAppName;
	protected Serializable currentTestId;
	protected Component clifApp;
	protected Component storage;
	protected Component supervisor;
	/** clif application components indexed by their name (storage, supervisor, analyzer) */
	protected Map<String,Component> components = new HashMap<String,Component>();
	protected BindingController storageBc;
	protected BindingController supervisorBc;
	protected ContentController clifAppCc;
	/** contains bound clif server components, indexed by their names */
	private TestControl testControl;
	private SupervisorInfo supInf;

	/**
	 * Create new ClifAppFacade with all components of a ClifApplication :
	 * supervisor, storage...
	 * @param testName the name in the Registry to be associated with the
	 * resulting Clif Application.
	 * @param appDefinition fully-qualified Fractal ADL definition file of the
	 * CLIF application to instantiate.
	 * @throws Error
	 */
	public ClifAppFacade(String testName, String appDefinition)
	{
		clifAppName = testName;
		clifAppDefinition = appDefinition;
		try {
//			clifApp = (Component)FactoryFactory.getFactory().newComponent(clifAppDefinition, null);
			clifApp = createClifApp();
			setClifAppComponents();
		} catch (Exception e) {
			throw new Error("Error creating Clif application " + appDefinition + " for " + testName, e);
		}
	}

	/**
	 * Creates a clifapp component using component api instead of fractal adl.
	 * This enables immediate services on the composite.
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InstantiationException
	 * @throws IllegalBindingException
	 * @throws IllegalLifeCycleException
	 * @throws IllegalContentException
	 * @throws NoSuchInterfaceException
	 * @throws java.lang.InstantiationException
	 */
	private Component createClifApp() throws InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchInterfaceException, InstantiationException, IllegalContentException, IllegalLifeCycleException, IllegalBindingException, java.lang.InstantiationException {
		ClifAppType appType = (ClifAppType) Class.forName(clifAppDefinition).newInstance();
		Component clifApp = appType.createClifApp(clifAppName);
		return clifApp;
	}

	/**
	 * Constructor for ClifAppFacade. Get all sub-components
	 * from the clifApp
	 * @param clifApp the existing Clif application component
	 */
	public ClifAppFacade (Component clifApp, String name)
	{
		this.clifApp = clifApp;
		this.clifAppName = name;
		try {
			setClifAppComponents();
		} catch (Exception e) {
			throw new Error("Error creating Clif application " + name, e);
		}
	}

	/**
	 * Set clifApp sub-components
	 * @throws ADLException
	 * @throws NoSuchInterfaceException
	 */
	private void setClifAppComponents()
		throws ADLException, NoSuchInterfaceException {
		Component[] subComp = GCM.getContentController(clifApp).getFcSubComponents();
		for (int i=0 ; i<subComp.length ; ++i)
		{
			components.put(GCM.getNameController(subComp[i]).getFcName(), subComp[i]);
			if (GCM.getNameController(subComp[i]).getFcName().equals("storage"))
			{
				storage = subComp[i];
			}
			else if (GCM.getNameController(subComp[i]).getFcName().equals("supervisor"))
			{
				supervisor = subComp[i];
			}
		}
		storageBc = GCM.getBindingController(storage);
		supervisorBc = GCM.getBindingController(supervisor);
		clifAppCc = GCM.getContentController(clifApp);
		testControl = (TestControl)ClifAppFacade.this.supervisor.getFcInterface(TestControl.TEST_CONTROL);
		supInf = (SupervisorInfo)ClifAppFacade.this.supervisor.getFcInterface(SupervisorInfo.SUPERVISOR_INFO);
	}

	/**
	 * Get a component of the clifApplication by his Fractal name
	 * @param name the name of the component to find
	 * @return the reference of a component contained by the Clif Application whose name equals the
	 * String passed as parameter, or null if there is no such component.
	 */
	@ImmediateService
	public Component getComponentByName(String name)
	{
		return components.get(name);
	}

	/**
	 * Get clifApp component
	 * @return Returns the clifApp component.
	 */
	@ImmediateService
	public Component getClifApp() {
		return clifApp;
	}

	/**
	 * Asynchronously deploys blades among CLIF servers according to the given definitions.
	 * First, a new CLIF code server is launched.
	 * Then, current CLIF application is stopped, reconfigured and started again.
	 * In case the CLIF application is doing wrong (typically one of its distributed parts
	 * is no longer available), a new one is instantiated. So, be careful to always call
	 * {@link #getClifApp()} from one deployment to another whenever you need to get the
	 * reference to the CLIF application component.
	 * Old blades that may have been previously deployed by this supervisor are removed,
	 * unless a new CLIF application is instantiated or previous blades became unreachable.
	 * DeployObservation objects will be send to Observers to inform about deployment
	 * success or failure. BladeObservation objects are also sent to notify blades state changes.
	 * @param definitions a Map containing test plan definitions, indexed by blades identifiers
	 * @param registry the CLIF registry to be used for getting CLIF servers references
	 * @param extDir the external directory for that clif app.  If null, will be ExecutionContext.getBaseDir() + "lib/ext"
	 * @see org.ow2.clif.deploy.DeployDefinition
	 * @see org.ow2.clif.deploy.DeployObservation
	 * @see org.ow2.clif.supervisor.lib.BladeObservation
	 * @see #syncDeploy(Map, ClifRegistry)
	 * @throws ClifException when the CLIF code server could not be started,
	 * or when the test plan definition is null
	 */
	@ImmediateService
	public void deploy(
		final Map<String,ClifDeployDefinition> definitions,
		final ClifRegistry registry,
		final String extDir)
	throws ClifException
	{
		if (definitions == null)
		{
			throw new ClifException("Could not deploy a null test plan.");
		}
		String extDirString = extDir;
		if(extDirString == null){
			extDirString = ExecutionContext.getBaseDir() + "lib" + File.separator + "ext" + File.separator;
		}
		try
		{
			String paCodeserver = ExecutionContext.getCLIFPACodeServer();
			if(paCodeserver != null){
				//Using ProActive classloading
				PACodeServer.launch(extDirString, System.getProperty(ExecutionContext.CLIF_CODESERVER_PATH_KEY, "."));
			}else{
				//Using default classloading
				int port = ExecutionContext.getCLIFCodeServerPort();
				try
				{
					CodeServer.launch(null, port, extDirString, System.getProperty(ExecutionContext.CLIF_CODESERVER_PATH_KEY, "."));
				}
				catch (Exception ex)
				{
		        	if (! ExecutionContext.codeServerIsShared())
		        	{
		        		throw new ClifException("Could not start the CLIF code server.", ex);
		        	}
		        	// otherwise, assume the code server is already running with an appropriate path
				}
			}
		}
	    catch (Exception ex)
	    {
	       throw new ClifException("Could not start the CLIF code server.", ex);
	    }
        testControl.deleteObservers();
		testControl.addObserver(new SerializableObserver(this));
    	new Thread(
    		new Runnable() {
        		@Override
				public void run()
        		{
        			DeployThread deployerThr = new DeployThread(definitions, registry);
        			deployerThr.start();
        		}},
        	"Test plan deployment"
    	).start();
	}

	/**
	 * Synchronous version of deploy method (returns when deployment is complete).
	 * @param definitions a Map containing test plan definitions, indexed by blades identifiers
	 * @param registry the CLIF registry to be used for getting CLIF servers references
	 * @param extDir the externals directory. If null, will be ExecutionContext.getBaseDir() + "lib/ext"
	 * @see #deploy(Map, ClifRegistry)
	 * @see org.ow2.clif.deploy.DeployDefinition
	 * @see org.ow2.clif.deploy.DeployObservation
	 */
	@ImmediateService
	public void syncDeploy(
		final Map<String,ClifDeployDefinition> definitions,
		final ClifRegistry registry,
		final String extDir)
	throws ClifException
	{
		deployed = false;
		deploy(definitions, registry, extDir);
		synchronized(deployLock)
		{
			while (!deployed)
			{
				try
				{
					deployLock.wait();
				}
				catch (InterruptedException ex)
				{
				}
			}
		}
	}

	/**
	 * Init selected blades if global state is deployed
	 * @param testId selected blades ids
	 */
	@ImmediateService
	public void init(String testId)
		throws ClifException
	{
		try
		{
			supInf.waitStationaryState(null).getBooleanValue();
		}
		catch (InterruptedException ex)
		{
			throw new ClifException("Can't initialize: interrupted while waiting for a stationary state.", ex);
		}
		BladeState globalState = supInf.getGlobalState(null);
		if (globalState.equals(BladeState.DEPLOYED)
			|| globalState.equals(BladeState.COMPLETED)
			|| globalState.equals(BladeState.ABORTED)
			|| globalState.equals(BladeState.STOPPED))
		{
			currentTestId = new TestNameAndDate(testId);
			if (! ExecutionContext.useGlobalTime())
			{
				currentTestId = currentTestId.toString();
			}
	        testControl.init(currentTestId);
		}
		else
		{
			if(!supInf.getGlobalState(null).equals(BladeState.INITIALIZED)){
				throw new ClifException("Can't initialize: lifecycle error");
			}
		}
	}

    /**
	 * Start selected blades if global state is initialized
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	@ImmediateService
	public int start(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			if (supInf.getGlobalState(selBladesId).equals(BladeState.INITIALIZED))
			{
				testControl.start(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Stop selected blades if global state is not stopped/completed/aborted
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	@ImmediateService
	public int stop(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			if (!supInf.getGlobalState(selBladesId).equals(BladeState.STOPPED)
				&& !supInf.getGlobalState(selBladesId).equals(BladeState.COMPLETED)
				&& !supInf.getGlobalState(selBladesId).equals(BladeState.ABORTED))
			{
				testControl.stop(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Suspend selected blades if global state is running
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	@ImmediateService
	public int suspend(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			if (supInf.getGlobalState(selBladesId).equals(BladeState.RUNNING))
			{
			    testControl.suspend(selBladesId);
			    return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Resume selected blades if global state is suspended
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	@ImmediateService
	public int resume(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			if (supInf.getGlobalState(selBladesId).equals(BladeState.SUSPENDED))
			{
			    testControl.resume(selBladesId);
			    return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Join selected blades if global state is running
	 * @param selBladesId selected blades ids
	 * @return the int code return for exit info
	 */
	@ImmediateService
	public int join(String[] selBladesId)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			if (supInf.getGlobalState(selBladesId).equals(BladeState.RUNNING))
			{
				testControl.join(selBladesId);
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (InterruptedException ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/**
	 * Collect selected blades if global state is completed or stopped
	 * @param selBladesId selected blades ids
	 * @return the int return code for exit information
	 */
	@ImmediateService
	public int collect(String[] selBladesId, CollectListener listener)
	{
		try
		{
			supInf.waitStationaryState(selBladesId).getBooleanValue();
			supInf.waitEndOfRun(selBladesId);
			if (supInf.getGlobalState(selBladesId).equals(BladeState.COMPLETED)
				|| supInf.getGlobalState(selBladesId).equals(BladeState.STOPPED)
				|| supInf.getGlobalState(selBladesId).equals(BladeState.ABORTED))
			{
				testControl.collect(selBladesId, listener, getFileServerImplementation());
				return BatchUtil.SUCCESS;
			}
			else
			{
				return BatchUtil.ERR_LIFECYCLE;
			}
		}
		catch (Throwable ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	/** To find the implementation to use as file server */
	private FileServer.Impl getFileServerImplementation(){
		String impl = System.getProperty(FileStorageCollect.FILE_SERVER_IMPLEMENTATION_PROP);
		if(impl == null){
			System.out.println("[Warning] Collecting results using " + SocketBasedFileServer.class.getSimpleName() + " implementation as property " + FileStorageCollect.FILE_SERVER_IMPLEMENTATION_PROP +
			" was not set.");
			StringBuilder sb = new StringBuilder();
			String lf = System.getProperty("line.separator");
			sb.append("[Warning] Valid values are :" + lf);
			for(FileServer.Impl i : FileServer.Impl.values()){
				sb.append(" "); sb.append(i.method);
			}
			return FileServer.Impl.SOCKET_BASED;
		}
		for(FileServer.Impl i : FileServer.Impl.values()){
			if(i.method.equalsIgnoreCase(impl)){
				return i;
			}
		}
		System.out.println("[Warning] Collecting results using " + SocketBasedFileServer.class.getSimpleName() + " implementation as property " + FileStorageCollect.FILE_SERVER_IMPLEMENTATION_PROP +
		" was invalid.");
		StringBuilder sb = new StringBuilder();
		String lf = System.getProperty("line.separator");
		sb.append("[Warning] Valid values are :" + lf);
		for(FileServer.Impl i : FileServer.Impl.values()){
			sb.append(" "); sb.append(i.method);
		}
		return FileServer.Impl.SOCKET_BASED;
	}

	/**
	 * Get global state for these blades.
	 * @param selBladesId the blades ids
	 * @return BladeState the global state
	 */
	@ImmediateService
	public BladeState getGlobalState(String[] selBladesId)
	{
		BladeState globalState = null;
		globalState = supInf.getGlobalState(selBladesId);
		return globalState;
	}

    /**
     * Get the state of a blade
     * @param id the bladeId tested
     * @return the blade state
     */
	@ImmediateService
	public BladeState getState(String id) {
        String [] testId = {id};
        return getGlobalState(testId);
    }

    /**
     * Waits until this state is reached
     * @param selBladesId
     * @param state
     * @return int return code
     */
    @ImmediateService
    public int waitForState(String[] selBladesId, BladeState state)
    {
    	try
    	{
	        if (supInf.waitForState(selBladesId, state))
	        {
	            return BatchUtil.SUCCESS;
	        }
	        else
	        {
	        	return BatchUtil.ERR_LIFECYCLE;
	        }
	    }
    	catch (InterruptedException ex)
    	{
    		ex.printStackTrace(System.err);
    		return BatchUtil.ERR_EXEC;
    	}
    }

	/**
	 * Get the identifiers of all blades that are currently
	 * under control (i.e. deployed).
	 * @return an array of blade identifiers
	 */
	@ImmediateService
	public String[] getBladesIds()
	{
		return testControl.getBladesIds();
	}
	

    /**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistical data from
	 * @return An array containing some informations about the test for this host
	 */
	@ImmediateService
	public long[] getStats(String bladeId) {
        return testControl.getStats(bladeId);
	}

	/**
	 * Retrieve the ActionStat of a specific host.
	 * @param bladeId The blade identifier to get the statistics labels from
	 * @return An array containing some informations about the test for this host
	 */
	@ImmediateService
	public String[] getStatLabels(String bladeId) {
		return testControl.getStatLabels(bladeId);
	}

	@ImmediateService
	public Map<String,Serializable> getCurrentParameters(String id) {
	    return testControl.getCurrentParameters(id);
    }

	/**
	 * Retrieves current test identifier.
	 * @return A string composed of the test name,
	 * and the date and time of its initialization
	 */
	@ImmediateService
	public String getCurrentTestId() {
	    return currentTestId.toString();
    }

    @ImmediateService
    public void changeParameter(String bladeId, String name, String text)
    	throws ClifException
    {
        testControl.changeParameter(bladeId, name, text);
    }

    @Override
    @ImmediateService
	public void update(Observable supervisor, Object observation) {
	    if(countObservers() == 0 && observation instanceof AlarmEvent)
	    {
	        AlarmEvent alarm = (AlarmEvent)observation;
			if (alarm.argument != null)
			{
				if (alarm.argument instanceof Throwable)
				{
				    ((Throwable)alarm.argument).printStackTrace();
				    System.exit(1);
				}
			}
	    }
		setChanged();
		notifyObservers(observation);
	}

	class DeployThread extends Thread
	{
		Map<String,ClifDeployDefinition> definitions;
		Map<String,Collection<Map.Entry<String,ClifDeployDefinition>>> definitionsByServer =
			new HashMap<String,Collection<Map.Entry<String,ClifDeployDefinition>>>();
		ClifRegistry registry;


		private void bindBlade(Component blade)
		throws
			IllegalLifeCycleException,
			IllegalContentException,
			IllegalBindingException,
			NoSuchInterfaceException
		{
			ClifAppFacade.this.clifAppCc.addFcSubComponent(blade);
			GCM.getBindingController(blade).bindFc(
				SupervisorInfo.SUPERVISOR_INFO,
				ClifAppFacade.this.supInf);
			ClifAppFacade.this.storageBc.bindFc(
				ItfName.gen(StorageProxyAdmin.STORAGEPROXY_ADMIN),
				blade.getFcInterface(StorageProxyAdmin.STORAGEPROXY_ADMIN));
			ClifAppFacade.this.supervisorBc.bindFc(
				ItfName.gen(DataCollectorAdmin.DATA_COLLECTOR_ADMIN),
				blade.getFcInterface(DataCollectorAdmin.DATA_COLLECTOR_ADMIN));
			ClifAppFacade.this.supervisorBc.bindFc(
				ItfName.gen(BladeControl.BLADE_CONTROL),
				blade.getFcInterface(BladeControl.BLADE_CONTROL));
		}


		DeployThread(Map<String,ClifDeployDefinition> testPlan, ClifRegistry registry)
		{
			super("ClifAppFacade deployment thread");
			this.definitions = testPlan;
			this.registry = registry;
			if (testPlan != null)
			{
				for (Map.Entry<String,ClifDeployDefinition> entry : testPlan.entrySet())
				{
					Collection<Map.Entry<String,ClifDeployDefinition>> blades =
						definitionsByServer.get(entry.getValue().getServerName());
					if (blades == null)
					{
						blades = new LinkedList<Map.Entry<String,ClifDeployDefinition>>();
						definitionsByServer.put(entry.getValue().getServerName(), blades);
					}
					blades.add(entry);
				}
			}
		}


		@Override
		public void run()
		{
			boolean bestEffort = ExecutionContext.deploymentIsBestEffort();
			List<String> undeployedBlades = new ArrayList<String>(definitions.size());
			DeployObservation result = null;
			Map<String,Component> serversComp = null;
			try
			{
				// get all available CLIF server components from the registry
				serversComp = registry.getServerComponents();
				// remove the ones that are not involved in the test plan
				Iterator<Map.Entry<String,Component>> serverIter = serversComp.entrySet().iterator();
				while (serverIter.hasNext())
				{
					if (! definitionsByServer.containsKey(serverIter.next().getKey()))
					{
						serverIter.remove();
					}
				}
				// check that necessary CLIF servers are available
				for (String serverName : definitionsByServer.keySet().toArray(new String[definitionsByServer.size()]))
				{
					if (!serversComp.containsKey(serverName))
					{
						System.err.println("Warning: CLIF server " + serverName + " is missing.");
						if (bestEffort)
						{
							for (Map.Entry<String,ClifDeployDefinition> entry : definitionsByServer.get(serverName))
							{
								definitions.remove(entry.getKey());
								undeployedBlades.add(entry.getKey());
								System.out.println(
									"Best effort deployment: blade "
									+ entry.getKey()
									+ " discarded from the test plan.");
							}
							definitionsByServer.remove(serverName);
						}
						else
						{
							throw new ClifException("Deployment aborted because CLIF server " + serverName + " is missing.");
						}
					}
				}
				String[] itfNames;
				// remove Supervisor bindings with Blades
				itfNames = ClifAppFacade.this.supervisorBc.listFc();
				for (String itfName : itfNames) {
					if (itfName.startsWith(DataCollectorAdmin.DATA_COLLECTOR_ADMIN) || itfName.startsWith(BladeControl.BLADE_CONTROL)) {
						ClifAppFacade.this.supervisorBc.unbindFc(itfName);
					}
				}
				// remove Storage bindings with Blades, Blades bindings with Supervisor,
				// and remove Blades from the CLIF application
				itfNames = ClifAppFacade.this.storageBc.listFc();
				for (String itfName : itfNames)
				{
					if (itfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
					{
						try
						{
							ClifAppFacade.this.storageBc.unbindFc(itfName);
							Interface bladeItf = ((Interface)ClifAppFacade.this.storageBc.lookupFc(itfName));
							if (bladeItf != null)
							{
								Component blade = bladeItf.getFcItfOwner();
								GCM.getBindingController(blade).unbindFc(SupervisorInfo.SUPERVISOR_INFO);
								/* Removing blades from the CLIF application may hang if at least one blade is not responding.
								 * So, we prefer leaving blades, which may not have any consequence, expect a small heap
								 * memory leak through consecutive deployments. Anyway, since there is no distributed garbage
								 * collection for blades, they will always remain however.
								clifAppCc.removeFcSubComponent(blade);
								*/
							}
						}
						catch (Exception ex)
						{
							// ignored: typically due to a CLIF server restart
						}
					}
				}
			}
			catch (Exception ex)
			{
				result = new DeployObservation(false, ex);
			}
			if (result == null && definitions != null)
			{
				try
				{
					// deploy new blades
					LinkedList<BladeDeploy> pendingBlades = new LinkedList<BladeDeploy>();
					synchronized (pendingBlades)
					{
						for (Map.Entry<String,Collection<Map.Entry<String,ClifDeployDefinition>>> entry : definitionsByServer.entrySet())
						{
							BladeDeploy deployThr = new BladeDeploy(
								entry.getKey(),
								serversComp.get(entry.getKey()),
								entry.getValue(),
								pendingBlades);
							pendingBlades.addLast(deployThr);
							deployThr.start();
							for (Map.Entry<String,ClifDeployDefinition> def : entry.getValue())
							{
								ClifAppFacade.this.setChanged();
								ClifAppFacade.this.notifyObservers(new BladeObservation(def.getKey(), BladeState.DEPLOYING));
							}
						}
						// add the new blades to the CLIF Application, and set bindings between
						// supervisor and storage components with deployed blades.
						while (! pendingBlades.isEmpty() && (result == null || bestEffort))
						{
							boolean hasTimedOut = true;
							pendingBlades.wait(ExecutionContext.getDeploymentTimeOut());
							ListIterator<BladeDeploy> iter = pendingBlades.listIterator();
							while (iter.hasNext() && (result == null || bestEffort))
							{
								BladeDeploy bd = iter.next();
								if (bd.isComplete())
								{
									hasTimedOut = false;
									iter.remove();
									try
									{
										for (Map.Entry<String,Object> entry : bd.get().entrySet())
										{
											String bladeId = entry.getKey();
											if (entry.getValue() != null && entry.getValue() instanceof Component)
											{
												bindBlade((Component)entry.getValue());
												ClifAppFacade.this.setChanged();
												ClifAppFacade.this.notifyObservers(
													new BladeObservation(bladeId,BladeState.DEPLOYED));
												ClifAppFacade.this.supInf.setBladeState(bladeId, BladeState.DEPLOYED);
											}
											else
											{
												undeployedBlades.add(bladeId);
												if (entry.getValue() == null)
												{
													result = new DeployObservation(
														false,
														new ClifException(
															"Deployment of blade " + entry.getKey() + " failed for unexpected reason."));
												}
												else
												{
													result = new DeployObservation(
														false,
														new ClifException(
															"Deployment of blade " + entry.getKey() + " failed.",
															(Exception) entry.getValue()));
												}
												if (bestEffort)
												{
													definitions.remove(entry.getKey());
													result.getException().printStackTrace(System.err);
													System.out.println(
														"Best effort deployment: blade "
														+ entry.getKey()
														+ " discarded from the test plan.");
												}
												ClifAppFacade.this.setChanged();
												ClifAppFacade.this.notifyObservers(
													new BladeObservation(bladeId, BladeState.UNDEPLOYED));
												ClifAppFacade.this.supInf.setBladeState(bladeId, BladeState.UNDEPLOYED);
											}
										}
									}
									catch (ClifException ex)
									{
										for (Map.Entry<String,ClifDeployDefinition> entry : bd.getDefinitions())
										{
											undeployedBlades.add(entry.getKey());
											if (bestEffort)
											{
												definitions.remove(entry.getKey());
												ex.printStackTrace(System.err);
												System.out.println(
													"Best effort deployment: blade "
													+ entry.getKey()
													+ " discarded from the test plan.");
											}
											ClifAppFacade.this.setChanged();
											ClifAppFacade.this.notifyObservers(
												new BladeObservation(entry.getKey(), BladeState.UNDEPLOYED));
										}
										result = new DeployObservation(false, ex);
									}
								}
							}
							if (hasTimedOut)
							{
								for (BladeDeploy bd : pendingBlades)
								{
									for (Map.Entry<String,ClifDeployDefinition> entry : bd.getDefinitions())
									{
										undeployedBlades.add(entry.getKey());
										if (bestEffort)
										{
											definitions.remove(entry.getKey());
											System.out.println(
												"Best effort deployment: blade "
												+ entry.getKey()
												+ " discarded because CLIF server "
												+ entry.getValue().getServerName()
												+ " is not responding.");
										}
										ClifAppFacade.this.setChanged();
										ClifAppFacade.this.notifyObservers(
											new BladeObservation(entry.getKey(), BladeState.UNDEPLOYED));
									}
								}
								pendingBlades.clear();
								if (! bestEffort)
								{
									result = new DeployObservation(
										false,
										new ClifException(
											"These blades could not be deployed because of mute CLIF server(s):\n"
											+ undeployedBlades));
								}
							}
						}
					}
				}
				catch (NoSuchInterfaceException ex)
				{
					result = new DeployObservation(
						false,
						new Error(
							"Fatal error in blade deployment: Clif server type mismatch",
							ex));
				}
				catch (Exception ex)
				{
					result = new DeployObservation(false, new ClifException("Deployment failure: " + ex.getMessage(), ex));
				}
			}
			ClifAppFacade.this.supInf.setDefinitions(definitions);
			if (result == null && undeployedBlades.isEmpty())
			{
				result = new DeployObservation();
			}
			else if (bestEffort)
			{
				if (definitions.size() == 0)
				{
					result = new DeployObservation(
						false,
						new ClifException("Deployment failed: no blade could be deployed."));
				}
				else
				{
					result = new DeployObservation(
						true,
						new ClifException(
							"Deployment partially completed with "
							+ undeployedBlades.size()
							+ " undeployed blade(s):\n"
							+ undeployedBlades));
				}
			}
			ClifAppFacade.this.setChanged();
			ClifAppFacade.this.notifyObservers(result);
			synchronized(ClifAppFacade.this.deployLock)
			{
				ClifAppFacade.this.deployed = true;
				ClifAppFacade.this.deployLock.notify();
			}
		}
	}

/*
	Should be fixed now

	 * FIX ME, used to bypass race condition when using ProActive as Backend. We can have INITIALIZED & DEPLOYED at the same time.
	 * @param states
	 * @return

	@ImmediateService
	public int waitForInitializedState(String[] selBladesId) {
	try
	{
	        if (supInf.waitForStateInitialized(selBladesId))
	        {
	            return BatchUtil.SUCCESS;
	        }
	        else
	        {
			return BatchUtil.ERR_LIFECYCLE;
	        }
	    }
	catch (InterruptedException ex)
	{
		ex.printStackTrace(System.err);
		return BatchUtil.ERR_EXEC;
	}
	}


	 * FIX ME, used to bypass race condition when using ProActive as Backend. We can have RUNNING & DEPLOYED at the same time.
	 * @param states
	 * @return

	@ImmediateService
	public int waitForRunningState(String[] bladeIds) {
	try
	{
	        if (supInf.waitForStateRunning(bladeIds))
	        {
	            return BatchUtil.SUCCESS;
	        }
	        else
	        {
			return BatchUtil.ERR_LIFECYCLE;
	        }
	    }
	catch (InterruptedException ex)
	{
		ex.printStackTrace(System.err);
		return BatchUtil.ERR_EXEC;
	}
	}
*/
}
