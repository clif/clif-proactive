/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004,2011 France Telecom R&D
* Copyright (C) 2003 INRIA
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import org.objectweb.fractal.api.Component;
import org.objectweb.proactive.api.PARemoteObject;
import org.objectweb.proactive.core.remoteobject.RemoteObjectAdapter;
import org.objectweb.proactive.core.remoteobject.RemoteObjectExposer;
import org.objectweb.proactive.core.remoteobject.RemoteObjectHelper;
import org.objectweb.proactive.core.remoteobject.RemoteRemoteObject;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.NetConfHelper;

/**
 * The clifRegistry is responsible for creating the initial FractalRMI
 * registry if not externally provided.
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */
public class ClifRegistry
{
	final static public int DEFAULT_PORT = 1234;
	/** Clif registry remote object's name */
	final static private String REGISTRY_NAME = "ClifRegistry";
		
	/** Unique instance of the registry */
	private static ClifRegistry instance;

	private URI uri;
	private RemoteObjectExposer<ClifRegistry> roe;
	
	/** To store all registered objects */
	private final Hashtable<String, Component> registry;
	private final Hashtable<String, Component> clifApps;
	private final Hashtable<String, Component> servers;
	

	/**
	 * public constructor but mustn't be used!!!
	 * exposed through PA Remote object API
	 */
	public ClifRegistry(){
		this.registry = null;
		this.clifApps = null;
		this.servers = null;
	}
	
	public ClifRegistry(Object notUsed) {
		this.registry = new Hashtable<String, Component>();
		this.clifApps = new Hashtable<String, Component>();
		this.servers = new Hashtable<String, Component>();
	}

	/**
	 * Accessor of the ClifRegistry singleton instance
	 * @param createRegistry if the registry must be created or not
	 * @return
	 * @throws Exception
	 */
//	public synchronized static final ClifRegistry getInstance(boolean createRegistry) throws Exception{
//		if(stubInstance == null || (currentURL != null && !currentURL.equalsIgnoreCase(ExecutionContext.getCLIFRegistry().trim()))){
//			if(createRegistry){
//				ClifRegistry reg = new ClifRegistry(null);
//				URI uri = RemoteObjectHelper.generateUrl(ClifRegistry.REGISTRY_NAME);
//				RemoteObjectExposer<ClifRegistry> roe = new RemoteObjectExposer<ClifRegistry>(ClifRegistry.class.getName(), reg);
//				RemoteRemoteObject rro = roe.createRemoteObject(uri);
//				ClifRegistry stub = (ClifRegistry) new RemoteObjectAdapter(rro).getObjectProxy();
//				
//				String uriString = uri.toString();
//				currentURL = uriString.substring(0, uriString.lastIndexOf("/")).trim();
//				System.out.println("ClifRegistry created on " + uri);
//			}else{
//				currentURL = ExecutionContext.getCLIFRegistry().trim();
//				stubInstance = (ClifRegistry) PARemoteObject.lookup(new URI(currentURL + ClifRegistry.REGISTRY_NAME));
//				stubInstance.list();
//			}
//		}
//		return stubInstance;
//	}
	
	public synchronized static ClifRegistry getInstance(boolean createRegistry) throws Exception
	{
		if (ClifRegistry.instance == null)
		{
			if (createRegistry)
			{
				ClifRegistry reg = new ClifRegistry(null);
				reg.roe = new RemoteObjectExposer<ClifRegistry>(ClifRegistry.class.getName(), reg);	
				reg.uri = RemoteObjectHelper.generateUrl(ClifRegistry.REGISTRY_NAME);
				RemoteRemoteObject rro = reg.roe.createRemoteObject(reg.uri);
				ClifRegistry.instance = (ClifRegistry) new RemoteObjectAdapter(rro).getObjectProxy();;
				new NetConfHelper().startServer(
					new InetSocketAddress(
						System.getProperty(
							"proactive.hostname",
							System.getProperty("fractal.registry.host", "localhost")),
						reg.uri.getPort() + 1));
			}
			else
			{
				ClifRegistry.instance = (ClifRegistry) PARemoteObject.lookup(ExecutionContext.getCLIFRegistry());
				ClifRegistry.instance.list();
			}
		}
		return ClifRegistry.instance;
	}

	public synchronized static void killRegistry() {
		if (ClifRegistry.instance == null) {
			return;
		}
		try { // Unregister all	
			ClifRegistry.instance.roe.unregisterAll();
		} catch (Exception e) {	
		}	
		try { // Unexport all
			ClifRegistry.instance.roe.unexportAll();
		} catch (Exception e) {	
		}
		// Empty all contents
		ClifRegistry.instance.registry.clear();
		ClifRegistry.instance.clifApps.clear();
		ClifRegistry.instance.servers.clear();
		ClifRegistry.instance = null;
	}

	/**
	 * Get bound servers in ClifRegistry
	 * @return an array of the names of all available CLIF servers (i.e. registered in the
	 * FractalRMI Registry)
	 */
	public String[] getServers()
	{
		String[] result = new String[servers.size()];
		int i = 0;
		for(String server : servers.keySet()){
			result[i++] = server;
		}
		return result;
	}


	/**
	 * Gets the list of registered CLIF server names and components
	 * @return a map with server names as keys and Clif server components as values
	 */
	public Map<String,Component> getServerComponents()
	{
		//remote object, no need to clone
		return servers;
	}

	/**
	 * Bind server component in registry namingService.
	 * Replace if binding allready exists.
	 * @param name name to be associated with the server
	 * @param server server to be associated with the given name
	 */
	public void bindServer(String name, Component server)
	{
		servers.put(name, server);
	}
	
	/**
	 * Bind clifApp component in registry namingService.
	 * Replace if binding allready exists.
	 * @param name name to be associated with the clifApp
	 * @param clifApp clifApp to be associated with the given name
	 */
	public void bindClifApp(String name, Component clifApp)
	{
		clifApps.put(name, clifApp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for binding a component to a name
	 * @param name the component name
	 * @param comp the component
	 */
	public void bind(String name, Component comp)
	{
		registry.put(name, comp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for rebinding a component to a name
	 * @param name the component name
	 * @param comp the component
	 */
	public void rebind(String name, Component comp)
	{
		registry.put(name, comp);
	}

	/**
	 * Direct access to CLIF/Fractal registry for unbinding a component name
	 * @param name the component name
	 */
	public void unbind(String name)
	{
		registry.remove(name);
	}

	/**
	 * Direct access to CLIF/Fractal registry for finding a component from its name
	 * @param name component name
	 * @return the component bound to the given name
	 */
	public Component lookup(String name)
	{
		return registry.get(name);
	}

	/**
	 * Direct access to CLIF/Fractal registry for listing all bound names
	 * @return the list of names bound to components
	 */
	public String[] list()
	{
		Set<String> keys = registry.keySet();
		return keys.toArray(new String[keys.size()]);
	}

	/**
	 * Looks for a server component.
	 * @param name the name to look for.
	 * @return Component if exists return server component else null
	 */
	public Component lookupServer(String name)
	{
		return servers.get(name);
	}
	
	/**
	 * Looks for a clifApp component.
	 * @param name the name to look for.
	 * @return Component if exists return clifApp component else null
	 */
	public Component lookupClifApp(String name)
	{
		return clifApps.get(name);
	}

	/**
	 * @return string representation in the form registry@host:port_number
	 */
	@Override
	public String toString()
	{		
		return uri == null ? null : uri.toString();
	}
}
