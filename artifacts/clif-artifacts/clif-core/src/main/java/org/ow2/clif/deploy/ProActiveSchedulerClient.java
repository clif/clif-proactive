/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.deploy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Strings;
import com.google.common.net.UrlEscapers;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.zeroturnaround.zip.ZipUtil;


/**
 * Basic ProActive client to interact with an instance of a ProActive Scheduler
 * using its REST API through HTTP.
 * <p>
 * ProActive Rest Client was not used due to dependencies' conflicts.
 *
 * @author Laurent Pellegrino
 */
public class ProActiveSchedulerClient {

    private static final String DEFAULT_ARCHIVE_NAME = "archive.zip";

    private static final String USER_AGENT = "ProActive Clif";

    private final String schedulerRestUrl;

    private final File credentials;

    private final String username;

    private final String password;

    /**
     * Creates a new instance of the client using the specified parameters.
     *
     * @param schedulerRestUrl the Scheduler REST API URL.
     * @param credentials      the credentials file of a user from the ProActive Scheduler to connect to.
     */
    public ProActiveSchedulerClient(String schedulerRestUrl, File credentials) {
        this(schedulerRestUrl, credentials, null, null);
    }

    /**
     * Creates a new instance of the client using the specified parameters.
     *
     * @param schedulerRestUrl the Scheduler REST API URL.
     * @param username         the username associated to the user of a user from the ProActive Scheduler to connect to.
     * @param password         the password associated to the given username.
     */
    public ProActiveSchedulerClient(String schedulerRestUrl, String username, String password) {
        this(schedulerRestUrl, null, username, password);
    }

    public ProActiveSchedulerClient(String schedulerRestUrl, File credentials, String username,
            String password) {
        if (!schedulerRestUrl.endsWith("/")) {
            schedulerRestUrl = schedulerRestUrl + '/';
        }

        this.schedulerRestUrl = schedulerRestUrl;
        this.credentials = credentials;
        this.username = username;
        this.password = password;
    }

    /**
     * Login to the Scheduler and returns a session ID.
     *
     * @return a valid session ID if authentication succeeds.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public String login() throws IOException {
        MultipartEntityBuilder entityBuilder;

        if (credentials == null) {
            entityBuilder = MultipartEntityBuilder.create().addTextBody("username", username)
                    .addTextBody("password", password);
        } else {
            entityBuilder = MultipartEntityBuilder.create().addBinaryBody("credential", credentials);
        }

        return Request.Post(schedulerRestUrl + "scheduler/login").body(entityBuilder.build())
                .userAgent(USER_AGENT)
                .execute().returnContent().asString();
    }

    /**
     * Compresses the given folder into a ZIP file, excluding {@code fileNamesToExclude}.
     *
     * @param folderToPack       the folder to pack.
     * @param outputDir          the output directory where to place the resulting ZIP.
     * @param fileNamesToExclude the name of the files that must not be copied to the ZIP that is generated.
     * @return the resulting ZIP file.
     */
    public File pack(File folderToPack, File outputDir, final Set<String> fileNamesToExclude) {
        if (!folderToPack.isDirectory()) {
            throw new IllegalArgumentException(
                    "The specified folderToPack is not a directory: " + folderToPack);
        }

        File resultFile = new File(outputDir, "archive.zip");

        ZipUtil.pack(folderToPack, resultFile);

        for (String excludeName : fileNamesToExclude) {
            ZipUtil.removeEntry(resultFile, excludeName);
        }

        return resultFile;
    }

    /**
     * Deletes the specified files from the given dataspace.
     *
     * @param sessionId        user sessionid to authenticate the action.
     * @param dataspace        the data space to delete files from ("user" or "global").
     * @param remotePathFolder the remote path folder that contains data to fetch (relative to the data space chosen).
     * @param includesPattern  glob pattern to select files to delete.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public void dataSpaceDelete(String sessionId, String dataspace, String remotePathFolder,
            String includesPattern) throws IOException {

        if (!dataspace.isEmpty()) {
            dataspace = dataspace + "/";
        }

        String optionalPart = "";

        if (!Strings.isNullOrEmpty(includesPattern)) {
            optionalPart = "?includes=" + UrlEscapers.urlPathSegmentEscaper().escape(includesPattern);
        }

        Request.Delete(schedulerRestUrl + "data/" + dataspace + remotePathFolder + optionalPart)
                .addHeader("sessionid", sessionId)
                .userAgent(USER_AGENT)
                .execute();
    }

    /**
     * Fetches all files from the specified {@code remotePathFolder}.
     *
     * @param sessionId         user sessionid to authenticate the action.
     * @param dataspace         the data space to fetch from ("user" or "global").
     * @param remotePathFolder  the remote path folder that contains data to fetch (relative to the data space chosen).
     * @param destinationFolder the folder where to put files which have been retrieved.
     * @param includesPattern   glob pattern to select files to fetch.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public void dataSpaceFetch(String sessionId, String dataspace, String remotePathFolder,
            File destinationFolder, String includesPattern) throws IOException {

        File tmpDir = Files.createTempDirectory("clif").toFile();

        try {
            File archiveZipFile = new File(tmpDir, DEFAULT_ARCHIVE_NAME);

            String optionalPart = "";

            if (!Strings.isNullOrEmpty(includesPattern)) {
                optionalPart = "?includes=" + UrlEscapers.urlPathSegmentEscaper().escape(includesPattern);
            }

            Request.Get(
                    schedulerRestUrl + "data/" + dataspace + "/" + remotePathFolder + optionalPart).addHeader(
                    "sessionid",
                    sessionId)
                    .addHeader("Accept-Encoding", "zip")
                    .userAgent(USER_AGENT)
                    .execute().saveContent(archiveZipFile);

            ZipUtil.unpack(archiveZipFile, destinationFolder);
        } finally {
            tmpDir.delete();
        }
    }

    /**
     * Push the specified file to the Scheduler data space.
     *
     * @param sessionId           user sessionid to authenticate the action.
     * @param dataspace           the data space to push to ("user" or "global").
     * @param file                the file to push.
     * @param destinationPathName the destination path where to put the file in.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public void dataSpacePush(String sessionId, String dataspace, File file, String destinationPathName)
            throws IOException {
        EntityBuilder entityBuilder = EntityBuilder.create().setFile(file);

        if (file.getName().endsWith(".zip")) {
            entityBuilder.setContentEncoding("zip");
        }

        Request.Put(schedulerRestUrl + "data/" + dataspace + "/" + destinationPathName)
                .userAgent(USER_AGENT)
                .addHeader("sessionid", sessionId)
                .body(entityBuilder.build())
                .execute().returnContent().asString();
    }

    /**
     * Submits the given Workflow for execution to the Scheduler.
     *
     * @param sessionId user sessionid to authenticate the action.
     * @param workflow  the path to a valid ProActive Workflow XML file.
     * @param variables the variables to pass when the instance of the Job is created.
     * @return the Job ID managed by the Scheduler.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public String submit(String sessionId, InputStream workflow, Map<String, String> variables)
            throws IOException {
        StringBuilder pathSegments = buildPathSegmentsForVariables(variables);

        String payload = Request
                .Post(schedulerRestUrl + "scheduler/submit" +
                        UrlEscapers.urlPathSegmentEscaper().escape(pathSegments.toString()))
                .addHeader("sessionid", sessionId)
                .body(MultipartEntityBuilder.create()
                        .addBinaryBody("file", workflow, ContentType.APPLICATION_XML,
                                "clif-execute-test-plan")
                        .build())
                .userAgent(USER_AGENT)
                .execute().returnContent().asString();

        try {
            JSONObject jsonObject = new JSONObject(payload);
            return jsonObject.getString("id");
        } catch (JSONException e) {
            return null;
        }
    }

    private StringBuilder buildPathSegmentsForVariables(Map<String, String> variables) {
        StringBuilder pathSegments = new StringBuilder();

        for (Map.Entry<String, String> entry : variables.entrySet()) {
            pathSegments.append(';');
            pathSegments.append(entry.getKey());
            pathSegments.append('=');
            pathSegments.append(entry.getValue());
        }
        return pathSegments;
    }

    /**
     * Retrieve the Job status for the specified Job ID.
     *
     * @param sessionId
     * @param jobId     the Job ID to get information from.
     * @return the Job status in uppercase.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public String getJobStatus(String sessionId, String jobId) throws IOException {
        String payload = httpGet(sessionId, schedulerRestUrl + "scheduler/jobs/" + jobId + "/info");

        try {
            JSONObject jsonObject = new JSONObject(payload);

            return jsonObject.getString("status");
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    /**
     * Returns the Scheduler version.
     *
     * @return the Scheduler version number or {@code null} if a communication error happened.
     */
    public String getSchedulerVersion() {
        try {
            String payload = httpGet(null, schedulerRestUrl + "scheduler/version");

            JSONObject jsonObject = new JSONObject(payload);

            return jsonObject.getString("rest");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the ProActive URL of the synchronization service.
     * <p>
     * It is assumed that the ProActive Scheduler has been configured as explained at:
     * <ul>
     * <li>https://github.com/ow2-proactive/inmemory-keyvalue-store-addons</li>
     * <li>https://github.com/ow2-proactive/inmemory-keyvalue-store-service</li>
     * </ul>
     *
     * @return the ProActive URL of the synchronization service.
     * @throws IOException if communication with the Scheduler REST endpoint fails.
     */
    public String getSynchronizationServiceUrl() throws IOException {
        return httpGet(null,
                schedulerRestUrl.replace("rest/", "") + "inmemory-keyvalue-store-service/proactive");
    }

    private String httpGet(String sessionId, String endpoint) throws IOException {
        Request request = Request.Get(endpoint);

        if (sessionId != null) {
            request.addHeader("sessionid", sessionId);
        }

        return request.execute().returnContent().asString();
    }

}
