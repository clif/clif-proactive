/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

/**
 * Bucket class stores the number of values (count) in a range defined by
 * minThreshold (included) and  maxThreshold (excluded). This object is used by
 * LongStatistics to create an ArrayList of Bucket objects during frequency analysis.
 * @author Xavier Spengler, Guy Vachet
 */
public class Bucket {
    private int count;
    private long minThreshold = -1;
    private long maxThreshold = -1;
    private long median = -1;

    public Bucket(int count, long minThreshold, long maxThreshold) {
        this.count = count;
        this.minThreshold = minThreshold;
        this.maxThreshold = maxThreshold;
        if (count == 0)
        	median = minThreshold;
    }

    public Bucket(int count, long minTld, long maxTld, long median) {
        this.count = count;
        this.minThreshold = minTld;
        this.maxThreshold = maxTld;
        this.median = median;
    }

    public int getCount() {
        return count;
    }

    public long getMinThreshold() {
        return minThreshold;
    }

    public long getMaxThreshold() {
        return maxThreshold;
    }

    public void setMedian(long median) {
        this.median = median;
    }

    public long getMedian() {
        return median;
    }

    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(minThreshold).append("\t").append(maxThreshold).append("\t");
        return sb.append(median).append("\t").append(count).toString();
    }
}
