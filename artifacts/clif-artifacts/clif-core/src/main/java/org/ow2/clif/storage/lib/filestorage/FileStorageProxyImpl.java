/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2008, 2010, 2013 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.proactive.annotation.ImmediateService;
import org.ow2.clif.deploy.TestNameAndDate;
import org.ow2.clif.storage.api.AbstractEvent;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.api.StorageWrite;
import org.ow2.clif.storage.lib.filestorage.server.FileServer;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.UniqueKey;


/**
 * Proxy part of a file-based storage system.
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class FileStorageProxyImpl
	implements
		StorageWrite,
		StorageProxyAdmin
{
	protected String bladeId;
	protected long dateOrigin;
	private Map<String,DelayedWriter> writers = new HashMap<String,DelayedWriter>();
	private Set<String> writeFailed;
	private String currentDir = null;


	public FileStorageProxyImpl()
	{
	}


	////////////////////////////
	// interface StorageWrite //
	////////////////////////////


	/**
	 * Writes a blade event in the storage system.
	 * @param event The event to write
	 * @throws ClifException the event couldn't be written to the corresponding file or
	 * the corresponding file could not be opened. This exception is thrown only once for
	 * current test. Then, subsequent calls to write() method are ignored for current test.
	 */
	@Override
	@ImmediateService
	public synchronized void write(BladeEvent event)
		throws ClifException
	{
		if (! writeFailed.contains(event.getTypeLabel()))
		{
			DelayedWriter wrt = writers.get(event.getTypeLabel());
			if (wrt == null)
			{
				String filename = currentDir
					+ File.separator
					+ event.getTypeLabel();
				try
				{
					BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
					wrt = new DelayedWriter(bw, dateOrigin);
					writers.put(event.getTypeLabel(), wrt);
					String[] fields = event.getFieldLabels();
					if (fields.length > 1)
					{
						bw.write(FileStorageCommons.COMMENT_PREFIX + " " + fields[0]);
						for (int i=1 ; i<fields.length ; ++i)
						{
							bw.write(AbstractEvent.DEFAULT_SEPARATOR + " " + fields[i]);
						}
						bw.newLine();
					}
					BufferedWriter classname_wrt = new BufferedWriter(
						new FileWriter(filename + FileStorageCommons.CLASSNAME_EXTENSION));
					classname_wrt.write(event.getClass().getName());
					classname_wrt.close();
				}
				catch (IOException ex)
				{
					writeFailed.add(event.getTypeLabel());
					throw new ClifException("Storage proxy can't create file " + filename, ex);
				}
			}
			try
			{
				wrt.write(event);
			}
			catch (IOException ex)
			{
				writeFailed.add(event.getTypeLabel());
				throw new ClifException(
					"Storage proxy can't write " + event.getTypeLabel() + " events to file "
					+ currentDir
					+ File.separator
					+ event.getTypeLabel(),
					ex);
			}
		}
	}


	/////////////////////////////////
	// interface StorageProxyAdmin //
	/////////////////////////////////


	/**
	 * Initializes the storage directories for the new test
	 */
	@Override
	@ImmediateService
	public void newTest(Serializable testId)
		throws ClifException
	{
		// close measurement files possibly opened by the previous test execution
		closeTest();
		// keep the initialization time as a reference time
		if (testId instanceof TestNameAndDate)
		{
			dateOrigin = ((TestNameAndDate)testId).getDate();
		}
		else
		{
			dateOrigin = System.currentTimeMillis();
		}
		writeFailed = new HashSet<String>();
		// create a new directory for this test execution's measurements
		File bladeDir = FileStorageCommons.newTestDir(testId, bladeId);
		currentDir = bladeDir.getPath();
		// record Java system properties
		try
		{
			OutputStream out = new FileOutputStream(new File(bladeDir, FileStorageCommons.JVMPROPS_FILENAME));
			System.getProperties().store(out, "System properties for blade " + bladeId);
			out.close();
		}
		catch (IOException ex)
		{
			throw new ClifException(
				"Could not write file " + currentDir + File.separator + FileStorageCommons.JVMPROPS_FILENAME,
				ex);
		}
	}


	/**
	 * Terminates the file storage system by closing every file stream.
	 */
	@Override
	@ImmediateService
	public void closeTest()
	{
		for (DelayedWriter writer : writers.values())
		{
			try
			{
				writer.close();
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
		writers.clear();
	}


	/**
	 * @return The identifier of the blade containing the storage proxy component
	 */
	@Override
	@ImmediateService
	public String getBladeId()
	{
		return bladeId;
	}


	/**
	 * Initializes the storage system
	 */
	@Override
	@ImmediateService
	public void init(String bladeId)
	{
		this.bladeId = bladeId;
	}


	/**
	 * Initializes a new collect for a given test
	 * @param testId the test identifier whose data must be collected
	 * @return a UniqueKey object identifying this collect, or null if the test identifier
	 * does not designate a valid test
	 * @see UniqueKey
	 */
	@Override
	@ImmediateService
	public UniqueKey initCollect(Serializable testId, FileServer.Impl impl)
	{
		closeTest();
		FileStorageCollect collect = FileStorageCollect.newCollect(new File(currentDir), impl);
		if (collect != null)
		{
			return collect.getKey();
		}
		return null;
	}


	@Override
	@ImmediateService
	public long getCollectSize(UniqueKey key)
	{
		return FileStorageCollect.getSize(key);
	}


	@Override
	@ImmediateService
	public FileStorageCollectStep collect(UniqueKey key)
	{
		return FileStorageCollect.collect(key);
	}


	@Override
	@ImmediateService
	public void closeCollect(UniqueKey key)
	{
		FileStorageCollect.close(key);
	}
}
