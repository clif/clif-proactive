/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to resume the execution of a suspended test, or just of a subset of its blades.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class ResumeCmd
{
	/**
	 * Resumes the execution of a suspended test, or just of a subset of its blades.
	 * @param args args[0] is the name of the target test plan,
	 * args[1] is optional, and may give a list of blade identifiers
	 * separated by : character bladeId1:bladeId2:...bladeIdn
	 */
	public static void main(String[] args)
	{
		if (args.length < 1)
		{
			BatchUtil.usage("arguments expected: <name of running test plan> [<blade id1>:<blade id2>:...<blade idn>]");
		}
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], BatchUtil.getSelBlades(args, 1)));
	}


	/**
	 * Resumes the execution of a suspended test, or just of a subset of its blades.
	 * @param testPlan the name of the target test plan
	 * @param blades array of target blade identifiers, or null to suspend every blade
	 * @return execution status code
	 */
	static public int run(String testPlan, String[] blades)
	{
		try
		{
			ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlan);
			if (clifApp == null)
			{
				System.err.println("Unknown test plan");
				return BatchUtil.ERR_DEPLOY;
			}
			int res = clifApp.resume(blades);
			if (res == BatchUtil.SUCCESS)
			{
				res = clifApp.waitForState(blades, BladeState.RUNNING);
				if (res == BatchUtil.SUCCESS)
				{
					System.out.println("Resumed");
					return BatchUtil.SUCCESS;
				}
			}
			System.err.println("Blades are not in the suspended state");
			return BatchUtil.ERR_LIFECYCLE;
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
