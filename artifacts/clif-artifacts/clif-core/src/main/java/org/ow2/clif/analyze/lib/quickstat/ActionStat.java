/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012 France Telecom R&D
* Copyright (C) 2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.quickstat;

import java.io.PrintStream;
import org.ow2.clif.analyze.statistics.util.data.StatOnLongs;
import org.ow2.clif.storage.api.ActionEvent;


/**
 * Manages a list of requests response times and computes statistical values:
 * min, max, mean, median, throughput, errors.
 * @author Bruno Dillenseger
 */
class ActionStat extends StatOnLongs
{
	private long beginDate = -1;
	private long endDate = -1;
	private long errors = 0;
	private double cleaningLimit = -1;
	private double cleaningFactor = 0;


	ActionStat(double factor, double limit)
	{
		cleaningFactor = factor;
		cleaningLimit = limit;
		if (cleaningFactor > 0)
		{
			setStatisticalSortFactor(cleaningFactor);
			setStatisticalSortPercentage(cleaningLimit);
		}
	}


	void add(ActionEvent event)
	{
		if (event.successful)
		{
			addLong(event.duration);
			if (beginDate == -1 || event.getDate() < beginDate)
			{
				beginDate = event.getDate();
			}
			if (endDate == -1 || event.getDate() > endDate)
			{
				endDate = event.getDate();
			}
		}
		else
		{
			++errors;
		}
	}


	double getThroughput()
	{
		if (beginDate == endDate)
		{
			return Double.NaN;
		}
		else
		{
			return 1000 * size() / (double)(endDate - beginDate);
		}
	}


	long getErrors()
	{
		return errors;
	}


	void report(PrintStream out)
	{
		Long min, max, median;
		int size;
		double mean, stdDev;
		if (cleaningFactor > 0)
		{
			min = getMinStatSortValue();
			max = getMaxStatSortValue();
			mean = getStatSortMean();
			median = getStatSortMedian();
			stdDev = getStatSortStd();
			size = getStatSortDataNumber();
		}
		else
		{
			min = getMin();
			max = getMax();
			mean = getMean();
			median = getMedian();
			stdDev = getStd();
			size = size();
		}
		out.printf("%6d\t", size);
		if (size > 0)
		{
			out.printf("%6d\t", min);
			out.printf("%6d\t", max);
			out.printf("%6.3f\t", mean);
			out.printf("%6d\t", median);
			out.printf("%6.3f\t", stdDev);
			out.printf("%6.3f\t", getThroughput());
		}
		else
		{
			out.printf("%6s\t%6s\t%6s\t%6s\t%6s\t%6s\t", "NaN", "NaN", "NaN", "NaN", "NaN", "NaN");
		}
		out.printf("%6d", getErrors());
	}
}
