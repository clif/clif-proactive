/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.etsi.uri.gcm.api.type.GCMTypeFactory;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.proactive.core.component.Constants;
import org.objectweb.proactive.core.component.ContentDescription;
import org.objectweb.proactive.core.component.ControllerDescription;
import org.objectweb.proactive.core.component.Utils;
import org.objectweb.proactive.core.component.factory.PAGenericFactory;
import org.ow2.clif.datacollector.api.DataCollectorAdmin;
import org.ow2.clif.deploy.ClifAppType;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.lib.CFractiveComposite;
import org.ow2.clif.storage.api.StorageAdmin;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.lib.filestorage.ConsoleFileStorageImpl;
import org.ow2.clif.supervisor.api.SupervisorInfo;
import org.ow2.clif.supervisor.api.TestControl;
import org.ow2.clif.supervisor.lib.SupervisorImpl;

public class ClifApp implements ClifAppType {

	public ClifApp(){}

	/**
	 * {@inheritDoc}
	 * @throws NoSuchInterfaceException
	 * @throws InstantiationException
	 * @throws IllegalLifeCycleException
	 * @throws IllegalContentException
	 * @throws IllegalBindingException
	 */
	public Component createClifApp(String appName) throws NoSuchInterfaceException, InstantiationException, IllegalContentException, IllegalLifeCycleException, IllegalBindingException {
	    Component boot = Utils.getBootstrapComponent();
        GCMTypeFactory tf = GCM.getGCMTypeFactory(boot);
        PAGenericFactory gf = Utils.getPAGenericFactory(boot);
        //Creating the composite (blade) component
        Type clifAppType = tf.createFcType(new InterfaceType[]{});
		Component clifApp = gf.newFcInstance(clifAppType, new ControllerDescription(appName, Constants.COMPOSITE),
                new ContentDescription(CFractiveComposite.class.getName(), new Object[] {}));

		//Creating the supervisor
        Type supervisorType = tf.createFcType(new InterfaceType[]{
			tf.createFcItfType(TestControl.TEST_CONTROL, TestControl.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(SupervisorInfo.SUPERVISOR_INFO, SupervisorInfo.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(DataCollectorAdmin.DATA_COLLECTOR_ADMIN, DataCollectorAdmin.class.getName(), TypeFactory.CLIENT, TypeFactory.OPTIONAL, TypeFactory.COLLECTION),
			tf.createFcItfType(BladeControl.BLADE_CONTROL, BladeControl.class.getName(), TypeFactory.CLIENT, TypeFactory.OPTIONAL, TypeFactory.COLLECTION),
			tf.createFcItfType(StorageAdmin.STORAGE_ADMIN, StorageAdmin.class.getName(), TypeFactory.CLIENT, TypeFactory.MANDATORY, TypeFactory.SINGLE),
        });
		Component supervisor = gf.newFcInstance(supervisorType, new ControllerDescription("supervisor", Constants.PRIMITIVE),
				new ContentDescription(SupervisorImpl.class.getName(), new Object[] {}));

		//creating storage
		Type storageType = tf.createFcType(new InterfaceType[]{
				tf.createFcItfType(StorageAdmin.STORAGE_ADMIN, StorageAdmin.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
				tf.createFcItfType(StorageRead.STORAGE_READ, StorageRead.class.getName(), TypeFactory.SERVER, TypeFactory.MANDATORY, TypeFactory.SINGLE),
			tf.createFcItfType(StorageProxyAdmin.STORAGEPROXY_ADMIN, StorageProxyAdmin.class.getName(), TypeFactory.CLIENT, TypeFactory.OPTIONAL, TypeFactory.COLLECTION)
		});
		Component storage = gf.newFcInstance(storageType, new ControllerDescription("storage", Constants.PRIMITIVE),
				new ContentDescription(ConsoleFileStorageImpl.class.getName(), new Object[] {}));

		//bindings
		ContentController cc = GCM.getContentController(clifApp);
		cc.addFcSubComponent(supervisor);
		cc.addFcSubComponent(storage);
        BindingController bcSupervisor = GCM.getBindingController(supervisor);
        bcSupervisor.bindFc(StorageAdmin.STORAGE_ADMIN, storage.getFcInterface(StorageAdmin.STORAGE_ADMIN));
        return clifApp;
	}
}
