/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;

/**
 * @author Emmanuel Varoquaux
 */
/* 
 * A group has two running modes :
 *   1. constant load ;
 *   2. load given by a load profile.
 * 
 * The field loadValue informs about the current mode.
 * If loadValue < 0, the current mode is mode 2. If loadValue >= 0,
 * the current mode is mode 1, and the positive value of loadValue is the value
 * of the (constant) load.
 * 
 * If at least one load profile is defined, the group starts in mode 2.
 * Otherwise, it starts in mode 1.
 *
 * The IsacExtendedEngine's interface provides a feature to move from mode 1 to
 * mode 2, via the changeParameter("population", ...) call. On the contrary,
 * there is no mean to move from mode 2 to mode 1. 
 */
class Group {
	/* States */
	private static final int	RUNNING			= 0;
	private static final int	COMPLETED		= 1;
	private static final int	ABORTED			= 2;

	/* Parameters */
	private final Supervisor	supervisor;
	private final Clock			clock;
	private final Scheduler		scheduler;
	private final List<Instruction> code;
	private final Set<LoadProfile> loadProfiles;
	private final boolean		interruptible;

	/* Fields */
	private int					state			= RUNNING;
	private int					population		= 0;
	private long				endDate;
	private int					loadValue;

	/* Locks */
	private final Object		loadValueLock	= new Object();

	/* Constructors */

	protected Group(Supervisor supervisor, Clock clock, Scheduler scheduler,
			List<Instruction> code, Set<LoadProfile> loadProfiles, boolean interruptible) {
		this.supervisor = supervisor;
		this.clock = clock;
		this.scheduler = scheduler;
		this.code = code;
		this.loadProfiles = loadProfiles;
		this.interruptible = interruptible;

		if (loadProfiles.isEmpty()) {
			loadValue = 0;
		}
		else {
			loadValue = -1;
			endDate = 0;
			for (Iterator<LoadProfile> i = loadProfiles.iterator(); i.hasNext();) {
				long endDate = i.next().endDate();
				if (this.endDate < endDate)
					this.endDate = endDate;
			}
		}
	}

	/* Private methods */

	private int load(long date) {
		synchronized (loadValueLock) {
			if (loadValue >= 0)
				return loadValue;
		}
		int res = 0;
		for (Iterator<LoadProfile> i = loadProfiles.iterator(); i.hasNext();)
			res += i.next().load(date);
		return res;
	}

	private void finish(boolean status) {
		state = status ? COMPLETED : ABORTED;
		supervisor.signal();
	}

	/* Interface */

	protected synchronized void incPopulation() {
		population++;
	}

	protected synchronized void decPopulation() {
		population--;
	}

	protected int getLoad() {
		synchronized (loadValueLock) {
			return loadValue;
		}
	}

	protected void setLoad(int value) throws Exception {
		if (state != RUNNING)
			return;

		if (value < 0)
			throw new Exception("Group: bad load value");
		synchronized (loadValueLock) {
			loadValue = value;
		}
	}

	/* Returns the code of the behavior of the group. */
	protected List<Instruction> getCode() {
		return code;
	}

	/* If the load profile is not finished, causes the group to finish with the ABORTED status. */
	protected synchronized void abort() {
		if (state != RUNNING)
			return;

		finish(false);
	}

	/* Returns the end status of the group. A return value of false indicates an abnormal termination. */
	protected boolean getStatus() {
		return state == COMPLETED;
	}

	/**
	 * Compares the population of the group to the population given by its load
	 * profile. If lower, asks the scheduler to add jobs into its job queue. If
	 * greater and the jobs of this group are interruptible, asks the scheduler
	 * to remove jobs from its job queue. If equal, does nothing.
	 * 
	 * Doesn't check that the clock is not stopped.
	 */
	public synchronized void balance() {
		if (state != RUNNING)
			return;

		long date = clock.getDate();
		synchronized (loadValueLock) {
			if (loadValue < 0 && date > endDate && population == 0) {
				finish(true);
				return;
			}
		}

		int diff = load(date) - population;
		if (diff > 0)
			scheduler.add(this, diff);
		else if (diff < 0)
			if (interruptible)
				scheduler.remove(this, -diff);
	}

	/**
	 * Tests whether this group is interruptible, in the
	 * sense of the "force stop" flag in the group's load profile.
	 * @return true if the group is interruptible, false otherwise
	 */
	public boolean isInterruptible()
	{
		return interruptible;
	}
}
