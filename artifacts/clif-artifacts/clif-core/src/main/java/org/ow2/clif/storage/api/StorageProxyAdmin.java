/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import org.ow2.clif.storage.lib.filestorage.FileStorageCollectStep;
import org.ow2.clif.storage.lib.filestorage.server.FileServer.Impl;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.UniqueKey;

import java.io.Serializable;


/**
 * Optional interface for controlling local storage system
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public interface StorageProxyAdmin
{
	static public final String STORAGEPROXY_ADMIN = "Storage proxy administration";

	/**
	 * Initialize the storage proxy
	 */
	public void init(String bladeId);

	/**
	 * Informs the storage system of a new local test
	 * @param testId test unique identifier
	 */
	public void newTest(Serializable testId) throws ClifException;


	/**
	 * Informs the storage system that the local test is terminated
	 */
	public void closeTest();


	/**
	 * Get the hostName of this interface
	 * @return The host name
	 */
	public String getBladeId();


	/**
	 * Initializes a new collect for a given test
	 * @param testId the test identifier whose results must be collected
	 * @param impl
	 * @return an identifier for this collect
	 * @see #collect(UniqueKey)
	 * @see #getCollectSize(UniqueKey)
	 * @see #closeCollect(UniqueKey)
	 */
	public UniqueKey initCollect(Serializable testId, Impl impl);


	/**
	 * Performs one collect step forward. Repeated calls to this method may be done
	 * to achieve a full collect (until it returns a null value).
	 * @param key the collect identifier
	 * @return null if the collect is completed, or an arbitrary serializable object
	 * whose interpretation depends on the StorageProxy implementation.
	 */
	public FileStorageCollectStep collect(UniqueKey key);


	/**
	 *
	 */
	public long getCollectSize(UniqueKey key);


	/**
	 *
	 */
	public void closeCollect(UniqueKey key);
}
