/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2010 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.scenario.isac.engine.instructions.ControlInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.GotoInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.Instruction;
import org.ow2.clif.scenario.isac.engine.instructions.NChoiceInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.ParamsHolder;
import org.ow2.clif.scenario.isac.engine.instructions.PlugInParamPart;
import org.ow2.clif.scenario.isac.engine.instructions.SampleInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.TestInstruction;
import org.ow2.clif.scenario.isac.engine.instructions.TimerInstruction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.plugin.TimerAction;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;

/**
 * Job execution thread for ISAC extended execution engine.
 * Each instance loops on asking a job to execute, and then
 * executing it if any.
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
class ExecutionThread extends Thread {
	/* Parameters */
	private final Clock					clock;
	private final Scheduler				scheduler;
	private final DataCollectorWrite	dataCollectorWrite;
	private final BladeInsertResponse	bladeInsertResponse;

	/* Fields */
	private static volatile long nextId;
	private static final Random random	= new Random();
	private volatile Job job;
	private volatile boolean interrupted;
	private final IsacExtendedEngine engine;

	/* Scheduling data */
	protected ExecutionThread			previous;
	protected ExecutionThread			next;

	protected ExecutionThread(ThreadGroup group, Clock clock,
			Scheduler scheduler, DataCollectorWrite dataCollectorWrite,
			BladeInsertResponse bladeInsertResponse,
			IsacExtendedEngine engine) throws ClifException {
		super(group, "ISAC execution thread " + nextId++);
		this.clock = clock;
		this.scheduler = scheduler;
		this.dataCollectorWrite = dataCollectorWrite;
		this.bladeInsertResponse = bladeInsertResponse;
		this.engine = engine;
		setContextClassLoader(ClifClassLoader.getClassLoader());
	}

	/**
	 * Warns that an exception has been thrown by an ISAC primitive:
	 * prints a warning message and Java stack trace in the standard
	 * error output and sends an alarm to the CLIF application supervisor.
	 * @param plugInId identifier of faulty ISAC plug-in
	 * @param method name of faulty method
	 * @param t thrown exception
	 * @param action textual comment about how this exception is handled by the ISAC engine 
	 */
	private void warning(String plugInId, String method, Throwable t, String action)
	{
		String message = "Job " + job.id + ": " + method + " failed in plug-in " + plugInId + " (" + action + ").";
		System.err.println("Warning: " + message);
		t.printStackTrace(System.err);
		bladeInsertResponse.alarm(
			new AlarmEvent(
				System.currentTimeMillis(),
				AlarmEvent.WARNING,
				t.getMessage()));
	}

	/**
	 * Builds a single parameter string by concatenating constant
	 * parts and variable parts, substituting variables with their
	 * value.
	 * @param parts array of parameter constituents (constant parts
	 * or variable parts: either current vUser unique id or variables
	 * provided by some ISAC plug-in session object through its
	 * DataProvider interface.
	 * @return concatenation of constant parts and variable parts' values.
	 */
	private String collapse(PlugInParamPart[] parts)
	{
		StringBuilder buf = new StringBuilder();
		for (int i = 0, len = parts.length; i < len; i++)
		{
			switch (parts[i].type)
			{
			case PlugInParamPart.STRING:
				buf.append(parts[i].str);
				break;
			case PlugInParamPart.SESSION_ID:
				buf.append(job.id);
				break;
			case PlugInParamPart.CONTEXT_CALL:
				try
				{
					SessionObjectAction dpObj = job.sessionObjectMap.get(parts[i].plugInId);
					if (dpObj == null)
					{
						throw new IsacRuntimeException("No such plug-in id: " + parts[i].plugInId);
					}
					else if (dpObj instanceof DataProvider)
					{
						buf.append(((DataProvider)dpObj).doGet(parts[i].variable));
					}
					else
					{
						throw new IsacRuntimeException("Plug-in " + parts[i].plugInId + " is not a data provider.");
					}
				}
				catch (Throwable t)
				{
					warning(parts[i].plugInId, "variable " + parts[i].variable, t, "Ignored");
				}
				break;
			case PlugInParamPart.PROPERTY:
				buf.append(System.getProperty(parts[i].variable, ""));
				break;
			}
		}
		return buf.toString();
	}

	/**
	 * Handles a full set of parameters to resolve all possible
	 * variables expressions.
	 * @param paramsHolder 
	 * @return map of parameters name/value with variables resolved
	 */
	private Map<String,String> collapse(ParamsHolder paramsHolder)
	{
		if (!paramsHolder.isSplit())
		{
			return paramsHolder.getPlainParams();
		}
		else
		{
			Map<String,String> map = new HashMap<String,String>();
			for (
				Iterator<Map.Entry<String,PlugInParamPart[]>> i = paramsHolder.getSplitParams().entrySet().iterator();
				i.hasNext();)
			{
				Map.Entry<String,PlugInParamPart[]> entry = i.next();
				map.put(entry.getKey(), collapse(entry.getValue()));
			}
			return map;
		}
	}

	/**
	 * Processes this thread's current job until:
	 * <ul>
	 *   <li>the thread is interrupted,</li>
	 *   <li>or the test execution is suspended,</li>
	 *   <li>or a timer instruction is executed (thus this thread
	 *   is freed for another job),</li>
	 *   <li>or an exit instruction is executed (end of job).</li>
	 * </ul>
	 */
	private void doWork() {
		for (; !interrupted && !clock.isStopped();) {
			Instruction inst = job.group.getCode().get(job.ip);
			switch (inst.getType()) {
			case Instruction.SAMPLE:
				SampleInstruction sampleInst = (SampleInstruction)inst;
				try {
					ActionEvent report = new ActionEvent();
					report.sessionId = job.id;
					report = ((SampleAction)job.sessionObjectMap.get(sampleInst.plugInId)).doSample(
						sampleInst.actionNumber,
						collapse(sampleInst.paramsHolder), report);
					if (report != null)
						dataCollectorWrite.add(report);
				}
				catch (Throwable t) {
					warning(sampleInst.plugInId, "doSample", t, "Ignored");
				}
				job.ip++;
				continue;
			case Instruction.TIMER:
				TimerInstruction timerInst = (TimerInstruction)inst;
				job.ip++;
				try {
					scheduler
							.sleep(job, ((TimerAction)job.sessionObjectMap
									.get(timerInst.plugInId)).doTimer(
									timerInst.actionNumber,
									collapse(timerInst.paramsHolder)));
				}
				catch (Throwable t) {
					warning(timerInst.plugInId, "doTimer", t, "Job destroyed");
					job.free();
				}
				return;
			case Instruction.CONTROL:
				ControlInstruction controlInst = (ControlInstruction)inst;
				try {
					((ControlAction)job.sessionObjectMap
							.get(controlInst.plugInId)).doControl(
							controlInst.actionNumber,
							collapse(controlInst.paramsHolder));
				}
				catch (Throwable t) {
					warning(controlInst.plugInId, "doControl", t, "Ignored");
				}
				job.ip++;
				continue;
			case Instruction.TEST:
				TestInstruction testInst = (TestInstruction)inst;
				try {
					if (!((TestAction)job.sessionObjectMap
							.get(testInst.plugInId)).doTest(
							testInst.actionNumber, collapse(testInst.paramsHolder)))
						job.ip = testInst.labelFalse;
					else
						job.ip++;
				}
				catch (Throwable t) {
					warning(testInst.plugInId, "doTest", t, "Assumed false");
					job.ip = testInst.labelFalse;
				}
				continue;
			case Instruction.GOTO:
				job.ip = ((GotoInstruction)inst).label;
				continue;
			case Instruction.NCHOICE:
				NChoiceInstruction nChoiceInst = (NChoiceInstruction)inst;
				int tirage,
				i;
				synchronized (random) {
					tirage = random.nextInt(nChoiceInst.total);
				}
				for (i = 0; i < nChoiceInst.choices.size() - 1
						&& tirage >= ((NChoiceInstruction.Choice)nChoiceInst.choices
								.get(i)).proba; i++);
				job.ip = ((NChoiceInstruction.Choice)nChoiceInst.choices.get(i)).label;
				continue;
			case Instruction.EXIT:
				scheduler.free(job);
				return;
			default:
				throw new Error("Unknown instruction");
			}
		}
		scheduler.sleep(job, 0);
	}

	/**
	 * Thread activity: loops on trying to get and execute a job.
	 */
	public void run()
	{
		engine.threadIsBorn();
		while (! interrupted)
		{
			try
			{
				job = scheduler.getJob();
				if (job == null)
				{
					engine.threadIsIdle();
				}
				else
				{
					doWork();
				}
			}
			catch (InterruptedException e)
			{
			}
		}
		engine.threadIsDead();
	}

	/**
	 * Interruption overridden to memorize interrupted status.
	 */
	public void interrupt()
	{
		interrupted = true;
		super.interrupt();
	}
}
