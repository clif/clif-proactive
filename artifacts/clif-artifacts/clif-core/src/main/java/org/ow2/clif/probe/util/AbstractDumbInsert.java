/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2011 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.proactive.annotation.ImmediateService;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.datacollector.lib.GenericFilter;
import org.ow2.clif.scenario.isac.util.BooleanHolder;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.server.util.EventStorageState;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * This abstract implementation of a blade insert component manages a single thread,
 * doing a periodic task (probing) with a given period (in milliseconds) and during
 * an given actual time. It provides a "dumb" interpretation of the blade lifecycle
 * with regard to the ActivityControl interface, inasmuch as it actually probes from
 * its initialization time to its stop time, regardless of start(), suspend() and
 * resume() orders.
 * @author Bruno Dillenseger
 * @author Emmanuel Varoquaux
 */
abstract public class AbstractDumbInsert
	implements
		BladeControl,
		BindingController,
		Runnable
{
	static private final String[] interfaceNames = new String[] {
		DataCollectorWrite.DATA_COLLECTOR_WRITE,
		BladeInsertResponse.BLADE_INSERT_RESPONSE };
	protected String probeId;
	protected Object activity_lock = new Object();
	protected BladeInsertResponse bir;
	protected Object sr_lock = new Object();
	protected DataCollectorWrite dcw;
	protected Object dc_lock = new Object();
	protected long arg_period_ms = 0;
	protected long arg_duration_ms = 0;
	protected long baseTime_ms = 0;
	protected volatile boolean started;
	protected volatile boolean stopped;
	protected volatile boolean suspended;
	protected volatile boolean terminated;
	protected Thread poller;

	protected final BooleanHolder storeLifeCycleEvents = new BooleanHolder(true);
	protected final BooleanHolder storeAlarmEvents = new BooleanHolder(true);
	protected final BooleanHolder storeProbeEvents = new BooleanHolder(true);
	protected final Map<String,BooleanHolder> eventStorageStatesMap = new HashMap<String,BooleanHolder>();

	/**
	 * Define this method to perform your measure and return it
	 * @return an probe event holding the measure
	 */
	@ImmediateService
	abstract protected ProbeEvent doProbe();

	/**
	 * Gives probe help. This basic implementation mentions the 2 first
	 * arguments: polling period and execution time.
	 * Override this method to extend the help message by adding extra
	 * arguments when necessary.
	 * @return a message giving basic help to use this probe
	 */
	public String getHelpMessage()
	{
		return "Arguments required by this probe: <polling period in milliseconds> <execution time is seconds>";
	}

    /** 
     * Override this method when you are willing to pass some extra parameters to the Insert,
     * below the sampling period and duration standard arguments.
     */

    protected void setExtraArguments(List<String> extraArgs) throws ClifException
    {
    }

    /** 
     * Override this method when you are willing to release some resources
     * when the probe ends its activity.
     */
	@ImmediateService
    protected void close() throws ClifException
    {
    }

	///////////////////////////////////////////////////
	// implementation of interface BindingController //
	///////////////////////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			return dcw;
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			return bir;
		}
		else
		{
			return null;
		}
	}


	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			synchronized (dc_lock)
			{
				dcw = (DataCollectorWrite) serverItf;
			}
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			synchronized (sr_lock)
			{
				bir = (BladeInsertResponse) serverItf;
			}
		}
	}


	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			synchronized (dc_lock)
			{
				dcw = null;
			}
		}
		else if (clientItfName.equals(BladeInsertResponse.BLADE_INSERT_RESPONSE))
		{
			synchronized (sr_lock)
			{
				bir = null;
			}
		}
	}


	public String[] listFc()
	{
		return interfaceNames;
	}


	//////////////////////////////////////////////
	// implementation of interface BladeControl //
	//////////////////////////////////////////////


	/**
	 * @param testId unique identifier of the new test
	 */
	@ImmediateService
	public BooleanWrapper init(Serializable testId)
	{
		started = false;
		stopped = false;
		suspended = false;
		terminated = false;
		poller = new Thread(this, "CLIF probe");
		poller.start();
		return new BooleanWrapper(true);
	}


	@ImmediateService
	public void start()
	{
		started = true;
		baseTime_ms = System.currentTimeMillis();
	}


	@ImmediateService
	public void stop()
	{
		stopped = true;
		poller.interrupt();
		synchronized (activity_lock)
		{
			if (started && ! terminated)
			{
				try
				{
					activity_lock.wait();
				}
				catch (InterruptedException ex)
				{
				}
			}
		}
	}


	@ImmediateService
	public void suspend()
	{
		suspended = true;
		baseTime_ms = System.currentTimeMillis() - baseTime_ms;
	}


	@ImmediateService
	public void resume()
	{
		baseTime_ms = System.currentTimeMillis() - baseTime_ms;
		suspended = false;
	}

	@ImmediateService
	public int join()
	{
		// no implementation because join() is caught/implemented by the blade insert adapter
		throw new Error("A blade insert's join() method should never be called (call its adapter's join() method instead)");
	}

	/**
	 * Sets sampling period duration parameters. Possible extra arguments are passed through
	 * @param arg should hold 2 integer parameters (separated with usual separators) setting
	 * (1) the observation polling period in ms, and (2) the test duration in seconds.
	 */

	@ImmediateService
	public void setArgument(String arg)
		throws ClifException
	{
		StringTokenizer parser = new StringTokenizer(arg);
		try
		{     
			arg_period_ms = Integer.parseInt(parser.nextToken());
			arg_duration_ms = Integer.parseInt(parser.nextToken()) * 1000;
		}
		catch (Exception ex)
		{
			throw new ClifException("probe expects at least 2 arguments: <period_ms> <duration_s>");
		}
		// Probe inner configuration will be done thanks to ArrayList arg_probe_config
        List<String> extraArgs = new ArrayList<String>(parser.countTokens());
		while (parser.hasMoreTokens())
		{
        	extraArgs.add(parser.nextToken());
        }
        setExtraArguments(extraArgs);
	}


	/**
	 * Sets this scenario's unique identifier
	 */
	@ImmediateService
	public void setId(String id)
	{
		probeId = id;
	}


	/**
	 * @return the scenario/blade identifier
	 */
	@ImmediateService
	 public String getId()
	{
		return probeId;
	}


	public void run()
	{
		while (!stopped && (!started || suspended || arg_duration_ms > System.currentTimeMillis() - baseTime_ms))
		{
			try
			{
				dcw.add(doProbe());
			}
			catch (Exception ex)
			{
				bir.alarm(new AlarmEvent(
					System.currentTimeMillis(),
					AlarmEvent.WARNING,
					new ClifException(
						"Exception in probe " + probeId + " while probing",
						ex)));
			}
			try
			{
				long time = System.currentTimeMillis();
				if (!started || suspended || arg_duration_ms > time - baseTime_ms + arg_period_ms)
				{
					Thread.sleep(arg_period_ms);
				}
				else
				{
					long sleep = arg_duration_ms - time + baseTime_ms;
					if (sleep > 0)
					{
						Thread.sleep(sleep);
					}
				}
			}
			catch (InterruptedException ex)
			{
			}
		}
		synchronized (activity_lock)
		{
			terminated = true;
			try
			{
				close();
			}
			catch(Exception ex)
			{
				bir.alarm(new AlarmEvent(
					System.currentTimeMillis(),
					AlarmEvent.WARNING,
					new ClifException(
						"Exception in probe " + probeId + " while terminating",
						ex)));
			}
			if (! stopped)
			{
				bir.completed();
			}
			else
			{
				activity_lock.notify();
			}
		}
	}

	@ImmediateService
	public void changeParameter(String parameter, Serializable value) throws ClifException {
		if (EventStorageState.setEventStorageState(eventStorageStatesMap, parameter, value)) {
			dcw.setFilter(new GenericFilter(false, storeAlarmEvents.getBooleanValue(), storeLifeCycleEvents.getBooleanValue(), storeProbeEvents.getBooleanValue()));
			return;
		}
	}

	@ImmediateService
	public Map<String,Serializable> getCurrentParameters() {
		Map<String,Serializable> parameters = new HashMap<String,Serializable>();
		EventStorageState.putEventStorageStates(parameters, eventStorageStatesMap);
		return parameters;
	}
}
