/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2004, 2007, 2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.lib;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.annotation.ImmediateService;
import org.objectweb.proactive.api.PAActiveObject;
import org.objectweb.proactive.core.component.body.ComponentEndActive;
import org.objectweb.proactive.core.component.body.ComponentInitActive;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.ow2.clif.datacollector.api.DataCollectorWrite;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeInsertResponse;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.api.StorageProxyAdmin;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.api.SupervisorInfo;

/**
 * Implementation of a Blade Insert Adapter component (kind of an asynchronous wrapper to the
 * Blade Insert component).
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class BladeInsertAdapterImpl
	implements
		BladeControl,
		BladeInsertResponse,
		BindingController,
		Runnable,
		ComponentInitActive,
		ComponentEndActive
{
	static final String[] interfaceNames = new String[] {
		SupervisorInfo.SUPERVISOR_INFO,
		BladeControl.BLADE_INSERT_CONTROL,
		StorageProxyAdmin.STORAGEPROXY_ADMIN,
		DataCollectorWrite.DATA_COLLECTOR_WRITE };
	static final int OP_IDLE = 0;
	static final int OP_INIT = 1;
	static final int OP_START = 2;
	static final int OP_STOP = 3;
	static final int OP_SUSPEND = 4;
	static final int OP_RESUME = 5;
	static final int OP_TERM = 6;

	volatile int current_op = OP_IDLE;
	volatile Serializable current_testId = null;
	private final Serializable lock = new ReentrantLock();
	private Thread op_thread;
	volatile boolean exit = false;
	private static final long DEFAULT_TIMEOUT = 5000;//5 seconds

	private SupervisorInfo sis;
	private BladeControl bladeCtl;
	private StorageProxyAdmin spa;
	private DataCollectorWrite dcw;
	private volatile BladeState bladeState = BladeState.DEPLOYED;
	private String bladeId;


	public BladeInsertAdapterImpl() {}

	////////////////////////
	// interface Runnable //
	////////////////////////

	/**
	 * handles asynchronous calls on control operations, enforcing mutual exclusion
	 */
	@ImmediateService
	public void run()
	{
		try{
			synchronized (lock)
			{
				while (!exit)
				{
					switch (current_op)
					{
						case OP_IDLE:
							try
							{
								lock.wait(BladeInsertAdapterImpl.DEFAULT_TIMEOUT);
							}
							catch (InterruptedException ex)
							{
								ex.printStackTrace(System.err);
							}
							break;
						case OP_INIT:
							if (bladeState == BladeState.DEPLOYED
								|| bladeState == BladeState.COMPLETED
								|| bladeState == BladeState.STOPPED
								|| bladeState == BladeState.ABORTED)
							{
								do_init();
							}
							current_op = OP_IDLE;
							lock.notify();
							break;
						case OP_START:
							if (bladeState == BladeState.INITIALIZED)
							{
								do_start();
							}
							current_op = OP_IDLE;
							lock.notify();
							break;
						case OP_STOP:
							if (bladeState == BladeState.DEPLOYED
							    || bladeState == BladeState.INITIALIZED
								|| bladeState == BladeState.SUSPENDED
								|| bladeState == BladeState.RUNNING)
							{
								do_stop(bladeState != BladeState.DEPLOYED);
							}
							current_op = OP_IDLE;
							lock.notify();
							break;
						case OP_SUSPEND:
							if (bladeState == BladeState.RUNNING)
							{
								do_suspend();
							}
							current_op = OP_IDLE;
							lock.notify();
							break;
						case OP_RESUME:
							if (bladeState == BladeState.SUSPENDED)
							{
								do_resume();
							}
							current_op = OP_IDLE;
							lock.notify();
							break;
						default:
							throw new Error("unexpected operation id: " + current_op);
					}
				}
				current_op = OP_TERM;
				lock.notifyAll();
			}
		}catch(Throwable t){
			t.printStackTrace();
		}
	}

/* LifeCycleController removed
	public synchronized void startFc()
	{
		current_op = OP_IDLE;
		bladeState = BladeState.DEPLOYED;
		op_thread = new Thread(this, "Blade adapter control");
		op_thread.start();
	}


	@ImmediateService
	public synchronized void stopFc()
	{
		if (op_thread != null)
		{
			Object lock = op_thread;
			synchronized (lock)
			{
				op_thread = null;
				lock.notifyAll();
				if (current_op != OP_TERM)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
		}
	}
*/

	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	@ImmediateService
	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			return sis;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			return bladeCtl;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			return spa;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			return dcw;
		}
		else
		{
			return null;
		}
	}


	@ImmediateService
	public void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			sis = (SupervisorInfo) serverItf;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			bladeCtl = (BladeControl) serverItf;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			spa = (StorageProxyAdmin) serverItf;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dcw = (DataCollectorWrite) serverItf;
		}
	}


	@ImmediateService
	public void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(SupervisorInfo.SUPERVISOR_INFO))
		{
			sis = null;
		}
		else if (clientItfName.equals(BladeControl.BLADE_INSERT_CONTROL))
		{
			bladeCtl = null;
		}
		else if (clientItfName.startsWith(StorageProxyAdmin.STORAGEPROXY_ADMIN))
		{
			spa = null;
		}
		else if (clientItfName.equals(DataCollectorWrite.DATA_COLLECTOR_WRITE))
		{
			dcw = null;
		}
	}


	@ImmediateService
	public String[] listFc()
	{
		return interfaceNames;
	}


	////////////////////////////
	// interface BladeControl //
	////////////////////////////


	/**
	 * Sets blade argument.
	 */
	@ImmediateService
	public void setArgument(String argument) throws ClifException
	{
		bladeCtl.setArgument(argument);
	}


	/**
	 * Sets the blade identifier.
	 */
	@ImmediateService
	public void setId(String id)
	{
		bladeId = id;
		spa.init(id);
		bladeCtl.setId(bladeId);
	}


	/**
	 * @return the blade identifier
	 */
	@ImmediateService
	public String getId()
	{
		return bladeId;
	}


	/**
	 * asynchronously initialize the blade
	 * @param testId should contain a unique test identifier
	 */
	@ImmediateService
	public synchronized BooleanWrapper init(Serializable testId)
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_testId = testId;
				current_op = OP_INIT;
				lock.notifyAll();
			}
		}
		return new BooleanWrapper(true);
	}

	@ImmediateService
	public int do_init()
	{
		sis.setBladeState(bladeId, BladeState.INITIALIZING);
		try
		{
			spa.newTest(current_testId);
			dcw.init(current_testId, bladeId);
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.INITIALIZING));
			bladeCtl.init(current_testId).getBooleanValue();
			bladeState = BladeState.INITIALIZED;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.INITIALIZED));
			System.gc();
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable ex)
		{
			AlarmEvent alarm = new AlarmEvent(
				System.currentTimeMillis(),
				AlarmEvent.ERROR,
				ex);
			dcw.add(alarm);
			sis.alarm(bladeId, alarm);
			aborted();
		}
		return 0;
	}


	/**
	 * asynchronously starts the blade
	 */
	@ImmediateService
	public synchronized void start()
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_START;
				lock.notifyAll();
			}
		}
	}


	@ImmediateService
	public int do_start()
	{
		sis.setBladeState(bladeId, BladeState.STARTING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STARTING));
			bladeCtl.start();
			bladeState = BladeState.RUNNING;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RUNNING));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
		return 0;
	}


	/**
	 * asynchronously stops the blade
	 */
	@ImmediateService
	public synchronized void stop()
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_STOP;
				lock.notifyAll();
			}
		}
	}


	@ImmediateService
	protected int do_stop(boolean initialized)
	{
		sis.setBladeState(bladeId, BladeState.STOPPING);
		try
		{
			if (initialized)
			{
				dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STOPPING));
				bladeCtl.stop();
			}
			bladeState = BladeState.STOPPED;
			if (initialized)
			{
				dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.STOPPED));
				dcw.terminate();
				spa.closeTest();
			}
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
		return 0;
	}


	/**
	 * asynchronously suspends the blade
	 */
	@ImmediateService
	public synchronized void suspend()
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_SUSPEND;
				lock.notifyAll();
			}
		}
	}


	@ImmediateService
	protected int do_suspend()
	{
		sis.setBladeState(bladeId, BladeState.SUSPENDING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.SUSPENDING));
			bladeCtl.suspend();
			bladeState = BladeState.SUSPENDED;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.SUSPENDED));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
		return 0;
	}


	/**
	 * asynchronously resumes the blade
	 */
	@ImmediateService
	public synchronized void resume()
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (current_op != OP_IDLE)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
				current_op = OP_RESUME;
				lock.notifyAll();
			}
		}
	}


	@ImmediateService
	protected int do_resume()
	{
		sis.setBladeState(bladeId, BladeState.RESUMING);
		try
		{
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RESUMING));
			bladeCtl.resume();
			bladeState = BladeState.RUNNING;
			dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.RUNNING));
			sis.setBladeState(bladeId, bladeState);
		}
		catch (Throwable e)
		{
			e.printStackTrace(System.err);
			aborted();
		}
		return 0;
	}


	/**
	 * Waits for the end of the activity
	 */
	@ImmediateService
	public int join()
	{
		initOpThread();
		if (!exit)
		{
			synchronized(lock)
			{
				while (
					bladeState != BladeState.DEPLOYED
					&& bladeState != BladeState.COMPLETED
					&& bladeState != BladeState.STOPPED
					&& bladeState != BladeState.ABORTED)
				{
					try
					{
						lock.wait(BladeInsertAdapterImpl.DEFAULT_TIMEOUT);
					}
					catch (InterruptedException ex)
					{
						ex.printStackTrace(System.err);
					}
				}
			}
		}
		return 0;
	}

	/**
	 * asynchronously changes a parameter of the blade
	 */
	@ImmediateService
	public synchronized void changeParameter(String parameter, Serializable value) throws ClifException {
		bladeCtl.changeParameter(parameter, value);
	}

	/**
	 * asynchronously gets parameters of the blade
	 */
	@ImmediateService
	public synchronized Map<String,Serializable> getCurrentParameters() {
		return bladeCtl.getCurrentParameters();
	}

	///////////////////////////////////
	// interface BladeInsertResponse //
	///////////////////////////////////


	@ImmediateService
	public void aborted()
	{
		if (!exit)
		{
			synchronized(lock)
			{
				bladeState = BladeState.ABORTED;
				lock.notifyAll();
			}
		}
		dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.ABORTED));
		dcw.terminate();
		spa.closeTest();
		sis.setBladeState(bladeId, bladeState);
	}


	@ImmediateService
	public void completed()
	{
		if (!exit)
		{
			synchronized(lock)
			{
				bladeState = BladeState.COMPLETED;
				lock.notifyAll();
			}
		}
		dcw.add(new LifeCycleEvent(System.currentTimeMillis(), BladeState.COMPLETED));
		dcw.terminate();
		spa.closeTest();
		sis.setBladeState(bladeId, bladeState);
	}
	

	@ImmediateService
	public void alarm(AlarmEvent alarm)
	{
		dcw.add(alarm);
		sis.alarm(bladeId, alarm);
	}

	public void initComponentActivity(Body body) {
        PAActiveObject.setImmediateService("alarm");
        PAActiveObject.setImmediateService("completed");
        PAActiveObject.setImmediateService("aborted");
        PAActiveObject.setImmediateService("getCurrentParameters");
        PAActiveObject.setImmediateService("join");
        PAActiveObject.setImmediateService("do_resume");
        PAActiveObject.setImmediateService("resume");
        PAActiveObject.setImmediateService("suspend");
        PAActiveObject.setImmediateService("do_suspend");
        PAActiveObject.setImmediateService("do_stop");
        PAActiveObject.setImmediateService("stop");
        PAActiveObject.setImmediateService("do_start");
        PAActiveObject.setImmediateService("start");
        PAActiveObject.setImmediateService("do_init");
        PAActiveObject.setImmediateService("init");
        PAActiveObject.setImmediateService("setId");
        PAActiveObject.setImmediateService("getId");
        PAActiveObject.setImmediateService("setArgument");
        PAActiveObject.setImmediateService("listFc");
        PAActiveObject.setImmediateService("unbindFc");
        PAActiveObject.setImmediateService("bindFc");
        PAActiveObject.setImmediateService("lookupFc");
        PAActiveObject.setImmediateService("stopFc");
        PAActiveObject.setImmediateService("startFc");
        PAActiveObject.setImmediateService("getFcState");
        PAActiveObject.setImmediateService("run");

        PAActiveObject.setImmediateService("getComponentParameters");
        PAActiveObject.setImmediateService("migrateControllersDependentActiveObjectsTo");
        PAActiveObject.setImmediateService("getOutputInterceptors");
        PAActiveObject.setImmediateService("getInputInterceptors");
        PAActiveObject.setImmediateService("toString");
        PAActiveObject.setImmediateService("getRepresentativeOnThis");
        PAActiveObject.setImmediateService("setControllerObject");
        PAActiveObject.setImmediateService("getID");
        PAActiveObject.setImmediateService("getBody");
        PAActiveObject.setImmediateService("getReferenceOnBaseObject");
        PAActiveObject.setImmediateService("isFcInternalItf");
        PAActiveObject.setImmediateService("getFcItfType");
        PAActiveObject.setImmediateService("getFcItfOwner");
        PAActiveObject.setImmediateService("getFcItfName");
        PAActiveObject.setImmediateService("getNFType");
        PAActiveObject.setImmediateService("getFcType");
        PAActiveObject.setImmediateService("getFcInterfaces");
        PAActiveObject.setImmediateService("getFcInterface");

        PAActiveObject.setImmediateService("migrateDependentActiveObjectsTo");
        PAActiveObject.setImmediateService("isComposite");
        PAActiveObject.setImmediateService("isPrimitive");
        PAActiveObject.setImmediateService("getHierarchicalType");
        PAActiveObject.setImmediateService("setControllerItfType");
        PAActiveObject.setImmediateService("setItfType");
        PAActiveObject.setImmediateService("checkLifeCycleIsStopped");
        PAActiveObject.setImmediateService("getFcItfOwner");
        PAActiveObject.setImmediateService("getFcItfType");
        PAActiveObject.setImmediateService("getFcItfName");
        PAActiveObject.setImmediateService("isFcInternalItf");
        PAActiveObject.setImmediateService("initController");
        PAActiveObject.setImmediateService("getState");
        PAActiveObject.setImmediateService("duplicateController");
        PAActiveObject.setImmediateService("removeFcSubComponent");
        PAActiveObject.setImmediateService("addFcSubComponent");
        PAActiveObject.setImmediateService("removeFcSubComponent");
        PAActiveObject.setImmediateService("addFcSubComponent");
        PAActiveObject.setImmediateService("isSubComponent");
        PAActiveObject.setImmediateService("getFcSubComponents");
        PAActiveObject.setImmediateService("getFcInternalInterfaces");
        PAActiveObject.setImmediateService("setControllerItfType");

        PAActiveObject.setImmediateService("equals");
		PAActiveObject.setImmediateService("hashCode");
		PAActiveObject.setImmediateService("isInternal");
		PAActiveObject.setImmediateService("isStreamItf");
		PAActiveObject.setImmediateService("isGCMCollectiveItf");
		PAActiveObject.setImmediateService("isGCMMulticastItf");
		PAActiveObject.setImmediateService("isGCMGathercastItf");
		PAActiveObject.setImmediateService("isGCMCollectionItf");
		PAActiveObject.setImmediateService("isGCMSingletonItf");
		PAActiveObject.setImmediateService("getGCMCardinality");
		PAActiveObject.setImmediateService("isFcSubTypeOf");
		PAActiveObject.setImmediateService("isFcOptionalItf");
		PAActiveObject.setImmediateService("isFcCollectionItf");
		PAActiveObject.setImmediateService("isFcClientItf");
		PAActiveObject.setImmediateService("getFcItfSignature");
		PAActiveObject.setImmediateService("getFcItfName");
		PAActiveObject.setImmediateService("endComponentActivity");
	}

	/**
	 * Terminates the op_thread gracefully
	 */
	@ImmediateService
	public void endComponentActivity(Body body) {
		synchronized (lock) {
			exit=true;
			lock.notifyAll();
			if(op_thread != null){
				op_thread.interrupt();
			}
		}
	}

	/**
	 * This method is intended to initialize the op_thread.
	 * It must be called before all API methods
	 */
	@ImmediateService
	protected synchronized void initOpThread() {
		//This code was initially in the constructor but moved here
		//because of object copy in ProActive world & AO
		if(op_thread == null){
			op_thread = new Thread(null, (BladeInsertAdapterImpl) PAActiveObject.getStubOnThis(), "Blade adapter control");
			op_thread.start();
			current_op = OP_IDLE;
		}
	}
}
