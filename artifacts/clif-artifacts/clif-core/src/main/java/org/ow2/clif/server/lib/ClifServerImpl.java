/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2010-2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.proactive.annotation.ImmediateService;
import org.objectweb.proactive.core.component.adl.FactoryFactory;
import org.objectweb.proactive.core.node.Node;
import org.objectweb.proactive.core.node.NodeFactory;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.server.api.BladeControl;
import org.ow2.clif.server.api.BladeType;
import org.ow2.clif.server.api.ClifServerControl;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.ExecutionContext.InitObject;


/**
 * Implementation of a CLIF server, supporting blade creation and removal.
 * @author Bruno Dillenseger
 */
public class ClifServerImpl
	implements ClifServerControl
{
	/**
	 * Creates a CLIF server in current/local Fractal environment, and
	 * binds it with the given name in the given registry. If this name
	 * is already bound, the existing binding is overridden by this one.
	 * @param name bound name in the registry
	 * @param registry registry where to bind this new CLIF server
	 * @return the newly created clifserver
	 */
	static public Component create(String name, ClifRegistry registry, Node node)
		throws ClifException
	{
		try
		{
			Factory factory = FactoryFactory.getFactory();
			Map<String, Object> context = null;
			if(node != null){
				context = new HashMap<String, Object>();
				List<Node> nodes = new ArrayList<Node>();
				nodes.add(node);
				context.put("nodes", nodes);
			}
			Component clifServer = (Component) factory.newComponent(ClifServerImpl.class.getName(), context);
			if(node != null){
				//clif server instantiated on a remote host... maybe need initialization of ExecutionContext
				ExecutionContext.InitObject initExecutionContext = ExecutionContext.getExecutionContext();
				((ClifServerControl)clifServer.getFcInterface(ClifServerControl.CLIF_SERVER_CONTROL)).initContext(initExecutionContext);
			}
			GCM.getGCMLifeCycleController(clifServer).startFc();
			registry.bindServer(name, clifServer);
			return clifServer;
		}
		catch (Exception ex)
		{
			throw new ClifException("Could not create ClifServer " + name, ex);
		}
	}

	@Override
	@ImmediateService
	public synchronized void initContext(InitObject initExecutionContext) {
		ExecutionContext.init(initExecutionContext);
		System.out.println("Server component started and initialized");
	}


	/**
	 * Creates a CLIF server and binds it in the CLIF registry. Optionally preloads
	 * some classes if the class prefetch configuration file is found. 
	 * @param args if no argument is given, the CLIF server is bound to a default name
	 * that is the network name of the local environment. Otherwise, the new CLIF server
	 * is bound to the name given by the first argument (i.e. args[0]).
	 * @see ExecutionContext#PREFETCH_FILE
	 */
	static public void main(String[] args)
	throws Exception
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		List<Class<?>> prefetchedClasses = new ArrayList<Class<?>>();
		try
		{
			BufferedReader prefetchReader = new BufferedReader(new FileReader(
				new File(ExecutionContext.getBaseDir(), ExecutionContext.PREFETCH_FILE)));
			String classname = prefetchReader.readLine();
			while (classname != null)
			{
				prefetchedClasses.add(Class.forName(classname));
				classname = prefetchReader.readLine();
			}
			prefetchReader.close();
			System.gc();
			long time = System.currentTimeMillis();
			long memory = Runtime.getRuntime().freeMemory();
			ClifClassLoader.fetchClassDependencies(prefetchedClasses);
			time -= System.currentTimeMillis();
			System.gc();
			memory -= Runtime.getRuntime().freeMemory();
			System.out.println(
				"Prefetched "
				+ prefetchedClasses.size()
				+ " classes (about "
				+ (memory >> 10)
				+ "kB in "
				+ (-time)
				+ "ms).");
		}
		catch (FileNotFoundException ex)
		{
			// just ignore
		}
		catch (Exception ex)
		{
			System.out.println("Class prefetching failed: " + ex);
		}
		String name = args.length >= 1 ? args[0] : InetAddress.getLocalHost().getHostName();
		Node node = args.length >= 2 ? ((!args[1].equals(""))?NodeFactory.getNode(args[1]): null) : null;
		create(name, ClifRegistry.getInstance(false), node);
		if(node == null){
			System.out.println("CLIF server " + name + " is ready");
		}else{
			System.out.println("CLIF server " + name + " is ready on " + args[1]);
		}
	}


	/** contains blade components of this CLIF server, indexed by their identifier */
	protected HashMap<String, Component> blades = new HashMap<String, Component>();


	/**
	 * Does nothing.
	 */
	public ClifServerImpl()
	{
	}


	/////////////////////////////////
	// interface ClifServerControl //
	/////////////////////////////////


	/**
	 * Creates a new blade component managed by this Clif server from an ADL definition.
	 * Once the blade component is instantiated, its setId() and setArgument() methods
	 * specified by interface BladeControl are then called.
	 * @param adlDefinition ADL definition name of a blade implementation
	 * @param adlParams ADL parameters that will be used for blade instantiation
	 * (parameters used by the ADL definition)
	 * @param bladeId identifier for the new blade (see {@link BladeControl#setId(String)}),
	 * also used to name the new blade component with regard to Fractal NameController
	 * @param argument blade argument (used to call method setArgument() in BladeControl
	 * @return the new Blade component
	 * @throws ClifException
	 */
	@Override
	@ImmediateService
	public synchronized Component addBlade(
		String adlDefinition,
		Map<String,String> adlParams,
		String bladeId,
		String argument)
	throws ClifException
	{
		try
		{
			Map<String, Object> context = new HashMap<String, Object>();
			context.putAll(adlParams);
			context.put("classloader", ClifClassLoader.getClassLoader());
			//To bypass the ProActive problem related to immediate services on the composite component.
			//Blade is built thanks to the API...
			BladeType bt = (BladeType) Class.forName(adlDefinition).newInstance();
			Component blade = bt.createBlade(bladeId, context);
			BladeControl bladeCtl = (BladeControl)blade.getFcInterface(BladeControl.BLADE_CONTROL);
			bladeCtl.setId(bladeId);
			bladeCtl.setArgument(argument);
			blades.put(bladeId, blade);
			return blade;
		}
		catch (NoSuchInterfaceException ex)
		{
			ClifClassLoader.clear();
			ex.printStackTrace();
			throw new Error(
				"Bad configuration: unable to add blade " + bladeId + " to the Clif Application",
				ex);
		}
		catch (Throwable ex)
		{
			ClifClassLoader.clear();
			ex.printStackTrace();
			throw new ClifException("Unable to deploy blade " + bladeId, ex);
		}
	}


	/**
	 * Removes every blade from this CLIF server,
	 * and clears the CLIF class loader cache.
	 * @throws ClifException if at least one blade could not be removed.
	 * Note: all removable blades are removed before throwing the exception,
	 * and the CLIF class loader cache is cleared anyway.
	 */
	@Override
	@ImmediateService
	public synchronized void removeAllBlades()
	throws ClifException
	{
		ClifException exception = null;
		String[] keys = blades.keySet().toArray(new String[blades.size()]);
		for (String id : keys)
		{
			try
			{
				removeBlade(id);
			}
			catch (ClifException ex)
			{
				exception = ex;
			}
		}
		if (exception != null)
		{
			ClifClassLoader.clear();
			throw exception;
		}	
	}


	/**
	 * Removes a blade from this Clif server.
	 * Once this blade is removed, the CLIF class loader cache is cleared if and only
	 * if there is no other blade remaining in this CLIF server.
	 * @param id blade identifier
	 * @throws ClifException if this blade is unknown by this CLIF server.
	 */
	@Override
	@ImmediateService
	public synchronized void removeBlade(String id)
	throws ClifException
	{
	    Component blade = blades.remove(id);
	    try{
			GCM.getLifeCycleController(blade).stopFc();
	    }catch(Throwable t){
			//miam miam
	    }
	    try{
			GCM.getGCMLifeCycleController(blade).stopFc();
	    }catch(Throwable t){
			//miam miam
	    }
	    try{
			for(Component subComponent : GCM.getContentController(blade).getFcSubComponents()){
				GCM.getGCMLifeCycleController(subComponent).terminateGCMComponent();
			}
	    }catch(Throwable t){
			//miam miam
	    }
	    try{
			GCM.getGCMLifeCycleController(blade).terminateGCMComponent();
	    }catch(Throwable t){
			//miam miam
	    }
	    if (blade == null)
	    {
	        throw new ClifException("Unknown blade identifier " + id);
	    }
		if (blades.isEmpty())
        {
            ClifClassLoader.clear();
        }
	}
}
