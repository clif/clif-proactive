/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;
import java.util.ArrayList;
import java.util.List;

/**
 * Insert for RTP probe
 * 
 * @author Rémi Druilhe
 */
public class Insert extends AbstractDumbInsert
{
	private RTPStats rtpStats;
	
	public Insert()
	{
		super();
		
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + RTPEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}
	
	@Override
	protected void setExtraArguments(List<String> arg_probe_config) throws ClifException
	{
        if (!arg_probe_config.isEmpty())
        {
        	ArrayList<Integer> localPorts = new ArrayList<Integer>();
        	
        	if(arg_probe_config.get(0).contains("-"))
        	{
        		Integer interval = 1;
        		String[] tempPorts = null;
        		
        		if(arg_probe_config.get(0).contains("/"))
        		{
        			String[] temp = arg_probe_config.get(0).split("/");
        			interval = new Integer(temp[1]);
        			
        			tempPorts = temp[0].split("-");
        		}
        		else
        		{
        			tempPorts = arg_probe_config.get(0).split("-");
        		}
        		
        		Integer minTempPort = new Integer(tempPorts[0]);
        		Integer maxTempPort = new Integer(tempPorts[1]);
        		
        		for(Integer i=minTempPort; i<=maxTempPort; i+=interval)
    			{
    				localPorts.add(new Integer(i.toString()));
    			}
        	}
        	else
        	{
        		localPorts.add(new Integer(arg_probe_config.get(0)));
        	}
        	
        	rtpStats = RTPStats.getInstance();
        	rtpStats.openStats(localPorts);
    		rtpStats.startStats();
        } 
        else
        {
        	throw new IsacRuntimeException("No port(s) found. Can't configure probe.");
        }
	}

	@Override
	public ProbeEvent doProbe()
	{
		RTPEvent event = null;
		
		long[] results = rtpStats.getResults();
		
		if(results.length != 0)
		{
			event = new RTPEvent(System.currentTimeMillis(), results);
		}
		
		return event;
	}
	
	@Override
	protected void close()
	{
		rtpStats.close();
	}

	@Override
	public String getHelpMessage()
	{
		return super.getHelpMessage() + " <port number or range>";
	}
}
