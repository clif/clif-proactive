/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2005, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;


import java.io.Serializable;

import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.objectweb.proactive.core.util.wrapper.IntWrapper;
import org.objectweb.proactive.core.util.wrapper.LongWrapper;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.StringSplitter;


/**
 * Class of events produced by load injectors.
 * @author Bruno Dillenseger
 */
public class ActionEvent extends AbstractEvent
{
	private static final long serialVersionUID = 1661170565610135600L;
	static public final String EVENT_TYPE_LABEL = "action";
	static private final String[] EVENT_FIELD_LABELS =
		new String[] {
			"date",
			"session id",
			"action type",
			"iteration",
			"success",
			"duration",
			"comment",
			"result"};


	static
	{
		AbstractEvent.registerEventFieldLabels(
			EVENT_TYPE_LABEL,
			EVENT_FIELD_LABELS,
			new EventFactory() {
				@Override
				public BladeEvent makeEvent(String separator, String line)
					throws ClifException
				{
					try
					{
						String[] values = StringSplitter.split(line, separator, new String[8]);
						ActionEvent event= new ActionEvent(
							Long.parseLong(values[0]),
							Long.parseLong(values[1]),
							values[2],
							Long.parseLong(values[3]),
							values[4].equalsIgnoreCase(Boolean.toString(true)),
							Integer.parseInt(values[5]),
							values[6],
							values[7]);
						return event;
					}
					catch (Exception ex)
					{
						throw new ClifException(
							"Could not make an ActionEvent instance from " + line,
							ex);
					}
				}
			});
	}


	public String type;
	public long iteration;
	public long sessionId;
	public boolean successful;
	public int duration;
	public Serializable result;
	public String comment;


	public ActionEvent()
	{
		super();
	}


	public ActionEvent(
		long date,
		long sessionId,
		String type,
		long iteration,
		boolean successful,
		int duration,
		String comment,
		Serializable result)
	{
		super(date);
		this.type = type;
		this.iteration = iteration;
		this.sessionId = sessionId;
		this.successful = successful;
		this.duration = duration;
		this.comment = comment;
		this.result = result;
	}


	public BooleanWrapper isSuccessful()
	{
		return new BooleanWrapper(successful);
	}


	public IntWrapper getDuration()
	{
		return new IntWrapper(duration);
	}


	public LongWrapper getSession()
	{
		return new LongWrapper(sessionId);
	}


	public Serializable getResult()
	{
		return result;
	}


	public String getComment()
	{
		return comment;
	}


	public String getType()
	{
		return type;
	}


	@Override
	public String toString()
	{
		return toString(0, DEFAULT_SEPARATOR);
	}


	//////////////////////////
	// BladeEvent interface //
	//////////////////////////


	@Override
	public String getTypeLabel()
	{
		return EVENT_TYPE_LABEL;
	}


	@Override
	public String toString(long dateOrigin, String separator)
	{
		return
			(date - dateOrigin) + separator
			+ sessionId + separator
			+ type + separator
			+ iteration + separator
			+ successful + separator
			+ duration + separator
			+ comment + separator
			+ result;
	}


	@Override
	public String[] getFieldLabels()
	{
		return EVENT_FIELD_LABELS;
	}


	@Override
	public Object getFieldValue(String fieldLabel)
	{
		if (fieldLabel != null)
		{
			if (fieldLabel.equals(EVENT_FIELD_LABELS[0]))
			{
				return new Long(date);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[1]))
			{
				return new Long(sessionId);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[2]))
			{
				return type;
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[3]))
			{
				return new Long(iteration);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[4]))
			{
				return new Boolean(successful);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[5]))
			{
				return new Integer(duration);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[6]))
			{
				return comment;
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[7]))
			{
				return result;
			}
		}
		return null;
	}
}
