/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*
* @authors: Julien Buret
* @authors: Nicolas Droze
*/

package org.ow2.clif.scenario.transitions;

import java.util.Properties;
import java.util.StringTokenizer;

public class Transition {

	private String id;
	//private String action;
	private String[] args;
	private boolean isEndSession = false;
	private Properties prop;

	private StringTokenizer st;
	private int nbTokens;
	private String parsedId;

	
	public Transition() {
	}

	/**
	 * Set the transition and parse the parameters: id, action, and optional arguments.
	 * @param transition The complete String from the matrix.
	 */
	public void setTransition(String transition) {
		if (transition == null)
			isEndSession = true;
		else {
			isEndSession = false;
			st = new StringTokenizer(transition, ";");
			nbTokens = st.countTokens();

			id = st.nextToken();
			//action = st.nextToken();

			args = new String[nbTokens - 1];

			for (int i = 0; i < nbTokens - 1; i++) {
				args[i] = st.nextToken();
			}
		}
	}

	public int getId() {
		try {
			// The ID begins with $, it is a variable
			// We replace the variable with its value from the properties
			if (id.indexOf("$") == 0) {
				parsedId = prop.getProperty(id.substring(1, id.length()));
				return Class
					.forName(getIdClass(parsedId))
					.getDeclaredField(getIdField(parsedId))
					.getInt(null);
			}
			// Else the ID is written entirely and we parse the string
			else {
				return Class.forName(getIdClass(id)).getDeclaredField(
					getIdField(id)).getInt(
					null);
			}

		} catch (Exception e) {
			System.out.println(
				"Error parsing data: A value maybe incorrect or not defined: ");
			e.printStackTrace();
			return -1;
		}
	}

	public String[] getAction() {
		return args;
	}

/*	public String[] getArgs() {
		return args;
	}
*/
/*	public int getNbArgs() {
		if (args == null)
			return 0;
		else
			return args.length;
	}
*/
	public boolean isEndSession() {
		return isEndSession;
	}

	private String getIdClass(String id) {
		return id.substring(0, id.lastIndexOf("."));
	}

	private String getIdField(String id) {
		return id.substring(id.lastIndexOf(".") + 1, id.length());
	}

	public void setProperties(Properties prop) {
		this.prop = prop;
	}
}
