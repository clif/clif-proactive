/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Iterator;


/**
 * That is the main class of the statistical analyzer dedicated to
 * CLIF.
 * 
 * @author Guy Vachet
 */
public class AnalyzerImpl implements Constants {
	public static final boolean VERBOSE = false;
	public static final String LABEL = "Statistical analyze";
	public static final String FR_DATE_PATTERN = "EEEEE dd MMMMM yyyy HH:mm:ss";
	// What and how you want analyze
	private AnalyzerParameters parameters = null;
	private FileStoreReader storeReader = null;
	private TestFilter testFilter = null;
	private BladeFilter bladeFilter = null;
	// in order to set an analysis range of elapsed time
	private long minCommonTime;
	private long maxCommonTime;
	private String analyzeRangeLabel;

	/**
	 * 
	 */
	public AnalyzerImpl() {
	}

	// ///////////////////////
	// Setters and Getters //
	// ///////////////////////

	public void setAnalyzeParameters(AnalyzerParameters parameters) {
		this.parameters = parameters;
	}

	public AnalyzerParameters getAnalyzeParameters() {
		return parameters;
	}

	public FileStoreReader getStoreReader() {
		return storeReader;
	}

	public void setStoreReader(FileStoreReader storeReader) {
		this.storeReader = storeReader;
	}

	public void setTestFilter(TestFilter filter) {
		testFilter = filter;
	}

	public TestFilter getTestFilter() {
		return testFilter;
	}

	public void setBladeFilter(BladeFilter filter) {
		bladeFilter = filter;
	}

	public BladeFilter getBladeFilter() {
		return bladeFilter;
	}

	public void setMinCommonTime(long minTime) {
		if (minTime > minCommonTime) {
			minCommonTime = minTime;
		}
	}

	public long getMinCommonTime() {
		return minCommonTime;
	}

	public void setMaxCommonTime(long maxTime) {
		if (maxTime < maxCommonTime) {
			maxCommonTime = maxTime;
		}
	}

	public long getMaxCommonTime() {
		return maxCommonTime;
	}
	
	public void setAnalyzeRangeLabel(String analyzeRangeLabel) {
		this.analyzeRangeLabel = analyzeRangeLabel;
	}

	public String getAnalyzeRangeLabel() {
		if (analyzeRangeLabel == null) {
			analyzeRangeLabel = getAnalyzeParameters().getAnalyzeRangeLabel();
		}
		return analyzeRangeLabel;
	}
	
	// ///////////////
	// CLIF stuffs //
	// ///////////////

	public void initStoreReader() {
		// get access to the storage component
		setStoreReader(new FileStoreReader(getAnalyzeParameters().getReportPath()));
	}

	public void initTestFilter() {
		setTestFilter(new TestFilterImpl(getAnalyzeParameters().getTestNameFilter()));
	}

	public void initBladeFilter() {
		BladeFilterImpl filter = null;
		Iterator<String> iter = null;
		if (getAnalyzeParameters().getEventTypeFilter() != null) {
			// Filtering based on blade event type
			filter = new BladeFilterImpl();
			for (iter = getAnalyzeParameters().getEventTypeFilter().iterator(); iter.hasNext();)
				filter.addEventFilter(iter.next());
		}
		if (getAnalyzeParameters().getBladeIdFilter() != null) {
			// Filtering based on blade identifier
			if (filter == null)
				filter = new BladeFilterImpl();
			for (iter = getAnalyzeParameters().getBladeIdFilter().iterator(); iter.hasNext();)
				filter.addIdFilter(iter.next());
		}
		setBladeFilter(filter);
	}

	// ///////////////////
	// Analyze methods //
	// ///////////////////

	private void analyzeInjection(String testName, BladeDescriptor[] blades,
			DateFilter filter) throws ClifException {
		InjectorAnalyst analyst = null;
		for (int i = 0; i < blades.length; i++) {
			if (blades[i].isInjector()) {
				if (analyst == null) {
					// atypical data sort is required to perform analyze
					analyst = new InjectorAnalyst(getAnalyzeRangeLabel());
					analyst.setRegexCtrl(getAnalyzeParameters().getRegexCtrl());
					analyst.setStatSortFactor(getAnalyzeParameters().getInjectorStatSortFactor());
					analyst.setStatSortRatio(getAnalyzeParameters().getInjectorStatSortRatio());
				}
				System.out.println("\t" + blades[i]);
				analyst.addProfilingData(getStoreReader().getBladeReader(
						testName, blades[i]), filter);
			}
		}
		if (analyst != null) {
			System.out.println();
			analyst.outputAnalysis(getAnalyzeParameters().isDetailedAnalysis(),
					getAnalyzeParameters().getSliceSize());
			System.out.println();
		}
	}

	private void analyzeCpuProbes(String testName, BladeDescriptor[] blades,
			DateFilter filter) throws ClifException {
		CpuAnalyst analyst = null;
		for (int i = 0; i < blades.length; i++) {
			if (blades[i].isProbe()) {
				String[] evtTypeLabs = blades[i].getEventTypeLabels();
				for (int j = 0; j < evtTypeLabs.length; j++) {
					if (evtTypeLabs[j].equalsIgnoreCase(CPU_EVENT_TYPE_LABEL)) {
						if (analyst == null) {
							analyst = new CpuAnalyst(getAnalyzeRangeLabel());
							// atypical data sort any more used because of
							// raw data analysis, so no analysis tuning
						}
						System.out.println("\t" + blades[i]);
						analyst.addProfilingData(getStoreReader()
								.getBladeReader(testName, blades[i]), filter);
					}
				}
			}
		}
		if (analyst != null) {
			System.out.println();
			analyst.outputAnalysis(getAnalyzeParameters().isDetailedAnalysis(),
					getAnalyzeParameters().getSliceSize());
			System.out.println();
		}
	}

	private void analyzeMemoryProbes(String testName, BladeDescriptor[] blades,
			DateFilter filter) throws ClifException {
		MemoryAnalyst analyst = null;
		for (int i = 0; i < blades.length; i++) {
			if (blades[i].isProbe()) {
				String[] evtTypeLabs = blades[i].getEventTypeLabels();
				for (int j = 0; j < evtTypeLabs.length; j++) {
					if (evtTypeLabs[j].equalsIgnoreCase(MEMORY_EVENT_TYPE_LABEL)) {
						if (analyst == null) {
							analyst = new MemoryAnalyst(getAnalyzeRangeLabel());
						}
						System.out.println("\t" + blades[i]);
						analyst.addProfilingData(getStoreReader()
								.getBladeReader(testName, blades[i]), filter);
					}
				}
			}
		}
		if (analyst != null) {
			System.out.println();
			analyst.outputAnalysis(getAnalyzeParameters().isDetailedAnalysis(),
					getAnalyzeParameters().getSliceSize());
			System.out.println();
		}
	}

	private void analyzeJvmProbes(String testName, BladeDescriptor[] blades,
			DateFilter filter) throws ClifException {
		JvmAnalyst analyst = null;
		for (int i = 0; i < blades.length; i++) {
			if (blades[i].isProbe()) {
				String[] evtTypeLabs = blades[i].getEventTypeLabels();
				for (int j = 0; j < evtTypeLabs.length; j++) {
					if (evtTypeLabs[j].equalsIgnoreCase(JVM_EVENT_TYPE_LABEL)) {
						if (analyst == null) {
							analyst = new JvmAnalyst(getAnalyzeRangeLabel());
						}
						System.out.println("\t" + blades[i]);
						analyst.addProfilingData(getStoreReader()
								.getBladeReader(testName, blades[i]), filter);
					}
				}
			}
		}
		if (analyst != null) {
			System.out.println();
			analyst.outputAnalysis(getAnalyzeParameters().isDetailedAnalysis(),
					getAnalyzeParameters().getSliceSize());
			System.out.println();
		}
	}

	/**
	 * 
	 * @param testName
	 * @throws ClifException
	 */
	public void analyzeTest(String testName) throws ClifException {
		BladeDescriptor[] bladesc = null;
		DateFilter dateFilter;
		BladeStoreReader bladeReader;
		LifeCycleReader lifeCycleReader;
		minCommonTime = DEFAULT_MIN_TIME;
		maxCommonTime = DEFAULT_MAX_TIME;
		// get list of blades thru the optional blade filter
		bladesc = getStoreReader().getTestPlan(testName, getBladeFilter());
		// first of all, set the min and the max common time thru LifeCycles
		for (int i = 0; i < bladesc.length; i++) {
			bladeReader = getStoreReader().getBladeReader(testName, bladesc[i]);
			//if (VERBOSE) System.out.println("\t" + bladeReader);
			lifeCycleReader = new LifeCycleReader(bladeReader);
			setMinCommonTime(lifeCycleReader.getMinTime());
			setMaxCommonTime(lifeCycleReader.getMaxTime());
		}
		dateFilter = new DateFilter(getMinCommonTime(), getMaxCommonTime());
		dateFilter.updateBounds(getAnalyzeParameters().getTimeStart(), getAnalyzeParameters()
				.getTimeEnd());
		if (VERBOSE)
			System.out.println(dateFilter);
		// ready to extract profiling data
		System.out.println();
		analyzeInjection(testName, bladesc, dateFilter);
		analyzeCpuProbes(testName, bladesc, dateFilter);
		analyzeMemoryProbes(testName, bladesc, dateFilter);
		analyzeJvmProbes(testName, bladesc, dateFilter);
		// todo: what about other probes?
	}
	
	/**
	 * 
	 */
	public void analyzeAllTests() throws Exception {
		TestDescriptor[] allTests = null;
		String testName = null;
		StringBuffer sb = null;
		// in order to display date in French format
		SimpleDateFormat sdf = new SimpleDateFormat(FR_DATE_PATTERN,
				new Locale("fr", "FR"));
		sdf.setCalendar(new GregorianCalendar());
		// get list of tests thru the test id filter
		allTests = getStoreReader().getTests(getTestFilter());
		if (getAnalyzeParameters().isOutputFile()) {
			String childOutputFile = null;
			File result = null;
			PrintStream outStream = null;
			try {
				for (int i = 0; i < allTests.length; i++) {
					testName = allTests[i].getName();
					childOutputFile = testName + ".txt";
					result = new File(getAnalyzeParameters().getResultFilePath(),
							childOutputFile);
					outStream = new PrintStream(new BufferedOutputStream(
							new FileOutputStream(result)));
					System.setOut(outStream);
					sb = new StringBuffer(testName);
					sb.append(" : Analyse du ");
					System.out.println(sb.append(sdf.format(new Date())));
					analyzeTest(testName);
					outStream.close();
				}
			} catch (Exception e) {
				sb = new StringBuffer("Unable to set output file as ");
				sb.append(result).append(" for test ");
				throw new Exception(sb.append(testName).toString(), e);
			} finally {
				try {
					if (outStream != null)
						outStream.close();
				} catch (Exception e) {
					sb = new StringBuffer("Unable to release output file for test ");
					throw new Exception(sb.append(testName).toString(), e);
				}
			}
		} else {
			for (int i = 0; i < allTests.length; i++) {
				testName = allTests[i].getName();
				sb = new StringBuffer(testName);
				sb.append(" : Analyse du ");
				System.out.println(sb.append(sdf.format(new Date())));
				analyzeTest(testName);
			}
		}
		if (allTests.length < 1) {
			System.out.println("WARNING: nothing to analyze!");
		}
	}

    /**
     */
    public static void printUsageOfCmdLine() {
        StringBuffer sb = new StringBuffer("Usage: java -cp ..... ");
        sb.append("..AnalyzerImpl [args] where arguments include:\n");
        sb.append("  [-f|-file] <fileName>  Input of analyze parameters (default: ");
        System.out.println(sb.append(DEFAULT_ANALYZE_PROPERTY_FILE).append(")\n"));
    }

    /**
     */
    public static String getFileFromCmdLine(String[] args) {
        String fileName = null;
        for (int n = 0; n < args.length; n++) {
            if ((n + 1 < args.length) && (args[n].trim().equals("-file"))
            		|| ((n + 1 < args.length) && (args[n].trim().equals("-f")))) {
                fileName = args[++n].trim();
                break;
            }
        }
        return fileName;
    }

	/**
	 * @param args
	 */
	static public void main(String[] args) {
		AnalyzerImpl analyzer = new AnalyzerImpl();
		try {
			AnalyzerParameters params = new AnalyzerParameters();
			if (args.length > 1) {
				params.loadParameters(getFileFromCmdLine(args));
			} else {
				params.loadParameters(DEFAULT_ANALYZE_PROPERTY_FILE);
			}
			analyzer.setAnalyzeParameters(params);
			System.out.println(params);
		} catch (Exception e) {
			System.err.println("Exception when set analyze parameters: " +
					e.getMessage());
			if (args.length > 1)
				AnalyzerImpl.printUsageOfCmdLine();
			e.printStackTrace();
			System.exit(1);
		}
		try {
			analyzer.initStoreReader();
			analyzer.initTestFilter();
			analyzer.initBladeFilter();
			analyzer.analyzeAllTests();
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
	}
}
