/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2011 France Telecom
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.io.IOException;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.util.ExecutionContext;


/**
 * Batch command to print the list of all CLIF servers registered in the Registry,
 * or the list of CLIF serversused by one or several test plans.
 * 
 * @author Bruno Dillenseger
 */
public class ListServersCmd
{
	/**
	 * @see #run()
	 * @param args if no argument is provided, this command prints
	 * the list of all CLIF servers registered in the Registry; if
	 * one or several test plan file names are given, this command
	 * prints the list of CLIF servers used by th/is/ese test plans.
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length > 0)
		{
			try
			{
				TestPlanReader.main(args);
			}
			catch (IOException ex)
			{
				System.err.println("Error while trying to read test plan file(s): " + ex);
				System.exit(BatchUtil.ERR_ARGS);
			}
		}
		else
		{
			ExecutionContext.init("./");
			System.exit(run()); 
		}
	}


	/**
	 * Prints the list of registered CLIF servers.
	 * @return command status code (@see BatchUtil)
	 */
	static public int run()
	{
		try
		{
			ClifRegistry clifReg = null;
			try
			{
				System.out.println("Trying to connect to the CLIF Registry...");
				clifReg = ClifRegistry.getInstance(false);
				System.out.println("Connected to " + clifReg);
			}
			catch (Exception ex)
			{
				return BatchUtil.ERR_REGISTRY;
			}
			for (String server : clifReg.getServers())
			{
				System.out.println(server);
			}
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.err.println("Execution problem while getting the list of CLIF servers from the registry.");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
