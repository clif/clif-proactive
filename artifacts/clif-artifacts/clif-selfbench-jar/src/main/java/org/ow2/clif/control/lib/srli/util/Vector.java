/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.srli.util;

import org.ow2.clif.control.lib.srli.InjectionDriverException;

/**
 * Vector representation and arithmetic, as a specialization of Matrix
 * with n lines and a single column.
 * @author Colin PUY
 * @author Bruno Dillenseger
 */
public class Vector extends Matrix
{
	/**
	 * Creates a new vector of the given dimension
	 * @param dimension the number of coefficients hold by the new vector
	 */
	public Vector(int dimension)
	{
		// a vector is a single column matrix
		super(dimension, 1);
	}

	/**
	 * Copy constructor
	 */
	public Vector(Vector vector)
	{
		super(vector);
	}
	
	/**
	 * Creates a new vector out of a single column matrix
	 * @param matrix the source matrix, assumed to be a single-column matrix
	 * @return a new vector holding the source matrix's coefficient
	 */
	public Vector(Matrix matrix)
	{
		this(matrix.getLines());
		try
		{
			setData(matrix.getData());
		}
		catch (InjectionDriverException ex)
		{
			// should never occur
			throw new Error("Algorithm error in single-column matrix to vector conversion.", ex);
		}
	}

	/**
	 * Get the norm of this vector
	 * @return this vector's norm
	 */
	public double getNorm() 
	{
		double norm = 0;
		double sum = 0;

		for (int i = 0; i < getLines(); i++)
		{
			try
			{
				sum += Math.pow(getData(i), 2);
			}
			catch (InjectionDriverException ex)
			{
				// should never occur
				throw new Error("Algorithm error in vector norm calculus.", ex);
			}
		}
		norm = Math.sqrt(sum);
		return norm;
	}

	/**
	 * Set the coefficient value at a given position in this vector
	 * @param index the coefficient position in this vector (starting with 0)
	 * @param value	the coefficient value to set
	 * @throws InjectionDriverException if the specified position is out
	 * of this vector's bounds
	 */
	public void setData(int index, double value) throws InjectionDriverException
	{
		setData(index, 0, value);
	}

	/**
	 * Get the coefficient value at a given position in this vector
	 * @param index the coefficient position in this vector (starting with 0)
	 * @return the coefficient value at the specified position
	 * @throws InjectionDriverException if the specified position is out
	 * of this vector's bounds
	 */
	public double getData(int index) throws InjectionDriverException
	{
		return getData(index, 0);
	}
}
