/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.control.lib.saturation.SaturationController;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.TestControl;
import org.ow2.clif.util.ExecutionContext;

/**
 * Runs a SaturationController component attached to a given deployed test.
 * @author Bruno Dillenseger
 */
public class SaturationControlCmd
{
	/**
	 * @param args args[0] is the name of the target deployed test plan
	 */
	public static void main(String[] args)
	{
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length != 1)
		{
			BatchUtil.usage("expected argument: <name of deployed test plan>");
		}
		ExecutionContext.init("./");
		run(args[0]);
	}


	/**
	 * @param testPlanName the name of the target deployed test plan
	 */
	static public void run(String testPlanName)
	{
		try
		{
			ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlanName);
			TestControl tcItf = (TestControl)
				clifApp.getComponentByName("supervisor")
				.getFcInterface(TestControl.TEST_CONTROL);
			@SuppressWarnings("unused")
			SaturationController ctrl = new SaturationController(tcItf);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			System.exit(BatchUtil.ERR_EXEC);
		}
	}
}
