/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.srli.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Miscellaneous String utilities for pretty printing
 * 
 * @author Colin PUY
 * @author Bruno Dillenseger
 */
public abstract class StringUtil
{
	//////////////////////////////////////////
	// String utilities for pretty printing //
	//////////////////////////////////////////

	/**
	 * Builds a white spaces string of the specified length
	 *
	 * @param n the number of white spaces
	 * @return the white spaces string
	 */
	static private String whiteSpaces(int n)
	{
		if (n <= 0)
		{
			return "";
		}
		else
		{
			String space = " ";
			while (space.length() < n)
			{
				space += space;
			}
			return space.substring(0, n);
		}
	}

	
	static public String formatNumber(Object p_Number, int p_FractionDigit)
	{
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		
		DecimalFormat df = new DecimalFormat();
		df.setDecimalFormatSymbols(dfs);
		df.setMaximumFractionDigits(p_FractionDigit) ; 
		df.setMinimumFractionDigits(p_FractionDigit) ; 
		df.setGroupingUsed(false);
		
		return df.format(p_Number);
	}
	

	static public String formatOutput(Object p_Output, int p_length)
	{
		return "| " + p_Output + whiteSpaces(p_length - String.valueOf(p_Output).length() - 1);
	}


	static public String formatOutputCenter(Object p_Output, int p_length)
	{
		int halfLength = (p_length - String.valueOf(p_Output).length() - 1) / 2;
		
		return
			"| "
			+ whiteSpaces(halfLength)
			+ p_Output
			+ whiteSpaces(p_length - String.valueOf(p_Output).length() - 1 - halfLength);
	}	
}
