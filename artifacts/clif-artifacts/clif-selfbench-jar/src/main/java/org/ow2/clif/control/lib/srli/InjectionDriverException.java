/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib.srli;

/**
 * @author Colin PUY
 */
public class InjectionDriverException extends Exception
{
	static private final long serialVersionUID = 1L;

	static public enum Type
	{
		ILLEGAL_MATRIX_DIMENSION,
		ILLEGAL_GAUSS_PIVOT_INVERSION
	}

	public InjectionDriverException(Type type)
	{
		super(getText(type));
	}

	static private String getText(Type type)
	{
		switch (type)
		{
			case ILLEGAL_MATRIX_DIMENSION :
				return "Illegal matrix dimensions.";
			case ILLEGAL_GAUSS_PIVOT_INVERSION :
				return "Reverse GAUSS pivot is impossible, division by 0";
			default :
				return "Unknown cause";
		}
	}
}
