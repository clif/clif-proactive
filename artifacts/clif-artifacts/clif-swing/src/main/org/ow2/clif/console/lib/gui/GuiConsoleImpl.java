/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2005, 2012 France Telecom R&D
* Copyright (C) 2003 INRIA
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;
import org.ow2.clif.analyze.api.AnalyzerLink;
import org.ow2.clif.analyze.lib.AnalyzerRegistry;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.console.lib.TestPlanWriter;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.deploy.DeployObservation;
import org.ow2.clif.server.lib.ClifServerImpl;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.supervisor.lib.BladeObservation;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.ThrowableHelper;
import org.ow2.clif.util.gui.GuiAlert;

/**
 * Implementation of a graphic user interface for Clif
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class GuiConsoleImpl
	implements Observer, ActionListener
{
	/** Fractal ADL definition file of the CLIF application to deploy */
	static final String CLIF_APPLICATION = "org.ow2.clif.console.lib.gui.ClifApp";
	/** name of the CLIF Application component in the registry */
	static public final String CLIFAPP_NAME = "console";
	/* Size definitions for the application.
	 * Internal frames width is not specified because it is automatically resized according to
	 * the main window width.
	 */
	static public int clifWidth = 900;
	static public int clifHeight = 700;

	// GUI commands
	static private final String ABOUT_CMD = "about";
	static private final String DEPLOY_CMD = "deploy";
	static private final String INIT_CMD = "init";
	static private final String START_CMD = "start";
	static private final String STOP_CMD = "stop";
	static private final String COLLECT_CMD = "collect";
	static private final String SUSPEND_CMD = "suspend";
	static private final String RESUME_CMD = "resume";
	static private final String UPDATE_CMD = "update";
	static private final String EXIT_CMD = "exit";
	static private final String SAVE_CMD = "save";
	static private final String OPEN_CMD = "open";
	static private final String EDIT_CMD = "edit";
	static private final String ANALYZER_CMD = "analyzer";
	static private CtpFilter ctpFilter;
    private AnalyzerLink[] analyzerLink;
    private ClifRegistry clifReg;
	private ClifAppFacade clifApp;
	private String currentDirectory = ".";
	private BladeState globalState = BladeState.UNDEPLOYED;
	private Map<String,ClifDeployDefinition> testPlan;
	private JFrame frame;
	private JMenuBar menuBar;
	private TestPlanWindow testPlanFrame;
	private JDesktopPane desktop;
	private JInternalFrame[] analyzerFrame;
	private JCheckBoxMenuItem[] boxAnalysisTool;
	private JMenuItem itemDeploy;
	private JMenuItem itemEdit;
	private JMenuItem itemInit;
	private JMenuItem itemStart;
	private JMenuItem itemStop;
	private JMenuItem itemSuspend;
	private JMenuItem itemResume;
	private JMenuItem itemCollect;
	private JMenuItem itemRebind;
	private JMenuItem itemAbout;
	private long ellapsedTime;
	private long lastStartTime;
	private Timer ellapsedTimeTimer;

	static public void main(String[] args)
	{
        GuiConsoleImpl guiConsole = new GuiConsoleImpl();
        guiConsole.configureAndGetRegistry();
        guiConsole.initGui();
        guiConsole.frame.setVisible(true);
    }

    public void initGui() {
        clifApp = new ClifAppFacade("CLIF test", CLIF_APPLICATION);
		clifReg.bindClifApp(CLIFAPP_NAME, clifApp.getClifApp());
        try
        {
            ClifServerImpl.create(ExecutionContext.DEFAULT_SERVER, clifReg, null);
        }
        catch (Exception ex)
        {
            throw new Error(
                "fatal, abnormal situation: could not register local host CLIF server",
                ex);
        }
        clifApp.addObserver(this);
        ctpFilter = new CtpFilter();
        testPlan = null;
        globalState = BladeState.UNDEPLOYED;
        ImageIcon clifIcon = new ImageIcon(GuiConsoleImpl.class.getClassLoader().getResource("icons/clificon.png"));

        frame = new JFrame("CLIF is a Load Injection Framework");
        frame.setIconImage(clifIcon.getImage());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        menuBar = new JMenuBar();
        JMenu menu;
        JMenuItem item;

        // File menu
        menu = new JMenu("File");

        item = new JMenuItem("Save this test plan");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        item.setActionCommand(SAVE_CMD);
        menu.add(item).addActionListener(this);

        item = new JMenuItem("Open a test plan");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        item.setActionCommand(OPEN_CMD);
        menu.add(item).addActionListener(this);

        menu.addSeparator();
        item = new JMenuItem("Quit");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        item.setActionCommand(EXIT_CMD);
        menu.add(item).addActionListener(this);
        menuBar.add(menu);

        /*
* test plan Menu
*/
        menu = new JMenu("Test plan");

        itemRebind = new JMenuItem("Refresh server list");
        itemRebind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        itemRebind.setActionCommand(UPDATE_CMD);
        menu.add(itemRebind).addActionListener(this);

        itemEdit = menu.add("Edit");
        itemEdit.setActionCommand(EDIT_CMD);
        itemEdit.addActionListener(this);
        itemEdit.setEnabled(false);

        itemDeploy = menu.add("Deploy");
        itemDeploy.setActionCommand(DEPLOY_CMD);
        itemDeploy.addActionListener(this);
        itemDeploy.setEnabled(false);

        menu.addSeparator();

        itemInit = menu.add("Initialize");
        itemInit.setActionCommand(INIT_CMD);
        itemInit.addActionListener(this);

        itemStart = menu.add("Start");
        itemStart.setActionCommand(START_CMD);
        itemStart.addActionListener(this);

        itemStop = menu.add("Stop");
        itemStop.setActionCommand(STOP_CMD);
        itemStop.addActionListener(this);

        itemSuspend = menu.add("Suspend");
        itemSuspend.setActionCommand(SUSPEND_CMD);
        itemSuspend.addActionListener(this);

        itemResume = menu.add("Resume");
        itemResume.setActionCommand(RESUME_CMD);
        itemResume.addActionListener(this);

        itemCollect = menu.add("Collect");
        itemCollect.setActionCommand(COLLECT_CMD);
        itemCollect.addActionListener(this);

        menuBar.add(menu);

        frame.setJMenuBar(menuBar);

        desktop = new JDesktopPane();
        testPlanFrame = new TestPlanWindow(frame);
        testPlanFrame.setFrameIcon(clifIcon);
        testPlanFrame.setVisible(true);
        testPlanFrame.setSize((clifWidth * 8) / 10, (clifHeight * 8) / 10);
        testPlanFrame.setAvailableServers(clifReg.getServers());
        desktop.add(testPlanFrame);

        // integrates registered analyzers
        analyzerLink = AnalyzerRegistry.getAnalyzers();
        analyzerFrame = new JInternalFrame[analyzerLink.length];
        boxAnalysisTool = new JCheckBoxMenuItem[analyzerLink.length];
        menu = new JMenu("Tools");
        for (int i=0 ; i<analyzerLink.length ; ++i)
        {
            analyzerFrame[i] = new JInternalFrame(analyzerLink[i].getLabel(), true, false, true, false);
            analyzerLink[i].setUIContext(analyzerFrame[i]);
            analyzerFrame[i].setVisible(false);
            analyzerFrame[i].setSize((clifWidth * 8) / 10, (clifHeight * 8) / 10);
            desktop.add(analyzerFrame[i]);
            boxAnalysisTool[i] = new JCheckBoxMenuItem(analyzerLink[i].getLabel(), false);
            boxAnalysisTool[i].setActionCommand(ANALYZER_CMD);
            menu.add(boxAnalysisTool[i]).addActionListener(this);
        }
        menuBar.add(menu);

        // About menu
        menu = new JMenu("?");
        itemAbout = new JMenuItem("About...");
        itemAbout.setActionCommand(ABOUT_CMD);
        itemAbout.addActionListener(this);
        menu.add(itemAbout);
        menuBar.add(menu);

        frame.add(desktop);
        frame.setSize(clifWidth, clifHeight);
        updateMenus();
        try
        {
			testPlanFrame.setMaximum(true);
		}
        catch (PropertyVetoException e)
        {
			e.printStackTrace(System.err);
		}
        ellapsedTimeTimer = new Timer(1000, this);
    }

    /**
     * Configure context and set the registry
     */
    public void configureAndGetRegistry() {
        ExecutionContext.init("./");
        try
        {
            System.out.println("Trying to connect to an existing CLIF Registry...");
            clifReg = ClifRegistry.getInstance(false);
            System.out.println("Connected to " + clifReg);
        }
        catch (Exception ex)
        {
            try
            {
                System.out.println("Failed.\nCreating a CLIF Registry...");
                clifReg = ClifRegistry.getInstance(true);
            }
            catch (Throwable th)
            {
                System.out.println("Failed.");
                System.err.println("Please check your configuration with regard to registry host and port number.");
                System.exit(-1);
            }
        }
    }

	/**
	 * Enables or disables menu items depending on the current state
	 */
	public void updateMenus()
	{
		itemDeploy.setEnabled(testPlan == null);
		itemRebind.setEnabled(testPlan == null);
		itemEdit.setEnabled(testPlan != null);
		itemInit.setEnabled(false);
		itemStart.setEnabled(false);
		itemStop.setEnabled(false);
		itemSuspend.setEnabled(false);
		itemResume.setEnabled(false);
		itemCollect.setEnabled(false);

		if (globalState.equals(BladeState.DEPLOYED))
		{
			itemInit.setEnabled(testPlan != null);
			if (testPlan == null)
			{
				testPlanFrame.setEditable(true);
			}
			else
			{
				testPlanFrame.setStatusLine(globalState, 0);
			}
		}
		else if (globalState.equals(BladeState.INITIALIZED))
		{
			itemStart.setEnabled(true);
			itemStop.setEnabled(true);
			testPlanFrame.initTest(clifApp);
			testPlanFrame.setStatusLine(globalState, 0);
		}
		else if (globalState.equals(BladeState.RUNNING))
		{
			itemStop.setEnabled(true);
			itemSuspend.setEnabled(true);
		}
		else if (globalState.equals(BladeState.SUSPENDED))
		{
			itemResume.setEnabled(true);
			itemStop.setEnabled(true);
		}
		else if (globalState.equals(BladeState.STOPPED) || globalState.equals(BladeState.COMPLETED))
		{
			itemInit.setEnabled(testPlan != null);
			itemCollect.setEnabled(testPlan != null);
			if (testPlan == null)
			{
				testPlanFrame.setEditable(true);
			}
		}
		else
		{
			testPlanFrame.setStatusLine(globalState, 0);
		}
	}


	//////////////////////////////
	// interface ActionListener //
	//////////////////////////////


	@Override
	public void actionPerformed(ActionEvent e)
	{
		String command = e.getActionCommand();

		if (e.getSource() == ellapsedTimeTimer)
		{
			testPlanFrame.setStatusLine(
				globalState,
				globalState.equals(BladeState.RUNNING)
					? ellapsedTime + System.currentTimeMillis() - lastStartTime
					: ellapsedTime);
		}
		else if (command.equals(SAVE_CMD))
		{
			JFileChooser filechooser = new JFileChooser(currentDirectory);
			filechooser.addChoosableFileFilter(ctpFilter);
			try
			{
				if (filechooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION)
				{
					currentDirectory = filechooser.getSelectedFile().getParent();
					Map<String,ClifDeployDefinition> testPlan = testPlanFrame.getTestPlan();
					if (testPlan != null)
					{
						TestPlanWriter.write2prop(
							new FileOutputStream(filechooser.getSelectedFile()),
							testPlan);
						testPlanFrame.setTitle(filechooser.getSelectedFile().getName());
					}
				}
			}
			catch (Exception ex)
			{
				new GuiAlert(desktop, "Can't save test plan " + filechooser.getSelectedFile(), ex.toString()).alert();
			}
		}
		else if (command.equals(OPEN_CMD))
		{
			JFileChooser filechooser = new JFileChooser(currentDirectory);
			filechooser.addChoosableFileFilter(ctpFilter);
			try
			{
				if ((testPlanFrame.isEmpty() || new GuiClearConfirm(frame).ask())
					&& filechooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
				{
					currentDirectory = filechooser.getSelectedFile().getParent();
					testPlan = null;
					if (globalState.equals(BladeState.RUNNING)
						|| globalState.equals(BladeState.SUSPENDED)
						|| globalState.equals(BladeState.INITIALIZED))
					{
						clifApp.stop(null);
					}
					testPlanFrame.setTestPlan(
						TestPlanReader.readFromProp(
							new FileInputStream(filechooser.getSelectedFile())));
					testPlanFrame.setTitle(filechooser.getSelectedFile().getName());
				}
			}
			catch (Exception ex)
			{
				String message = "Can't open test plan " + filechooser.getSelectedFile();
				new GuiAlert(desktop, message, ex.toString()).alert();
			}
		}
		else if (command.equals(EDIT_CMD) && new GuiClearConfirm(frame).ask())
		{
			testPlan = null;
			if (globalState.equals(BladeState.RUNNING)
				|| globalState.equals(BladeState.SUSPENDED)
				|| globalState.equals(BladeState.INITIALIZED))
			{
				clifApp.stop(null);
			}
		}
		else if (command.equals(UPDATE_CMD))
		{
			testPlanFrame.setAvailableServers(clifReg.getServers());
		}
		else if (command.equals(INIT_CMD))
		{
			String testId = new GUIInitDialog(frame).ask();
			if (testId != null)
			{
				try
				{
					clifApp.init(testId);
				}
				catch (Exception ex)
				{
					ex.printStackTrace(System.err);
					new GuiAlert(
						desktop,
						"Can't initialize test plan",
						ThrowableHelper.getMessages(ex)).alert();
				}
			}
		}
		else if (command.equals(START_CMD))
		{
			clifApp.start(null);
			ellapsedTime = 0;
			lastStartTime = System.currentTimeMillis();
			ellapsedTimeTimer.start();
		}
		else if (command.equals(STOP_CMD))
		{
			clifApp.stop(null);
		}
		else if (command.equals(SUSPEND_CMD))
		{
			clifApp.suspend(null);
		}
		else if (command.equals(RESUME_CMD))
		{
			clifApp.resume(null);
			lastStartTime = System.currentTimeMillis();
			ellapsedTimeTimer.start();
		}
		else if (command.equals(COLLECT_CMD))
		{
			new GuiCollectDialog(frame, clifApp).go();
		}
		else if (command.equals(EXIT_CMD)
			&& (testPlanFrame.isEmpty() || new GuiClearConfirm(frame).ask()))
		{
			if (globalState.equals(BladeState.RUNNING)
				|| globalState.equals(BladeState.SUSPENDED)
				|| globalState.equals(BladeState.INITIALIZED))
			{
				clifApp.stop(null);
			}
/* deploy(null,...) not supported anymore (and useless)
			try
			{
				clifApp.deploy(null, clifReg, null);
			}
			catch (ClifException ex)
			{
				ex.printStackTrace(System.err);
			}
*/			System.exit(0);
		}
		else if (command.equals(DEPLOY_CMD))
		{
			Map<String,ClifDeployDefinition> newTestPlan = testPlanFrame.getTestPlan();
			if (newTestPlan != null)
			{
				testPlanFrame.setEditable(false);
				testPlan = newTestPlan;
				testPlanFrame.setStatusLine(BladeState.DEPLOYING, 0);
				try
				{
					clifApp.deploy(testPlan, clifReg, null);
				}
				catch (ClifException ex)
				{
					new GuiAlert(desktop, "Deployment error", ex.toString()).alert();
				}
			}
		}
		else if (command.equals(ABOUT_CMD))
		{
			new GuiAboutDialog(this.frame).setVisible(true);
		}
		else if (command.equals(ANALYZER_CMD))
		{
			for (int i=0 ; i<boxAnalysisTool.length ; ++i)
			{
				toggleAnalysisTool(i,boxAnalysisTool[i].getState());
			}
		}
		updateMenus();
	}

	public void toggleAnalysisTool(int i, boolean state)
	{
		if (state)
		{
			try
			{
				analyzerFrame[i].setMaximum(true);
			}
			catch (PropertyVetoException e)
			{
				e.printStackTrace(System.err);
			}
		}
		analyzerFrame[i].setVisible(state);
	}


	////////////////////////
	// interface Observer //
	////////////////////////


	/**
	 * Receive alarms and blade state changes from the supervisor
	 */
	@Override
	public void update(Observable supervisor, Object observation)
	{
		if (observation instanceof BladeObservation)
		{
			String id = ((BladeObservation)observation).getBladeId();
			BladeState state = ((BladeObservation)observation).getState();
			if (! state.equals(BladeState.DEPLOYED))
			{
				globalState = clifApp.getGlobalState(null);
			}
			if (globalState.equals(BladeState.STOPPED)
				|| globalState.equals(BladeState.SUSPENDED))
			{
				if (lastStartTime != 0)
				{
					ellapsedTime += System.currentTimeMillis() - lastStartTime;
					lastStartTime = 0;
					ellapsedTimeTimer.stop();
				}
				testPlanFrame.setStatusLine(globalState, ellapsedTime);
			}
			testPlanFrame.setBladeState(id, state);
		}
		else if (observation instanceof DeployObservation)
		{
			DeployObservation obs = (DeployObservation)observation;
			if (obs.isSuccessful())
			{
				//clifApp = obs.getTestControl();
				clifApp.addObserver(this);
				globalState = BladeState.DEPLOYED;
			}
			else
			{
				obs.getException().printStackTrace(System.err);
				String message = "Deployment failure";
				new GuiAlert(
					desktop,
					message,
					ThrowableHelper.getMessages(obs.getException())).alert();
				testPlan = null;
			}
		}
		else if (observation instanceof AlarmEvent)
		{
			AlarmEvent alarm = (AlarmEvent)observation;
			String argument = null;
			if (alarm.argument != null)
			{
				if (alarm.argument instanceof Throwable)
				{
					((Throwable)alarm.argument).printStackTrace(System.err);
					argument = ThrowableHelper.getMessages((Throwable)alarm.argument);
				}
				else
				{
					argument = alarm.argument.toString();
				}
			}
			new GuiAlert(
				desktop,
				"Alarm",
				argument).alert();
		}
		updateMenus();
	}


    ///////////////////////////////////////////////
	// inner class for filtering test plan files //
	///////////////////////////////////////////////


	private class CtpFilter extends FileFilter
	{
		@Override
		public boolean accept(File file)
		{
			return file.isDirectory() || file.getName().endsWith(TestPlanReader.FILE_EXT);
		}

		@Override
		public String getDescription()
		{
			return "CLIF testplan files (*" + TestPlanReader.FILE_EXT + ")";
		}
	}

	/**
	 * Toggle the checkbox with to the given state
	 * @param box
	 */
	public void toggleBoxAnalysisTool(int box){
		boxAnalysisTool[box].doClick();
	}

    public JFrame getFrame() {
        return frame;
    }
}
