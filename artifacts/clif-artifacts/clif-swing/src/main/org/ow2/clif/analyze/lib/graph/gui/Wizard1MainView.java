/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;
import org.ow2.clif.analyze.lib.graph.gui.ReportManager.Phases;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.Dataset.DatasetType;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.LifeCycleEvent;
import org.ow2.clif.storage.lib.util.NameBladeFilter;
import org.ow2.clif.supervisor.api.ClifException;


@SuppressWarnings("serial")
public class Wizard1MainView extends JPanel {

	JFrame frame;
	ReportManager control;
	String name = "Selection Wizard";
	JPanel globalPanel = new JPanel();
	Phases next;
	DataAccess data;

	public Wizard1MainView(ReportManager _control, JFrame _frame) {
		frame = _frame;
		control = _control;
		data = control.getDataAccess();
		frame.setTitle("Step 1/2 - Data source(s) selection");
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE); // EXIT_ON_CLOSE
		init();
		frame.pack();
	}

	private void init() {

		/////////////////// GUI objects
		// jpMain_
		// --- jspFrame
		// ------- jpMainLeft
		//	---------- jpDatasetTypeButtons
		// --------------- jlDatasetType
		//	-------------- jrbSimpleDatasetButton
		//	-------------- jrbMultipleDatasetButton
		//	-------------- jrbAggregateDatasetButton
		//	---------- jpAvailableDatasets
		// ----------- jScrollPaneTree
		// ------- jpMainRight
		// ------- jScrollTable
		// ----------- jpEventInside
		// --------------- jpEvent
		// ------------------- jlSelectEvent
		// ------------------- jcomboChooseEvent
		// ----------- jpButtons
		// --------------- jbCancel
		// --------------- jbNext
		// --- jpStatus
		// ------- jlStatus
		
		
		/////////////////// GUI objects creation
		//
		jpMain_ = new JPanel(); 						//	jpMain_
		jspFrame = new JSplitPane(); 					//	--- jspFrame
		jpMainLeft = new JPanel();						//	------- jpMainLeft
		jpDatasetTypeButtons = new TitledPanel("Dataset Type");			//	----------- jpDatasetTypeButtons
		jrbSimpleDatasetButton = new JRadioButton();	//	--------------- jrbSimpleDatasetButton
		jrbMultipleDatasetButton = new JRadioButton();	//	--------------- jrbMultipleDatasetButton
		jrbAggregateDatasetButton = new JRadioButton();	//	--------------- jrbAggregateDatasetButton
		jpAvailableDatasets = new TitledPanel("Available Test Runs");				//	---------- jpAvailableDatasets
		jScrollPaneTree = new Wizard1TreePanel(this);	//	----------- jScrollPaneTree (tree at left side)
		jpMainRight = new JPanel();						//	------- jpMainRight
		jScrollTable = new Wizard1TablePanel(this);		//	----------- jScrollTable
		jpEventInside = new JPanel();					//	----------- jpEventInside
		jpEvent = new JPanel();							//	--------------- jpEvent
		jlSelectEvent = new JLabel("Event Type");	//	------------------- jlSelectEvent
		jcomboChooseEvent = new JComboBox<String>();				//	------------------- jcomboChooseEvent
		jpFlowButtons = new JPanel();					//	----------- jpFlowButtons
		jbCancel = new JButton();						//	--------------- jbCancel
		jbNext = new JButton();							//	--------------- jbNext
		jpStatus = new JPanel();						//	--- jpStatus
		jlStatus = new JLabel();						//	------- jlStatus

		
		/////////////////// GUI objects assembly
		frame.add(jpMain_);
		// // jpMain_
		jpMain_.setLayout(new BorderLayout());
		jpMain_.add(jspFrame, BorderLayout.CENTER);
		jpMain_.add(jpStatus, BorderLayout.SOUTH);
		// // jspFrame
		jspFrame.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
		jspFrame.setLeftComponent(jpMainLeft);
		jspFrame.setRightComponent(jpMainRight);
		// // jpMainLeft
		jpMainLeft.setLayout(new BorderLayout());
		jpMainLeft.add(jpDatasetTypeButtons, BorderLayout.NORTH);
		jpMainLeft.add(jpAvailableDatasets, BorderLayout.WEST);
		// // jpDatasetTypeButtons
		jpDatasetTypeButtons.setLayout(new GridLayout(1, 3));
		jpDatasetTypeButtons.add(jrbSimpleDatasetButton);
		jpDatasetTypeButtons.add(jrbMultipleDatasetButton);
		jpDatasetTypeButtons.add(jrbAggregateDatasetButton);
		// --------------- jlDatasetType
		// // jrbSimpleDatasetButton
		// // jrbMultipleDatasetButton
		// // jrbAggregateDatasetButton
		// // jpAvailableDatasets
		jpAvailableDatasets.add(jScrollPaneTree, BorderLayout.WEST);
		// // jScrollPaneTree
		// // jpMainRight
		jpMainRight.setLayout(new BorderLayout());
		jpMainRight.add(jScrollTable, BorderLayout.NORTH);
		jpMainRight.add(jpEventInside, BorderLayout.CENTER);
		jpMainRight.add(jpFlowButtons, BorderLayout.SOUTH);
		// // jScrollTable
		// // jpEventInside
		jpEventInside.add(jpEvent);
		// // jpEvent
		// // jlSelectEvent
		jpEvent.add(jlSelectEvent);
		jpEvent.add(jcomboChooseEvent);
		// // jpButtons
		jpFlowButtons.add(jbCancel);
		jpFlowButtons.add(jbNext);
		// // jbCancel
		// // jbNext
		// // jpStatus
		jpStatus.add(jlStatus);
		// // jlStatus

		
		/////////////////// GUI objects initialization
		// jpMain_
		// --- jspFrame
		// ------- jpMainLeft
		//	---------- jpDatasetTypeButtons
		// --------------- jlDatasetType
		//	-------------- jrbSimpleDatasetButton
		jrbSimpleDatasetButton.setFont(new java.awt.Font("Dialog", 0, 12));
		jrbSimpleDatasetButton.setText("Simple");
		jrbSimpleDatasetButton.setSize(73,26);
		//  --------------- jrbMultipleDatasetButton
		jrbMultipleDatasetButton.setFont(new java.awt.Font("Dialog", 0, 12));
		jrbMultipleDatasetButton.setText("Multiple");
		jrbMultipleDatasetButton.setSize(89,26);
		//  --------------- jrbAggregateDatasetButton
		jrbAggregateDatasetButton.setFont(new java.awt.Font("Dialog", 0, 12));
		jrbAggregateDatasetButton.setText("Aggregate");
		jrbAggregateDatasetButton.setSize(90,26);
		// ---------- jpAvailableDatasets
		// ----------- jScrollPaneTree
		// ------- jpMainRight
		// ------- jScrollTable
		// ----------- jpEventInside
		jpEventInside.setPreferredSize(new Dimension(70, 70));
		jpEventInside.setMinimumSize(new Dimension(70, 70));
		jpEventInside.setMaximumSize(new Dimension(70, 70));
		// --------------- jpEvent
		// ------------------- jlSelectEvent
		// ------------------- jcomboChooseEvent
		// ----------- jpButtons
		// --------------- jbCancel
		jbCancel.setFont(new java.awt.Font("Dialog", 0, 12));
		jbCancel.setText("Cancel");
		jbCancel.setSize(70,26);
		// --------------- jbNext
		jbNext.setFont(new java.awt.Font("Dialog", 0, 12));
		jbNext.setText("Next");
		jbNext.setSize(70,26);
		jbNext.setEnabled(false);
		// --- jpStatus
		FlowLayout statusLayout = (FlowLayout) jpStatus.getLayout();
		statusLayout.setAlignment((int) LEFT_ALIGNMENT);

		////// Listeners
		
		jbCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				data.wizardDataset.clearDatasources();
				data.wizardDataset.setDatasetType(DatasetType.SIMPLE_DATASET);
				control.nextState(Phases.REPORT_MANAGER);
			}
		});

		jbNext.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				if (checkSameBladeType(data.wizardDataset)) {
					control.nextState(Phases.WIZARD_2_FILTERING);
				} else {
					JOptionPane.showMessageDialog(jbNext, "You must select same class blades");
				}

			}
		});

		// // jrbSimpleDatasetButton
		jrbSimpleDatasetButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateGuiFromDatasetType(DatasetType.SIMPLE_DATASET);
			}
		});

		// // jrbMultipleDatasetButton
		jrbMultipleDatasetButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateGuiFromDatasetType(DatasetType.MULTIPLE_DATASET);
			}
		});

		// // jrbAggregateDatasetButton
		jrbAggregateDatasetButton.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateGuiFromDatasetType(DatasetType.AGGREGATE_DATASET);
			}
		});
		
		
		// attributes initialization
//		updateDatasetType(DatasetType.SIMPLE_DATASET);
		jpEvent.removeAll();
		jbNext.setEnabled(false);
		
	}		//	init()

	


	
	protected boolean checkSameBladeType(Dataset dataset) {
		Set<String> oneNameBladeFilter = new HashSet<String>();
		if (0 == dataset.getDatasources().size()){
			System.err.println("[Wizard1MainView]checkSameBladeType(<dataset>): dataset has no datasources.");
			return true;
		}
		for (Datasource datasource : dataset.getDatasources()){
			String testName = datasource.getTestName();
			String blade = datasource.getBladeId();
			NameBladeFilter filter = new NameBladeFilter(blade);
			BladeDescriptor[] bd = null;
			try {
				bd = data.getBladeDescriptor(testName, filter);
			} catch (ClifException e) {
				e.printStackTrace();
			}
			if (bd.length > 0){
				oneNameBladeFilter.add(bd[0].getClassname());
			}
		}

		if (oneNameBladeFilter.size() == 1) {
			return true;
		} else {
			return false;
		}
	}


	// GUI objects declaration
	JPanel jpMain_; // jpMain_
	JSplitPane jspFrame; 					// --- jspFrame
	JPanel jpMainLeft; 						// ------- jpMainLeft
	TitledPanel jpDatasetTypeButtons; 			// ----------- jpDatasetTypeButtons
//	JLabel jlDatasetType;               	// --------------- jlDatasetType
	JRadioButton jrbSimpleDatasetButton; 	// --------------- jrbSimpleDatasetButton
	JRadioButton jrbMultipleDatasetButton; 	// --------------- jrbMultipleDatasetButton
	JRadioButton jrbAggregateDatasetButton; // --------------- jrbAggregateDatasetButton
	TitledPanel jpAvailableDatasets;				//	---------- jpAvailableDatasets
	Wizard1TreePanel jScrollPaneTree; 		// ----------- jScrollPaneTree
	JPanel jpMainRight; 					// ------- jpMainRight
	JScrollPane jScrollTable; 				// ----------- jScrollTable
	JPanel jpEventInside; 					// ----------- jpEventInside
	JPanel jpEvent; 						// --------------- jpEvent
	JLabel jlSelectEvent; 					// ------------------- jlSelectEvent
	JComboBox<String> jcomboChooseEvent; 				// ------------------- jcomboChooseEvent
	JPanel jpFlowButtons; 					// ----------- jpFlowButtons
	JButton jbCancel; 						// --------------- jbCancel
	JButton jbNext; 						// --------------- jbNext
	JPanel jpStatus; 						// --- jpStatus
	JLabel jlStatus; 						// ------- jlStatus

	// ///////////////// Other GUI objects declaration
	
	
	// update methods


	void updateStatus() {
		String str = "[" + data.wizardDataset.getDatasetType().toString() + "]";
		String pre = " ";
		for(Datasource dd: data.wizardDataset.getDatasources()){
			str += pre + dd.getTestName() + "/" + dd.getBladeId() + "/" + dd.getEventType();
			pre = ", \n";
		}
		setStatus(str);
	}

	public void setStatus(String string) {
		jlStatus.setText(string);
		frame.repaint();
	}
	
	public void updateEvGroupCB(ArrayList<SortedSet<String>> eventTypesLists) {
		SortedSet<String> commonEventTypes = findCommonEventTypes(eventTypesLists);

		jcomboChooseEvent = new JComboBox<String>();
		for (String label : commonEventTypes)
		{
			jcomboChooseEvent.addItem(label);
			// select this event type if it is not lifecycle nor alarm 
			if (! label.equalsIgnoreCase(LifeCycleEvent.EVENT_TYPE_LABEL)
				&& ! label.equalsIgnoreCase(AlarmEvent.EVENT_TYPE_LABEL))
			{
				jcomboChooseEvent.setSelectedItem(label);
			}
		}
		String eventType = (String) jcomboChooseEvent.getSelectedItem();
		setEventTypeToCurrentDs(eventType);

		jcomboChooseEvent.setFont(new java.awt.Font("Dialog", 0, 12));
		jcomboChooseEvent.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				String eventType = jcomboChooseEvent.getSelectedItem().toString();
				setEventTypeToCurrentDs(eventType);
			}
		});
		// update the ComboBox jcomboChooseEvent
		jpEvent.removeAll();
		jpEvent.add(jlSelectEvent);
		jpEvent.add(jcomboChooseEvent);
		jpEvent.updateUI();
//		updateStatus();
		jbNext.setEnabled(jcomboChooseEvent.getItemCount() > 0);
//		data.tracem("()");
	}


	/**
	 * @param eventType
	 *	 assign eventType to all datasets in currentDs
	 */
	void setEventTypeToCurrentDs(String eventType){
		for (Datasource dd : data.wizardDataset.getDatasources()){
			dd.setEventType(eventType);
		}
		updateStatus();
		jbNext.setEnabled(true);
	}

	
	/**
	 * find the eventTypes present in every list
	 * @param eventTypesLists list of String arrays containing event types
	 * @return an array of String objects giving the event types
	 * common to all String arrays in the provided list
	 */
	private SortedSet<String> findCommonEventTypes(ArrayList<SortedSet<String>> eventTypesList)
	{
		SortedSet<String> result = new TreeSet<String>();
		// step 1: merge all event types
		for (SortedSet<String> eventTypes : eventTypesList)
		{
			result.addAll(eventTypes);
		}
		// step 2: discard event types that are not present in some event types set
		ArrayList<String> discard = new ArrayList<String>(result.size()); 
		for (String commonEventType : result)
		{
			for (SortedSet<String> eventTypes : eventTypesList)
			{
				if (! eventTypes.contains(commonEventType))
				{
					discard.add(commonEventType);
				}
			}
		}
		result.removeAll(discard);
		return result;
	}


	protected void updateGuiFromDatasetType(DatasetType _dsType) {
//		data.tracep("()");
		updateCurrentDsType(_dsType);
		// select/unselect buttons
		jrbSimpleDatasetButton.setSelected(DatasetType.SIMPLE_DATASET == _dsType);
		jrbMultipleDatasetButton.setSelected(DatasetType.MULTIPLE_DATASET == _dsType);
		jrbAggregateDatasetButton.setSelected(DatasetType.AGGREGATE_DATASET == _dsType);
		
		jrbSimpleDatasetButton.setFont(new java.awt.Font("Dialog", DatasetType.SIMPLE_DATASET == _dsType?Font.BOLD:Font.PLAIN, 12));
		jrbMultipleDatasetButton.setFont(new java.awt.Font("Dialog", DatasetType.MULTIPLE_DATASET == _dsType?Font.BOLD:Font.PLAIN, 12));
		jrbAggregateDatasetButton.setFont(new java.awt.Font("Dialog", DatasetType.AGGREGATE_DATASET == _dsType?Font.BOLD:Font.PLAIN, 12));

		unselectTree();
		jScrollPaneTree.updateTreeSelectionMode(data.wizardDataset.getDatasetType());
		updateStatus();
//		data.tracem("()");
	}


	private void updateCurrentDsType(DatasetType dsType) {
		if (null == control.w1View){
			return;				// while Wizard1 Constructor is being called
		}
		data.wizardDataset.setDatasetType(dsType);
		updateStatus();
	}


	void unselectTree() {
//		data.tracep("()");
		jScrollPaneTree.initTree();
//		data.tracem("()");
	}

	// methods

	public void clearCurrentDs() {
		if (null == control.w1View){
			return;				// while Wizard1 Constructor is being called
		}
		if (null != data.wizardDataset){
			data.wizardDataset.clearDatasources();		// clean the current dataset	// w1MainView.tableClean()
			LogAndDebug.trace(" removing datasources from wizardDataset.");
			tableUpdate();
			updateStatus();
		}
	}


	//  table methods
	public void tableClean() {
		((Wizard1TablePanel) jScrollTable).tableClean();
	}


	public void tableUpdate() {
		((Wizard1TablePanel) jScrollTable).tableUpdate();
	}
	
	
	public void tableUpdate(String testName, String bladeName) {
		((Wizard1TablePanel) jScrollTable).tableAdd(testName, bladeName);
	}
	
	
	public DataAccess getDataAccess() {
		return data;
	}

	public void initializeFrom(Phases previousState) {
		switch(previousState){
		case INIT:
		case REPORT_MANAGER:
			jScrollPaneTree.refresh();
			tableClean();
			updateGuiFromDatasetType(data.wizardDataset.getDatasetType());
			unselectTree();
			jbNext.setEnabled(false);
			break;
		case WIZARD_1_SELECTION:
			break;
		case WIZARD_2_FILTERING:
			jbNext.setEnabled(true);
			break;
		case EXIT:
		default:
			;
		}
	}

	public String findTextName(int testNameIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	public String findBlade(String testName, int bladeIndex) {
		// TODO Auto-generated method stub
		return null;
	}
}