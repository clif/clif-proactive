/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import java.awt.BorderLayout;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JSplitPane;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;


/**
 *
 * @author Bruno Dillenseger
 */
public class TestPlanWindow extends JInternalFrame
{
	private static final long serialVersionUID = 1097995348222699090L;
	GuiPanelBladeState guiPanelBladeState;
	JSplitPane splitPane = null;
	GuiMonitorPanel monitorPanel;
	JLabel statusLine = new JLabel(" ");


	public TestPlanWindow(JFrame frame)
	{
		super("Test plan definition", true, false, true, true);
		guiPanelBladeState = new GuiPanelBladeState(frame);
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, guiPanelBladeState);
		add(BorderLayout.SOUTH, statusLine);
	}


	public void setAvailableServers(String[] servers)
	{
		guiPanelBladeState.setAvailableServers(servers);
	}


	public Map<String,ClifDeployDefinition> getTestPlan()
	{
		return guiPanelBladeState.getTestPlan();
	}


	public void setTestPlan(Map<String,ClifDeployDefinition> testPlan)
	{
		guiPanelBladeState.setTestPlan(testPlan);
	}


	public void setBladeState(String bladeId, BladeState state)
	{
		guiPanelBladeState.setBladeState(bladeId, state);
	}


	public synchronized void initTest(ClifAppFacade clifApp)
	{
		setEditable(false);
		if (splitPane == null)
		{
			monitorPanel = new GuiMonitorPanel(getTestPlan(), clifApp);
			remove(guiPanelBladeState);
			addContainerListener(monitorPanel);
			splitPane =	new JSplitPane(JSplitPane.VERTICAL_SPLIT, guiPanelBladeState, monitorPanel);
			splitPane.setDividerLocation(getHeight()/2);
			splitPane.setOneTouchExpandable(true);
			add(BorderLayout.CENTER, splitPane);
		}
	}


	public synchronized void setEditable(boolean editable)
	{
		guiPanelBladeState.setEditable(editable);
		if (editable && splitPane != null)
		{
			remove(splitPane);
			add(BorderLayout.CENTER, guiPanelBladeState);
			guiPanelBladeState.setEditable(true);
			removeContainerListener(monitorPanel);
			monitorPanel = null;
			splitPane = null;
		}
	}


	public boolean isDeployable()
	{
		return guiPanelBladeState.isDeployable();
	}


	public boolean isEmpty()
	{
		return guiPanelBladeState.isEmpty();
	}


	public void setStatusLine(BladeState status, long ellapsedTime)
	{
		String line = status.toString();
		if (status.equals(BladeState.RUNNING)
			|| status.equals(BladeState.SUSPENDED)
			|| status.equals(BladeState.STOPPED))
		{
			ellapsedTime /= 1000;
			int sec = (int)ellapsedTime % 60;
			ellapsedTime /= 60;
			int min = (int)ellapsedTime % 60;
			ellapsedTime /= 60;
			int hour = (int)ellapsedTime % 60;
			line += " - ellapsed time " + hour + ":" + min + ":" + sec;
		}
		statusLine.setText(line);
	}
}
