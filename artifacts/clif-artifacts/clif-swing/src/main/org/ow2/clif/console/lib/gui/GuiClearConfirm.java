/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.gui;

import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.BorderLayout;


/**
 *
 * @author Bruno Dillenseger
 */
public class GuiClearConfirm extends JDialog implements ActionListener
{
	private static final long serialVersionUID = -7568024965290491180L;
	protected JButton okBtn, cancelBtn;
 	protected boolean value = false;


	GuiClearConfirm(JFrame frame)
	{
		super(frame, "Test plan reset", true);
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, new JLabel("Current test plan will be discarded"));
		JPanel buttonPnl = new JPanel();
		buttonPnl.add(okBtn = new JButton("OK"));
		okBtn.addActionListener(this);
		buttonPnl.add(cancelBtn = new JButton("cancel"));
		cancelBtn.addActionListener(this);
		add(BorderLayout.SOUTH, buttonPnl);
		addWindowListener(new WindowCloser());
	}


	public boolean ask()
	{
		pack();
		setVisible(true);
		return value;
	}


	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == okBtn)
		{
			value = true;
		}
		this.dispose();
	}


	class WindowCloser extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent e)
		{
			GuiClearConfirm.this.dispose();
		}
	}
}
