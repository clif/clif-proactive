/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib;

import java.util.LinkedList;
import java.util.List;
import org.ow2.clif.analyze.api.AnalyzerLink;

/**
 * Registry for Java Swing-based analyzer singletons.
 * The Swing-based console looks up this registry and integrates
 * these singletons in JInternalFrame instances.
 * 
 * @see org.ow2.clif.console.lib.gui.ClifApp
 * @author Bruno Dillenseger 
 */
abstract public class AnalyzerRegistry
{
	// list of analyzer instances, viewed as AnalyzerLink implementations
	static private List<AnalyzerLink>analyzers = new LinkedList<AnalyzerLink>();

	/**
	 * Registers a Java Swing-based analyzer instance for integration
	 * in the CLIF Swing console.
	 * @param link a Java Swing-based analyzer instance (must implement
	 * the AnalyzerLink interface)
	 */
	static public void register(AnalyzerLink link)
	{
		analyzers.add(link);
	}

	/**
	 * Get registered Java Swing-based analyzer instances.
	 * @return an array of registered AnalyzerLink instances
	 */
	static public AnalyzerLink[] getAnalyzers()
	{
		return analyzers.toArray(new AnalyzerLink[analyzers.size()]);
	}
}
