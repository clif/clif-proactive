/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JInternalFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTree;
import javax.swing.MenuElement;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Report;
import org.ow2.clif.analyze.lib.report.Section;



@SuppressWarnings("serial")
public class RMReportTree extends JPanel{

	private boolean updatingReportTree = false;

	private ReportManager control;
	private DataAccess data;
	private JInternalFrame frame;
	private ReportManagerView rmView;

	//	private MyTreeRenderer renderer;
	//	private JTree jTree1;

	private DefaultMutableTreeNode rootNode;

	private DefaultTreeModel treeModel;
	private JTree tree;
	JPopupMenu popup;
	PopupHandler handler;	//  = new PopupHandler(tree, popup);
	protected TreePath m_clickedPath;

	
	static private final String ADD_DATASET_IN_NEW_SECTION           = "add dataset in a new section";
	static private final String ADD_SIMPLE_DATASET_IN_NEW_SECTION    = "add simple dataset in new section";
	static private final String ADD_MULTIPLE_DATASET_IN_NEW_SECTION  = "add multiple dataset in new section";
	static private final String ADD_AGGREGATE_DATASET_IN_NEW_SECTION = "add aggregate dataset in new section";
	static private final String LOAD_ROUTINE = "load routine";
	static private final String SAVE_ROUTINE = "save routine";
	
	static private final String EXPORT_TO_TXT  = "export to txt";
	static private final String EXPORT_TO_XML  = "export to xml";
	static private final String EXPORT_TO_HTML = "export to html";
	static private final String DUMP_REPORT    = "dump report";
	static private final String DEBUG_AND_TRACE          = "debug and trace";

	static private final String DELETE_SECTION    = "delete section";
	static private final String MOVE_SECTION_UP   = "move section up";
	static private final String MOVE_SECTION_DOWN = "move section down";
	static private final String DUMP_SECTION      = "dump section";

	static private final String ADD_DATASET_TO_SECTION           = "add dataset to section";
	static private final String ADD_SIMPLE_DATASET_TO_SECTION    = "add simple dataset to section";
	static private final String ADD_MULTIPLE_DATASET_TO_SECTION  = "add multiple dataset to section";
	static private final String ADD_AGGREGATE_DATASET_TO_SECTION = "add aggregate dataset to section";
	static private final String DELETE_DATASET    = "delete dataset";
	static private final String DUMP_DATASET      = "dump datataset";
	static private final String MOVE_DATASET_UP   = "move dataset up";
	static private final String MOVE_DATASET_DOWN = "move dataset down";

	
	// report popup menu JMenuItems
//	private JMenuItem addDatasetInNewSectionMenuItem;
//	private JMenuItem addSimpleDatasetInNewSectionMenuItem;
//	private JMenuItem addMultipleDatasetInNewSectionMenuItem;
//	private JMenuItem addAggregateDatasetInNewSectionMenuItem;

//	private JMenuItem exportToTxtMenuItem;
//	private JMenuItem exportToXmlMenuItem;
//	private JMenuItem exporToHtmlMenuItem;

//	private JMenuItem loadRoutineMenuItem;
//	private JMenuItem saveRoutineMenuItem;

	// section popup menu JMenuItems
//	private JMenuItem addDatasetToSectionMenuItem;
//	private JMenuItem addSimpleDatasetToSectionMenuItem;
//	private JMenuItem addMultipleDatasetToSectionMenuItem;
//	private JMenuItem addAggregateDatasetToSectionMenuItem;

//	private JMenuItem deleteSectionMenuItem;
//	private JMenuItem moveSectionDownMenuItem;
//	private JMenuItem moveSectionUpMenuItem; 
	
	//dataset popup menu JMenuItems
//	private JMenuItem deleteDatasetMenuItem;
//	private JMenuItem moveDatasetDownMenuItem;
//	private JMenuItem moveDatasetUpMenuItem;

	// report popup menu Actions
//	private Action addDatasetInNewSectionAction = null;
//	private Action addSimpleDatasetInNewSection = null;
//	private Action addMultipleDatasetInNewSectionAction = null;
//	private Action addAggregateDatasetInNewSectionAction = null;
//	private Action exportToHtmlAction = null;
//	private Action exportToTxtAction = null;
//	private Action exportToXmlAction = null;
//	private Action loadRoutineAction = null;
//	private Action saveRoutineAction = null;
	// section popup menu Actions
//	private Action addDatasetToSectionAction = null;
//	private Action addSimpleDatasetToSectionAction = null;
//	private Action addMultipleDatasetToSectionAction = null;
//	private Action addAggregateDatasetToSectionAction = null;
//	private Action moveSectionDownAction = null;
//	private Action moveSectionUpAction = null;
//	private Action deleteSectionAction = null;
	//dataset popup menu Actions

//	private Action deleteDatasetAction = null;
//	private Action moveDatasetDownAction = null;
//	private Action moveDatasetUpAction = null;

	
	JCheckBoxMenuItem debugCheckBox = null;
	
	// report popup menu
	private JMenuItem m_menuItemAddDatasetInNewSection;
	private JMenuItem m_menuItemAddSimpleDatasetInNewSection;
	private JMenuItem m_menuItemAddMultipleDatasetInNewSection;
	private JMenuItem m_menuItemAddAggregateDatasetInNewSection;

	private JMenuItem m_menuItemTxtExport;
	private JMenuItem m_menuItemXmlExport;
	private JMenuItem m_menuItemHtmlExport;

	private JMenuItem m_menuItemLoadRoutine;
	private JMenuItem m_menuItemSaveRoutine;
	
	private JMenuItem m_menuItemDumpReport;
	private JMenuItem m_menuItemTrace;


	// section popup menu
	private JMenuItem m_menuItemAddDatasetInThisSection;
	private JMenuItem m_menuItemAddSimpleDatasetInThisSection;
	private JMenuItem m_menuItemAddMultipleDatasetInThisSection;
	private JMenuItem m_menuItemAddAggregateDatasetInThisSection;

	private JMenuItem m_menuItemDeleteSection;
	private JMenuItem m_menuItemMoveSectionDown;
	private JMenuItem m_menuItemMoveSectionUp; 
	private JMenuItem m_menuItemDumpSection;
	
	//dataset popup menu
	private JMenuItem m_menuItemDeleteDataset;
	private JMenuItem m_menuItemMoveDatasetDown;
	private JMenuItem m_menuItemMoveDatasetUp;
	private JMenuItem m_menuItemDumpDataset;
	
	private Toolkit toolkit = Toolkit.getDefaultToolkit();
	private TreePath[] oldTreePathArray = null;

	public RMReportTree() {
		super(new GridLayout(1,0));


	}


	public RMReportTree(ReportManager _control, ReportManagerView _rmView, DataAccess _data, JInternalFrame _frame) {
		this();
		data = _data;
		control = _control;
		frame = _frame;
		rmView = _rmView;
		//		LogAndDebug.tracep("(_control, _data, frame)");
		data.setBorder(this, "Report tree");

		rootNode = new DefaultMutableTreeNode(data.report);
		treeModel = new DefaultTreeModel(rootNode);
		treeModel.addTreeModelListener(new MyTreeModelListener());
		tree = new JTree(treeModel);
		tree.setEditable(true);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(true);
		tree.setEditable(false);
		
		tree.addTreeSelectionListener(new TreeSelectionListener(){
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
				if (updatingReportTree){
					LogAndDebug.trace("() node: \"" + node  + "\" already updating reportTree");
				}
				LogAndDebug.tracep("() node: \"" + node  + "\"");
				jTreeValueChanged(e);
				LogAndDebug.tracem("() node: \"" + node  + "\"");
			}
		}) ;


		popup = new JPopupMenu();
		popup.setInvoker(tree);
//		final PopupHandler handler = new PopupHandler(tree, popup);
		handler = new PopupHandler(tree, popup);

		
		
	
		
		// report actions
		
		Action 	m_actionAddDatasetInNewSection;
		m_actionAddDatasetInNewSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddDatasetInNewSection = new JMenuItem(m_actionAddDatasetInNewSection);
		m_menuItemAddDatasetInNewSection.setName(ADD_DATASET_IN_NEW_SECTION);
		m_menuItemAddDatasetInNewSection.setText(ADD_DATASET_IN_NEW_SECTION);

		
		Action 	m_actionAddSimpleDatasetInNewSection;
		m_actionAddSimpleDatasetInNewSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
//				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddSimpleDatasetInNewSection = new JMenuItem(m_actionAddSimpleDatasetInNewSection);
		m_menuItemAddSimpleDatasetInNewSection.setName(ADD_SIMPLE_DATASET_IN_NEW_SECTION);
		m_menuItemAddSimpleDatasetInNewSection.setText(ADD_SIMPLE_DATASET_IN_NEW_SECTION);

		
		Action 	m_actionAddMultipleDatasetInNewSection;
		m_actionAddMultipleDatasetInNewSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddMultipleDatasetInNewSection = new JMenuItem(m_actionAddMultipleDatasetInNewSection);
		m_menuItemAddMultipleDatasetInNewSection.setName(ADD_MULTIPLE_DATASET_IN_NEW_SECTION);
		m_menuItemAddMultipleDatasetInNewSection.setText(ADD_MULTIPLE_DATASET_IN_NEW_SECTION);

		
	
		Action 	m_actionAddAggregateDatasetInNewSection;
		m_actionAddAggregateDatasetInNewSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddAggregateDatasetInNewSection = new JMenuItem(m_actionAddAggregateDatasetInNewSection);
		m_menuItemAddAggregateDatasetInNewSection.setName(ADD_AGGREGATE_DATASET_IN_NEW_SECTION);
		m_menuItemAddAggregateDatasetInNewSection.setText(ADD_AGGREGATE_DATASET_IN_NEW_SECTION);

		
		Action 	m_actionLoadRoutine;
		m_actionLoadRoutine = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemLoadRoutine = new JMenuItem(m_actionLoadRoutine);
		m_menuItemLoadRoutine.setName(LOAD_ROUTINE);
		m_menuItemLoadRoutine.setText(LOAD_ROUTINE);

		
		Action 	m_actionSaveRoutine;
		m_actionSaveRoutine = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemSaveRoutine = new JMenuItem(m_actionSaveRoutine);
		m_menuItemSaveRoutine.setName(SAVE_ROUTINE);
		m_menuItemSaveRoutine.setText(SAVE_ROUTINE);

		
		Action 	m_actionTxtExport;
		m_actionTxtExport = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemTxtExport = new JMenuItem(m_actionTxtExport);
		m_menuItemTxtExport.setName(EXPORT_TO_TXT);
		m_menuItemTxtExport.setText(EXPORT_TO_TXT);

		
		Action 	m_actionXmlExport;
		m_actionXmlExport = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemXmlExport = new JMenuItem(m_actionXmlExport);
		m_menuItemXmlExport.setName(EXPORT_TO_XML);
		m_menuItemXmlExport.setText(EXPORT_TO_XML);

		
		Action 	m_actionHtmlExport;
		m_actionHtmlExport = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemHtmlExport = new JMenuItem(m_actionHtmlExport);
		m_menuItemHtmlExport.setName(EXPORT_TO_HTML);
		m_menuItemHtmlExport.setText(EXPORT_TO_HTML);

		Action 	m_actionDumpReport;
		m_actionDumpReport = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
//				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemDumpReport = new JMenuItem(m_actionDumpReport);
		m_menuItemDumpReport.setName(DUMP_REPORT);
		m_menuItemDumpReport.setText(DUMP_REPORT);

		
		Action 	m_actionDebugAndTrace;
		m_actionDebugAndTrace = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
//				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		
		debugCheckBox = new JCheckBoxMenuItem (DEBUG_AND_TRACE, false);
		debugCheckBox.addActionListener(m_actionDebugAndTrace);
		m_menuItemTrace = debugCheckBox;
		m_menuItemTrace.setName(DEBUG_AND_TRACE);
		m_menuItemTrace.setText(DEBUG_AND_TRACE);

	
		
		// section actions
		
		Action 	m_actionDeleteSection;
		m_actionDeleteSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemDeleteSection = new JMenuItem(m_actionDeleteSection);
		m_menuItemDeleteSection.setName(DELETE_SECTION);
		m_menuItemDeleteSection.setText(DELETE_SECTION);

		Action 	m_actionMoveSectionDown;
		m_actionMoveSectionDown = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemMoveSectionDown = new JMenuItem(m_actionMoveSectionDown);
		m_menuItemMoveSectionDown.setName(MOVE_SECTION_DOWN);
		m_menuItemMoveSectionDown.setText(MOVE_SECTION_DOWN);

		Action 	m_actionMoveSectionUp;
		m_actionMoveSectionUp = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemMoveSectionUp = new JMenuItem(m_actionMoveSectionUp);
		m_menuItemMoveSectionUp.setName(MOVE_SECTION_UP);
		m_menuItemMoveSectionUp.setText(MOVE_SECTION_UP);

		Action 	m_actionAddDatasetInThisSection;
		m_actionAddDatasetInThisSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddDatasetInThisSection = new JMenuItem(m_actionAddDatasetInThisSection);
		m_menuItemAddDatasetInThisSection.setName(ADD_DATASET_TO_SECTION);
		m_menuItemAddDatasetInThisSection.setText(ADD_DATASET_TO_SECTION);

		Action 	m_actionAddSimpleDatasetInThisSection;
		m_actionAddSimpleDatasetInThisSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddSimpleDatasetInThisSection = new JMenuItem(m_actionAddSimpleDatasetInThisSection);
		m_menuItemAddSimpleDatasetInThisSection.setName(ADD_SIMPLE_DATASET_TO_SECTION);
		m_menuItemAddSimpleDatasetInThisSection.setText(ADD_SIMPLE_DATASET_TO_SECTION);

		Action 	m_actionAddMultipleDatasetInThisSection;
		m_actionAddMultipleDatasetInThisSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddMultipleDatasetInThisSection = new JMenuItem(m_actionAddMultipleDatasetInThisSection);
		m_menuItemAddMultipleDatasetInThisSection.setName(ADD_MULTIPLE_DATASET_TO_SECTION);
		m_menuItemAddMultipleDatasetInThisSection.setText(ADD_MULTIPLE_DATASET_TO_SECTION);

		
		Action 	m_actionAddAggregateDatasetInThisSection;
		m_actionAddAggregateDatasetInThisSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemAddAggregateDatasetInThisSection = new JMenuItem(m_actionAddAggregateDatasetInThisSection);
		m_menuItemAddAggregateDatasetInThisSection.setName(ADD_AGGREGATE_DATASET_TO_SECTION);
		m_menuItemAddAggregateDatasetInThisSection.setText(ADD_AGGREGATE_DATASET_TO_SECTION);

		Action 	m_actionDumpSection;
		m_actionDumpSection = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemDumpSection = new JMenuItem(m_actionDumpSection);
		m_menuItemDumpSection.setName(DUMP_SECTION);
		m_menuItemDumpSection.setText(DUMP_SECTION);

		
		
		
		// dataset actions
		
		Action 	m_actionDeleteDataset;
		m_actionDeleteDataset = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemDeleteDataset = new JMenuItem(m_actionDeleteDataset);
		m_menuItemDeleteDataset.setName(DELETE_DATASET);
		m_menuItemDeleteDataset.setText(DELETE_DATASET);

		
		
		Action 	m_actionDumpDataset;
		m_actionDumpDataset = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemDumpDataset = new JMenuItem(m_actionDumpDataset);
		m_menuItemDumpDataset.setName(DUMP_DATASET);
		m_menuItemDumpDataset.setText(DUMP_DATASET);

		Action 	m_actionMoveDatasetDown;
		m_actionMoveDatasetDown = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemMoveDatasetDown = new JMenuItem(m_actionMoveDatasetDown);
		m_menuItemMoveDatasetDown.setName("move down dataset");
		m_menuItemMoveDatasetDown.setText("move down dataset");

		Action 	m_actionMouveDatasetUp;
		m_actionMouveDatasetUp = new AbstractAction() { 
			public void actionPerformed(ActionEvent e){
				LogAndDebug.trace("");
				if (m_clickedPath==null) return;
				handler.actionPerformed(e);
			}
		};
		m_menuItemMoveDatasetUp = new JMenuItem(m_actionMouveDatasetUp);
		m_menuItemMoveDatasetUp.setName("move up dataset");
		m_menuItemMoveDatasetUp.setText("move up dataset");

		
		
		

		tree.add(popup);
		expand(new TreePath(tree.getModel().getRoot()));

		JScrollPane scrollPane = new JScrollPane(tree);
		add(scrollPane);

	}




	@SuppressWarnings("rawtypes")
	private void expand(TreePath path) {
		TreeNode node = (TreeNode)path.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			java.util.Enumeration e = node.children();
			while(e.hasMoreElements()) {
				TreeNode n = (TreeNode)e.nextElement();
				expand(path.pathByAddingChild(n));
			}
		}
		tree.expandPath(path);
	}


	/** Remove all nodes except the root node. */
	public void clear() {
		rootNode.removeAllChildren();
		treeModel.reload();
	}


	/** Remove the currently selected node. */
	public void removeCurrentNode() {
		TreePath currentSelection = tree.getSelectionPath();
		if (currentSelection != null) {
			DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)
			(currentSelection.getLastPathComponent());
			MutableTreeNode parent = (MutableTreeNode)(currentNode.getParent());
			if (parent != null) {
				treeModel.removeNodeFromParent(currentNode);
				return;
			}
		} 
		// Either there was no selection, or the root was selected.
		toolkit.beep();
	}


	/** Add child (Section) to the currently selected node. */
	public DefaultMutableTreeNode addObjectToTree(Object child) {
		DefaultMutableTreeNode ret = null;
		LogAndDebug.tracep("(" + toTexte(child) + ")");
		DefaultMutableTreeNode parentNode = null;
		TreePath parentPath = tree.getSelectionPath();

		if (parentPath == null) {
			parentNode = rootNode;
		} else {
			parentNode = (DefaultMutableTreeNode)(parentPath.getLastPathComponent());
		}
		boolean newChild = true;
		for(int idx = 0; idx < parentNode.getChildCount(); idx++){
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)parentNode.getChildAt(idx);
			Object obj = (node).getUserObject();
			String className = obj.getClass().getSimpleName();

			if (className.equals("Section")){
				Section currSection = (Section) obj;
				Section newSection  = (Section) child;
				if (currSection.getTitle().equals(newSection.getTitle())){
					newChild = false;
					ret = node;
					String sectionName = ((Section)obj).getTitle();
					LogAndDebug.trace("(" + toTexte(child) + ")  found section \""+ sectionName  +"\".");
					break;
				}
			}
		}
		if (newChild){
			ret =  addObjeToTreect(parentNode, child, true);
		}
		LogAndDebug.tracem("(" + toTexte(child) + ")");
		return ret;
	}


	public DefaultMutableTreeNode addObjectToTree(DefaultMutableTreeNode parent, Object child) {
		LogAndDebug.tracep("(" + toTexte(parent) + ", " + toTexte(child) + ")");
		DefaultMutableTreeNode ret =  addObjeToTreect(parent, child, true);
		LogAndDebug.tracem("(" + toTexte(parent) + ", " + toTexte(child) + ")");
		return ret;
	}


	public DefaultMutableTreeNode addObjeToTreect(DefaultMutableTreeNode parent, Object child,  boolean shouldBeVisible) {
		LogAndDebug.tracep("(" + toTexte(parent) + ", " + toTexte(child) + ", " + shouldBeVisible + ")");
		DefaultMutableTreeNode childNode =  new DefaultMutableTreeNode(child);

		if (parent == null) {
			parent = rootNode;
		}

		//It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
		treeModel.insertNodeInto(childNode, parent,  parent.getChildCount());

		//Make sure the user can see the lovely new node.
		if (shouldBeVisible) {
			tree.scrollPathToVisible(new TreePath(childNode.getPath()));
			tree.setSelectionPath(new TreePath(childNode.getPath()));		// fires valueChanged() node: "Section_1"
		}
		LogAndDebug.tracem("(" + toTexte(parent) + ", " + toTexte(child) + ", " + shouldBeVisible + ")");
		return childNode;
	}


	public void clearSelection()
	{
		tree.clearSelection();
	}


	private String toTexte(Object _object) {
		String className;
		if (_object.getClass().getSimpleName().equals("DefaultMutableTreeNode")){
			Object obj = ((DefaultMutableTreeNode) _object).getUserObject();
			_object = obj;
		}
		className = _object.getClass().getSimpleName();

		String ret = "<unknown class: " + className + ">";
		if (className.equals("Report")){		// it's the report itself
			ret = "<Report> \"" + ((Report) _object).getTitle() + "\"";
		} else if (className.equals("Section")){	// it's a Section
			ret = "<Section> \"" + ((Section) _object).getTitle() + "\"";
		}else if (className.equals("Dataset")){	// it's a Dataset
			ret = "<Dataset> \"" + ((Dataset) _object).getTitle() + "\"";
		}
		return ret;
	}


	public void addSectionToReport(Section section) {
		LogAndDebug.tracep("(" + toTexte(section) + ")");
		updatingReportTree = true;
		unselect();
		DefaultMutableTreeNode sectionNode = addObjectToTree(section);
		for(Dataset dataset : section.getDatasets()){
			addDatasetToTree(sectionNode, dataset);
		}
		updatingReportTree = false;
		LogAndDebug.tracem("(" + toTexte(section) + ")");
	}

	
	public void addDatasetToSection(Section sec, Dataset dataset) {
		LogAndDebug.tracep("(" + toTexte(sec) +", "+toTexte(dataset)+ ")");
		DefaultMutableTreeNode sectionNode = findTreeNode(sec);
		addDatasetToTree(sectionNode, dataset);
		
		LogAndDebug.tracem("(" + toTexte(sec) +", "+toTexte(dataset)+ ")");
	}



	private void addDatasetToTree(DefaultMutableTreeNode sectionNode, Dataset dataset) {
		addObjectToTree(sectionNode, dataset);
	}



	private void jTreeValueChanged(TreeSelectionEvent evt) {
		LogAndDebug.tracep("(evt)");
		if (true == updatingReportTree){
			LogAndDebug.tracem("() already updating ReportTree.");
			return;
		}
		updatingReportTree = true;
		TreeSelectionModel curentSelModel = tree.getSelectionModel();
		//  update from tree selection
		TreePath[] treePathArray = curentSelModel.getSelectionPaths();	
		if(null== treePathArray){
			LogAndDebug.log(" * no tree selection.");
		}else{
			if (null != oldTreePathArray){
				if (oldTreePathArray == treePathArray){
					LogAndDebug.tracem("() oldTreePathArray == treePathArray.");
					return;
				}
				LogAndDebug.log("() oldTreePathArray: "+ oldTreePathArray);
				if ((treePathArray.length > 0)&& (oldTreePathArray.length > 0)&& (oldTreePathArray[0] == treePathArray[0])){
					updatingReportTree = false;
					LogAndDebug.tracem("() " + treePathArray[0] + " already selected.");
					return;
				}
			}
			oldTreePathArray = treePathArray;
			// clear selection
			rmView.unselectObject();	//  needed so added nodes go at the right place ...
			// select according to "selectedSection" and "selectedDataset"
			for (TreePath p : treePathArray){
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)p.getPathComponent(0);
				if (p.getPathCount() > 1){
					node = (DefaultMutableTreeNode)p.getPathComponent(1);
					Section  selectedSection = (Section) node.getUserObject();
					if (p.getPathCount() > 2){
						node = (DefaultMutableTreeNode)p.getPathComponent(2);
						Dataset dataset = (Dataset) node.getUserObject();
						rmView.setSelectedDataset(dataset, 1);
					}else{
						LogAndDebug.trace("() selected object: "+LogAndDebug.toText(selectedSection));
						rmView.setSelectedSection(selectedSection, 1);
					}
				}
				if (null != rmView.rmOptions){
					rmView.updateReportManagerView();
				}
			}
		}
		updatingReportTree = false;
		frame.repaint();
		LogAndDebug.tracem("(evt)");
	}


	void unselect(){
		LogAndDebug.tracep();
		tree.clearSelection();
		LogAndDebug.tracem();
	}


	class MyTreeRenderer extends DefaultTreeCellRenderer {
		public Component getTreeCellRendererComponent(
				JTree tree,
				Object value,
				boolean sel,
				boolean expanded,
				boolean leaf,
				int row,
				boolean hasFocus) {

			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
			if (leaf && isSection(value)) {
				setToolTipText("This is a section.");
			} else if (leaf && isDataset(value)) {
				setToolTipText("This is a dataset.");
			}else {
				setToolTipText(null); //no tool tip
			} 

			return this;
		}

		private boolean isSection(Object value) {
			Class<? extends Report> c = data.report.getClass();
			return (value.getClass() == c);
		}

		private boolean isDataset(Object value) {
			Class<? extends Dataset> c = (new Dataset()).getClass();
			return (value.getClass() == c);
		}


	}

	class MyTreeModelListener implements TreeModelListener {
		public void treeNodesChanged(TreeModelEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)(e.getTreePath().getLastPathComponent());
			LogAndDebug.tracep("(<node> \"" + node.getUserObject() + "\")");

			/*
			 * If the event lists children, then the changed
			 * node is the child of the node we've already
			 * gotten.  Otherwise, the changed node and the
			 * specified node are the same.
			 */

			int index = e.getChildIndices()[0];
			node = (DefaultMutableTreeNode)(node.getChildAt(index));

			LogAndDebug.tracem("(<node> \"" + node.getUserObject() + "\")");
		}
		public void treeNodesInserted(TreeModelEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)(e.getTreePath().getLastPathComponent());
			LogAndDebug.tracep("(<node> \"" + node.getUserObject() + "\")");
			LogAndDebug.tracem("(<node> \"" + node.getUserObject() + "\")");
		}
		public void treeNodesRemoved(TreeModelEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)(e.getTreePath().getLastPathComponent());
			LogAndDebug.tracep("(<node> \"" + node.getUserObject() + "\")");
			LogAndDebug.tracem("(<node> \"" + node.getUserObject() + "\")");
		}
		public void treeStructureChanged(TreeModelEvent e) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)(e.getTreePath().getLastPathComponent());
			LogAndDebug.tracep("(<node> \"" + node.getUserObject() + "\")");
			LogAndDebug.tracem("(<node> \"" + node.getUserObject() + "\")");
		}
	}


	//  selecting an object implies selecting and updating a treePath, an options tab and a table's row
	public void selectReport() {
		tree.setSelectionPaths(findTreePath(data.report));

		// rootNode is root of the type DefaultMutableTreeNode
		//        tree.setSelectionPath( new TreePath( rootNode.getPath() ) );
		// This statement does the exact same thing as that above
		// Use one or the other - not both
		//        tree.setSelectionPath( new TreePath( ((DefaultTreeModel)tree.getModel()).getPathToRoot( node ) ) );
	}


	@SuppressWarnings("unused")
	private DefaultMutableTreeNode findNode(Object _obj, DefaultMutableTreeNode _node) {
		if (_node.getUserObject().equals(_obj)){
			return _node;
		}
		for(int i =0; i<_node.getChildCount();i++){
			return findNode(_obj, (DefaultMutableTreeNode) _node.getChildAt(i));
		}
		LogAndDebug.log("*** could not find object \""+_obj+"\" in the report tree.");
		return null;
	}
	


	public void selectReport(Report report, int depth) {
		tree.setSelectionPaths(findTreePath(report));
	}


	public void selectSection(Section section, int depth) {
		tree.setSelectionPaths(findTreePath(section));
	}


	public void selectChart(Dataset chart, int depth) {
		tree.setSelectionPaths(findTreePath(chart));
	}


	public void selectDataset(Dataset dataset, int depth) {
		tree.setSelectionPaths(findTreePath(dataset));
	}


	private TreePath[] findTreePath(Report report) {
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)tree.getModel().getRoot(); 
		return getPath(rootNode);
	}


	private TreePath[] findTreePath(Section sec) {
		DefaultMutableTreeNode node = findTreeNode(sec);
		return getPath(node);
	}


	private TreePath[] findTreePath(Dataset _dataset) {
		DefaultMutableTreeNode node = findTreeNode(_dataset);
		return getPath(node);
	}


	// finds the node associated with _object
	private DefaultMutableTreeNode findTreeNode(Object _object) {
		if (_object == null){
			return null;
		}
		DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)tree.getModel().getRoot(); 
		if (_object.getClass() == data.report.getClass()){		// it's the report itself
			return rootNode;
		} 
		if (_object.getClass().getSimpleName().equals("Section")){	// it's a Section
			for (Enumeration<?> children = rootNode.children(); children.hasMoreElements();){
				DefaultMutableTreeNode SectionNode = (DefaultMutableTreeNode) children.nextElement();
				Section currentSection = (Section) SectionNode.getUserObject();
				if (_object.equals(currentSection)){
					return SectionNode;
				}
			}
		}
		if (_object.getClass().getSimpleName().equals("Dataset")){	// it's a Dataset
			for (Enumeration<?> children = rootNode.children(); children.hasMoreElements();){
				DefaultMutableTreeNode sectionNode = (DefaultMutableTreeNode) children.nextElement();
				//				Section currentSection = (Section) sectionNode.getUserObject();
				for (Enumeration<?> grandChildren = sectionNode.children(); grandChildren.hasMoreElements();){
					DefaultMutableTreeNode datasetNode = (DefaultMutableTreeNode) grandChildren.nextElement();
					Dataset currentDataset = (Dataset) datasetNode.getUserObject();
					if (_object.equals(currentDataset)){
						return datasetNode;
					}

				}
			}
		}
		LogAndDebug.trace("*** the object of class " +_object.getClass()+ " does not exist in the tree...");
		return null;
	}


	// Returns a TreePath containing the specified node.
	public TreePath[] getPath(DefaultMutableTreeNode _node) {
		if (null == _node){
			return null;
		}
		List<TreePath> list = new ArrayList<TreePath>();
		TreePath path = new TreePath(((DefaultTreeModel)tree.getModel()).getPathToRoot(_node));
		list.add(path);
		TreePath[] array = list.toArray(new TreePath[list.size()]);

		return array;
	}


	// update selections according to selectedSection and SelectedDataset
	public void updateReportTreeSelections() {
		if (null != rmView.getSelectedDataset()){		//  the selected object is a dataset
			selectDataset(rmView.getSelectedDataset(), 1);
		}else if (null != rmView.getSelectedSection()){	// the selected object is a ReportSection
			selectSection(rmView.getSelectedSection(), 1);
		}else{
			selectReport();			// the only other possibility 
		}
	}


	void updateSelection(){
		TreePath[] paths = null;

		for (Section sec : data.report.getSections()){
			LogAndDebug.log("RMReportTree: updating selected sections. Not yet implemented..." + sec.getTitle());
			for (Dataset dataset : sec.getDatasets()){
				LogAndDebug.log("RMReportTree: updating selected dataset. Not yet implemented..." + dataset.getName());
			}
		}
		tree.setSelectionPaths(paths);
	}


	class PopupHandler implements ActionListener {
		JTree tree;
		JPopupMenu popup;
		Point loc;

		public PopupHandler(JTree tree, JPopupMenu popup) {
			this.tree = tree;
			this.popup = popup;
			tree.addMouseListener(ma);
		}

		private MouseListener ma = new MouseAdapter() {
			private void checkForPopup(MouseEvent e) {
				if(e.isPopupTrigger()) {
					loc = e.getPoint();
					popup.show(tree, loc.x, loc.y);
				}
			}

			public void mousePressed(MouseEvent e)  {
				checkForPopup(e);
			}

			public void mouseReleased(MouseEvent e) {
				checkForPopup(e);
				if (e.isPopupTrigger()){
					int x = e.getX();
					int y = e.getY();
					TreePath path = tree.getPathForLocation(x, y);
					// first: clear popup menu
					MenuElement[] menuElements = popup.getSubElements();
					int l = menuElements.length;
					LogAndDebug.log(" popup: removing " + l+" elements.");
					popup.removeAll();
					l = popup.getSubElements().length;
					LogAndDebug.log(" popup has now " + l+" elements.");
					dumpPopup(popup);
					if ((null == path)|| (0 == path.getPathCount())){		// nothing is pointed
						LogAndDebug.log("nothing is pointed, " + LogAndDebug.toText(rmView.getSelectedObject())+" is selected.");
//						popup.add(m_menuItemAddSimpleDatasetInNewSection);
//						popup.add(m_menuItemAddMultipleDatasetInNewSection);
//						popup.add(m_menuItemAddAggregateDatasetInNewSection);
//						popup.addSeparator();
						popup.add(m_menuItemHtmlExport);
						popup.add(m_menuItemXmlExport);
						popup.add(m_menuItemTxtExport);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemSaveRoutine);
						popup.add(m_menuItemLoadRoutine);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDumpReport);
						popup.add(m_menuItemTrace);
					}else if (1 == path.getPathCount()){		// the report is pointed
						LogAndDebug.log("report has been pointed, " + LogAndDebug.toText(rmView.getSelectedObject())+" is selected.");
						popup.add(m_menuItemAddSimpleDatasetInNewSection);
						popup.add(m_menuItemAddMultipleDatasetInNewSection);
						popup.add(m_menuItemAddAggregateDatasetInNewSection);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemHtmlExport);
						popup.add(m_menuItemXmlExport);
						popup.add(m_menuItemTxtExport);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemSaveRoutine);
						popup.add(m_menuItemLoadRoutine);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDumpReport);
					}else if (2 == path.getPathCount()){		// a section is pointed
						LogAndDebug.log(" a section has been pointed, " + LogAndDebug.toText(rmView.getSelectedObject())+" is selected.");
						popup.add(m_menuItemAddSimpleDatasetInThisSection);
						popup.add(m_menuItemAddMultipleDatasetInThisSection);
						popup.add(m_menuItemAddAggregateDatasetInThisSection);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemMoveSectionUp);
						popup.add(m_menuItemMoveSectionDown);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDeleteSection);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDumpSection);
					}else if (3 == path.getPathCount()){
						LogAndDebug.log(" a dataset has been pointed, " + LogAndDebug.toText(rmView.getSelectedObject())+" is selected.");
						popup.add(m_menuItemMoveDatasetUp);
						popup.add(m_menuItemMoveDatasetDown);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDeleteDataset);
						popup.addSeparator();
					    popup.add(new JSeparator());
						popup.add(m_menuItemDumpDataset);
					}
					dumpPopup(popup);
					popup.show(tree, x, y);
					m_clickedPath = path;
				}

			}

			void dumpPopup(JPopupMenu popup){
				String msg = "popup("+popup.getSubElements().length+") = [";
				String pre = "\n                        ";
				int i = 0;
				for (MenuElement me : popup.getSubElements()){
					msg += pre + i++ + " \"" +((JMenuItem)me).getText()+ "\"";
					pre = ",\n                        ";
				}
				msg += "                        ]";
				LogAndDebug.log(msg);

			}
			public void mouseClicked(MouseEvent e)  {
				checkForPopup(e);
			}
		};		// MouseAdapter
		public void actionPerformed(ActionEvent e) {
			String ac = e.getActionCommand();
			if (null == ac) return;
			TreePath path  = tree.getPathForLocation(loc.x, loc.y);
			LogAndDebug.log("actionPerformed: \"" + ac + "\" - " + path);
			LogAndDebug.trace(": " + ac + " - " + path);

//			if(null == path)    {LogAndDebug.log("no selections...");return;}

			
			if ((null == path) 							// no selections
					||(0 == path.getPathCount())
					||(1 == path.getPathCount())){		// pointing to report
				if(ac.equals("ADD SIMPLE DATASET"))		     {addSimpleDataset();		return;}
				if(ac.equals("ADD MULTIPLE DATASET"))	     {addSimpleDataset();		return;}
				if(ac.equals("ADD AGGREGATE DATASET"))	     {addAggregateDataset();	return;}
				if(ac.equals(DUMP_REPORT))                   {data.report.dump(); return;}
				if(ac.equals(DEBUG_AND_TRACE))               {toggleDebugAndTraceCheckBox(); return;}

				// report is pointed
				if(ac.equals(ADD_DATASET_IN_NEW_SECTION))           {addSimpleDataset();    return;}
				if(ac.equals(ADD_SIMPLE_DATASET_IN_NEW_SECTION))    {addSimpleDataset();    return;}
				if(ac.equals(ADD_MULTIPLE_DATASET_IN_NEW_SECTION))  {addMultipleDataset();  return;}
				if(ac.equals(ADD_AGGREGATE_DATASET_IN_NEW_SECTION)) {addAggregateDataset(); return;}
				if(ac.equals(EXPORT_TO_HTML))                       {htmlExport();  return;}
// TODO: uncomment when XML export is ready				if(ac.equals(EXPORT_TO_XML))                        {xmlExport();   return;}
// TODO: uncomment when text export is ready			if(ac.equals(EXPORT_TO_TXT))                        {txtExport();   return;}
				if(ac.equals(SAVE_ROUTINE))                         {saveRoutine(); return;}
				if(ac.equals(LOAD_ROUTINE))                         {loadRoutine(); return;}
				if(ac.equals(DUMP_REPORT))                          {data.report.dump(); return;}
			}else 

			if (2 == path.getPathCount()){		// a section is pointed
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getPathComponent(1);
				Section section = (Section) node.getUserObject();
				if(ac.equals(ADD_DATASET_TO_SECTION))            {addDatasetToSection(section); return;}
				if(ac.equals(ADD_SIMPLE_DATASET_TO_SECTION))     {addSimpleDatasetToSection(section); return;}
				if(ac.equals(ADD_MULTIPLE_DATASET_TO_SECTION))   {addMultipleDatasetToSection(section); return;}
				if(ac.equals(ADD_AGGREGATE_DATASET_TO_SECTION))  {addAggregateDatasetToSection(section); return;}
				if(ac.equals(MOVE_SECTION_UP))                   {moveSectionUp(section);   return;}
				if(ac.equals(MOVE_SECTION_DOWN))                 {moveSectionDown(section); return;}
				if(ac.equals(DELETE_SECTION))                    {removeSection(section);   return;}
				if(ac.equals(DUMP_SECTION))                      {System.out.println(section.dump(1)); return;}
			}else 

			if (3 == path.getPathCount()){		// a dataset is pointed
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getPathComponent(2);
				Dataset dataset = (Dataset) node.getUserObject();
				if(ac.equals(MOVE_DATASET_UP))     {moveDatasetUp(dataset);   return;}
				if(ac.equals(MOVE_DATASET_DOWN))   {moveDatasetDown(dataset); return;}
				if(ac.equals(DELETE_DATASET))      {removeDataset(dataset);   return;}
				if(ac.equals(DUMP_DATASET))        {System.out.println(dataset.dump()); return;}
				if(ac.equals("...."))              {actionNotImplemented(dataset);      return;}
			}
			LogAndDebug.log(" Action not recognized \"" + ac + "\"");
		}

		

		private void toggleDebugAndTraceCheckBox()
		{
        	LogAndDebug.debug = debugCheckBox.isSelected();
		}



		// report actions
		
		private void addSimpleDataset() {
			control.addSimpleDataset();
		}

		private void addMultipleDataset() {
			control.addMultipleDataset();
		}

		private void addAggregateDataset() {
			control.addAggregateDataset();
		}

		private void htmlExport()
		{
			rmView.exportReportToHtml();
		}

/* TODO: uncomment when XML export is ready
		private void xmlExport() {
			rmView.exportReportToXml();
		}
*/
/* TODO: uncomment  when text export is ready
		private void txtExport() {
			rmView.exportReportToTxt();
		}
*/
		private void saveRoutine() {
			data.report.saveRoutine();
		}

		private void loadRoutine() {
			data.report.loadRoutine();
		}


		// section actions
		
		private void addDatasetToSection(Section sec) {
			control.addSimpleDataset(sec);
			LogAndDebug.unimplemented();
		}

		private void addSimpleDatasetToSection(Section sec) {
			control.addSimpleDataset(sec);
		}

		private void addMultipleDatasetToSection(Section sec) {
			control.addMultipleDataset(sec);
		}

		private void addAggregateDatasetToSection(Section sec) {
			control.addAggregateDataset(sec);
		}

		private void moveSectionUp(Section sec) {
			// TODO Auto-generated method stub
			LogAndDebug.unimplemented();
		}

		private void moveSectionDown(Section sec) {
			// TODO Auto-generated method stub
			LogAndDebug.unimplemented();
		}

		private void removeSection(Section sec) {
			LogAndDebug.tracep("("+LogAndDebug.toText(sec)+")");
			Section toSelect = rmView.getPreviousSection(sec);
			if (null == toSelect){
				toSelect = rmView.getNextSection(sec);
			}
			control.removeSection(sec);
			if (null == toSelect){
				rmView.selectReport();
			}
			LogAndDebug.tracem("("+LogAndDebug.toText(sec)+")");
		}

		
		// dataset actions
		
		private void moveDatasetUp(Dataset dataset) {
			// TODO Auto-generated method stub
			LogAndDebug.unimplemented();
		}

		private void moveDatasetDown(Dataset dataset) {
			// TODO Auto-generated method stub
			LogAndDebug.unimplemented();
		}
		
		private void removeDataset(Dataset dataset) {
			control.removeDataset(dataset);
		}


		private void actionNotImplemented(Dataset dataset) {
			LogAndDebug.unimplemented();
			// TODO Auto-generated method stub
			LogAndDebug.unimplemented();
		}
	}		// class PopupHandler


	public void setselectedObject(Object obj, int depth) {		//  called by rmView
		oldTreePathArray = null;
		if (obj == rmView.getSelectedObject()){
			LogAndDebug.trace("("+LogAndDebug.toText(obj)+", "+depth+") already selected");
			return;
		}
		String className = obj.getClass().getSimpleName();

		if (className.equals("Report")){
			selectReport();
		}else if(className.equals("Section")){
			selectSection((Section) obj, depth);
		}else if (className.equals("Dataset")){
			selectDataset((Dataset) obj, depth);
		}else{
			;
		}
	}
}
