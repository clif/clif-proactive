/*
* CLIF is a Load Injection Framework
* Copyright (C) 2017 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * Instances of this class are Swing panels with a titled border.
 * They take into account their title when computing their
 * preferred size so that the title is not truncated.
 * 
 * @author Bruno Dillenseger
 */
public class TitledPanel extends JPanel
{
	private static final long serialVersionUID = 5013237511973267654L;

	private String title;

	public TitledPanel(String title)
	{
		super();
		setTitle(title);
	}

	public void setTitle(String title)
	{
		this.title = title;
		setBorder(BorderFactory.createTitledBorder(title));
	}

	public String getTitle()
	{
		return title;
	}

	@Override
    public Dimension getPreferredSize()
	{
		Dimension result = super.getPreferredSize();
		TitledBorder border = (TitledBorder)getBorder();
		if (border != null && title != null)
		{
			int width =
				getFontMetrics(border.getTitleFont()).stringWidth(title)
				+ border.getBorderInsets(this).left
				+ border.getBorderInsets(this).right;
			if (result.getWidth() < width)
			{
				result = new Dimension(width, result.height);
			}
		}
		return result;
    }
}
