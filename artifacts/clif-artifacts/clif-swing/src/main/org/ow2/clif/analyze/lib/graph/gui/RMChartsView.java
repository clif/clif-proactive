/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.tree.DefaultMutableTreeNode;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Report;
import org.ow2.clif.analyze.lib.report.Section;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 * 
 * Third and last (bottom) part of ReportManager View
 */
public class RMChartsView extends JTabbedPane{
	private static final long serialVersionUID = -8738534743094814200L;
	private int preferredHeight = 450;
	private int preferredWidth = 450;
//	private ReportManager control;
	private DataAccess data;
//	private JInternalFrame frame;
	private ReportManagerView rmView;

	Component selectedTab;
	private boolean updatingChartsView = false;

	// Constructors //

	public RMChartsView(ReportManager _control, ReportManagerView _rmView, DataAccess _data, JInternalFrame _frame) {
		super();
		data = _data;
//		control = _control;
//		frame = _frame;
		rmView = _rmView;
		data.setBorder(this, "Charts");
		setPreferredSize(new Dimension(preferredWidth, preferredHeight));
		JLabel jlNoChart = new JLabel("No charts defined.");

		if (0 == data.report.getSections().size()){
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.BOTH;
			c.anchor = GridBagConstraints.CENTER;

			JPanel panel = new JPanel();
			panel.setLayout(new GridBagLayout());
			panel.add(jlNoChart, c);
			this.add(panel);
		}

		// Register a change listener
		this.addChangeListener(new ChangeListener() {
			// This method is called whenever the selected tab changes
			public void stateChanged(ChangeEvent evt) {
				LogAndDebug.tracep("(evt)");
				if (updatingChartsView){
					LogAndDebug.trace(" - under modification - ");
				}else{
				JTabbedPane pane = (JTabbedPane)evt.getSource();
				if (null != pane){
					int selectedTabIndex = pane.getSelectedIndex();
					// Get current tab
					if (-1 == selectedTabIndex){
						LogAndDebug.trace("()  selecting <null>");
					} else if (selectedTab == pane.getSelectedComponent()){
						LogAndDebug.trace("() " + selectedTab.getName() + " already selected.");
					}else{
						selectedTab = pane.getSelectedComponent();
						if (null != selectedTab){
							if (selectedTab.getClass().getSimpleName().equals("JScrollPane")){
								LogAndDebug.trace("() selecting \"" + selectedTab.getName() + "\"");
								selectPointedTab(selectedTab, 1);
							}
						}
					}
				}
			}
				LogAndDebug.tracem("(evt)");
			}	// stateChanged()
		});		// addChangeListener
	}


	private JScrollPane graphTabCreation(Section sec){
		LogAndDebug.tracep("(\"" + sec.getTitle() + "\" (" + + sec.getId() + ") ).");
		JScrollPane graphTab = new JScrollPane();
		graphTab.setName(sec.getTitle());
		JFreeChart jfChart = sec.getJfChart();
		if (null == jfChart){
			JLabel emptyChartLabel = new JLabel ("No chart.");
			JPanel panel = new JPanel();
//			setBorder(panel, "panel");
			panel.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.BOTH;
			c.anchor = GridBagConstraints.CENTER;
			panel.add(emptyChartLabel, c);
			graphTab.setViewportView(panel);
		}else{
			ChartPanel testPanel = new ChartPanel(jfChart);
			int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
			int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED ;
			graphTab.setVerticalScrollBarPolicy(v);
			graphTab.setHorizontalScrollBarPolicy(h);
			graphTab.setViewportView(testPanel);
		}

		LogAndDebug.tracem("(\"" + sec.getTitle() + "\" (" + + sec.getId() + ") ).");
		return graphTab;
	}


	public void updateChartsView() {
		LogAndDebug.tracep();
		if (updatingChartsView){
			LogAndDebug.trace(" already updating ChartsView...");
			LogAndDebug.tracem();
			return;
		}
		updatingChartsView = true;
		boolean rebuild = false;
		int numberOfSections = data.report.getSections().size();
		int numberOfTabs = this.getComponents().length;
		
		// check if Sections correspond to Tabs
		if (numberOfSections == numberOfTabs){
			int i = 0;
			for(Section sec : data.report.getSections()){
				Component c = this.getComponent(i++);
				if ("JScrollPane".equals(c.getClass().getSimpleName())){
					String panelName = ((JScrollPane)c).getName();
					String sectionName = sec.getTitle();
					if ( ! panelName.equals(sectionName)){
						rebuild = true;			// tab and section do not correspond
						LogAndDebug.trace("  tab and section do not correspond");
					}else{
						;  // TODO what if datasets or datasources or drawingOptions have changed ?? Should be tested on the calling method
					}
				}else{
					LogAndDebug.trace(" must be a JPanel (or a JLabel)...");
					rebuild = true;	
					break;
				}
			}
		}else{
			LogAndDebug.trace(" there must be a new or a deleted section...");
			rebuild = true;
		}

		if (rebuild){
			LogAndDebug.trace(" rebuilding....");
			// remove all charts
			for (Component c : this.getComponents()){
				LogAndDebug.trace(" removing "+ c.getClass().getSimpleName() + "\"" + c.getName() + "\"");;
				this.remove(c);		// fires [RMChartsView]stateChanged(evt)
			}
			// add tabs
			LogAndDebug.trace(" adding tabs....");
			for(Section sec : data.report.getSections()){
				LogAndDebug.trace(" adding section \"" + sec.getTitle() + "\"");
				this.add(graphTabCreation(sec), sec.getTitle());		// fires [RMChartsView]stateChanged(evt)
			}

			if (0 == data.report.getSections().size()){
				LogAndDebug.trace(" No charts defined.");
				JLabel jlNoChart = new JLabel("No charts defined.");
				this.add(jlNoChart);
				
				GridBagConstraints c = new GridBagConstraints();
				c.gridx = 0;
				c.gridy = 0;
				c.fill = GridBagConstraints.BOTH;
				c.anchor = GridBagConstraints.CENTER;

				JPanel panel = new JPanel();
				panel.setLayout(new GridBagLayout());
				panel.add(jlNoChart, c);
				this.add(panel);
			}
		}
		updatingChartsView = false;
		LogAndDebug.tracem();
	}


	private void selectTab(int index){
		LogAndDebug.tracep("(" + index+")");
		updatingChartsView = true;
		setSelectedIndex(index);
		updatingChartsView = false;
		LogAndDebug.tracem("(" + index+")");
	}



	public void selectTabBySectionName(Section sec, int depth) {
		LogAndDebug.tracep("(" + toTexte(sec)+", "+depth+")");
		for(Component comp : this.getComponents()){
			if (comp.getClass().getSimpleName().equals("JScrollPane")){
				if (comp.getName().equals(sec.getTitle())){
					selectSection(sec, depth);
				}
			}
		}
		LogAndDebug.tracem("(" + toTexte(sec)+", "+depth+")");
	}

	private int getTabIndex(String title) {
		int ret = -1;
		for (Component c : this.getComponents()){
			if ("JScrollPane".equals(c.getClass().getSimpleName())){
				ret++;
				String tabName = c.getName();
				if (tabName.equals(title)){
					break;
				}
			}
		}
		return ret;
	}



	void selectObject(Object obj, int depth) {							// called by rmView
		LogAndDebug.tracep("(" + toTexte(obj)+", "+depth+")");
		String className = obj.getClass().getSimpleName();

		if (className.equals("Report")){			// it's the report itself
			;		// don't change selection
		} else if (className.equals("Section")){	// it's a Section
			selectTabBySectionName((Section) obj, depth);
		}else if (className.equals("Dataset")){		// it's a Dataset
			selectDataset((Dataset) obj, depth);
		}else{
			LogAndDebug.trace("(" + toTexte(obj)+") Object not selectable");
		}
		LogAndDebug.tracem("(" + toTexte(obj)+", "+depth+")");
	}




	public void setSelectedObject(Object obj, int depth) {		//  called by rmView
		LogAndDebug.tracep("(" + toTexte(obj)+", "+depth+")");
		String className = obj.getClass().getSimpleName();

		if (className.equals("Report")){
			;		// don't touch charts selections
		}else if(className.equals("Section")){
			selectTabBySectionName((Section) obj, depth);
		}else if (className.equals("Dataset")){
			LogAndDebug.trace("(" + toTexte(obj)+") don't touch charts selections");
		}else{
			LogAndDebug.trace(" unrecognized object to select :" + toTexte(obj));
		}
		LogAndDebug.tracem("(" + toTexte(obj)+", "+depth+")");
	}



	private void selectPointedTab(Component selectedComponent, int depth) {
		LogAndDebug.tracep("(" + toTexte(selectedComponent)+", "+depth+")");
		if (selectedComponent.getClass().getSimpleName().equals("JScrollPane")){
			String selectedTabName = ((JScrollPane) selectedComponent).getName();	//  because TabName == SectionName
			LogAndDebug.trace("("+selectedTabName+")");
			if (depth > 0){
				rmView.selectSection(selectedTabName, depth);
			}
		}
		LogAndDebug.tracem("(" + toTexte(selectedComponent)+", "+depth+")");
	}


	public void selectReport(Report report) {
		LogAndDebug.trace("()  don't touch charts selections");
	}

	
	public void selectSection(Section section, int depth) {
		boolean alreadySelected = false;

		LogAndDebug.tracep("(" + toTexte(section)+", "+depth+")");
		if (null != getSelectedComponent()){
			if (getSelectedComponent().getClass().getSimpleName().equals("JScrollPane")){
				String selectedTabName = ((JScrollPane) getSelectedComponent()).getName();	// because TabName == SectionName
				if (selectedTabName == section.getTitle()){		// TODO should compare with displayed TAB
					alreadySelected = true;
					LogAndDebug.trace("("+LogAndDebug.toText(section)+", "+depth+") already selected");
				}
			}
		}
		if (!alreadySelected){
			if (depth > 0){
				rmView.selectSection(section, depth);
			}
			int tabIndex = getTabIndex(section.getTitle());
			selectTab(tabIndex);
		}
		LogAndDebug.tracem("(" + toTexte(section)+", "+depth+")");
	}




	public void selectDataset(Dataset dataset, int depth) {
		LogAndDebug.tracep("(" + toTexte(dataset)+", "+depth+")");
		Section sec = data.getSection(dataset);
		if (null == sec){
			LogAndDebug.trace("("+ LogAndDebug.toText(dataset) +", "+depth+"): section not defined in dataset...");
		}
		selectSection(sec, 0);			// depth set to 0 : no rmView calls
		LogAndDebug.tracem("(" + toTexte(dataset)+", "+depth+")");
	}


	private String toTexte(Object _object) {
		String className;
		if (_object.getClass().getSimpleName().equals("DefaultMutableTreeNode")){
			Object obj = ((DefaultMutableTreeNode) _object).getUserObject();
			_object = obj;
		}
		className = _object.getClass().getSimpleName();

		String ret = "<unknown class: " + className + ">";
		if (className.equals("Report")){		// it's the report itself
			ret = "<Report> \"" + ((Report) _object).getTitle() + "\"";
		} else if (className.equals("Section")){	// it's a Section
			ret = "<Section> \"" + ((Section) _object).getTitle() + "\"";
		}else if (className.equals("Dataset")){	// it's a Dataset
			ret = "<Dataset> \"" + ((Dataset) _object).getTitle() + "\"";
		}else if (className.equals("JScrollPane")){		// it's a tab
			ret = "<JScrollPane> \"" + ((JScrollPane) _object).getName() + "\"";
		}
		return ret;
	}
}
