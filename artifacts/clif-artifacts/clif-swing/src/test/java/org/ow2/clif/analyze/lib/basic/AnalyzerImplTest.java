package org.ow2.clif.analyze.lib.basic;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * just a trivial unit test in order to show how to build basic tests with JUnit and Maven.
 * @author Vincent ROSSIGNOL NRS
 */
public class AnalyzerImplTest 
{

	@Test
	public void testAnalyzerImpl() 
	{
		assertNotNull( new AnalyzerImpl() );
	}

}
