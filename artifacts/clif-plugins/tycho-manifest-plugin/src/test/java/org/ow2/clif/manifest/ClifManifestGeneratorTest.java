package org.ow2.clif.manifest;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;

public class ClifManifestGeneratorTest extends AbstractMojoTestCase 
{

    /**
     * @throws Exception if any
     */
    public void testNominal()
        throws Exception
    {
        File pom = getTestFile( "src/test/resources/simple/pom.xml" );
        assertNotNull( pom );
        assertTrue( pom.exists() );

        ClifManifestGenerator myMojo = (ClifManifestGenerator) lookupMojo( "tycho-manifest", pom );
        assertNotNull( myMojo );
        myMojo.execute();
    }
}
