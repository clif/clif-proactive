package org.ow2.clif.manifest;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * generates CLIF manifest files for Eclipse plugins.
 * @author Vincent ROSSIGNOL
 * @version 0.0.3
 */
@Mojo( name = "tycho-manifest", defaultPhase = LifecyclePhase.GENERATE_SOURCES )
public class ClifManifestGenerator extends AbstractMojo
{
    /**
     * output directory.
     */
    @Parameter( defaultValue = "${project.build.directory}", property = "outputDir", required = true )
    private File outputDirectory;
    
    /**
     * target name.
     */
    @Parameter( required = true, defaultValue = "${project.name}" )
    private String name; 

    /**
     * target version (i.e. current CLIF version)
     */
    @Parameter( required = true, defaultValue = "${project.version}" )
    private String version;
    
    /**
     * library directories
     */
    @Parameter( required = true )
    private LibDirectory[] directories;

    /**
     * additional jars
     */
    @Parameter
    private String[] additionals;
    
    /**
     * bundle activator.
     */
    @Parameter( required = true )
	private String activator;
    
    /**
     * bundle symbolic name
     */
    @Parameter( required = true )
    private String symbolicName;
    
    /**
     * required bundles
     */
    @Parameter
    private String requirebundle;
    
    /**
     * Export-Package
     */
    @Parameter
    private String exportpackage;
    
    /**
     * Import-Package
     */
    @Parameter
    private String importpackage;
    
    /**
     * plugin is dir ( "Eclipse-BundleShape: dir" in manifest
     * @since version 0.0.3
     */
    @Parameter( defaultValue = "false" )
    private boolean unpack;
    
	/**
	 * builds and returns MANIFEST.MF content.
	 * @return manifest content as a <code>String</code>
	 * @throws MojoExecutionException if an error occurred.
	 */
    protected String getManifestContent () throws MojoExecutionException
    {
    	this.getLog().info( "generating MANIFEST.MF content for " + this.getName() );
    	
    	String result = 
    			"Bundle-ManifestVersion: 2\n" +
    			"Bundle-RequiredExecutionEnvironment: JavaSE-1.6\n" +
    			"Bundle-Name: " + this.getName() + "\n" +
    			"Bundle-SymbolicName: " + this.getSymbolicName() + "\n" +
    			"Bundle-Version: " + this.getBundleVersion() + "\n" +
    			"Bundle-Activator: " + this.getActivator() + "\n" + 
    			"Bundle-Vendor: France Telecom\n" +
    			"Bundle-ActivationPolicy: lazy\n" +
    			"Bundle-ClassPath:" + this.getBundleClassPath() + "\n";
    	
    	String strRequireBundle = this.getRequireBundle();
    	if ( strRequireBundle != null )
    	{
    		result = result + strRequireBundle + "\n";
    	}
    	
    	String strExportPackage = this.getExportPackage();
    	if ( strExportPackage != null )
    	{
    		result = result + strExportPackage + "\n";
    	}
    	
    	String strImportPackage = this.getImportPackage();
    	if ( strImportPackage != null )
    	{
    		result = result + strExportPackage + "\n";
    	}
    	
    	String strBundleShape = this.getEclipseBundleShape();
    	if ( strBundleShape != null )
    	{
    		result = result + strBundleShape + "\n";
    	}
    	
    	return result;
    }
    
    /**
     * bundle symbolic name
     * @return symbolic name.
     * @throws MojoExecutionException if symbolic name is null or empty.
     */
    private String getSymbolicName() throws MojoExecutionException 
    {
    	if ( this.symbolicName == null || this.symbolicName.isEmpty() )
    	{
    		throw new MojoExecutionException("invalid symbolic name: null or empty isn't allowed." );
    	}
    	return this.symbolicName;
	}

	/**
     * require bundle content
     * @return required bundle list as a <code>String</code>
     */
    private String getRequireBundle() 
    {
    	String result = this.requirebundle;
    	
    	if ( (result != null) && (! result.isEmpty()) )
    	{
    		result = "Require-Bundle: " + result;
    		
    		result = result.replaceAll( "\\s\\s+", " " );
    		result = result.replaceAll( ",\\s", ",\n ");
    	}
    	
    	return result;
	}
    
    /**
     * exported packages
     * @return manifest's "ExportPackage:" section as a string.
     */
    private String getExportPackage()
    {
    	String result = this.exportpackage;
    	
    	if ( (result != null) && (! result.isEmpty()) )
    	{
    		result = "Export-Package: " + result;
    		
    		result = result.replaceAll( "\\s\\s+", " " );
    		result = result.replaceAll( ",\\s", ",\n ");
    	}
    	
    	return result;
    }
    
    /**
     * generates and returns the MANIFEST.MF's ImportPackage part.
     * @return ImportPackage part as a <code>String</code>
     */
    private String getImportPackage()
    {
    	String result = this.importpackage;
    	
    	if ( (result != null) && (! result.isEmpty()) )
    	{
    		result = "Import-Package: " + result;
    		
    		result = result.replaceAll( "\\s\\s+", " " );
    		result = result.replaceAll( ",\\s", ",\n ");
    	}
    	
    	return result;
    }
   
    
    /**
     * generates and return the "Bundle-ClassPath" part.
     * @return bundle class path as a <code>String</code>.
     * @throws MojoExecutionException if an error occurred.
     */
	private String getBundleClassPath() throws MojoExecutionException 
	{
		if ( this.directories == null )
		{
			throw new MojoExecutionException( "no lib directory has been specified." );
		}
		
		this.getLog().info( "generating Bundle-ClassPath" );
			
		LibDirectory.MojoLog = this.getLog();
		
		String result = null;
		
		if ( this.additionals != null )
		{
			for ( int i = 0 ; i < this.additionals.length ; i++ )
			{
				this.getLog().info( "adding jar: " + this.additionals[i] );
				if ( result == null ) { result = " " + this.additionals[i]; }
				else                  { result = result + ",\n " + this.additionals[i]; }
			}
		}
		
		for ( int i = 0 ; i < directories.length ; i++ )
		{
			this.getLog().info( "retreiving jars from " + this.directories[i] );
			String list = directories[i].getJarList();
			if ( list != null )
			{
				if ( result == null ) { result = list; }
				else                  { result = result + ",\n" + list; }
			}
		}
	
		return result;
	}

	/**
	 * bundle activator
	 * @return bundle activator as a <code>String</code>
	 * @throws MojoExecutionException if activator is null or empty
	 */
	private String getActivator() throws MojoExecutionException 
	{
		if ( this.activator == null || this.activator.isEmpty() ) 
		{ 
			throw new MojoExecutionException( "invalid activator: null or empty string aren't allowed." ); 
		}
		return this.activator;
	}

	/**
	 * bundle version
	 * @return bundle version
	 * @throws MojoExecutionException if an error occurred.
	 */
	private String getBundleVersion() throws MojoExecutionException 
	{
		String result = this.version;
		
		if ( result == null )
		{
			throw new MojoExecutionException( "invalid version: null isn't allowed." );
		}
		
		result = result.replaceAll("-SNAPSHOT", ".qualifier" );
		return result;
	}

	/**
	 * Eclipse-BundleShape part.
	 * @return the line corresponding to Eclipse-Bundle shape to be added to the header,
	 * 	or null if default must be used.
	 * @since version 0.0.3 
	 */
	private String getEclipseBundleShape ()
	{
		String result = null;
		
		if ( this.unpack )
		{
			result = "Eclipse-BundleShape: dir";
		}
		
		return result;
	}
	
	/**
	 * bundle name
	 * @return bundle name
	 */
	private String getName() 
	{
		return this.name;
	}

	/** {@inheritDoc} */
	public void execute() throws MojoExecutionException
    {
    	
    	// target output directory
        File targetDirectory = new File( outputDirectory, "META-INF" );
        if ( ! targetDirectory.exists() )
        {
            targetDirectory.mkdirs();
        }
        this.getLog().debug( "target directory created: " + targetDirectory.getPath() );
        
        // target output file
        File targetFile = new File( targetDirectory, "MANIFEST.MF" );
        FileWriter w = null;
        try
        {
            w = new FileWriter( targetFile );
            w.write( this.getManifestContent() );
        }
        catch ( IOException e )
        {
            throw new MojoExecutionException( "Error creating file " + targetFile, e );
        }
        finally
        {
            if ( w != null )
            {
                try
                {
                    w.close();
                }
                catch ( IOException e )
                {
                    // ignore
                }
            }
        }
        
    }
}
