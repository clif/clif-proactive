/**
 * 
 */
package org.ow2.clif.manifest;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;


/**
 * Retrieves jar files in a given directory.
 * @author Vincent ROSSIGNOL
 * @version 0.0.1
 */
public class LibDirectory 
{
	
	
	
	/**
	 * associated directory.
	 */
	private File directory;
	
	/**
	 * prefix to be added to jar names.
	 */
	private String prefix;
	
	/**
	 * a log provided by Mojo.
	 */
	public static Log MojoLog = null;
	
	/**
	 * constructor.
	 */
	public LibDirectory ()
	{
		super();
	}
	
	/**
	 * retreives all files present in the current directory.
	 * @return a list of file as a <code>String</code>
	 * @throws MojoExecutionException if an error occurred.
	 */
	public String getJarList() throws MojoExecutionException
	{
		String result = "";

		File[] files = this.directory.listFiles();
		
		if ( files == null )
		{
			throw new MojoExecutionException("invalid directory: " + this.directory.getPath() );
		}
		
		for ( int i = 0 ; i < files.length ; i++ )
		{
			if ( files[i].isFile() )
			{
				
				
				if ( ! result.isEmpty() ) 
				{ 
					result = result + ",\n"; 
				}
				
				result = result + " " + this.prefix + files[i].getName();
				MojoLog.info( prefix + files[i].getName() + " added to list." );
			}
		}
		
		return result;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String toString()
	{
		return "[" + this.directory.getAbsolutePath() + "]";
	}
	
}
