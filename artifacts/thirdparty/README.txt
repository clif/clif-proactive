CLIF third party jars

Those Maven projects contains old jars needed in CLIF projects as Maven dependencies.
Those artefacts has never been published by their developpers / owners as Maven artefacts. 
They must be replaced in future version with :
- a stable version published in central repository
- a already published version (ealier).

For instance:

org.htmlparser is a project hosted at sourceforge, and published as a Maven artifact since version 1.6.
CLIF uses version 1.42, which is unpublished yet.

2 solutions :
- modify CLIF and used the published 1.6 version.
- publish org.htmlparser:htmlparser:jar:1.42 into maven central repository.
