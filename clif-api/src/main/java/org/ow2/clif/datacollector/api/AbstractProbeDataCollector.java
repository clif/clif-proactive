/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.datacollector.api;

import java.io.Serializable;
import org.objectweb.proactive.annotation.ImmediateService;
import org.ow2.clif.storage.api.ProbeEvent;

/**
 * Data collector generic implementation computing statistical values for probes generating
 * ProbeEvent-derived events.
 *
 * @author Bruno Dillenseger
 */
abstract public class AbstractProbeDataCollector extends AbstractDataCollector
{
	protected long[] stats;
	protected long samples = 0;


	/**
	 * resets all statistics but the cumulative ones
	 */
	private void resetStats()
	{
		samples = 0;
		for (int i=0 ; i<stats.length ; stats[i++] = 0);
	}


	/**
	 * new test initialization => call super class' init() method and resets all statistics,
	 * including the cumulative ones
	 */
	@ImmediateService
	public void init(Serializable testId, String probeId)
	{
		super.init(testId, probeId);
		resetStats();
	}


	/**
	 * new measure available => call super class' add() method and compute statistics
	 */
	@ImmediateService
	public void add(ProbeEvent measure)
	{
		if (measure != null)
		{
			super.add(measure);
			for (int i=0 ; i<stats.length ; ++i)
			{
				stats[i] += measure.values[i];
			}
			++samples;
		}
	}


	//////////////////////////////////
	// interface DataCollectorAdmin //
	//////////////////////////////////


	/**
	 * Get statistics computed since previous call (or initialization for 1st call)
	 */
	@ImmediateService
	public long[] getStat()
	{
		if (samples > 0)
		{
			long[] mean = new long[stats.length];
			for (int i=0 ; i<stats.length ; ++i)
			{
				mean[i] = stats[i] / samples;
			}
			resetStats();
			return mean;
		}
		else
		{
			return null;
		}
	}
}
