/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004, 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/


package org.ow2.clif.supervisor.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


/**
 * Representation of a blade state and utility methods
 * to determine a global state resulting from individual
 * blades states.
 * @author Bruno Dillenseger
 * @author Joan Chaumont
 */
public class BladeState implements Serializable
{
	static private final long serialVersionUID = -9151463968342375159L;
	static private int instanceCount = 0;
	static private BladeState[] instances = new BladeState[16];
	static public final BladeState UNDEPLOYED = new BladeState("undeployed");
	static public final BladeState DEPLOYING = new BladeState("deploying");
	static public final BladeState DEPLOYED = new BladeState("deployed");
	static public final BladeState INITIALIZING = new BladeState("initializing");
	static public final BladeState INITIALIZED = new BladeState("initialized");
	static public final BladeState STARTING = new BladeState("starting");
	static public final BladeState RUNNING = new BladeState("running");
	static public final BladeState SUSPENDING = new BladeState("suspending");
	static public final BladeState SUSPENDED = new BladeState("suspended");
	static public final BladeState RESUMING = new BladeState("resuming");
	static public final BladeState COMPLETED = new BladeState("completed");
	static public final BladeState STOPPING = new BladeState("stopping");
	static public final BladeState STOPPED = new BladeState("stopped");
	static public final BladeState ABORTED = new BladeState("aborted");
	static public final BladeState INCOHERENT = new BladeState("incoherent");
    static public final BladeState NONE = new BladeState("none");


    /**
     * Gets the blade state instance associated to a state code
     * @param code the code representing a blade state
     * @return the blade state object associated to the provided code.
     * This object is immutable and unique (the same object is always
     * returned by subsequent calls with the same state code). 
     */
	static public BladeState get(int code)
	{
		return instances[code];
	}


	/**
	 * Return the global state resulting from some individual states.
	 * @param states individual states.
	 * @return the state object representing the global state
	 */
	static public BladeState getGlobalState(Collection<BladeState> states)
	{
		BladeState result = null;
		// transitional states
        if (states == null || states.isEmpty() || states.contains(null))
        {
            result = NONE;
        }
		else if (states.contains(DEPLOYING))
		{
			result = DEPLOYING;
		}
		else if (states.contains(INITIALIZING))
		{
			result = INITIALIZING;
		}
		else if (states.contains(STARTING))
		{
			result = STARTING;
		}
		else if (states.contains(SUSPENDING))
		{
			result = SUSPENDING;
		}
		else if (states.contains(RESUMING))
		{
			result = RESUMING;
		}
		else if (states.contains(STOPPING))
		{
			result = STOPPING;
		}
		// stationary states
		else
		{
			Iterator<BladeState> statesIt = states.iterator();
			result = statesIt.next();

			while (statesIt.hasNext() && result != INCOHERENT)
			{
			    BladeState current = statesIt.next();
			    if (! current.equals(result))
			    {
			        if (
			        	! (result.equals(STOPPED) || result.equals(COMPLETED) || result.equals(ABORTED))
			        	|| ! (current.equals(STOPPED) || current.equals(COMPLETED) || current.equals(ABORTED)))
			        {
			            result = INCOHERENT;
			        }
			    }
			}
			// priorities for terminated macro-state: ABORTED > STOPPED > COMPLETED
			if (result.equals(COMPLETED) && states.contains(STOPPED))
			{
				result = STOPPED;
			}
			if (result.equals(STOPPED) && states.contains(ABORTED))
			{
				result = ABORTED;
			}
		}
		return result;
	}


	/**
	 * Determines if the global state resulting from some
	 * individual states is a stationary state or a transitional state.
	 * @param states individual states
	 * @return true if the global state is stationary, false if it is transitional
	 */
	static public boolean isStationaryState(Collection<BladeState> states)
	{
		return
			!(
				states.contains(INITIALIZING)
				|| states.contains(STOPPING)
				|| states.contains(SUSPENDING)
				|| states.contains(DEPLOYING)
				|| states.contains(STARTING)
				|| states.contains(RESUMING));
	}


	/**
	 * Determines if the global state resulting from some individual states
	 * is "running", i.e. if at least one state is "running" and the others
	 * are either running or terminated (stopped, completed or aborted).
	 * @param states individual states
	 * @return true if at least one blade is running and the others are
	 * stopped, completed or aborted, false otherwise
	 */
	static public boolean isRunning(Collection<BladeState> states)
	{
	    if (isStationaryState(states))
	    {
	        for (BladeState oneState : states) 
	        {
                if(
                	! oneState.equals(RUNNING)
                    && !oneState.equals(STOPPED)
                    && !oneState.equals(COMPLETED)
                    && !oneState.equals(ABORTED))
                {
                    return false;
                }
            }
	        return states.contains(RUNNING);
	    }
	    return false;
	}


	///////////////////////////////
	// end of static definitions //
	///////////////////////////////


	/**
	 * integer value uniquely representing a blade state
	 */
	protected int code;

	/**
	 * an explicit state name (running, stopping, stopped...) 
	 */
	protected transient String label;


	/**
	 * Creates a BladeState instance with the provided
	 * state label (unique name), and associates a
	 * unique code to this object. Also adds this new instance
	 * to an internal instances array, using the unique code
	 * as index. 
	 * Not to be called directly, unless if extending the BladeState class
	 * to add other states. Get instances through {@link #get(int)} instead.
	 * @param label an explicit, and preferably unique and short, name describing
	 * this state. 
	 */
	protected BladeState(String label)
	{
		code = instanceCount;
		this.label = label;
		instances[instanceCount++] = this;
	}


	/**
	 * Gets the explicit name of this state (e.g. running, stopped, etc.) 
	 * @return this state's explicit name
	 */
	public String toString()
	{
		return label;
	}


	/**
	 * Gets the unique code representing this blade state. 
	 * @return the state code
	 */
	public int getCode()
	{
		return code;
	}


	/**
	 *  equality is based on the state code value
	 */
	public boolean equals(Object obj)
	{
		return (obj != null) && (code == ((BladeState)obj).code);
	}


	/**
	 * Ensures that deserialization of a BladeState instance
	 * gives a singleton object representing the corresponding
	 * state. The actual state is identified by the state code.
	 * @return the BladeState singleton instance associated to
	 * the deserialized object (according to its state code). 
	 */
	private Object readResolve()
	{
		return instances[code];
	}

/* should be fixed now

	 * FIX ME, used to bypass race condition when using ProActive as Backend. We can have INITIALIZED & DEPLOYED at the same time.
	 * @param states
	 * @return

	public static BladeState getGlobalStateInitializing(Collection<BladeState> states) {
		int nbDep = 0, nbInitialized = 0, nbInitializing = 0;
		for(BladeState bs : states){
			if(bs.equals(BladeState.DEPLOYED)){
				nbDep++;
			}else if(bs.equals(BladeState.INITIALIZED)){
				nbInitialized++;
			}else if(bs.equals(BladeState.INITIALIZING)){
				nbInitializing++;
			}else{
			}
		}
		if(nbInitialized == states.size()){
			return BladeState.INITIALIZED;
		}else if(nbDep != 0 || nbInitialized != 0 || nbInitializing != 0 && (nbDep + nbInitialized + nbInitializing) == states.size()){
			return BladeState.INITIALIZING;
		}else{
			return getGlobalState(states);
		}
	}


	 * FIX ME, used to bypass race condition when using ProActive as Backend. We can have INITIALIZED & DEPLOYED at the same time.
	 * @param states
	 * @return

	public static BladeState getGlobalStateStarting(Collection<BladeState> states) {
		int nbInitialized = 0, nbStarting = 0, nbRunning = 0;
		for(BladeState bs : states){
			if(bs.equals(BladeState.STARTING)){
				nbStarting++;
			}else if(bs.equals(BladeState.INITIALIZED)){
				nbInitialized++;
			}else if(bs.equals(BladeState.RUNNING)){
				nbRunning++;
			}else{
			}
		}
		if(nbRunning == states.size()){
			return BladeState.RUNNING;
		}else if(nbRunning != 0 || nbInitialized != 0 || nbStarting != 0 && (nbRunning + nbInitialized + nbStarting) == states.size()){
			return BladeState.STARTING;
		}else{
			return getGlobalState(states);
		}
	}
*/
}
