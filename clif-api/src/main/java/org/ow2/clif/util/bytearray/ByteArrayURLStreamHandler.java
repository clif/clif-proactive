/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util.bytearray;


import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import org.ow2.clif.util.ClifClassLoader;


/**
 *
 * @author dillense
 */
public class ByteArrayURLStreamHandler extends URLStreamHandler
{
	ClifClassLoader my_cl;


	public ByteArrayURLStreamHandler(ClifClassLoader classloader)
	{
		super();
		my_cl = classloader;
	}


	@Override
	public URLConnection openConnection(URL u)
	{
		return new ByteArrayURLConnection(u, my_cl);
	}


	@Override
	public void parseURL(URL u, String spec, int start, int limit)
	{
		setURL(u, "bytearray", "", -1, null, null, spec.substring(start, limit), null, null);
	}


	@Override
	public String toExternalForm(URL u)
	{
		return "bytearray:" + u.getPath();
	}
}
