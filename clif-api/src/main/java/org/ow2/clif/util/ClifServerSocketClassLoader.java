/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;

import org.ow2.clif.supervisor.api.ClifException;

public class ClifServerSocketClassLoader extends ClifClassLoader {

	/** The client socket */
	Socket sock = null;
	/** Streams to get the definitions */
	DataInputStream dins;
	DataOutputStream douts;

	@Override
	protected synchronized byte[] getClassDef(String name) throws ClifException {
		byte[] result = null;
		try{
			if (sock == null){
				//creating the socketclient
				sock = new Socket(
						ExecutionContext.getCLIFCodeServerHost(),
						ExecutionContext.getCLIFCodeServerPort());
				dins = new DataInputStream(sock.getInputStream());
				douts = new DataOutputStream(sock.getOutputStream());
			}
			douts.writeUTF(name);
			douts.flush();
			int length = dins.readInt();
			if (length >= 0){
				result = new byte[length];
				int offset = 0;
				int n = 1;
				while (length > 0 && n > 0)
				{
					n = dins.read(result, offset, length);
					length -= n;
					offset += n;
				}
			}else{
				if (length == CodeServer.NO_SUCH_RESOURCE){
					throw new FileNotFoundException("Code server could not find " + name);
				}else if (length == CodeServer.RESOURCE_TOO_BIG){
					throw new IOException("Resource or class " + name + " is too big");
				}else{
					throw new IOException("Code server cannot deliver " + name + " for unknown reason");
				}
			}
		}catch(Throwable e){
			throw new ClifException("Cannot find " + name + " definition", e);
		}
		return result;
	}

}
