/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006, 2009-2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.Properties;

/**
 * This class helps gathering configuration parameters to ease portability between
 * several assemblies of CLIF. It also configures log libraries and system properties.
 * 
 * @author Bruno Dillenseger
 * @author Florian Francheteau
 */
public abstract class ExecutionContext
{
	// Properties related to possible multiple retries, "best effort/fault tolerant"
	// policy in case of deployment failure, and deployment time out.
	static public final String DEPLOY_RETRIES_PROP_NAME = "clif.deploy.retries";
	static public final String DEPLOY_RETRIES_PROP_DEFAULT = "0";
	static public final String DEPLOY_RETRYDELAY_PROP_NAME = "clif.deploy.retrydelay";
	static public final String DEPLOY_RETRYDELAY_PROP_DEFAULT = "2000";
	static private final String DEPLOY_TIMEOUT_PROP_NAME = "clif.deploy.timeout";
	static private final String DEPLOY_TIMEOUT_PROP_DEFAULT = "0";

	/**
	 * When this property's value is true/on/yes/1, deployment will be successful even
	 * if some blades could not be deployed (they are discarded from the test)
	 */
	static private final String DEPLOY_BESTEFFORT_PROP_NAME = "clif.deploy.besteffort";
	static private final String DEPLOY_BESTEFFORT_PROP_DEFAULT = "false";

	/**
	 * When this property's value is true/on/yes/1, CLIF assumes
	 * that a global time is maintained, with a protocol such as
	 * Network Time Protocol. In this case, all events/measures
	 * timestamps are relative to the global initialization date,
	 * i.e. the date when initialization is triggered by calling
	 * the Supervisor's init() method. The accuracy of time-correlation
	 * between events/measures coming from distributed blades is NTP's
	 * accuracy.  
	 * Otherwise, timestamps are relative to local initialization
	 * dates. The accuracy of time-correlation between events/measures
	 * coming from distributed blades is the time elapsed between the
	 * call to the Supervisor's init() method and the call on the blade's
	 * init() method.
	 */
	static private final String NTP_PROP_NAME = "clif.globaltime";
	static private final String NTP_PROP_DEFAULT = "false";

	/**
	 * When this property's value is true/on/yes/1, all deployments share
	 * a common code server (hopefully with a relevant common code server path).
	 * Otherwise, each deployment creates its own code server. 
	 */
	static private final String CODESERVER_SHARED_PROP_NAME = "clif.codeserver.shared";
	static private final String CODESERVER_SHARED_PROP_DEFAULT = "false";

	/**
	 * Name of the default CLIF server possibly launched by a user
	 * interface for convenience
	 */
	static public final String DEFAULT_SERVER = "local host";

	/**
	 * Base directory of CLIF runtime environment (may be a relative or absolute filesystem path).
	 * Must end with a filesystem path separator.
	 */
	static private String baseDir = null;
	
	/**
	 * absolute path to the file holding MaxQ properties in current project.
	 */
	static private String maxQPropFile = null;

	/**
	 * relative path to the template file for CLIF system properties
	 */
	static public final String TEMPLATE_PATH = "etc" + File.separator + "clif.props.template";
	
	/**
	 * relative path to the file holding MaxQ properties in console plugin
	 */
	static public final String DEFAULT_MAXQ_PROPERTY_FILE = "etc" + File.separator + "maxq.properties";

	/**
	 * relative path to the file holding CLIF system properties
	 */
	static public final String PROPS_PATH = "etc" + File.separator + "clif.props";

	/**
	 * relative path to the file holding the list of classes to prefetch (including all dependencies)
	 */
	static public final String PREFETCH_FILE = "etc/prefetch.list";

	/**
	 * absolute path to the workspace
	 */
	static public String WORKSPACE_PATH;

	/**
	 * The key to retrieve the filestorage dir java property
	 */
	static public final String CLIF_FILESTORAGE_DIR_KEY = "clif.filestorage.dir";

	/**
	 * The key to retrieve the codeserver path java property
	 */
	static public final String CLIF_CODESERVER_PATH_KEY = "clif.codeserver.path";

	/**
	 * Names of CLIF system properties whose value is a path
	 */
	static public String[] PROPERTIES_WITH_PATH = {
		"java.security.policy","julia.config","clif.codeserver.path",CLIF_FILESTORAGE_DIR_KEY};

	/**
	 * The host of the code server
	 */
	static private String CLIF_CODESERVER_HOST = null;

	/**
	 * The port on which one the code server is running
	 */
	static private int CLIF_CODESERVER_PORT = -1;

	/**
	 * The url of the PA code server
	 */
	static private String CLIF_PACODESERVER = null;

	/**
	 * The number of threads used by the isac engine.
	 * Requirement to run on PACAGrid
	 */
	static private int CLIF_ISAC_THREADS = -1;

	/**
	 * Gather all the properties used in Clif engine
	 */
	private static enum PROPERTIES{
		FRACTAL_REGISTRY_HOST("fractal.registry.host".intern()),
		FRACTAL_REGISTRY_PORT("fractal.registry.port".intern()),
		CLIF_CODESERVER_HOST("clif.codeserver.host".intern()),
		CLIF_CODESERVER_PORT("clif.codeserver.port".intern()),
		CLIF_PACODESERVER("clif.pacodeserver".intern()),
		CLIF_PARAMETERS("clif.parameters".intern()),
		CLIF_ISAC_THREADS("clif.isac.threads".intern());

		public final String key;
		private PROPERTIES(String key){
			this.key = key;
		}
	}

	/**
	 * Initializes the ExecutionContext of the VirtualMachine with the given InitObject
	 * @param initExecutionContext
	 */
	public static void init(InitObject initExecutionContext) {
		File pathFile = new File(initExecutionContext.baseDir);
		if( baseDir == null){
			if(pathFile.exists()){
				baseDir = pathFile.getAbsolutePath();
			}else{
				baseDir = System.getProperty("user.dir");
			}
			if(!baseDir.endsWith(File.separator)){
				baseDir = baseDir + File.separator;
			}
		}
		if(initExecutionContext.WORKSPACE_PATH != null){
			pathFile = new File(initExecutionContext.WORKSPACE_PATH);
			if( WORKSPACE_PATH == null){
				if(pathFile.exists()){
					WORKSPACE_PATH = pathFile.getAbsolutePath();
					if(!WORKSPACE_PATH.endsWith(File.separator)){
						WORKSPACE_PATH = WORKSPACE_PATH + File.separator;
					}
				}
			}
		}
		ExecutionContext.maxQPropFile = initExecutionContext.maxQPropFile;
		ExecutionContext.CLIF_CODESERVER_HOST = initExecutionContext.CLIF_CODESERVER_HOST;
		ExecutionContext.CLIF_CODESERVER_PORT = initExecutionContext.CLIF_CODESERVER_PORT;
		ExecutionContext.CLIF_PACODESERVER = initExecutionContext.PA_CODESERVER;
		ExecutionContext.CLIF_ISAC_THREADS = initExecutionContext.CLIF_ISAC_THREADS;
	}

	/**
	 * Sets the base directory of CLIF runtime environment, and configures log libraries.
	 * @param path the filesystem path to the base directory of CLIF runtime environment.
	 * Must have a trailing filesystem separator.
	 */
	static public void init(String path)
	{
		File pathFile = new File(path);
		if (pathFile.isAbsolute())
		{
			baseDir = path;
		}
		else
		{
			baseDir = pathFile.getAbsolutePath();
		}
		if(!baseDir.endsWith(File.separator)){
			baseDir = baseDir + File.separator;
		}
	}


	/**
	 * Gets the base directory of CLIF runtime environment.
	 * @return the base directory of CLIF runtime environment, or null if this directory
	 * has not been set.
	 */
	static public String getBaseDir()
	{
		return baseDir;
	}


	/**
	 * Sets system properties according to the contents of an input stream.
	 * The stream is read as a properties-formatted content, from which
	 * property clif.parameters is analyzed and transformed into system
	 * properties.
	 * @param ins input stream properties are read from
	 */
	static public void setProperties(InputStream ins)
	{
		try
		{
			Properties props = new Properties();
			props.load(ins);
			String allParam = (String)props.get(PROPERTIES.CLIF_PARAMETERS.key);
			String[] parameters = allParam.split(" ");
			for (int i = 0; i < parameters.length; i++)
			{
				if(parameters[i].startsWith("-D"))
				{
					String[] property = parameters[i].replaceFirst("-D","").split("=");
					System.setProperty(property[0], property[1]);
					for (String propName : PROPERTIES_WITH_PATH)
					{
						if (propName.equals(property[0]))
						{
							if ((!CLIF_FILESTORAGE_DIR_KEY.equals(property[0])) || WORKSPACE_PATH == null)
								System.setProperty(property[0], completePath(property[0]));
							break;
						}
					}
				}
			}
			props.remove(PROPERTIES.CLIF_PARAMETERS.key);
			for (Object key : props.keySet())
			{
				System.setProperty(
					(String)key,
					props.getProperty((String)key));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
	}


	static private String completePath(String property)
	{
		String path = "";
		try
		{
			String[] paths = System.getProperty(property).replace("\"","").split(";");
			for (int i = 0; i < paths.length-1; i++)
			{
				path += foundPath(paths[i]) + ";";
			}
			path += foundPath(paths[paths.length-1]);
			return path;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return path;
	}


	/**
	 * @param path
	 * @return String the correct path
	 * @throws IOException
	 */
	static private String foundPath(String path) throws IOException
	{
		if (new File(path).isAbsolute())
		{
			return path;
		}
		else
		{
			return getBaseDir() + path;
		}
	}
	
	
	/**
	 * Sets absolute location of maxq.properties file in current project
	 * @param dir absolute location of maxq.properties
	 */
	static public void setMaxQPropFile(String dir){
		maxQPropFile=dir;
	}

	/**
	 * Gets location of maxq.properties file in current project
	 * @return String 	the absolute path of maxq.properties
	 */
	static public String getMaxQPropFile(){
		return maxQPropFile;
	}

	/**
	 * @return true if CLIF can assume that a global time is
	 * maintained by some protocol such as Network Time Protocol,
	 * false otherwise.
	 */
	static public boolean useGlobalTime()
	{
		return StringHelper.isEnabled(
			System.getProperty(NTP_PROP_NAME, NTP_PROP_DEFAULT));
	}

	/**
	 * @return true if a test plan can be considered as successfully deployed
	 * event if some blades could not be deployed, false otherwise (i.e. all
	 * blades must be successfully deployed).
	 */
	static public boolean deploymentIsBestEffort()
	{
		return StringHelper.isEnabled(System.getProperty(
			DEPLOY_BESTEFFORT_PROP_NAME,
			DEPLOY_BESTEFFORT_PROP_DEFAULT));
	}

	/**
	 * @return the deployment time out setting, in ms
	 */
	static public long getDeploymentTimeOut()
	{
		return Long.parseLong(
			System.getProperty(
				DEPLOY_TIMEOUT_PROP_NAME,
				DEPLOY_TIMEOUT_PROP_DEFAULT));
	}

	/**
	 * To get a clone of the execution context (for remote instantiation of clif servers)
	 * @return the init object containing the execution context settings
	 */
	static public InitObject getExecutionContext(){
		return new InitObject();
	}

	/**
	 * To get the port of the code server to use for this deployment
	 * @return the port of the code server to use for this deployment
	 */
	static synchronized public int getCLIFCodeServerPort(){
		if(ExecutionContext.CLIF_CODESERVER_PORT <= 0){
			try{
				ExecutionContext.CLIF_CODESERVER_PORT = Integer.parseInt(System.getProperty(PROPERTIES.CLIF_CODESERVER_PORT.key, "1357"));
			}catch(Exception e){
				ExecutionContext.CLIF_CODESERVER_PORT = 1357;
			}
		}
		return ExecutionContext.CLIF_CODESERVER_PORT;
	}

	/**
	 * To get the host of the code server to use for this deployment
	 * @return the host of the code server to use for this deployment
	 */
	static synchronized public String getCLIFCodeServerHost(){
		if(ExecutionContext.CLIF_CODESERVER_HOST == null){
			ExecutionContext.CLIF_CODESERVER_HOST = System.getProperty(PROPERTIES.CLIF_CODESERVER_HOST.key, "localhost");
		}
		return ExecutionContext.CLIF_CODESERVER_HOST;
	}

	/**
	 * To get the registry to use for this deployment
	 * @return the registry to use for this deployment
	 */
	public static synchronized URI getCLIFRegistry()
	throws Exception
	{
		String host = System.getProperty(PROPERTIES.FRACTAL_REGISTRY_HOST.key, "localhost");
		String port = System.getProperty(PROPERTIES.FRACTAL_REGISTRY_PORT.key, "1234");
		String protocol = System.getProperty("proactive.communication.protocol", "pnp");
		return new URI(protocol + "://" + host + ":" + port + "/ClifRegistry");
	}

	/**
	 * To get the PA code server's url to use for this deployment
	 * @return the PA code server's url to use for this deployment
	 */
	public static synchronized String getCLIFPACodeServer(){
		if(ExecutionContext.CLIF_PACODESERVER == null){
			ExecutionContext.CLIF_PACODESERVER = System.getProperty(PROPERTIES.CLIF_PACODESERVER.key);
		}
		return ExecutionContext.CLIF_PACODESERVER;
	}

	public static synchronized int getCLIFIsacThreads(){
		if(ExecutionContext.CLIF_ISAC_THREADS < 0){
			try{
				ExecutionContext.CLIF_ISAC_THREADS = Integer.parseInt(System.getProperty(PROPERTIES.CLIF_ISAC_THREADS.key));
			}catch(Throwable t){
				ExecutionContext.CLIF_ISAC_THREADS = 100;
			}
		}
		return ExecutionContext.CLIF_ISAC_THREADS;
	}

	public static class InitObject implements Serializable{
		private static final long serialVersionUID = -3486736109780587308L;
		public final String maxQPropFile;
		public final String baseDir;
		public final String WORKSPACE_PATH;
		public final String PA_CODESERVER;
		public final String CLIF_CODESERVER_HOST;
		public int CLIF_CODESERVER_PORT;
		public int CLIF_ISAC_THREADS = 100;
		public final String CLIF_REGISTRY;
		private InitObject(){
			this.maxQPropFile = ExecutionContext.maxQPropFile;
			this.baseDir = ExecutionContext.baseDir;
			this.WORKSPACE_PATH = ExecutionContext.WORKSPACE_PATH;
			this.PA_CODESERVER = System.getProperty(ExecutionContext.PROPERTIES.CLIF_PACODESERVER.key);
			this.CLIF_CODESERVER_HOST = System.getProperty(ExecutionContext.PROPERTIES.CLIF_CODESERVER_HOST.key);
			try{
				this.CLIF_CODESERVER_PORT = Integer.parseInt(System.getProperty(ExecutionContext.PROPERTIES.CLIF_CODESERVER_PORT.key));
			}catch(Throwable e){
				this.CLIF_CODESERVER_PORT = 1357;
			}
			try{
				this.CLIF_ISAC_THREADS = Integer.parseInt(System.getProperty(ExecutionContext.PROPERTIES.CLIF_ISAC_THREADS.key));
			}catch(Throwable e){
				// miam miam, number of threads remains to its default value
			}
			this.CLIF_REGISTRY = System.getProperty(ExecutionContext.PROPERTIES.FRACTAL_REGISTRY_HOST.key);
		}
		public String toString(){
			String lf = System.getProperty("line.separator");
			return "MAXQPropFile : " + maxQPropFile + lf +
			"BaseDir : " + baseDir + lf +
			"Wokspace path : " + WORKSPACE_PATH + lf +
			"PA Codeserver : " + PA_CODESERVER + lf +
			"CLIF Codeserver host : " + CLIF_CODESERVER_HOST + lf +
			"CLIF Codeserver port : " + CLIF_CODESERVER_PORT + lf +
			"CLIF Registry : " + CLIF_REGISTRY + lf;
		}
	}


	/**
	 * @return true if a unique, shared code server must be used by all
	 * deployments, false otherwise (one code server per deployment).
	 */
	public static boolean codeServerIsShared()
	{
		return StringHelper.isEnabled(
			System.getProperty(CODESERVER_SHARED_PROP_NAME, CODESERVER_SHARED_PROP_DEFAULT));
	}
}
