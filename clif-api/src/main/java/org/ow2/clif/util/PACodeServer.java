/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.objectweb.proactive.core.ProActiveException;
import org.objectweb.proactive.core.remoteobject.RemoteObjectAdapter;
import org.objectweb.proactive.core.remoteobject.RemoteObjectExposer;
import org.objectweb.proactive.core.remoteobject.RemoteObjectHelper;
import org.objectweb.proactive.core.remoteobject.RemoteRemoteObject;
import org.ow2.clif.util.CodeServer.NoSuchResourceException;
import org.ow2.clif.util.CodeServer.ResourceTooBigException;

public class PACodeServer {

	/** The singleton instance */
	private static RemoteObjectExposer<PACodeServer> remoteObjectExposer = null;
	private static PACodeServer instance = null;

	private final CodeServer delegate;

	/** The name of the remote object */
	public static final String CODESERVER_NAME = "CodeServer";

	public static synchronized void launch(String extdir, String path) throws ProActiveException {
		if(instance != null && remoteObjectExposer != null){
			remoteObjectExposer.unregisterAll();
		}
		PACodeServer tmp = new PACodeServer(extdir, path);
		remoteObjectExposer = new RemoteObjectExposer<PACodeServer>(
				PACodeServer.class.getName(), tmp);
		URI uri = RemoteObjectHelper.generateUrl(PACodeServer.CODESERVER_NAME);
		RemoteRemoteObject rro = remoteObjectExposer.createRemoteObject(uri);
		instance = (PACodeServer) new RemoteObjectAdapter(rro).getObjectProxy();
		System.out.println("ProActive based codeserver created on " + uri);
	}

	/**
	 * Public ProActive constructor, mustn't be used by the API!
	 */
	public PACodeServer(){
		super();
		delegate = null;
	}

	protected PACodeServer(String extdir, String path){
		super();
		this.delegate = new CodeServer();
		this.delegate.init(extdir, path);
	}

	/**
	 * Returns the class definition associated with the given name
	 * @param name resource name
	 * @return the resource content
	 * @throws NoSuchResourceException could not find the resource
	 * @throws ResourceTooBigException the resource is too big
	 * @throws IOException an exception occurred while trying to get the resource 
	 */
	public byte[] findClassDefinition(String name) throws NoSuchResourceException, ResourceTooBigException, IOException{
		Object[] tmp = this.delegate.findClassDefinition(name);
		InputStream is = null;
		ByteArrayOutputStream bos = null;
		try{
			is = (InputStream) tmp[1];
			byte[] buffer = new byte[1024];
			bos = new ByteArrayOutputStream();
			int read = -1;
			while((read = is.read(buffer, 0, buffer.length)) != -1){
				bos.write(buffer, 0, read);
			}
			return bos.toByteArray();
		}finally{
			if(is != null){
				is.close();
			}
			if(bos != null){
				bos.close();
			}
		}
	}

	/**
	 * To find a resource not available locally by the clif PA Class Loader
	 * @param resource the resource name
	 * @return the resource content
	 */
	public byte[] getResource(String resource){
		 InputStream is = null;
		 ByteArrayOutputStream baos = null;
		 try {
			Object[] results = this.delegate.findClassDefinition(resource);
			baos = new ByteArrayOutputStream();
			is = (InputStream) results[1];
			byte[] buffer = new byte[1024];
			int read = -1;
			while((read = is.read(buffer, 0, buffer.length)) > 0){
				baos.write(buffer, 0, read);
			}
			return baos.toByteArray();
		} catch (Exception e) {
			return null;
		}finally{
			if(baos != null){
				try {
					baos.close();
				} catch (IOException e) {//MIAM MIAM
				}
			}
			if(is != null){
				try {
					is.close();
				} catch (IOException e) {
					//MIAM MIAM
				}
			}
		}
	 }
}
