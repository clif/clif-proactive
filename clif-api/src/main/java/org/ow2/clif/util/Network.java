/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004-2005, 2010, 2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This utility class provides a method to get all IPv4 addresses of local network
 * interfaces, either randomly or in a round-robin way.
 * If at least an IPv4 address is configured, then loopback/localhost addresses are
 * ignored. Otherwise, if no network interface holds any configured IPv4 address,
 * a default localhost address will be selected. IPv6 addresses are ignored anyway.
 * The main() method provides a helper that smartly chooses and prints a local address
 * from which a given remote address can be reached.
 * @author Bruno Dillenseger
 */
abstract public class Network
{
	static protected List<InterfaceAddress> allAddresses = null;

	static {
		try
		{
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			allAddresses = new ArrayList<InterfaceAddress>();
			InterfaceAddress loopbackItfAddr = null;

			while (interfaces.hasMoreElements())
			{
				for (InterfaceAddress itfAddr : interfaces.nextElement().getInterfaceAddresses())
				{						
					if (itfAddr.getAddress() instanceof Inet4Address)
					{
						if (itfAddr.getAddress().isLoopbackAddress())
						{
							loopbackItfAddr = itfAddr;
						}
						else
						{
							allAddresses.add(itfAddr);
						}
					}
				}
			}
			if (allAddresses.size() == 0)
			{
				allAddresses.add(loopbackItfAddr);
			}
		}
		catch (Exception ex)
		{
			throw new Error("Could not get a network address on this host", ex);
		}
	}


	/**
	 * @param subnet if not null, this string must specify an IPv4 subnetwork address
	 * in the form b1.b2.b3.b4/maskbits (e.g. 192.168.1.0/24)
	 * @return an arbitrary local IPv4 address belonging to the specified subnet
	 * if not null. Successive calls always return the same address.
	 */
	static public InetAddress getInetAddress(String subnet)
	{
		InetAddress result = null;
		if (allAddresses != null)
		{
			if (subnet == null)
			{
				result = allAddresses.get(0).getAddress();
			}
			else
			{
				for (int i=0 ; i<allAddresses.size() && result == null ; ++i)
				{
					if (belongsToSubnet((Inet4Address)allAddresses.get(i).getAddress(), subnet))
					{
						result = allAddresses.get(i).getAddress();
					}
				}
			}
		}
		return result;
	}


	/**
	 * Get all locally available IPv4 addresses but the loopback address.
	 * Nevertheless, when the loopback address is the only one configured,
	 * it is returned. 
	 * @return either all locally available IPv4 addresses different from
	 * the loopback address, or just the loopback address if it is the only
	 * one configured.
	 */
	static public InetAddress[] getInetAddresses()
	{
		InetAddress[] result = new InetAddress[allAddresses.size()];
		int i = 0;
		for (InterfaceAddress itfAddr : allAddresses)
		{
			result[i++] = itfAddr.getAddress();
		}
		return result;
	}


	/**
	 * Checks if a given IPv4 address belongs to a given subnet
	 * @param address the IPv4 address
	 * @param subnet the subnet address, specified as b1.b2.b3.b4/maskbits
	 * (e.g. 192.168.1.0/24)
	 * @return true if the address belongs to the subnet, false otherwise
	 */
	static public boolean belongsToSubnet(Inet4Address address, String subnet)
	{
		byte[] bytes = address.getAddress();
		long addressHash = 255 & bytes[0];
		int subnetBits = 32 - Integer.parseInt(subnet.substring(subnet.indexOf('/') + 1));
		StringTokenizer parser = new StringTokenizer(subnet, "./");
		long subnetHash = Integer.parseInt(parser.nextToken());
		for (int i=1 ; i<4 ; ++i)
		{
			addressHash = (addressHash << 8) + (255 & bytes[i]);
			subnetHash = (subnetHash << 8) + Integer.parseInt(parser.nextToken());
		}
		addressHash >>= subnetBits;
		subnetHash >>= subnetBits;
		return addressHash == subnetHash;
	}


	/**
	 * Checks that the given IP address is bound to a local network interface
	 * @param address the classical String representation (xxx.xxx.xxx.xxx) of
	 * the IP address to check
	 * @return true if the address is bound to a local network interface, false
	 * otherwise
	 */
	static public boolean isLocalAddress(String address)
	{
		try
		{
			return isLocalAddress(InetAddress.getByName(address));
		}
		catch (UnknownHostException e)
		{
			return false;
		}
	}


	/**
	 * Checks that the given IP address is bound to a local network interface
	 * @param address the IP address to check
	 * @return true if the address is bound to a local network interface, false
	 * otherwise
	 */
	static public boolean isLocalAddress(InetAddress address)
	{
		for (InterfaceAddress local : allAddresses)
		{
			if (local.getAddress().equals(address))
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * Prints to standard output the local IP address from
	 * which the provided host IP address or name is reachable.
	 * Keeps mute if no local address routes to the target host.
	 * Reports possible error messages to the standard error output.
	 * Exits with status code 0 if a correct local address was found,
	 * 1 if the target host is unreachable from any local address,
	 * or a greater value if an error occurred. The output is printed
	 * as: "network.address=ddd.ddd.ddd.ddd/nn"
	 * @param args 2 arguments expected:
	 * args[0] the target host IP address or name,
	 * args[1] the timeout in seconds for reachability assessment
	 * of each local address.
	 */
	static public void main(String[] args)
	{
		if (args.length != 2)
		{
			System.err.println("2 arguments expected: target_host_name_or_IP timeout_ms");
			System.exit(2);
		}
		try
		{
			InetAddress targetHost = InetAddress.getByName(args[0]);
			if (targetHost.isLoopbackAddress())
			{
				System.out.println("network.address=" + targetHost.getHostAddress());
				System.exit(0);
			}
			else
			{
				// if the target address is one of local interfaces' addresses, choose this one
				for (InterfaceAddress addr : allAddresses)
				{
					if (addr.getAddress().equals(targetHost))
					{
						System.out.println("network.address=" + addr.getAddress().getHostAddress() + "/" + addr.getNetworkPrefixLength());
						System.exit(0);
					}
				}
				int timeout = new Integer(args[1]) * 1000;
				// preference for an address belonging to the same subnet as the target address
				for (InterfaceAddress addr : allAddresses)
				{
					if (
						belongsToSubnet((Inet4Address)targetHost, addr.getAddress().getHostAddress() + "/" + addr.getNetworkPrefixLength())
						&& targetHost.isReachable(NetworkInterface.getByInetAddress(addr.getAddress()), 0, timeout))
					{
						System.out.println("network.address=" + addr.getAddress().getHostAddress() + "/" + addr.getNetworkPrefixLength());
						System.exit(0);
					}
				}
				for (InterfaceAddress addr : allAddresses)
				{
					if (targetHost.isReachable(NetworkInterface.getByInetAddress(addr.getAddress()), 0, timeout))
					{
						System.out.println("network.address=" + addr.getAddress().getHostAddress() + "/" + addr.getNetworkPrefixLength());
						System.exit(0);
					}
				}
			}
		}
		catch (UnknownHostException ex)
		{
			System.err.println("Unknown host " + args[0]);
			System.exit(3);
		}
		catch (NumberFormatException ex)
		{
			System.err.println("Bad timeout value " + args[1]);
			System.exit(4);
		}
		catch (IOException ex)
		{
			System.err.println("Network error: " + ex);
			System.exit(5);
		}
		System.exit(1);
	}
}
