/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2010 France Telecom R&D
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;


/**
 * This class implements a Java resource or class server. The typical client of this code server is
 * the ClifClassLoader.
 * The requested resource or class is first looked for in jar files in CLIF's extension libraries
 * directory.
 * Then, current directory and directories set by property clif.codeserver.path are visited, in
 * this order. The property, when set, should contain a ';' separated list of directory paths.
 * <p>
 * In detail, the codeserver listens on a given port, and upon any connection request:
 * <ul>
 * <li>reads from the socket an UTF String representing the class or resource name
 * <li>writes a positive int stating the resource or class file length or a negative int providing
 * an error code (resource/class not found or resource/class too big).
 * <li>if the class/resource file has been found and can be transfered, all of its bytes are written
 * in the socket.
 * <li>the socket is not closed by the code server, except when an exception occurs while handling
 * the socket.
 * </ul>
 * As of current implementation, class/resource files bigger than 2GB can't be transfered.
 * @see org.ow2.clif.util.ClifClassLoader
 * @author Bruno Dillenseger
 */
public class CodeServer implements Runnable
{
	static public final int NO_SUCH_RESOURCE = -1;
	static public final int RESOURCE_TOO_BIG = -2;
	static public final String DEFAULT_PATH = "";
	private Map<String,JarFile> libExtMap = new HashMap<String,JarFile>();
	private File[] paths;
	private ServerSocket server = null;
	static CodeServer singleton = null;
	static volatile boolean renew = false;


	public synchronized static void launch(InetAddress localAddr, int port, String extdir, String path)
	throws IOException
	{
		if (singleton != null)
		{
			try
			{
				synchronized (singleton) {
					renew = true;
					singleton.close();
					singleton.wait();
					renew = false;
				}
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		singleton = new CodeServer(null, port);
		singleton.init(extdir, path);
		System.out.println("Socket based codeserver created on: " + singleton.server.getLocalSocketAddress());
	}

	/**
	 * Closes the socket server
	 * @throws IOException
	 */
	 private void close() throws IOException {
		if(this.server != null){
			server.close();
		}
	}

	/**
	 * Initializes the state of the code server
	 */
	 void init(String extdir, String path){
		 // index content of jar files in Clif's lib/ext directory
		 File dir = new File(extdir);
		 if (dir.canRead())
		 {
			 String[] list = dir.list();
			 for (int i=0 ; i<list.length ; ++i)
			 {
				 if (list[i].endsWith(".jar"))
				 {
					 try
					 {
						 JarFile jar = new JarFile(new File(extdir, list[i]));
						 Enumeration<JarEntry> entries = jar.entries();
						 while (entries.hasMoreElements())
						 {
							 JarEntry nextElement = entries.nextElement();
							 libExtMap.put(nextElement.getName(), jar);
						 }
					 }
					 catch(Exception ex)
					 {
						 System.err.println("Warning - problem while reading jar " + extdir + list[i]);
						 ex.printStackTrace(System.err);
					 }
				 }
			 }
		 }
		 // parses the path property
		 StringTokenizer parser = new StringTokenizer(path, File.pathSeparator);
		 paths = new File[parser.countTokens()];
		 for (int i=0 ; i<paths.length ; ++i)
		 {
			 paths[i] = new File(parser.nextToken());
		 }
	 }

	 /**
	  * Creates a new code server that could be use as utilitary.
	  * Doesn't use any serversocket object
	  */
	 CodeServer(){}

	 /**
	  * Creates a new code server wich will serve requests using a serversocket object.
	  * @param localAddr the IP address available on local host the code server must bind to.
	  * If null, the code server will listen on all available local addresses.
	  * @param port the port number the code server must listen to, between 0 and 65535 inclusive.
	  * If zero, an arbitrary free port will be used.
	  * @throws IOException if the server socket this code server relies on
	  * could not be launched.
	  */
	 private CodeServer(InetAddress localAddr, int port)
	 throws IOException
	 {
		 this.server = new ServerSocket(port, 0, localAddr);
		 this.server.setReuseAddress(true);
		 new Thread(this, "code server on " + this.server.getLocalSocketAddress()).start();
	 }
	 @Override
	 public void run()
	 {
		 boolean go_on = true;

		 while (go_on)
		 {
			 try
			 {
				 new ClassServerRequest(this.server.accept());
			 }
			 catch (IOException ex)
			 {
				 synchronized(singleton)
				 {
					 if (renew)
					 {
						 go_on = false;
						 singleton.notify();
					 }
					 else
					 {
						 ex.printStackTrace(System.err);
					 }
				 }
			 }
		 }
	 }

	 /**
	  * @return the class definition of the given class
	  * the resulting array contains at index 0 the lenght of the definition and at index 1 the definition itself in an input stream
	  * @throws IOException
	  */
	 Object[] findClassDefinition(String filename) throws NoSuchResourceException, ResourceTooBigException, IOException{
		 // try to get content from local file system
		 File inputFile = new File(filename);
		 // try to get content from lib/ext/*.jar
		 JarFile jar = libExtMap.get(filename);
		 // for possible jar content, ignore heading / if any
		 while (jar == null && filename.startsWith("/"))
		 {
			 filename = filename.substring(1);
			 jar = libExtMap.get(filename);
		 }
		 if (jar == null)
		 {
			 for (int i=0 ; i<paths.length && !inputFile.canRead() ; ++i)
			 {
				 inputFile = new File(paths[i], filename);
			 }
		 }
		 if (jar == null && !inputFile.canRead())
		 {
			 throw new NoSuchResourceException("No Such Class: " + filename);
		 }
		 else
		 {
			 long file_length = 0;
			 ZipEntry entry = null;
			 if (jar == null)
			 {
				 file_length = inputFile.length();
			 }
			 else
			 {
				 entry = jar.getEntry(filename);
				 file_length = entry.getSize();
			 }
			 InputStream is;
			 if (jar != null)
			 {
				 is = jar.getInputStream(entry);
			 }
			 else
			 {
				 is = new FileInputStream(inputFile);
			 }
			 if (file_length > Integer.MAX_VALUE)
			 {
				 throw new ResourceTooBigException("Resource " + filename + " too big");
			 }
			 else
			 {
				 Object[] result = new Object[2];
				 result[0] = new Integer((int)file_length);
				 result[1] = is;
				 return result;
			 }
		 }
	 }

	 class ClassServerRequest extends Thread
	 {
		 Socket sock;


		 public ClassServerRequest(Socket sock)
		 {
			 super("code server request handler " + sock);
			 this.sock = sock;
			 try
			 {
				 sock.setReuseAddress(true);
				 sock.setSoLinger(false, -1);
			 }
			 catch (SocketException ex)
			 {
				 ex.printStackTrace(System.err);
			 }
			 start();
		 }

		 @Override
		 public void run()
		 {
			 DataOutputStream douts;
			 DataInputStream dins;
			 try
			 {
				 douts = new DataOutputStream(sock.getOutputStream());
				 dins = new DataInputStream(sock.getInputStream());
			 }
			 catch (IOException ex)
			 {
				 throw new Error("can't properly handle incoming connection to codeserver", ex);
			 }
			 while (sock != null)
			 {
				 try
				 {
					 String filename = dins.readUTF();
					 try{
						 Object[] klass = CodeServer.this.findClassDefinition(filename);
						 int file_length = new Integer((Integer)klass[0]);
						 InputStream is = (InputStream)klass[1];
						 douts.writeInt(file_length);
						 byte[] buffer = new byte[file_length];
						 while (file_length > 0)
						 {
							 int n = is.read(buffer, 0, file_length);
							 douts.write(buffer, 0, n);
							 file_length -= n;
						 }
						 douts.flush();
					 }catch (NoSuchResourceException e) {
						 //Resource not found, notifying the client
						 douts.writeInt(NO_SUCH_RESOURCE);
						 douts.flush();
					 }catch (ResourceTooBigException e) {
						 //Resource too big
						 douts.writeInt(RESOURCE_TOO_BIG);
						 douts.flush();
					 }
				 }
				 catch (EOFException ex)
				 {
					 // normal situation when renewing the code server singleton
					 try
					 {
						 sock.close();
					 }
					 catch (IOException exc)
					 {
						 System.err.println("Exception while closing code server on " + sock);
						 ex.printStackTrace(System.err);
					 }
					 sock = null;
				 }
				 catch (IOException ex)
				 {
					 try
					 {
						 sock.close();
					 }
					 catch (IOException exc)
					 {
						 System.err.println("Exception while closing code server on " + sock);
						 ex.printStackTrace(System.err);
					 }
					 System.err.println("Code server on " + sock + " closed for unexpected reason");
					 ex.printStackTrace(System.err);
					 sock = null;
				 }
			 }
		 }
	 }

	 public static class NoSuchResourceException extends Exception {
		private static final long serialVersionUID = 6081485040329305614L;
		public NoSuchResourceException(String mess) {
			 super(mess);
		 }
		 public NoSuchResourceException(String mess,Throwable cause){
			 super(mess, cause);
		 }
	 }

	 public static class ResourceTooBigException extends Exception {
		private static final long serialVersionUID = -6696421513690209579L;
		public ResourceTooBigException(String mess) {
			 super(mess);
		 }
		 public ResourceTooBigException(String mess,Throwable cause){
			 super(mess, cause);
		 }
	 }
}
