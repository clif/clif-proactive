/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.objectweb.proactive.api.PARemoteObject;
import org.objectweb.proactive.core.ProActiveException;
import org.ow2.clif.supervisor.api.ClifException;

public class ClifPAClassLoader extends ClifClassLoader {

	protected final PACodeServer codeServer;

	public ClifPAClassLoader(String host) throws URISyntaxException, ProActiveException {
		super();
		if(!host.endsWith("/")){
			host = host + "/";
		}
		URI uri = new URI(host + PACodeServer.CODESERVER_NAME);
		try{
			codeServer = (PACodeServer) PARemoteObject.lookup(uri);
		}catch(org.objectweb.proactive.core.remoteobject.exception.UnknownProtocolException e){
			throw new ProActiveException("Unknown protocol for ProActive classloader", e);
		}
	}

	@Override
	protected byte[] getClassDef(String name) throws ClifException{
		try {
			return codeServer.findClassDefinition(name);
		} catch (Exception e) {
			throw new ClifException("Cannot find " + name + " definition", e);
		}
	}

	@Override
	public InputStream getResourceAsStream(String name){
		InputStream result = super.getResourceAsStream(name);
		if(result == null){
			byte[] bytes = codeServer.getResource(name);
			if(bytes != null){
				return new ByteArrayInputStream(bytes);
			}else{
				return null;
			}
		}else{
			return result;
		}
	}
}
