/*
* CLIF is a Load Injection Framework
* Copyright (C) 2013 France Telecom R&D
* Copyright (C) 2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * File utilities
 * @author Bruno Dillenseger
 */
public abstract class FileUtil
{
	/**
	 * Recursively deletes a directory or just a single file.
	 * Fails as soon as one file can't be deleted.
	 * @param root the file or directory to delete
	 * @return true if the file or directory have been deleted,
	 * false if at least one file could not be deleted.
	 */
	static public boolean delete(File root)
	{
		if (root.canWrite())
		{
			if (root.isDirectory())
			{
				for (File file : root.listFiles())
				{
					if (! delete(file))
					{
						return false;
					}
				}
			}
			return root.delete(); 
		}
		else
		{
			return false;
		}
	}

	/**
	 * Creates/overwrites a file copying content from a resource. 
	 * @param resource the resource file path (to be resolved using
	 * this class' class loader)
	 * @param target the file path to create (all directories in the
	 * path are created when necessary and possible)
	 * @throws IOException an exception occurred either while trying
	 * to get the resource or creating the target file
	 */
	static public void copyFromResource(String resource, File target)
	throws IOException
	{
		InputStream is = FileUtil.class.getClassLoader().getResourceAsStream(resource);
		if (is == null)
		{
			throw new FileNotFoundException(resource);
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		if (target.getParentFile() != null)
		{
			target.getParentFile().mkdirs();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(target));
		int character = reader.read();
		while (character > -1)
		{
			writer.write(character);
			character = reader.read();
		}
		writer.close();
		reader.close();
	}
}
