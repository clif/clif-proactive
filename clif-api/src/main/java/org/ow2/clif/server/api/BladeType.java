/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.server.api;

import java.util.Map;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * Used instead of ADL for Blade (component) instantiation.
 */
public interface BladeType {
	/**
	 * Creates the blade component
	 * @param bladeId blade identifier
	 * @param adlParams declaration of classes implementing some
	 * sub-components of the blade component (blade specialization
	 * into a load injector or to some given probe) 
	 * @return the created blade component
	 * @throws InstantiationException could not instantiate the blade
	 * @throws NoSuchInterfaceException component type conformity issue
	 * @throws IllegalContentException refer to GCM specification (ETSI TS 102 830)
	 * @throws IllegalLifeCycleException component life cycle issue
	 * @throws IllegalBindingException component binding issue
	 */
	public Component createBlade(String bladeId, Map<String, Object> adlParams) throws InstantiationException, NoSuchInterfaceException, IllegalContentException, IllegalLifeCycleException, IllegalBindingException;
}
