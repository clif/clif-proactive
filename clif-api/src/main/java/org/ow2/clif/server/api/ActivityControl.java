/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.api;


import java.io.Serializable;

import org.objectweb.proactive.annotation.ImmediateService;
import org.objectweb.proactive.core.util.wrapper.BooleanWrapper;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * Generic control of an activity
 *
 * @author Bruno Dillenseger
*/
public abstract interface ActivityControl
{
	/**
	 * Initialize the activity
	 * @param testId an arbitrary initialization name
	 * @throws ClifException 
	 */
	@ImmediateService
	public BooleanWrapper init(Serializable testId) throws ClifException;

	/**
	 * Initial start of the activity
	 */
	@ImmediateService
	public void start();

	/**
	 * Final stop of the activity
	 */
	@ImmediateService
	public void stop();

	/**
	 * Suspend the activity
	 */
	@ImmediateService
	public void suspend();

	/**
	 * Resume the activity (if suspended)
	 */
	@ImmediateService
	public void resume();

	/**
	 * Waits until the end of the activity
	 */
	@ImmediateService
	public int join();
}
