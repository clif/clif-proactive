/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.api;

import org.ow2.clif.storage.api.AlarmEvent;

/**
 * Interface used by the Blade Insert component to give information about its state to its
 * Blade Insert Adapter component.
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */

public interface BladeInsertResponse
{
	public final String BLADE_INSERT_RESPONSE = "Blade insert response";

	/**
	 * Informs that the scenario has successfully completed
	 */
	public void completed();

	/**
	 * Informs that the scenario stopped before completion
	 */
	public void aborted();
	
	/**
	 * Triggers an alarm event
	 */
	public void alarm(AlarmEvent alarm);
}
