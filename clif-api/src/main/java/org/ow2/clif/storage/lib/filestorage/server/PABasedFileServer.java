/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.filestorage.server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.objectweb.proactive.core.ProActiveException;
import org.objectweb.proactive.core.remoteobject.RemoteObjectExposer;
import org.objectweb.proactive.core.remoteobject.RemoteObjectHelper;
import org.objectweb.proactive.core.util.ProActiveCounter;
import org.ow2.clif.storage.lib.filestorage.FileStorageCollect;
import org.ow2.clif.storage.lib.filestorage.client.FileServerClientFactory;
import org.ow2.clif.storage.lib.filestorage.client.PABasedFileServerClientFactory;

/**
 * File Server over ProActive Dataspaces implementation
 */
public class PABasedFileServer implements FileServer {
	private static final long serialVersionUID = 2051025669168626833L;
	private static final String DEPLOYER_NAME = "PAFileServer";
	/** The buffer used to process downloads */
	private final String deployerName = PABasedFileServer.DEPLOYER_NAME + "-" + ProActiveCounter.getUniqID();
	private final URI uri;

	/** Constructor
	 * @throws IOException
	 * @throws ProActiveException */
	public PABasedFileServer(FileStorageCollect fsc) throws IOException, ProActiveException{
		RequestHandler tmp = new RequestHandler(fsc);
		RemoteObjectExposer<RequestHandler> remoteObjectExposer = new RemoteObjectExposer<RequestHandler>(
				RequestHandler.class.getName(), tmp);
		this.uri = RemoteObjectHelper.generateUrl(deployerName);
		remoteObjectExposer.createRemoteObject(uri);
	}

	/** Gets the PABaseFileServerClientFactory */
	public FileServerClientFactory getClientFactory() {
		return new PABasedFileServerClientFactory(uri);
	}

	/** Nested class to handle client requests */
	public static class RequestHandler{
		private final FileStorageCollect fsc;
		private byte[] buffer = new byte[FileStorageCollect.BLOCK_SIZE];

		/** Empty PA Constructor */
		public RequestHandler(){
			this.fsc = null;
		}

		/** FSC Constructor */
		public RequestHandler(FileStorageCollect fsc){
			this.fsc = fsc;
		}

		/** Processes a client request */
		public byte[] process(){
			ByteArrayOutputStream out = null;
			InputStream in;
			in = null;
			out = null;
			try {
				out = new ByteArrayOutputStream();
				File file = fsc.getFile();
				in = new FileInputStream(file);
				int n = in.read(buffer);
				while (n != -1) {
					out.write(buffer, 0, n);
					n = in.read(buffer);
				}
			}catch (IOException ex){
				ex.printStackTrace(System.err);
			}finally {
				try	{
					if (out != null)
					{
						if (in != null)
						{
							in.close();
						}
						out.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace(System.err);
				}
			}
			if(out != null){
				return out.toByteArray();
			}else{
				return null;
			}
		}
	}

	//TODO envision a solution using dataspaces??
	//        Runtime.getRuntime().addShutdownHook(new Thread() {
	//            @Override
	//            public void run() {
	//                try {
	//                    PABasedFileServer.this.stopServer();
	//                } catch (ProActiveException e) {
	//                    throw new ProActiveRuntimeException(e);
	//                }
	//            }
	//        });
	//
	//        String root = System.getProperty(PABasedFileServer.ROOT_DIR_PROP, System.getProperty("user.dir"));
	//        deployer = new FileSystemServerDeployer(deployerName, root, true);
	//        url = deployer.getVFSRootURL();
	//        System.out.println("PABasedFileServer.PABasedFileServer() ProActive dataserver successfully started.\nVFS URL of this provider: " + url + " on " + root);
}
