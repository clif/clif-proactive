/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.filestorage.client;

import java.io.File;
import java.io.IOException;

import org.objectweb.proactive.core.ProActiveException;
import org.ow2.clif.storage.api.CollectListener;

/**
 * Interface of the FileServerClient in order to process the download of the reports
 */
public interface FileServerClient {
	/** Effectively downloads the reports
	 * @throws ProActiveException */
	void process(String bladeId, File outputFile, long progress, CollectListener listener) throws IOException, ProActiveException;

}
