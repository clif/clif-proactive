/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.ow2.clif.storage.lib.filestorage.server.FileServer;
import org.ow2.clif.storage.lib.filestorage.server.SocketBasedFileServer;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.UniqueKey;


/**
 * Each instance of this class represents and manages the collect of test data files located in
 * a given directory. Each file is transfered by socket, within a dedicated "collect step".
 * @author Bruno Dillenseger
 */
public class FileStorageCollect
{
	///////////////////////////////////
	// static attributes and methods //
	///////////////////////////////////

	/** property used by user to determine what kind of implementation must be use to collect results */
	public static final String FILE_SERVER_IMPLEMENTATION_PROP = "clif.fileserver.impl";
	public static final String COLLECT_HOST_PROP = "clif.filestorage.host";
	static public final int BLOCK_SIZE = 4096;
	static protected Map<UniqueKey,FileStorageCollect> active_collects = new HashMap<UniqueKey,FileStorageCollect>();
	static protected Stack<FileStorageCollect> old_collects = new Stack<FileStorageCollect>();

	static public FileStorageCollect newCollect(File dir, FileServer.Impl impl)
	{
		synchronized (active_collects)
		{
			FileStorageCollect collect = null;
			if (old_collects.isEmpty())
			{
				try
				{
					collect = new FileStorageCollect(impl);
				}
				catch (ClifException ex)
				{
					throw new Error("Can't initiate FileStorage data collect", ex);
				}
			}
			else
			{
				collect = old_collects.pop();
			}
			UniqueKey key = new UniqueKey();
			try
			{
				collect.setDirectory(dir);
				collect.setKey(key);
				active_collects.put(key, collect);
			}
			catch (FileNotFoundException ex)
			{
				old_collects.push(collect);
				collect = null;
			}
			return collect;
		}
	}


	static public FileStorageCollect getCollect(UniqueKey key)
	{
		return active_collects.get(key);
	}

	//TODO implement a solution using genericity to relax the return type
	static public FileStorageCollectStep collect(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			return collect.collect();
		}
		else
		{
			return null;
		}
	}


	static public void close(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			collect.close();
		}
	}


	public static long getSize(UniqueKey key)
	{
		FileStorageCollect collect = getCollect(key);
		if (collect != null)
		{
			return collect.getSize();
		}
		else
		{
			return -1;
		}
	}


	///////////////////////////////
	// end of static definitions //
	///////////////////////////////


	protected UniqueKey key;
	protected int fileIndex;
	protected File[] files;
	protected FileServer fileServer;
	protected long size;

	/** Factory implementation */
	private FileStorageCollect(FileServer.Impl impl) throws ClifException {
		this.fileServer = getFileServerImpl(impl);
	}

	/**
	 *  Factory instanciation
	 *  Retrieves the good implementation to use given the property, one of socket, proactive
	 */
	private FileServer getFileServerImpl(FileServer.Impl impl) throws ClifException {
		if(impl == null){
			System.out.println("[Warning] Collecting results using " + SocketBasedFileServer.class.getSimpleName() + " as no implementation was specified.");
			try {
				return new SocketBasedFileServer(this);
			} catch (IOException e) {
				throw new ClifException("Cannot instanciate " + SocketBasedFileServer.class.getSimpleName());
			}
		}
		try {
			Constructor<? extends FileServer> constructor = impl.provider.getConstructor(new Class[]{FileStorageCollect.class});
			return constructor.newInstance(this);
		} catch (Exception e) {
			throw new ClifException("Cannot instanciate FileServer object", e);
		}
	}

	public UniqueKey getKey()
	{
		return key;
	}


	private void setKey(UniqueKey key)
	{
		this.key = key;
	}


	private synchronized void setDirectory(File dir)
	throws FileNotFoundException
	{
		files = dir.listFiles();
		if (files == null)
		{
			throw new FileNotFoundException("directory " + dir + " does not exist");
		}
		fileIndex = -1;
		size = 0;
		for (int i=0 ; i<files.length ; size += files[i++].length());
	}


	//TODO implement a solution using genericity to relax the return type
	private synchronized FileStorageCollectStep collect()
	{
		if (++fileIndex < files.length)
		{
			return new FileStorageCollectStep(
				files[fileIndex].getName(),
				fileServer.getClientFactory());
		}
		else
		{
			close();
			return null;
		}
	}


	private void close()
	{
		synchronized (active_collects)
		{
			if (active_collects.containsKey(key))
			{
				old_collects.push(active_collects.remove(key));
			}
		}
	}


	private long getSize()
	{
		return size;
	}


	public synchronized File getFile() {
		File result = files[fileIndex];
		return result;
	}
}
