/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.filestorage.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;

import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.lib.filestorage.FileStorageCollect;

/**
 * Implementation of a FileServerClient for socket communication
 */
public class SocketBasedFileServerClient implements FileServerClient {

	/** The server socket address, used to connect */
	private final SocketAddress socketEndpoint;

	/** Constructor */
	public SocketBasedFileServerClient(SocketAddress localSocketAddress) {
		this.socketEndpoint = localSocketAddress;
	}

	/** Process the download of a report for a given blade */
	public void process(String bladeId, File outputFile, long progress,	CollectListener listener) throws IOException {
		//instantiated on the server (agent), the listener pointer is valid (not remote)
		Socket sock = new Socket();
		sock.connect(socketEndpoint, 0);
		InputStream in = sock.getInputStream();
		OutputStream out = new FileOutputStream(outputFile);
	    byte[] buffer = new byte[FileStorageCollect.BLOCK_SIZE];
		int n = in.read(buffer);
		while ((listener == null || ! listener.isCanceled() || ! listener.isCanceled(bladeId)) && n != -1) {
			out.write(buffer, 0, n);
			progress += n;
			if (listener != null)
			{
				listener.progress(bladeId, progress);
			}
			n = in.read(buffer);
		}
		in.close();
		out.close();
		sock.close();
	}

}
