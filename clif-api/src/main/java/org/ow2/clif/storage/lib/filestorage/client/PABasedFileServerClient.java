/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.filestorage.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import org.objectweb.proactive.api.PARemoteObject;
import org.objectweb.proactive.core.ProActiveException;
import org.ow2.clif.storage.api.CollectListener;
import org.ow2.clif.storage.lib.filestorage.FileStorageCollect;
import org.ow2.clif.storage.lib.filestorage.server.PABasedFileServer;
import org.ow2.clif.storage.lib.filestorage.server.PABasedFileServer.RequestHandler;

/**
 * FileServerClient over ProActive implementation
 */
public class PABasedFileServerClient implements FileServerClient {

	/** The uri of the server remote object */
	private final URI uri;

	/** Constructor */
	public PABasedFileServerClient(URI serverURL){
		this.uri = serverURL;
	}

	/** Processes the download of a report for a given blade
	 * @throws ProActiveException */
	public void process(String bladeId, File outputFile, long progress,	CollectListener listener) throws IOException, ProActiveException {
		PABasedFileServer.RequestHandler server = (RequestHandler) PARemoteObject.lookup(uri);
		byte[] content = server.process();
		OutputStream out = new FileOutputStream(outputFile);
		InputStream in = new ByteArrayInputStream(content);
	    byte[] buffer = new byte[FileStorageCollect.BLOCK_SIZE];
		int n = in.read(buffer);
		while ((listener == null || ! listener.isCanceled() || ! listener.isCanceled(bladeId)) && n != -1) {
			out.write(buffer, 0, n);
			progress += n;
			if (listener != null)
			{
				listener.progress(bladeId, progress);
			}
			n = in.read(buffer);
		}
		in.close();
		out.close();
	}

	//TODO envision a solution using Dataspaces??
//	private String namingServiceURL;
//    private NamingServiceDeployer namingServiceDeployer;
//    private NamingService namingService;
//    namingServiceDeployer = new NamingServiceDeployer(true);
//    namingServiceURL = namingServiceDeployer.getNamingServiceURL();
//    namingService = NamingService.createNamingServiceStub(namingServiceURL);
//
//
//    Set<SpaceInstanceInfo> predefinedSpaces = new HashSet<SpaceInstanceInfo>();
//    InputOutputSpaceConfiguration isc;
//    isc = InputOutputSpaceConfiguration.createInputSpaceConfiguration(inputURL, null, null,
//            PADataSpaces.DEFAULT_IN_OUT_NAME);
//
//    predefinedSpaces.add(new SpaceInstanceInfo(applicationId, isc));
//    // register application
//    namingService.registerApplication(12345, predefinedSpaces);
}
