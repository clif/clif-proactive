/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003-2005, 2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.StringSplitter;


/**
 * This class represents the different events
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class LifeCycleEvent extends AbstractEvent
{
	private static final long serialVersionUID = 2851479315344662648L;
	static public final String EVENT_TYPE_LABEL = "lifecycle";
	static private final String[] EVENT_FIELD_LABELS =
		new String[] { "date", "state id", "state label" };


	static
	{
		AbstractEvent.registerEventFieldLabels(
			EVENT_TYPE_LABEL,
			EVENT_FIELD_LABELS,
			new EventFactory() {
				@Override
				public BladeEvent makeEvent(String separator, String line)
					throws ClifException
				{
					try
					{
						String[] values = StringSplitter.split(line, separator, new String[2]);
						LifeCycleEvent event = new LifeCycleEvent(
							Long.parseLong(values[0]),
							BladeState.get(Integer.parseInt(values[1])));
						return event;
					}
					catch (Exception ex)
					{
						throw new ClifException(
							"Could not make a LifeCycleEvent instance from " + line,
							ex);
					}
				}
			});
	}


	protected BladeState state;


	public LifeCycleEvent(long date, BladeState state)
	{
		super(date);
		this.state = state;
	}


	@Override
	public String toString()
	{
		return toString(0, DEFAULT_SEPARATOR);
	}


	//////////////////////////
	// BladeEvent interface //
	//////////////////////////


	@Override
	public String getTypeLabel()
	{
		return EVENT_TYPE_LABEL;
	}


	public int getStateId()
	{
		return state.getCode();
	}


	@Override
	public String toString(long dateOrigin, String separator)
	{
		return
			(date - dateOrigin) + separator
			+ state.getCode() + separator
			+ state;
	}


	@Override
	public String[] getFieldLabels()
	{
		return EVENT_FIELD_LABELS;
	}


	@Override
	public Object getFieldValue(String fieldLabel)
	{
		if (fieldLabel != null)
		{
			if (fieldLabel.equals(EVENT_FIELD_LABELS[0]))
			{
				return new Long(date);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[0]))
			{
				return new Long(date);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[1]))
			{
				return new Integer(state.getCode());
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[2]))
			{
				return state.toString();
			}
		}
		return null;
	}
}

