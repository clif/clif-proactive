/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.storage.lib.filestorage.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.ow2.clif.storage.lib.filestorage.FileStorageCollect;
import org.ow2.clif.storage.lib.filestorage.client.FileServerClientFactory;
import org.ow2.clif.storage.lib.filestorage.client.SocketBasedFileServerClientFactory;
import org.ow2.clif.util.Network;

/**
 * FileServer over socket implementation
 */
public class SocketBasedFileServer extends ServerSocket implements FileServer, Runnable{
	private static final long serialVersionUID = 4892716270680251911L;
	/** The file storage collect object that requested the collect process */
	private FileStorageCollect fsc = null;
	/** The buffer used to process downloads */
	private byte[] buffer = new byte[FileStorageCollect.BLOCK_SIZE];

	/** Constructor */
	public SocketBasedFileServer(FileStorageCollect fsc) throws IOException{
		super();
		this.fsc = fsc;
		InetSocketAddress addr = null;
		String fixed_address_prop = System.getProperty(FileStorageCollect.COLLECT_HOST_PROP);
		//bind the server socket
		if (fixed_address_prop != null)
		{
			// subnet (form d.d.d.d/n) or host address?
			if (fixed_address_prop.indexOf('/') == -1)
			{
				addr = new InetSocketAddress(InetAddress.getByName(fixed_address_prop), 0);
			}
			else
			{
				addr = new InetSocketAddress(Network.getInetAddress(fixed_address_prop), 0);
			}
		}
		else
		{
			addr = new InetSocketAddress(Network.getInetAddress(null), 0);
		}
		try
		{
			bind(addr);
		}
		catch (IOException ex)
		{
			IOException ex2 = new IOException("Can't bind address " + addr);
			ex2.setStackTrace(ex.getStackTrace());
			throw ex2;
		}
		new Thread(this).start();
	}

	/**
	 * Processes client connection and treats requests
	 */
	public void run()
	{
		Socket sock;
		OutputStream out;
		InputStream in;
		while (true)
		{
			sock = null;
			in = null;
			out = null;
			try
			{
				sock = accept();
				out = sock.getOutputStream();
				File file = fsc.getFile();
				in = new FileInputStream(file);
				int n = in.read(buffer);
				while (n != -1)
				{
					out.write(buffer, 0, n);
					n = in.read(buffer);
				}
			}
			catch (IOException ex)
			{
				ex.printStackTrace(System.err);
			}
			finally
			{
				try
				{
					if (sock != null)
					{
						if (out != null)
						{
							if (in != null)
							{
								in.close();
							}
							out.close();
						}
						sock.close();
					}
				}
				catch (IOException ex)
				{
					ex.printStackTrace(System.err);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public FileServerClientFactory getClientFactory() {
		return new SocketBasedFileServerClientFactory(this.getLocalSocketAddress());
	}
}
