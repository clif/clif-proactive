/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.io.Serializable;
import org.objectweb.proactive.annotation.ImmediateService;
import org.ow2.clif.datacollector.api.AbstractProbeDataCollector;

/**
 * DataCollector for RTP probe
 * 
 * @author Rémi Druilhe
 */
public class DataCollector extends AbstractProbeDataCollector
{
	static public final String[] LABELS = new String[] 
	{
		"packets number throughput (packet/s)",
		"cumulative number of packets lost",
		"minimum jitter (ms)",
		"maximum jitter (ms)",
		"average jitter (ms)",
		"standard deviation jitter (ms)",
		"jump per second",
		"inversion per second"
	};

	@Override
	@ImmediateService
	public void init(Serializable testId, String probeId)
	{
		stats = new long[LABELS.length];
		super.init(testId, probeId);
	}


	//////////////////////////////////
	// DataCollectorAdmin interface //
	//////////////////////////////////

	@Override
	@ImmediateService
	public String[] getLabels()
	{
		return LABELS;
	}
}
